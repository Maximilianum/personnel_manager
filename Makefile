#
# Makefile
#


# compilatore da usare
CPP = g++

# la lista delle librerie da utilizzare deve essere separata dagli spazi
# ogni libreria deve avere il prefisso -l

TARGET ?= debug
LIBS = `pkg-config --libs gtkmm-3.0 librsvg-2.0` -lsqlite3

ifeq ($(TARGET),debug)
 CPPFLAGS = -std=c++14 -O0 -g `pkg-config --cflags gtkmm-3.0 librsvg-2.0`
endif
ifeq ($(TARGET),release)
 CPPFLAGS = -std=c++14 -O3 `pkg-config --cflags gtkmm-3.0 librsvg-2.0`
endif
ifeq ($(TARGET),experimental)
 CPPFLAGS = -std=c++17 -O0 `pkg-config --cflags gtkmm-4.0 librsvg-2.0`
 LIBS = `pkg-config --libs gtkmm-4.0 librsvg-2.0` -lsqlite3
endif

# nome dell'eseguibile
EXE = personnel_manager

# automated creation of object files list from all files with .cc suffix
OBJS := $(patsubst %.cc,%.o,$(wildcard *.cc))

$(EXE): $(OBJS) Makefile
	$(CPP) $(CPPFLAGS) -o $@ $(OBJS) $(LIBS)

# automated dependencies
#$(OBJS): $(HDRS) Makefile

main.o : main.cc main_window.hh

main_window.o : main_window.cc main_window.hh ctrl_app_engine.hh ctrl_engine.hh single_string_tmcr.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license_type.hh license.hh label.hh offwork.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh control_dialog.hh members_dialog.hh member_dialog.hh license_dialog.hh shift_dialog.hh time_preset_tree_model_column_record.hh license_combobox_tree_model_column_record.hh ranks_dialog.hh rank_tree_model_column_record.hh time_preset_dialog.hh meal_break_dialog.hh work_time_dialog.hh labels_dialog.hh label_tree_model_column_record.hh offworks_dialog.hh offwork_dialog.hh offwork_tree_model_column_record.hh license_types_dialog.hh license_type_dialog.hh license_type_tree_model_column_record.hh view_dialog.hh enable_tree_model_column_record.hh title_dialog.hh line_text_tree_model_column_record.hh sign_dialog.hh license_tree_model_column_record.hh main_tree_model_column_record.hh members_tree_model_column_record.hh drawing_shifts.hh drawing_document.hh drawing_week.hh drawing_siris.hh printable_window.hh drawing_document.hh print_operation_document.hh errors_dialog.hh print_dialog.hh licenses_dialog.hh autocomplete_dialog.hh stats_dialog.hh

autocomplete_dialog.o : autocomplete_dialog.cc autocomplete_dialog.hh control_dialog.hh single_string_tmcr.hh dual_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

calendar.o : calendar.cc calendar.hh custom_date.hh

calendar_data.o : calendar_data.cc calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

control_dialog.o : control_dialog.cc control_dialog.hh single_string_tmcr.hh

ctrl_app_engine.o : ctrl_app_engine.cc ctrl_app_engine.hh ctrl_engine.hh single_string_tmcr.hh

ctrl_engine.o : ctrl_engine.cc ctrl_engine.hh single_string_tmcr.hh

custom_date.o : custom_date.cc custom_date.hh

database.o : database.cc database.hh db_engine.hh shift.hh duty.hh member.hh service.hh rank.hh license.hh offwork.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

drawing_document.o : drawing_document.cc drawing_document.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

drawing_shifts.o : drawing_shifts.cc drawing_shifts.hh drawing_document.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

drawing_siris.o : drawing_siris.cc drawing_siris.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

drawing_week.o : drawing_week.cc drawing_week.hh drawing_document.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

duty.o : duty.cc duty.hh custom_date.hh

errors_dialog.o : errors_dialog.cc errors_dialog.hh

label.o : label.cc label.hh

labels_dialog.o : labels_dialog.cc labels_dialog.hh label_tree_model_column_record.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

license.o : license.cc license.hh custom_date.hh

license_dialog.o : license_dialog.cc license_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

license_type.o : license_type.cc license_type.hh label.hh

license_type_dialog.o : license_type_dialog.cc license_type_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

license_types_dialog.o : license_types_dialog.cc license_types_dialog.hh license_type_dialog.hh license_type_tree_model_column_record.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

licenses_dialog.o : licenses_dialog.cc licenses_dialog.hh license_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

meal_break_dialog.o : meal_break_dialog.cc meal_break_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

member.o : member.cc member.hh rank.hh custom_date.hh license.hh

member_dialog.o : member_dialog.cc member_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

members_dialog.o : members_dialog.cc members_dialog.hh member_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

offwork.o : offwork.cc offwork.hh label.hh

offwork_dialog.o : offwork_dialog.cc offwork_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh offwork.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

offwork_sub_dialog.o : offwork_sub_dialog.cc offwork_sub_dialog.hh offwork_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh offwork.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

offworks_dialog.o : offworks_dialog.cc offworks_dialog.hh offwork_tree_model_column_record.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh offwork.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

printable_window.o : printable_window.cc printable_window.hh drawing_document.hh print_operation_document.hh

print_dialog.o : print_dialog.cc print_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

print_operation_document.o : print_operation_document.cc print_operation_document.hh drawing_document.hh calendar_data.hh calendar.hh custom_date.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh text_line.hh settings.hh time_preset.hh siris_month.hh siris_week.hh

rank.o : rank.cc rank.hh

ranks_dialog.o : ranks_dialog.cc ranks_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

service.o : service.cc service.hh custom_date.hh

settings.o : settings.cc settings.hh member.hh time_preset.hh database.hh

shift.o : shift.cc shift.hh duty.hh custom_date.hh member.hh rank.hh license.hh label.hh

shift_dialog.o : shift_dialog.cc shift_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh calendar_data.hh

sign_dialog.o : sign_dialog.cc sign_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

siris_month.o : siris_month.cc siris_month.hh siris_week.hh

siris_week.o : siris_week.cc siris_week.hh custom_date.hh shift.hh duty.hh

stats_dialog.o : stats_dialog.cc stats_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh calendar_data.hh

text_line.o : text_line.cc text_line.hh

time_preset.o : time_preset.cc time_preset.hh

time_preset_dialog.o : time_preset_dialog.cc time_preset_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

title_dialog.o : title_dialog.cc title_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

view_dialog.o : view_dialog.cc view_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

work_time_dialog.o : work_time_dialog.cc work_time_dialog.hh control_dialog.hh single_string_tmcr.hh database.hh db_engine.hh shift.hh duty.hh member.hh rank.hh license.hh label.hh custom_date.hh text_line.hh settings.hh time_preset.hh

# pulizia
.PHONY: clean
clean:
	rm -f core $(EXE) *.o

install:
	mkdir -p $(DESTDIR)/usr/bin
	strip --strip-unneeded --remove-section=.comment --remove-section=.note $(EXE)
	install -m 0755 $(EXE) $(DESTDIR)/usr/bin/$(EXE)
	mkdir -p $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 autocomplete_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 item_picker_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 labels_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 license_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 licenses_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 license_types_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 meal_break_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 member_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 members_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 offworks_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 personnel_manager.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 print_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 ranks_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 shift_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 sign_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 stats_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 time_preset_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 title_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 view_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 work_time_dialog.ui $(DESTDIR)/usr/share/$(EXE)/ui
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/$(EXE)/ui/$(EXE).png
	mkdir -p $(DESTDIR)/usr/share/applications
	install -m 0644 $(EXE).desktop $(DESTDIR)/usr/share/applications
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	install -m 0644 icons/48x48/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/48x48/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	install -m 0644 icons/64x64/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/64x64/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	install -m 0644 icons/72x72/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/72x72/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	install -m 0644 icons/96x96/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/96x96/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	install -m 0644 icons/128x128/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/128x128/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	install -m 0644 icons/192x192/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/192x192/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	install -m 0644 icons/256x256/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/256x256/apps
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	install -m 0644 icons/512x512/$(EXE).png $(DESTDIR)/usr/share/icons/hicolor/512x512/apps
	gtk-update-icon-cache /usr/share/icons/hicolor

uninstall:
	rm -f $(DESTDIR)/usr/bin/$(EXE)
	rm -f -R $(DESTDIR)/usr/share/$(EXE)
	rm -f $(DESTDIR)/usr/share/applications/$(EXE).desktop
	rm -f $(DESTDIR)/usr/share/icons/hicolor/48x48/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/64x64/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/72x72/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/96x96/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/128x128/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/192x192/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/256x256/apps/$(EXE).png
	rm -f $(DESTDIR)/usr/share/icons/hicolor/512x512/apps/$(EXE).png

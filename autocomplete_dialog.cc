/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * autocomplete_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "autocomplete_dialog.hh"

void AutocompleteDialog::build_gui(AutocompleteDialog* ad) {
	Gtk::Box* box = ad->get_content_area();
	ad->member_liststore = Gtk::ListStore::create(ad->combobox2_tmcr);
	ad->bldr->get_widget("member_combobox", ad->member_combobox);
	ad->member_combobox->set_model(ad->member_liststore);
	ad->bldr->get_widget("autocomplete_box", ad->autocomplete_box);
	ad->bldr->get_widget("autocomp_weekday_combobox", ad->autocomp_weekday_combobox);
	ad->autocomp_weekday_liststore = Gtk::ListStore::create(ad->combobox_tmcr);
	ad->autocomp_weekday_combobox->set_model(ad->autocomp_weekday_liststore);
	ad->bldr->get_widget("autocomp_saturday_combobox", ad->autocomp_saturday_combobox);
	ad->autocomp_saturday_liststore = Gtk::ListStore::create(ad->combobox_tmcr);
	ad->autocomp_saturday_combobox->set_model(ad->autocomp_saturday_liststore);
	ad->bldr->get_widget("autocomp_sunday_combobox", ad->autocomp_sunday_combobox);
	ad->autocomp_sunday_liststore = Gtk::ListStore::create(ad->combobox_tmcr);
	ad->autocomp_sunday_combobox->set_model(ad->autocomp_sunday_liststore);
	ad->bldr->get_widget("autocomp_holiday_combobox", ad->autocomp_holiday_combobox);
	ad->autocomp_holiday_liststore = Gtk::ListStore::create(ad->combobox_tmcr);
	ad->autocomp_holiday_combobox->set_model(ad->autocomp_holiday_liststore);
	box->pack_start(*(ad->autocomplete_box));
	/*
	 * load values
	 */
	ad->load_members_in_combobox();
	ad->member_combobox->set_active(0);
	ad->update_autocomp_settings();
	/*
	 * signals connections
	 */
	ad->member_combobox->signal_changed().connect(sigc::mem_fun(ad, &AutocompleteDialog::update_autocomp_settings));
	ad->autocomp_weekday_combobox->signal_changed().connect(sigc::mem_fun(ad, &AutocompleteDialog::on_autocomp_weekday_selection_changed));
	ad->autocomp_saturday_combobox->signal_changed().connect(sigc::mem_fun(ad, &AutocompleteDialog::on_autocomp_saturday_selection_changed));
	ad->autocomp_sunday_combobox->signal_changed().connect(sigc::mem_fun(ad, &AutocompleteDialog::on_autocomp_sunday_selection_changed));
	ad->autocomp_holiday_combobox->signal_changed().connect(sigc::mem_fun(ad, &AutocompleteDialog::on_autocomp_holiday_selection_changed));
}

void AutocompleteDialog::load_members_in_combobox() const {
	member_liststore->clear();
	try {
		auto members { database->get_members() };
		for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { member_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[combobox2_tmcr.column1] = Glib::ustring::compose("%1 %2 %3",iter->get_rank().get_short_label(),iter->get_first_name(),iter->get_last_name());
			row[combobox2_tmcr.column2] = iter->get_id();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "AutocompleteDialog::load_members_in_combobox() " << dbe.get_message() << std::endl;
	}
}

Glib::ustring AutocompleteDialog::get_selected_member() const {
	Gtk::TreeModel::iterator iter { member_combobox->get_active() };
	if (iter != member_liststore->children().end()) {
		Gtk::TreeModel::Row row { *iter };
		return row[combobox2_tmcr.column2];
	}
	return Glib::ustring();
}

void AutocompleteDialog::update_autocomp_settings() {
	Glib::ustring id { get_selected_member() };
	if (id.compare(Glib::ustring()) != 0) {
		std::vector<Glib::ustring> weekday_strings;
		std::vector<Glib::ustring> saturday_strings;
		std::vector<Glib::ustring> sunday_strings;
		std::vector<Glib::ustring> holiday_strings;
		try {
			weekday_strings = database->get_labels_str_with_weekday(true);
			saturday_strings = database->get_labels_str_with_saturday(true);
			sunday_strings = database->get_labels_str_with_sunday(true);
			holiday_strings = database->get_labels_str_with_holiday(true);
		} catch (const Database::DBError& dbe) {
			std::cerr << "AutocompleteDialog::update_autocomp_settings() " << dbe.get_message() << std::endl;
			return;
		}
		load_data_combobox(weekday_strings.cbegin(), weekday_strings.cend(), autocomp_weekday_liststore);
		load_data_combobox(saturday_strings.cbegin(), saturday_strings.cend(), autocomp_saturday_liststore);
		load_data_combobox(sunday_strings.cbegin(), sunday_strings.cend(), autocomp_sunday_liststore);
		load_data_combobox(holiday_strings.cbegin(), holiday_strings.cend(), autocomp_holiday_liststore);
		Glib::ustring weekday;
		Glib::ustring saturday;
		Glib::ustring sunday;
		Glib::ustring holiday;
		try {
			weekday = database->get_member_setting(id, AUTOCOMP_WEEKDAY);
			saturday = database->get_member_setting(id, AUTOCOMP_SATURDAY);
			sunday = database->get_member_setting(id, AUTOCOMP_SUNDAY);
			holiday = database->get_member_setting(id, AUTOCOMP_HOLIDAY);
		} catch (const Database::DBError& dbe) {
			std::cerr << "AutocompleteDialog::update_autocomp_settings() " << dbe.get_message() << std::endl;
			return;
		} catch (const Database::DBEmptyResultException& dber) {}
		Gtk::TreeModel::iterator iter { get_combobox_item(autocomp_weekday_liststore, weekday) };
		if (iter != autocomp_weekday_liststore->children().end()) {
			autocomp_weekday_combobox->set_active(iter);
		} else {
			autocomp_weekday_combobox->set_active(-1);
		}
		iter = get_combobox_item(autocomp_saturday_liststore, saturday);
		if (iter != autocomp_saturday_liststore->children().end()) {
			autocomp_saturday_combobox->set_active(iter);
		} else {
			autocomp_saturday_combobox->set_active(-1);
		}		
		iter = get_combobox_item(autocomp_sunday_liststore, sunday);
		if (iter != autocomp_sunday_liststore->children().end()) {
			autocomp_sunday_combobox->set_active(iter);
		} else {
			autocomp_sunday_combobox->set_active(-1);
		}
		iter = get_combobox_item(autocomp_holiday_liststore, holiday);
		if (iter != autocomp_holiday_liststore->children().end()) {
			autocomp_holiday_combobox->set_active(iter);
		} else {
			autocomp_holiday_combobox->set_active(-1);
		}
	}
}

void AutocompleteDialog::on_autocomp_weekday_selection_changed(void) {
	if (autocomp_weekday_combobox->get_active_row_number() >= 0)	{
		Glib::ustring id { get_selected_member() };
		if (id.compare(Glib::ustring()) != 0) {
			Gtk::TreeModel::iterator iter { autocomp_weekday_combobox->get_active() };
			Gtk::TreeModel::Row row { *iter };
			Glib::ustring label { row[combobox_tmcr.column] };
			try {
				set_autocomplete_setting(id, AUTOCOMP_WEEKDAY, label);
			} catch (const Database::DBError& dbe) {
				std::cerr << "AutocompleteDialog::on_autocomp_weekday_selection_changed() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void AutocompleteDialog::on_autocomp_saturday_selection_changed(void) {
	if (autocomp_saturday_combobox->get_active_row_number() >= 0)	{
		Glib::ustring id { get_selected_member() };
		if (id.compare(Glib::ustring()) != 0) {
			Gtk::TreeModel::iterator iter { autocomp_saturday_combobox->get_active() };
			Gtk::TreeModel::Row row { *iter };
			Glib::ustring label { row[combobox_tmcr.column] };
			try {
				set_autocomplete_setting(id, AUTOCOMP_SATURDAY, label);
			} catch (const Database::DBError& dbe) {
				std::cerr << "AutocompleteDialog::on_autocomp_saturday_selection_changed() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void AutocompleteDialog::on_autocomp_sunday_selection_changed(void) {
	if (autocomp_sunday_combobox->get_active_row_number() >= 0)	{
		Glib::ustring id { get_selected_member() };
		if (id.compare(Glib::ustring()) != 0) {
			Gtk::TreeModel::iterator iter { autocomp_sunday_combobox->get_active() };
			Gtk::TreeModel::Row row { *iter };
			Glib::ustring label { row[combobox_tmcr.column] };
			try {
				set_autocomplete_setting(id, AUTOCOMP_SUNDAY, label);
			} catch (const Database::DBError& dbe) {
				std::cerr << "AutocompleteDialog::on_autocomp_sunday_selection_changed() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void AutocompleteDialog::on_autocomp_holiday_selection_changed(void) {
	if (autocomp_holiday_combobox->get_active_row_number() >= 0)	{
		Glib::ustring id { get_selected_member() };
		if (id.compare(Glib::ustring()) != 0) {
			Gtk::TreeModel::iterator iter { autocomp_holiday_combobox->get_active() };
			Gtk::TreeModel::Row row { *iter };
			Glib::ustring label { row[combobox_tmcr.column] };
			try {
				set_autocomplete_setting(id, AUTOCOMP_HOLIDAY, label);
			} catch (const Database::DBError& dbe) {
				std::cerr << "AutocompleteDialog::on_autocomp_weekday_selection_changed() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void AutocompleteDialog::set_autocomplete_setting(const Glib::ustring& id, const Glib::ustring& type, const Glib::ustring& label) {
	mbr_settings mbs { type, id, label };
	try {
		if (database->check_member_setting(id, type)) {
			database->update(mbs);
		} else {
			database->insert(mbs);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "AutocompleteDialog::set_autocomplete_setting() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

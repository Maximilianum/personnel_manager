/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * autocomplete_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _AUTOCOMPLETE_DIALOG_HH_
#define _AUTOCOMPLETE_DIALOG_HH_

#include <gtkmm/combobox.h>
#include "dual_string_tmcr.hh"
#include "control_dialog.hh"
#include "database.hh"

class AutocompleteDialog : public ControlDialog {
 public:
	class IteratorNotFoundException
	{
	public:
    IteratorNotFoundException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
	inline AutocompleteDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline AutocompleteDialog(const AutocompleteDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(AutocompleteDialog* ad);
	void load_members_in_combobox() const;
	Glib::ustring get_selected_member() const;
	void update_autocomp_settings();
	void on_autocomp_weekday_selection_changed(void);
	void on_autocomp_saturday_selection_changed(void);
	void on_autocomp_sunday_selection_changed(void);
	void on_autocomp_holiday_selection_changed(void);
	void set_autocomplete_setting(const Glib::ustring& id, const Glib::ustring& type, const Glib::ustring& label);

	Gtk::Box* autocomplete_box;
	Gtk::ComboBox* member_combobox;
	Glib::RefPtr<Gtk::ListStore> member_liststore;
	Gtk::ComboBox* autocomp_weekday_combobox;
	Glib::RefPtr<Gtk::ListStore> autocomp_weekday_liststore;
	Gtk::ComboBox* autocomp_saturday_combobox;
	Glib::RefPtr<Gtk::ListStore> autocomp_saturday_liststore;
	Gtk::ComboBox* autocomp_sunday_combobox;
	Glib::RefPtr<Gtk::ListStore> autocomp_sunday_liststore;
	Gtk::ComboBox* autocomp_holiday_combobox;
	Glib::RefPtr<Gtk::ListStore> autocomp_holiday_liststore;
	DualStringTreeModelColumnRecord combobox2_tmcr;
	Database* database;
};

#endif // _AUTOCOMPLETE_DIALOG_HH_

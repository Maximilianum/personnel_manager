/*
 * calendar.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calendar.hh"

void Calendar::set_next_year(void) {
  clear_selection();
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mday = 1;
  std_date->tm_year++;
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
}

void Calendar::set_previous_year(void) {
  clear_selection();
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mday = 1;
  std_date->tm_year--;
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
}

void Calendar::set_next_month(void) {
  clear_selection();
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mday = 1;
  std_date->tm_mon++;
  if (std_date->tm_mon > 11) {
    std_date->tm_mon = 0;
    std_date->tm_year++;
  }
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
}

void Calendar::set_previous_month(void) {
  clear_selection();
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mday = 1;
  std_date->tm_mon--;
  if (std_date->tm_mon < 0) {
    std_date->tm_mon = 11;
    std_date->tm_year--;
  }
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
}

/*
 * calendar.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CALENDAR_HH_
#define _CALENDAR_HH_

#include <glibmm/ustring.h>
#include "custom_date.hh"

typedef struct {
  time_t date;
  int days;
  Glib::ustring note;
} Selection;

class Calendar {
public:
  // constructors
  inline Calendar() : ref_date (time(nullptr)), selection ({ 0, 0, Glib::ustring() }) {}
  inline ~Calendar() {}
  // class member functions
  inline void set_reference_date(time_t tm) { ref_date = tm; }
  inline time_t get_reference_date(void) const { return ref_date; }
  inline CustomDate get_reference_cdate(void) const { return CustomDate(&ref_date); }
  inline void set_selection(const Selection& sel) { selection = sel; }
  inline Selection get_selection(void) const { return selection; }
  inline void clear_selection() { selection = { 0, 0, Glib::ustring() }; }
  inline bool is_valid_selection() { return selection.date > 0 && selection.note.compare(Glib::ustring()) != 0; }
  inline bool is_selected(time_t date, const Glib::ustring& id) { return CustomDate(&date).is_in_range(CustomDate(&selection.date), selection.days) && id.compare(selection.note) == 0; }
  void set_next_year(void);
  void set_previous_year(void);
  void set_next_month(void);
  void set_previous_month(void);
protected:
  time_t ref_date;
  Selection selection;
};

#endif // _CALENDAR_DATA_HH_

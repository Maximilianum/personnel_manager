/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * calendar_data.cc
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calendar_data.hh"

int CalendarData::daily_work_time = 360;

/**
 * 
 * operators
 * 
 */

CalendarData& CalendarData::operator=(const CalendarData &cd) {
	if (this != &cd) {
		ref_date = cd.ref_date;
		selection = cd.selection;
		set_missing_shifts(cd.missing_shifts);
	}
	return *this;
}
	
/**
 * 
 * class member functions
 * 
 */
	
void CalendarData::add_shifts(Database* database, const Selection& sel, const Shift& sft) {
	CustomDate start { sft.get_start() };
	CustomDate finish { sft.get_end() };
	Shift shift { sft };
	int n = 0;
	do {
		try {
			if (shift.check_consistency(database) && !database->has_shifts(sel.note, start)) {
				Glib::ustring op { database->compute_insert(shift) };
				database->execute(op);
				undo_stack.push(std::pair<Glib::ustring, Glib::ustring>(database->compute_delete_shift(sel.note, start), op));
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "CalendarData::add_shift() DBError" << std::endl;
			throw dbe;
		}
		if (sel.days < 0) {
			start.add_days(-1);
			finish.add_days(-1);
			n--;
		} else {
			start.add_days(1);
			finish.add_days(1);
			n++;
		}
		shift.set_start(start);
		shift.set_end(finish);
	} while (abs(n) <= abs(sel.days));
}

std::pair<Glib::ustring, Glib::ustring> CalendarData::create_shift_for_member(Database* database, const CustomDate& date, const Glib::ustring& id) {
	try {
		// get weekday work status label
		Glib::ustring weekday { database->get_member_setting(id, AUTOCOMP_WEEKDAY) };
		// get saturday work status label
		Glib::ustring saturday { database->get_member_setting(id, AUTOCOMP_SATURDAY) };
		// get sunday work status label
		Glib::ustring sunday { database->get_member_setting(id, AUTOCOMP_SUNDAY) };
		// get holiday work status label
		Glib::ustring holiday { database->get_member_setting(id, AUTOCOMP_HOLIDAY) };

		Shift shift;
		if (date.is_holiday()) {
			if (date.get_weekday() == 0) {
				if (sunday.compare(Glib::ustring()) != 0) {
					// create shift for sunday
					shift = Shift(sunday, id, CustomDate::start_of_day(date));
				}
			} else {
				if (holiday.compare(Glib::ustring()) != 0) {
					// create shift for holiday
					shift = Shift(holiday, id, CustomDate::start_of_day(date));
				}
			}
		} else {
			if (date.get_weekday() == 6) {
				if (saturday.compare(Glib::ustring()) != 0) {
					// create shift for saturday
					shift = Shift(saturday, id, CustomDate::start_of_day(date));
				}
			} else {
				if (weekday.compare(Glib::ustring()) != 0) {
					// create shift for weekday
					shift = Shift(weekday, id, CustomDate::start_of_day(date));
				}
			}
		}
		if (database->has_shifts(id, shift.get_start())) {
			return std::pair<Glib::ustring, Glib::ustring>("","");
		} else {
			Glib::ustring op { database->compute_insert(shift) };
			return std::pair<Glib::ustring, Glib::ustring>(database->compute_delete_shift(id, shift.get_start()), op);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::add_shift_for_member() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "CalendarData::add_shift_for_member() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void CalendarData::add_shifts_for_selected_member(Database* database) {
	CustomDate date { CustomDate::get_first_day_month(ref_date) };
	CustomDate end_month { CustomDate::get_last_day_month(ref_date) };
	std::pair<Glib::ustring, Glib::ustring> operation;
	while (date <= end_month) {
		std::pair<Glib::ustring, Glib::ustring> op = create_shift_for_member(database, date, selection.note);
		operation.first.append(op.first);
		operation.second.append(op.second);
		date.add_days(1);
	}
	try {
		database->execute(operation.second);
		undo_stack.push(operation);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::add_shifts_for_selected_member() " << dbe.get_message() << std::endl;
		throw dbe;
	}	
}

void CalendarData::add_shifts_for_all_members(Database* database) {
	// create a vector of members id
	try {
		std::vector<Member> members { database->get_active_members(CustomDate::get_last_day_month(ref_date)) };
		for (auto iter = members.begin(); iter != members.end(); iter++) {
			set_selection( { ref_date, 0, iter->get_id() } );
			add_shifts_for_selected_member(database);
		}
		clear_selection();
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::on_complete_month_toolbutton_clicked() " << dbe.get_message() << std::endl;
	}
}

void CalendarData::update_shift(Database* database, const Shift& old_sft, const Shift& new_sft) {
	try {
		Glib::ustring op { database->compute_update(new_sft, old_sft.get_member(), old_sft.get_start()) };
		undo_stack.push(std::pair<Glib::ustring, Glib::ustring>(database->compute_update(old_sft, new_sft.get_member(), new_sft.get_start()), op));
		database->execute(op);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::update_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

bool CalendarData::remove_selected_shifts(Database* database) {
	CustomDate cd { &selection.date };
	int n = 0;
	do {
		try {
			Shift sft { get_shift(database, cd, selection.note) };
			Glib::ustring op { database->compute_delete_shift(selection.note, cd) };
			database->execute(op);
			undo_stack.push(std::pair<Glib::ustring, Glib::ustring>(database->compute_insert(sft), op));
		} catch (const Database::DBError& dbe) {
			std::cerr << "CalendarData::remove_shifts() " << dbe.get_message() << std::endl;
			throw dbe;
		} catch (const Database::DBEmptyResultException& dbe) {
			// empty day, just ignore it
		}
		if (selection.days < 0) {
			cd.add_days(-1);
			n--;
		} else {
			cd.add_days(1);
			n++;
		}
	} while (abs(n) <= abs(selection.days));
	return true;
}

bool CalendarData::move_shift(Database* database, const int direction) {
	CustomDate clicked_day { &selection.date };
	CustomDate destination_day { clicked_day };
	destination_day.add_days(direction);
	Shift clicked_shift, destination_shift;
	try {
		// get clicked shift
		clicked_shift = get_shift(database, clicked_day, selection.note);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::move_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "CalendarData::move_shift() " << dbe.get_message() << std::endl;
		return false;
	}
	try {
		// get clicked member's shift from destination_day
		destination_shift = get_shift(database, destination_day, selection.note);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::move_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	// move clicked_member's shift from clicked_day to destination_day
	CustomDate start { clicked_shift.get_start() };
	CustomDate finish { clicked_shift.get_end() };
	start.add_days(direction);
	finish.add_days(direction);
	Shift test_shift { clicked_shift };
	test_shift.set_start(start);
	test_shift.set_end(finish);
	try {
		if (test_shift.check_consistency(database)) {
			Glib::ustring op, unop;
			CustomDate start2, finish2;
			/*
			 * remove shift in destination day if present
			 */
			if (destination_shift.get_member().compare(Glib::ustring()) != 0) {
				op.assign(database->compute_delete_shift(selection.note, destination_day));
				if (destination_shift.get_member().compare(Glib::ustring()) != 0) {
					unop.assign(database->compute_insert(destination_shift));
				}
			}
			// move clicked_shift to destination_day
			op.append(database->compute_update_shift_date(clicked_shift, start, finish));
			Shift moved_shift { clicked_shift };
			moved_shift.set_start(start);
			moved_shift.set_end(finish);
			unop.assign(database->compute_update_shift_date(moved_shift, clicked_shift.get_start(), clicked_shift.get_end()) + unop);
			// check if we've got a destination_shift
			if (destination_shift.get_member().compare(Glib::ustring()) != 0) {
				// copy shift copied from destination_day to clicked_day
				start2 = destination_shift.get_start();
				finish2 = destination_shift.get_end();
				start2.add_days(-direction);
				finish2.add_days(-direction);
				destination_shift.set_start(start2);
				destination_shift.set_end(finish2);
				if (destination_shift.check_consistency(database)) {
					op.append(database->compute_insert(destination_shift));
				}
			}
			unop.assign(database->compute_delete_shift(selection.note, start2) + unop);
			undo_stack.push(std::pair<Glib::ustring, Glib::ustring>(unop, op));
			database->execute(op);
			set_selection({ destination_day.get_time_t(), 0, selection.note });
		} else {
			return false;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::move_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	return true;
}

Shift CalendarData::get_shift(Database* database, const CustomDate& date, const Glib::ustring& id, bool cache) const {
	try {
		return cache ? shifts_cache.at(CustomDate::start_of_day(date).get_time_t()) : database->get_shift(id, date);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		throw dbe;
	}
}

Shift CalendarData::get_selected_shift(Database* database, bool cache) const {
	try {
		return cache ? shifts_cache.at(CustomDate::start_of_day(selection.date).get_time_t()) : database->get_shift(selection.note, CustomDate(&selection.date));
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_selected_shift() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		throw dbe;
	}
}

bool CalendarData::is_selected_member_active(Database* database) const {
	try {
		std::vector<Service> servs = database->get_services(selection.note);
		if (servs.size() > 0) {
			return servs.back().is_active(CustomDate::start_of_day(selection.date));
		}
		return false;
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::is_selected_member_active() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

OffWork CalendarData::get_offwork(Database* database, const Glib::ustring& lbl, bool cache) const {
	try {
		return cache ? offworks_cache.at(lbl) : database->get_offwork(lbl);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_offwork() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		//std::cerr << "CalendarData::get_offwork() " << dbe.get_message() << lbl.raw() << std::endl;
		throw dbe;
	} catch (const std::out_of_range& oor) {
		std::cerr << "CalendarData::get_offwork() " << oor.what() << lbl.raw() << std::endl;
		throw oor;
	}
}

Glib::ustring CalendarData::get_shift_string(Database* database, const CustomDate& date, const Glib::ustring &member_id, bool cache, bool plain, bool full) const {
	try {
		Shift shift = get_shift(database, date, member_id, cache);
		Glib::ustring str_up, str_down;
		if (shift.get_label().compare(Glib::ustring()) == 0) {
			struct tm* std_date = shift.get_start().get_struct_time();
			if (full) {
				str_up.append(Glib::ustring::format(std::setfill(L'0'), std::setw(2), std_date->tm_hour) + Glib::ustring(":") + Glib::ustring::format(std::setfill(L'0'), std::setw(2), std_date->tm_min));
			} else {
				str_up.append(Glib::ustring::format(std::setfill(L'0'), std::setw(2), std_date->tm_hour));
			}
			std_date = shift.get_end().get_struct_time();
			int finish = std_date->tm_hour;
			if (finish == 0) {
				finish = 24;
			}
			if (full) {
				str_down.append(Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish) + Glib::ustring(":") + Glib::ustring::format(std::setfill(L'0'), std::setw(2), std_date->tm_min));
			} else {
				str_down.append(Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish));
			}
		} else {
			Glib::ustring label;
			if (shift.get_license_year() > 0) {
				LicenseType lt { database->get_license_type(shift.get_label()) };
				label = lt.get_short_label();
			} else {
				OffWork ow { get_offwork(database, shift.get_label(), cache) };
				label = ow.get_short_label();
			}
			if (label.size() > 2) {
				size_t fh = label.size() / 2;
				str_up.assign(label.substr(0, fh));
				str_down.assign(label.substr(fh, std::string::npos));
			} else {
				str_up.assign(label);
			}
		}
		return plain ? str_up + str_down : str_up + Glib::ustring("\n") + str_down;
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_shift_string() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		return Glib::ustring();
	} catch (const std::out_of_range& ofr) {
		return Glib::ustring();
	}
}

Glib::ustring CalendarData::get_shift_note(Database* database, const CustomDate& date, const Glib::ustring &member_id, bool cache) const {
	try {
		Shift shift = get_shift(database, date, member_id, cache);
		return shift.get_note();
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_shift_note() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		return Glib::ustring();
	} catch (const std::out_of_range& ofr) {
		return Glib::ustring();
	}
}

bool CalendarData::has_shifts(Database* database, const Selection& sel) const {
	try {
		return database->has_shifts(sel.note, CustomDate(&sel.date), sel.days);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::has_shifts() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void CalendarData::clear_shifts(Database* database) {
	CustomDate start = CustomDate::start_of_day(CustomDate::get_first_day_month(ref_date));
	CustomDate end = CustomDate::end_of_day(CustomDate::get_last_day_month(ref_date));
	try {
		database->delete_shifts_within(start, end);
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::clear_shifts() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

bool CalendarData::is_working(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache) const {
	try {
		Shift shift { get_shift(database, date, id, cache) };
		return shift.is_working();
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::is_working() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return false;
}

bool CalendarData::is_absence(Database* database, const Shift& shift, bool cache) const {
	try {
		OffWork ow { get_offwork(database, shift.get_label(), cache) };
		return ow.get_absence();
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::is_absence() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return false;
}

int CalendarData::get_should_work_minutes(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id, bool cache) const {
	int min = 0;
	CustomDate cd_day { start_date };
	while (cd_day <= end_date) {
		int md = cd_day.is_holiday() ? 0 : daily_work_time;
		min += md;
		cd_day.add_days(1);
	}
	try {
		std::vector<Shift> shifts { database->get_non_working_shifts_for_member(id, start_date, end_date) };
		for (std::vector<Shift>::iterator iter = shifts.begin(); iter != shifts.end(); iter++) {
			if (iter->get_license_year() > 0) {
				min -= daily_work_time;
			} else {
				OffWork ow { get_offwork(database, iter->get_label(), cache) };
				if (!ow.get_absence() && !iter->get_start().is_holiday()) {
					min -= daily_work_time;
				}
			}
		}
		return min;
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_should_work_minutes() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int CalendarData::get_worked_minutes(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id) const {
	try {
		int min = 0;
		std::vector<Shift> shifts { database->get_working_shifts_for_member(id, start_date, end_date) };
		for (std::vector<Shift>::iterator iter = shifts.begin(), end = shifts.end(); iter != end; iter++) {
			min += iter->get_worked_minutes();
		}
		return min;
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_worked_minutes() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int CalendarData::get_presence_count(Database* database, const Glib::ustring &id) const {
	try {
		std::vector<Shift> shifts { database->get_working_shifts_for_member(id, CustomDate::get_first_day_month(ref_date), CustomDate::get_last_day_month(ref_date)) };
		return shifts.size();
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_presence_count() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int CalendarData::get_number_of_meals(Database* database, const Glib::ustring &id) const {
	short meals = 0;
	try {
		std::vector<Shift> shifts { database->get_working_shifts_for_member(id, CustomDate::get_first_day_month(ref_date), CustomDate::get_last_day_month(ref_date)) };
		for (std::vector<Shift>::iterator iter = shifts.begin(), end = shifts.end(); iter != end; iter++) {
			meals += iter->get_number_of_meals();
		}
		return meals;
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_number_of_meals() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void CalendarData::get_stats(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id, std::map<const Glib::ustring, std::pair<const Glib::ustring, int> >* off_map) const {
	try {
		std::vector<Shift> shifts { database->get_non_working_shifts_for_member(id, start_date, end_date) };
		for (std::vector<Shift>::iterator iter = shifts.begin(), end = shifts.end(); iter != end; iter++) {
			std::pair<std::map<const Glib::ustring, std::pair<const Glib::ustring, int> >::iterator, bool > result;
			Glib::ustring label = iter->get_label();
			Label lbl = database->get_label(label);
			Glib::ustring slabel = lbl.get_short_label();
			result = off_map->insert(std::pair<const Glib::ustring, std::pair<const Glib::ustring, int> >(slabel, std::pair<const Glib::ustring, int>(label, 1)));
			if (!result.second) {
				result.first->second.second++;
			}
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_stats() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int CalendarData::get_license_amount(Database* database, const Glib::ustring& id, const Glib::ustring& label, int ref_year, const CustomDate& end_date) {
	try {
		std::vector<Shift> shifts { database->get_license_shifts_for_member(id, label, ref_year, end_date) };
		return shifts.size();
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_license_amount() DBError" << std::endl;
		throw dbe;
	}
}

void CalendarData::check_shifts_for_members(Database* database, const std::vector<Member>& members, std::vector<shift_error>* errors) {
	CustomDate cd = CustomDate::get_first_day_month(ref_date);
	while (cd <= CustomDate::get_last_day_month(ref_date)) {
		// iter through members id vector
		for (std::vector<Member>::const_iterator iter = members.cbegin(), end = members.cend(); iter != end; iter++) {
			try {
				if (!database->has_shifts(iter->get_id(), cd)) {	
					shift_error err { iter->get_id(), cd.get_day(), ERRTYPE::missing, Glib::ustring() };
					errors->push_back(err);
					missing_shifts.insert(DayMember { cd.get_time_t(), iter->get_id() });
				}
			} catch (const Database::DBError& dbe) {
				std::cerr << "CalendarData::check_shift_for_members() " << dbe.get_message() << std::endl;
				throw dbe;
			}
		}
		cd.add_days(1);
	}
}

void CalendarData::check_time_coverage(Database* database, std::vector<shift_error>* errors) {
	check_errors.clear();
	CustomDate cd = CustomDate::get_first_day_month(ref_date);
	while (cd <= CustomDate::get_last_day_month(ref_date)) {
		int time;
		if (!check_day_coverage(database, cd, &time)) {
			check_errors.insert(cd.get_day());
			shift_error err;
			err.id = Glib::ustring();
			err.day = cd.get_day();
			err.type = time < 0 ? ERRTYPE::out_of_time : ERRTYPE::not_covered;
			err.descr = Glib::ustring::compose("%1:%2", Glib::ustring::format(abs(time) / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), abs(time) % 60));
			errors->push_back(err);
		}
		cd.add_days(1);
	}
}

bool CalendarData::check_day_coverage(Database* database, const CustomDate& date, int* ht) {
	// get work time for workdays
	int work_start = Duty::get_work_time_start();
	int work_end = Duty::get_work_time_end();
	// check if today is holiday
	if (date.is_holiday()) {
		// check if shouldn't work on holiday
		if (Duty::get_work_time_holiday_close()) {
			return true;
		}
		// check if holiday has a special work time
		if (Duty::get_work_time_holiday_enabled()) {
			// get work time for holidays
			work_start = Duty::get_work_time_holiday_start();
			work_end = Duty::get_work_time_holiday_end();
		}
	} else if (date.get_weekday() == 6) { // check if today is saturday
		// check if shouldn't work on saturday
		if (Duty::get_work_time_saturday_close()) {
			return true;
		}
		// check if saturday has a special work time
		if (Duty::get_work_time_saturday_enabled()) {
			// get work time for saturday
			work_start = Duty::get_work_time_saturday_start();
			work_end = Duty::get_work_time_saturday_end();
		}
	}
	*ht = work_start;
	try {
		std::vector<Shift> shifts { database->get_working_shifts(date) };
		std::vector<Shift>::const_iterator iter = shifts.begin();
		std::vector<Shift>::const_iterator end = shifts.end();
		while (*ht < work_end && iter != end) {
			// check if it start to work before the planned time
			if (iter->get_start_in_minute() < work_start) {
				*ht = iter->get_start_in_minute() - work_start;
				return false;
			}
			if (iter->get_start_in_minute() <= *ht && iter->get_finish_in_minute() > *ht) {
				*ht = iter->get_finish_in_minute();
			}
			iter++;
		}
		if (*ht == work_end) {
			return true;
		} else if (*ht > work_end) {
			*ht = work_end - *ht;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::check_day_coverage() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	return false;
}

void CalendarData::check_worked_time_for_members(Database* database, const std::vector<Member>& members, std::vector<shift_error>* errors) {
	CustomDate start_date { CustomDate::get_first_day_month(ref_date) };
	CustomDate end_date { CustomDate::get_last_day_month(ref_date) };
	int mnth = start_date.get_month();
	for (std::vector<Member>::const_iterator iter = members.cbegin(), end = members.cend(); iter != end; iter++) {
		short int week_index = 0;
		CustomDate cd { start_date };
		while (cd <= end_date && cd.get_month() == mnth) {
			CustomDate wend { CustomDate::get_last_day_week(cd.get_time_t()) };
			if (wend.get_month() != cd.get_month()) {
				wend = CustomDate::get_last_day_month(cd.get_time_t());
			}
			int week_diff = get_worked_minutes(database, start_date, wend, iter->get_id()) - get_should_work_minutes(database, start_date, wend, iter->get_id());
			if (week_diff < 0) {
				shift_error err { iter->get_id(), week_index, ERRTYPE::not_enough, Glib::ustring::format(week_diff / 60) };
				errors->push_back(err);
			}
			cd.add_days(7);
			week_index++;
		}
	}
}

void CalendarData::update_offworks_cache(Database* database) {
	offworks_cache.clear();
	try {
		std::vector<OffWork> vec { database->get_offworks() };
		for (std::vector<OffWork>::const_iterator iter = vec.cbegin(), end = vec.cend(); iter != end; iter++) {
			offworks_cache.insert(std::pair<Glib::ustring,OffWork>(iter->get_full_label(),*iter));
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::update_offworks_cache() DBError" << std::endl;
		throw dbe;
	}
}

void CalendarData::update_shifts_cache(Database* database, const Glib::ustring& id, const CustomDate& cds, const CustomDate& cde) {
	shifts_cache.clear();
	try {
		std::vector<Shift> shifts { database->get_shifts_for_member(id, cds, cde) };
		for (std::vector<Shift>::const_iterator iter = shifts.cbegin(), end = shifts.cend(); iter != end; iter++) {
			shifts_cache.insert(std::pair<time_t,Shift>(CustomDate::start_of_day(iter->get_start()).get_time_t(),*iter));
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::update_shifts_cache() DBError" << std::endl;
		throw dbe;
	}
}

void CalendarData::check_siris_for_members(Database* database, const CustomDate& date, const std::vector<Member>& members, std::vector<shift_error>* errors) {
	for (std::vector<Member>::const_iterator iter = members.cbegin(), end = members.cend(); iter != end; iter++) {
		SirisWeek week;
		week.set_month(date.get_month());
		collect_siris_week_data(database, date, iter->get_id(), &week);
		if (!week.check_computation()) {
			shift_error err;
			err.id = iter->get_id();
			err.day = date.get_day();
			err.type = ERRTYPE::siris;
			err.descr = Glib::ustring();
			errors->push_back(err);
		}
	}
}

void CalendarData::collect_siris_week_data(Database* database, const CustomDate& date, const Glib::ustring& member, SirisWeek* week) {
	CustomDate cd_day { CustomDate::get_day_of_week(date.get_time_t(), 1) };
	for (int n = 0; n < 7; n++) {
		collect_day_data(database, cd_day, member, week);
		cd_day.add_days(1);
	}
}

void CalendarData::clear_errors(void) {
	missing_shifts.clear();
	check_errors.clear();
}

void CalendarData::undo(Database* database) {
	if (!undo_stack.empty()) {
		try {
			database->execute(undo_stack.top().first);
			redo_stack.push(undo_stack.top());
			undo_stack.pop();
		} catch (const Database::DBError& dbe) {
			std::cerr << "CalendarData::undo() DBError" << std::endl;
			throw dbe;
		}
	}
}

void CalendarData::redo(Database* database) {
	if (!redo_stack.empty()) {
		try {
			database->execute(redo_stack.top().second);
			undo_stack.push(redo_stack.top());
			redo_stack.pop();
		} catch (const Database::DBError& dbe) {
			std::cerr << "CalendarData::redo() DBError" << std::endl;
			throw dbe;
		}
	}
}

/*
 * appearance
 */

Gdk::RGBA CalendarData::get_background_color(const CustomDate& date) const {
	if (date.is_holiday()) {
		return check_errors.count(date.get_day()) > 0 ? Gdk::RGBA("DarkRed") : Gdk::RGBA("Red");
	}
	if (date.get_weekday() == 6) {
		return check_errors.count(date.get_day()) > 0 ? Gdk::RGBA("Orange") : Gdk::RGBA("Yellow");
	}
	return check_errors.count(date.get_day()) > 0 ? Gdk::RGBA("Grey") : Gdk::RGBA("White");
}

Gdk::RGBA CalendarData::get_background_color(const CustomDate& date, const Glib::ustring& id) const {
	Gdk::RGBA colour = get_background_color(date);
	if (missing_shifts.count(DayMember { date.get_time_t(), id.raw() }) == 1) {
		colour.set_red(colour.get_red() / 2.0);
		colour.set_green(colour.get_green() / 4.0);
		colour.set_blue(colour.get_blue() / 2.0);
	}
	return colour;
}

Gdk::RGBA CalendarData::get_foreground_color(const CustomDate& date) const {
	if (date.is_holiday()) {
		return Gdk::RGBA("White");
	}
	if (date.get_weekday() == 6) {
		return Gdk::RGBA("Black");
	}
	return Gdk::RGBA("Black");
}

Gdk::RGBA CalendarData::get_foreground_color(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache) const {
	try {
		Shift shift { get_shift(database, date, id, cache) };
		bool is_abs = is_absence(database, shift, cache);
		if (shift.is_working()) {
			return date.is_holiday() ? Gdk::RGBA("White") : Shift::work_color;
		} else if (!is_abs) {
			return date.is_holiday() ? Gdk::RGBA("Orange") : Shift::license_color;
		} else if (is_abs) {
			if (date.get_weekday() == 6) {
				return Gdk::RGBA("LightGreen");
			}
			return date.is_holiday() ? Gdk::RGBA("Orange") : Shift::absent_color;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::get_foreground_color() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return Gdk::RGBA("Black");
}

bool CalendarData::is_font_weight_bold(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache) const {
	try {
		Shift shift { get_shift(database, date, id, cache) };
		bool is_abs = is_absence(database, shift, cache);
		if (shift.is_working() || is_abs) {
			return false;
		} else if (!is_abs) {
			return true;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::is_font_weight_bold() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return false;
}

void CalendarData::collect_day_data(Database* database, const CustomDate& date, const Glib::ustring& member, SirisWeek* week) {
	// collect data for siris
	int sw_index = date.get_weekday();
	try {
		week->set_duty(sw_index, get_should_work_minutes(database, date, date, member));
		week->set_date(sw_index, date);
		if (is_working(database, date, member)) {
			Shift sft = get_shift(database, date, member);
			week->set_start(sw_index, sft.get_start_in_minute());
			week->set_end(sw_index, sft.get_finish_in_minute());
			if (date.is_holiday()) {
				if (date.get_weekday() == 0) {
					week->set_rrs(week->get_rrs() + 1);
				} else {
					week->set_rri(week->get_rri() + 1);
				}
			}
		} else {
			Glib::ustring lbl = get_shift_string(database, date, member, false, true);
			if (lbl.compare(Glib::ustring("R.P.")) == 0) {
				if (week->get_rrs() > 0) {
					lbl.assign(Glib::ustring("R.R.S."));
					week->set_rrs(week->get_rrs() - 1);
				} else if (week->get_rri() > 0) {
					lbl.assign(Glib::ustring("R.R.I."));
					week->set_rri(week->get_rri() - 1);
				}
			}
			week->set_label(sw_index, lbl);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "CalendarData::collect_day_data() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void CalendarData::set_missing_shifts(const std::unordered_set<DayMember>& msft) {
	missing_shifts.clear();
	missing_shifts.insert(msft.begin(), msft.end());
}

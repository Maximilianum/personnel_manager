/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * calendar_data.hh
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CALENDAR_DATA_HH_
#define _CALENDAR_DATA_HH_

#include <glibmm/ustring.h>
#include <map>
#include <stack>
#include <unordered_set>
#include "calendar.hh"
#include "database.hh"
#include "siris_month.hh"

enum class ERRTYPE { unknown, missing, out_of_time, not_covered, not_enough, siris };

typedef struct {
	Glib::ustring id;
	int day;
	ERRTYPE type;
	Glib::ustring descr;
} shift_error;

class DayMember {
 public:
	inline DayMember() : date (0), member (std::string()) {}
	inline DayMember(time_t t, const std::string& str) : date (t), member (str) {}
	inline bool operator==(const DayMember& dm) const { return date == dm.date && member.compare(dm.member) == 0; }
	time_t date;
	std::string member;
};

// custom hash function needed to make DayMember object works with std::unordered_set
template<> struct std::hash<DayMember> {
	size_t operator()(const DayMember& dm) const {
		struct tm* timeinfo = localtime(&(dm.date));
		return std::hash<string>()(dm.member + std::string(asctime(timeinfo)));
	}
};

class CalendarData : public Calendar {
 public:
	// constructors
	inline CalendarData() : Calendar() {}
	inline ~CalendarData() {}
	// operators
	CalendarData& operator=(const CalendarData &cd);
	// class member functions
	void add_shifts(Database* database, const Selection& sel, const Shift& sft);
	std::pair<Glib::ustring, Glib::ustring> create_shift_for_member(Database* database, const CustomDate& date, const Glib::ustring& id);
	void add_shifts_for_selected_member(Database* database);
	void add_shifts_for_all_members(Database* database);
	void update_shift(Database* database, const Shift& old_sft, const Shift& new_sft);
	bool remove_selected_shifts(Database* database);
	bool move_shift(Database* database, const int direction);
	Shift get_shift(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache = false) const;
	Shift get_selected_shift(Database* database, bool cache = false) const;
	bool is_selected_member_active(Database* database) const;
	OffWork get_offwork(Database* database, const Glib::ustring& lbl, bool cache = false) const;
	Glib::ustring get_shift_string(Database* database, const CustomDate& date, const Glib::ustring &member_id, bool cache = false, bool plain = false, bool full = false) const;
	Glib::ustring get_shift_note(Database* database, const CustomDate& date, const Glib::ustring &member_id, bool cache = false) const;
	bool has_shifts(Database* database, const Selection& sel) const;
	void clear_shifts(Database* database);
	bool is_working(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache = false) const;
	bool is_absence(Database* database, const Shift& shift, bool cache = false) const;
	int get_should_work_minutes(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id, bool cache = false) const;
	int get_worked_minutes(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id) const;
	int get_presence_count(Database* database, const Glib::ustring &id) const;
	int get_number_of_meals(Database* database, const Glib::ustring &id) const;
	void get_stats(Database* database, const CustomDate& start_date, const CustomDate& end_date, const Glib::ustring &id, std::map<const Glib::ustring, std::pair<const Glib::ustring, int> >* off_map) const;
	int get_license_amount(Database* database, const Glib::ustring& id, const Glib::ustring& label, int ref_year, const CustomDate& end_date);
	void check_shifts_for_members(Database* database, const std::vector<Member>& members, std::vector<shift_error>* errors);
	void check_time_coverage(Database* database, std::vector<shift_error>* errors);
	bool check_day_coverage(Database* database, const CustomDate& date, int* ht);
	void check_worked_time_for_members(Database* database, const std::vector<Member>& members, std::vector<shift_error>* errors);
	void update_offworks_cache(Database* database);
	void update_shifts_cache(Database* database, const Glib::ustring& id, const CustomDate& cds, const CustomDate& cde);
	void check_siris_for_members(Database* database, const CustomDate& date, const std::vector<Member>& members, std::vector<shift_error>* errors);
	void collect_siris_week_data(Database* database, const CustomDate& date, const Glib::ustring& member, SirisWeek* week);
	void clear_errors(void);
	void undo(Database* database);
	inline bool is_undo_available(void) const { return !undo_stack.empty(); }
	void redo(Database* database);
	inline bool is_redo_available(void) const { return !redo_stack.empty(); }
	// appearance
	Gdk::RGBA get_background_color(const CustomDate& date) const;
	Gdk::RGBA get_background_color(const CustomDate& date, const Glib::ustring& id) const;
	Gdk::RGBA get_foreground_color(const CustomDate& date) const;
	Gdk::RGBA get_foreground_color(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache = false) const;
	bool is_font_weight_bold(Database* database, const CustomDate& date, const Glib::ustring &id, bool cache = false) const;
 protected:
	void collect_day_data(Database* database, const CustomDate& date, const Glib::ustring& member, SirisWeek* week);
 private:
	void set_missing_shifts(const std::unordered_set<DayMember>& msft);
	static int daily_work_time;
	std::map<Glib::ustring,OffWork> offworks_cache;
	std::map<time_t,Shift> shifts_cache;
	std::stack<std::pair<Glib::ustring, Glib::ustring>> undo_stack;
	std::stack<std::pair<Glib::ustring, Glib::ustring>> redo_stack;
	std::unordered_set<DayMember> missing_shifts;
	std::unordered_set<short> check_errors;
};

#endif // _CALENDAR_DATA_HH_


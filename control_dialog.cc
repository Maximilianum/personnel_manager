/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * control_dialog.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "control_dialog.hh"
#include <glibmm/fileutils.h>
#include <iostream>

ControlDialog::ControlDialog(const Glib::ustring& path, Gtk::Window& parent) : Gtk::Dialog(Glib::ustring(), parent, true) {
	try {
		Glib::ustring::size_type p = path.find_last_of('/');
		if (p != Glib::ustring::npos) {
			ui_path.assign(path.substr(0,p + 1));
		} else {
			ui_path.assign(Glib::ustring());
		}
		bldr = Gtk::Builder::create_from_file(path);
	} catch (const Glib::FileError& ex) {
		std::cerr << ex.what() << std::endl;
	}
}

void ControlDialog::load_data_combobox(std::vector<Glib::ustring>::const_iterator start, std::vector<Glib::ustring>::const_iterator end, Glib::RefPtr<Gtk::ListStore> liststore) {
	liststore->clear();
	for (auto iter = start; iter != end; iter++) {
		Gtk::TreeModel::iterator tmi = liststore->append();
		Gtk::TreeModel::Row row = *tmi;
		row[combobox_tmcr.column] = *iter;
	}
}

int ControlDialog::get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value) {
	int index = 0;
	for (Gtk::TreeModel::iterator iter = liststore->children().begin(), end = liststore->children().end(); iter != end; iter++) {
		Gtk::TreeModel::Row row = *iter;
		if (value.compare(Glib::ustring(row[combobox_tmcr.column])) == 0) {
			return index;
		}
		index++;
	}
	return -1;
}

Gtk::TreeModel::iterator ControlDialog::get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str) {
	for (Gtk::TreeModel::iterator iter = liststore->children().begin(), end = liststore->children().end(); iter != end; iter++) {
		Gtk::TreeModel::Row row = *iter;
		if (str.compare(Glib::ustring(row[combobox_tmcr.column])) == 0) {
			return iter;
		}
	}
	return liststore->children().end();
}

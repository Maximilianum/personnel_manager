/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * control_dialog.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CONTROL_DIALOG_HH_
#define _CONTROL_DIALOG_HH_

#include <gtkmm/dialog.h>
#include <gtkmm/liststore.h>
#include <gtkmm/builder.h>
#include "single_string_tmcr.hh"

class ControlDialog : public Gtk::Dialog {
 public:
	/*
	 * exception class for error handling
	 */
	class CDEmptyFieldException
	{
	public:
    CDEmptyFieldException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
  
	ControlDialog(const Glib::ustring& path, Gtk::Window& parent);
	inline ControlDialog(const ControlDialog& cd) : bldr (cd.bldr) {}
 protected:
	void load_data_combobox(std::vector<Glib::ustring>::const_iterator start, std::vector<Glib::ustring>::const_iterator end, Glib::RefPtr<Gtk::ListStore> list);
	int get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value);
	Gtk::TreeModel::iterator get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str);
  
	Glib::RefPtr<Gtk::Builder> bldr;
	Glib::ustring ui_path;
	SingleStringTreeModelColumnRecord combobox_tmcr;
};

#endif // _CONTROL_DIALOG_HH_

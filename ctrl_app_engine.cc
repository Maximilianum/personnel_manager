/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ctrl_app_engine.cc
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctrl_app_engine.hh"

std::string CtrlAppEngine::app_filename = "";
std::string CtrlAppEngine::app_name = "";
std::string CtrlAppEngine::resources_path = "";
Glib::RefPtr<Gtk::Application> CtrlAppEngine::this_app;
std::string CtrlAppEngine::version = "";
std::string CtrlAppEngine::build_number = "";
std::string CtrlAppEngine::copyright = "";
std::string CtrlAppEngine::project_website = "";

/**
 * printer
 */

void CtrlAppEngine::set_up_printer(void) {
	Glib::ustring printer_conf;
	Glib::RefPtr<Gio::File> file_printer = Gio::File::create_for_path(page_setup_path);
	if (file_printer->query_exists()) {
		if (printer_keyfile.load_from_file(page_setup_path, Glib::KEY_FILE_NONE)) {
			m_ref_pagesetup->load_from_key_file(printer_keyfile);
		} else {
			std::cerr << "CtrlAppEngine::set_up_printer() failed to load page setup from key file!" << std::endl;
		}
	}
}

/**
 * about panel
 **/

void CtrlAppEngine::about(void) {
	Gtk::AboutDialog about_dialog {};
	Glib::RefPtr<Gdk::Pixbuf> logo = get_icon();
	std::stringstream ss;
	ss << "version " << version << " (build# " << build_number << " - " << __DATE__ << ")";
	about_dialog.set_transient_for(*this);
	about_dialog.set_program_name(Glib::ustring(app_name));
	about_dialog.set_version(ss.str());
	about_dialog.set_copyright(Glib::ustring(copyright));
	about_dialog.set_comments(Glib::ustring(app_name) + Glib::ustring(" è un software libero (GPL v3)"));
	about_dialog.set_license_type(Gtk::LICENSE_GPL_3_0);
	about_dialog.set_website(Glib::ustring(project_website));
	about_dialog.set_website_label(Glib::ustring("sito web del progetto"));
	about_dialog.set_logo(logo);
	about_dialog.run();
}

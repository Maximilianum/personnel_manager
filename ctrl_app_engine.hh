/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ctrl_app_engine.hh
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CTRL_APPENGINE_HH_
#define _CTRL_APPENGINE_HH_

#include <gtkmm.h>
#include <giomm/file.h>
#include "ctrl_engine.hh"

enum class APP_STATUS { idle, starting, running };

class CtrlAppEngine : public CtrlEngine {
 public:
	inline static void set_resources_path(const Glib::ustring& path) { resources_path = path; }
	inline static void set_app(Glib::RefPtr<Gtk::Application> app) { this_app = app; }
	inline CtrlAppEngine(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder) : CtrlEngine(cobject, builder) {}
	virtual inline ~CtrlAppEngine() {}
 protected:
	// printer
	void set_up_printer(void);
	// main toolbar and menu
	inline void quit(void) { this_app->quit(); }
	void about(void);

	/**
	 * variables
	 **/

	// build
	static std::string app_filename;
	static std::string app_name;
	static std::string resources_path;
	static Glib::RefPtr<Gtk::Application> this_app;
	static std::string version;
	static std::string build_number;
	static std::string copyright;
	static std::string project_website;
	// application
	APP_STATUS app_status;
	// printing
	Glib::KeyFile printer_keyfile;
	Glib::ustring page_setup_path;
	Glib::RefPtr<Gtk::PageSetup> m_ref_pagesetup;
	Glib::RefPtr<Gtk::PrintSettings> m_ref_printsettings;
};

#endif // _CTRL_APPENGINE_HH_

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ctrl_engine.cc
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ctrl_engine.hh"
#include <glibmm/fileutils.h>

/**
 * general function to load text data from a vector in a combobox liststore
 */

void CtrlEngine::load_data_combobox(std::vector<Glib::ustring>::iterator start, std::vector<Glib::ustring>::iterator end, Glib::RefPtr<Gtk::ListStore> liststore) {
	liststore->clear();
	for (auto iter = start; iter != end; iter++) {
		Gtk::TreeModel::iterator tmi = liststore->append();
		Gtk::TreeModel::Row row = *tmi;
		row[combobox_tmcr.column] = *iter;
	}
}

int CtrlEngine::get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value) {
	int index = 0;
	for (Gtk::TreeModel::iterator iter = liststore->children().begin(), end = liststore->children().end(); iter != end; iter++) {
		Gtk::TreeModel::Row row = *iter;
		if (value.compare(Glib::ustring(row[combobox_tmcr.column])) == 0) {
			return index;
		}
		index++;
	}
	return -1;
}

Gtk::TreeModel::iterator CtrlEngine::get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str) {
	for (Gtk::TreeModel::iterator iter = liststore->children().begin(), end = liststore->children().end(); iter != end; iter++) {
		Gtk::TreeModel::Row row = *iter;
		if (str.compare(Glib::ustring(row[combobox_tmcr.column])) == 0) {
			return iter;
		}
	}
	return liststore->children().end();
}

/**
 * TreeView utility function
 **/

std::vector<Gtk::TreeRowReference> CtrlEngine::get_treeview_row_ref(const std::vector<Gtk::TreePath> &sel, Glib::RefPtr<Gtk::TreeModel> model) {
	std::vector<Gtk::TreeRowReference> sel_ref;
	for (std::vector<Gtk::TreePath>::const_iterator iter = sel.cbegin(); iter != sel.cend(); iter++) {
		Gtk::TreeRowReference ref = Gtk::TreeRowReference(model, *iter);
		sel_ref.push_back(ref);
	}
	return sel_ref;
}


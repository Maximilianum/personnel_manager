/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ctrl_engine.hh
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CTRL_ENGINE_HH_
#define _CTRL_ENGINE_HH_

#include <gtkmm/window.h>
#include <gtkmm/builder.h>
#include <gtkmm/liststore.h>
#include <gtkmm/treerowreference.h>
#include <gtkmm/box.h>
#include <giomm/file.h>
#include <iostream>
#include <fstream>
#include "single_string_tmcr.hh"

class CtrlEngine : public Gtk::Window {
 public:
	/*
	 * exception class for error handling
	 */
	class IteratorNotFoundException
	{
	public:
    IteratorNotFoundException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
	inline CtrlEngine(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder) : Gtk::Window(cobject), bldr (builder) {}
	virtual inline ~CtrlEngine() {}
 protected:
	virtual void connect_signals(void) {}
	inline void on_cancel_button_clicked(Gtk::Window *win) { win->hide(); }
	inline void on_window_hide(Gtk::Window* win) { delete win; }
	// combobox
	void load_data_combobox(std::vector<Glib::ustring>::iterator start, std::vector<Glib::ustring>::iterator end, Glib::RefPtr<Gtk::ListStore> list);
	int get_row_index_for_value_in_combobox(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring &value);
	Gtk::TreeModel::iterator get_combobox_item(Glib::RefPtr<Gtk::ListStore> liststore, const Glib::ustring& str);
	// win
	inline bool on_window_focus_in(GdkEventFocus *event) { return true; }
	inline bool on_window_focus_out(GdkEventFocus *event) { return true; }
	// TreeView
	std::vector<Gtk::TreeRowReference> get_treeview_row_ref(const std::vector<Gtk::TreePath> &sel, Glib::RefPtr<Gtk::TreeModel> model);

	/**
	 * variables
	 **/

	// window
	Glib::RefPtr<Gtk::Builder> bldr;
	SingleStringTreeModelColumnRecord combobox_tmcr;
};

#endif // _CTRL_ENGINE_HH_

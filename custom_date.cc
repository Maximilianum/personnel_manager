/*
 * custom_date.cc
 * Copyright (C) 2018 - 2024 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "custom_date.hh"
#include <regex>

int CustomDate::time_zone = 1;
std::ostream& operator<<(std::ostream& os, const CustomDate& cd) {
  time_t unixtime = cd.get_time_t();
  struct tm* std_date = localtime(&unixtime);
  return os << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << " unix time: " << unixtime;
}

CustomDate CustomDate::now() {
  time_t now;
  time(&now);
  return CustomDate(&now);
}

CustomDate CustomDate::parse_string(const char* cs, DATE_FORMAT format) {
  std::string str { cs };
  time_t unixtime = time(nullptr);
  struct tm* std_date = localtime(&unixtime);
  std::regex df;
	
  switch (format) {
  case DATE_FORMAT::xml:
    // "%1:%2:%3 %4/%5/%6", hour, minute, second, day, month, year;
    if (str.size() == 19) {
      std_date->tm_hour = atoi(str.substr(0, 2).c_str());
      std_date->tm_min = atoi(str.substr(3, 2).c_str());
      std_date->tm_sec =  atoi(str.substr(6, 2).c_str());
      std_date->tm_mday = atoi(str.substr(9, 2).c_str());
      std_date->tm_mon = atoi(str.substr(12, 2).c_str()) - 1;
      std_date->tm_year = atoi(str.substr(15, 4).c_str()) - 1900;
    } else {
      throw CustomDateException("XML date parse error");
    }
    break;
  case DATE_FORMAT::sql:
    // "%1-%2-%3 %4:%5:%6", year, month, day, hour, minute, second;
    if (str.size() == 19) {
      std_date->tm_hour = atoi(str.substr(11, 2).c_str());
      std_date->tm_min = atoi(str.substr(14, 2).c_str());
      std_date->tm_sec = atoi(str.substr(17, 2).c_str());
      std_date->tm_mday = atoi(str.substr(8, 2).c_str());
      std_date->tm_mon = atoi(str.substr(5, 2).c_str()) - 1;
      std_date->tm_year = atoi(str.substr(0, 4).c_str()) - 1900;
    } else {
      throw CustomDateException("SQLite date parse error");
    }
    break;
  case DATE_FORMAT::human:
    // "%1 %2/%3/%4  ore %5:%6:%7", wday, day, month, year, hour, minute, second;
    if (str.size() == 28) {
      std_date->tm_hour = atoi(str.substr(20, 2).c_str());
      std_date->tm_min = atoi(str.substr(23, 2).c_str());
      std_date->tm_sec = atoi(str.substr(26, 2).c_str());
      std_date->tm_mday = atoi(str.substr(4, 2).c_str());
      std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
      std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
    } else {
      throw CustomDateException("Human date parse error");
    }
    break;
  case DATE_FORMAT::label:
    // "%1 %2/%3/%4", wday, day, month, year;
    if (str.size() == 14) {
      std_date->tm_mday = atoi(str.substr(4, 2).c_str());
      std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
      std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
      std_date->tm_hour = 0;
      std_date->tm_min = 0;
      std_date->tm_sec = 0;
    } else {
      throw CustomDateException("Label date parse error");
    }
    break;
  case DATE_FORMAT::print:
    // "%1 %2/%3/%4 %5:%6", wday, day, month, year, hour, minute;
    if (str.size() == 20) {
      std_date->tm_hour = atoi(str.substr(15, 2).c_str());
      std_date->tm_min = atoi(str.substr(18, 2).c_str());
      std_date->tm_mday = atoi(str.substr(4, 2).c_str());
      std_date->tm_mon = atoi(str.substr(7, 2).c_str()) - 1;
      std_date->tm_year = atoi(str.substr(10, 4).c_str()) - 1900;
      std_date->tm_sec = 0;
    } else {
      throw CustomDateException("Printed date parse error");
    }
    break;
  case DATE_FORMAT::date: // "{2}/{2}/{4}" -> day/month/year
    df = "[[:digit:]]{1,2}/[[:digit:]]{1,2}/[[:digit:]]{2,4}";
    if (std::regex_match(str, df)) {
      std::size_t pos1 { str.find('/') };
      std_date->tm_mday = atoi(str.substr(0, pos1).c_str());
      pos1++;
      std::size_t pos2 { str.find('/', pos1) };
      std_date->tm_mon = atoi(str.substr(pos1, pos2 - pos1).c_str()) - 1;
      pos2++;
      int y = atoi(str.substr(pos2).c_str());
      if (y < 1000) {
	y += 2000;
      }
      std_date->tm_year = y - 1900;
      std_date->tm_hour = 0;
      std_date->tm_min = 0;
      std_date->tm_sec = 0;
    } else {
      throw CustomDateException("unknown date format, expected dd/mm/yyyy");
    }
    break;
  }
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
  return CustomDate { &unixtime };
}

CustomDate CustomDate::start_of_day(const CustomDate& cd) {
  time_t unixtime = cd.get_time_t();
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_hour = 0;
  std_date->tm_min = 0;
  std_date->tm_sec = 0;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
  return CustomDate { &unixtime };
}

CustomDate CustomDate::end_of_day(const CustomDate& cd) {
  time_t unixtime = cd.get_time_t();
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_hour = 23;
  std_date->tm_min = 59;
  std_date->tm_sec = 59;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
  return CustomDate { &unixtime };
}

CustomDate CustomDate::get_next_day(time_t ref_day) {
  CustomDate cd { &ref_day };
  cd.add_days(1);
  return cd;
}

CustomDate CustomDate::get_previous_day(time_t ref_day) {
  CustomDate cd { &ref_day };
  cd.add_days(-1);
  return cd;
}

CustomDate CustomDate::get_first_day_week(time_t ref_day) {
  struct tm* ref_day_struct = localtime(&ref_day);
  int ref_wday = ref_day_struct->tm_wday;
  // if the given day is Sunday then make it equals to 7
  if (ref_wday == 0) {
    ref_wday = 7;
  }
  CustomDate cd { &ref_day };
  cd.add_days(1 - ref_wday);
  return cd;
}

CustomDate CustomDate::get_last_day_week(time_t ref_day) {
  struct tm* ref_day_struct = localtime(&ref_day);
  int ref_wday = ref_day_struct->tm_wday;
  // if the given day is Sunday then make it equals to 7
  if (ref_wday == 0) {
    ref_wday = 7;
  }
  CustomDate cd { &ref_day };
  cd.add_days(7 - ref_wday);
  return cd;
}

CustomDate CustomDate::get_first_day_month(time_t ref_day) {
  CustomDate cd { &ref_day };
  cd.set_day(1);
  return cd;
}

CustomDate CustomDate::get_last_day_month(time_t ref_day) {
  CustomDate cd { &ref_day };
  switch (cd.get_month()) {
  case 0:
  case 2:
  case 4:
  case 6:
  case 7:
  case 9:
  case 11:
    cd.set_day(31);
    break;
  case 3:
  case 5:
  case 8:
  case 10:
    cd.set_day(30);
    break;
  case 1:
    if (is_leap_year(cd.get_year() + 1900)) {
      cd.set_day(29);
    } else {
      cd.set_day(28);
    }
    break;
  }
  return cd;
}

CustomDate CustomDate::get_first_day_year(time_t ref_day) {
  CustomDate cd { &ref_day };
  cd.set_month(0);
  cd.set_day(1);
  return cd;
}

CustomDate CustomDate::get_last_day_year(time_t ref_day) {
  CustomDate cd { &ref_day };
  cd.set_month(11);
  cd.set_day(31);
  return cd;
}

CustomDate CustomDate::get_day_of_week(time_t ref_day, int d) {
  if (d < 0 || d > 7) {
    throw CustomDateException("CustomDate::get_day_of_week() day index out of bounds");
  }
  if (d == 0) {
    d = 7;
  }
  struct tm* ref_day_struct = localtime(&ref_day);
  int ref_wday = ref_day_struct->tm_wday;
  // if the given day is Sunday then make it equals to 7
  if (ref_wday == 0) {
    ref_wday = 7;
  }
  CustomDate cd { &ref_day };
  cd.add_days(d - ref_wday);
  return cd;
}

CustomDate CustomDate::get_previous_month(const CustomDate& cd) {
  time_t ref_date { cd.get_time_t() };
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mon--;
  if (std_date->tm_mon < 0) {
    std_date->tm_mon = 11;
    std_date->tm_year--;
  }
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
  return { &ref_date };
}

CustomDate CustomDate::get_next_month(const CustomDate& cd) {
  time_t ref_date { cd.get_time_t() };
  struct tm* std_date = localtime(&ref_date);
  std_date->tm_mon++;
  if (std_date->tm_mon > 11) {
    std_date->tm_mon = 0;
    std_date->tm_year++;
  }
  std_date->tm_isdst = -1;
  ref_date = mktime(std_date);
  return { &ref_date };
}

short int CustomDate::get_month_size(int year, int month) {
  short int sz = 31;
  switch (month) {
  case 3:
  case 5:
  case 8:
  case 10:
    sz = 30;
    break;
  case 1:
    if (is_leap_year(year + 1900)) {
      sz = 29;
    } else {
      sz = 28;
    }
    break;
  }
  return sz;
}

bool CustomDate::is_leap_year(int year) {
  if (year % 400 == 0) {
    return true;
  } else if (year % 100 == 0) {
    return false;
  } else if (year % 4 == 0) {
    return true;
  }
  return false;
}

void CustomDate::set_date_time(short int y, short int mn, short int d, short int h, short int m, short int s) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_year = y;
  std_date->tm_mon = mn;
  std_date->tm_mday = d;
  std_date->tm_hour = h;
  std_date->tm_min = m;
  std_date->tm_sec = s;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

void CustomDate::set_year(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_year = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

short int CustomDate::get_year(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_year;
}

void CustomDate::set_month(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_mon = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

short int CustomDate::get_month(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_mon;
}

void CustomDate::set_day(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_mday = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

void CustomDate::add_days(short int val) {
  struct tm* original_date = localtime(&unixtime);
  int h = original_date->tm_hour;
  int d = original_date->tm_mday;
  int i = original_date->tm_isdst;
  unixtime += (ONE_DAY * val);
  struct tm* new_date = localtime(&unixtime);
  if (new_date->tm_isdst != i) {
    if (new_date->tm_hour != h) {
      set_hour(h);
    }
    if (abs(val) == 1 && new_date->tm_mday != d + val) {
      set_day(d +val);
    }
  }
}

short int CustomDate::get_day(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_mday;
}

bool CustomDate::is_in_range(const CustomDate& cd, int days) {
  CustomDate cd2 { cd };
  const CustomDate* start { nullptr };
  const CustomDate* end { nullptr };
  if (days == 0) {
    return is_same_day(cd);
  }
  cd2.add_days(days);
  if (days > 0) {
    start = &cd;
    end = &cd2;
  } else {
    start = &cd2;
    end = &cd;
  }
  return unixtime >= CustomDate::start_of_day(*start).get_time_t() && unixtime <= CustomDate::end_of_day(*end).get_time_t();
}

void CustomDate::set_hour(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_hour = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

short int CustomDate::get_hour(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_hour;
}

void CustomDate::set_minute(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_min = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

short int CustomDate::get_minute(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_min;
}

void CustomDate::set_second(short int val) {
  struct tm* std_date = localtime(&unixtime);
  std_date->tm_sec = val;
  std_date->tm_isdst = -1;
  unixtime = mktime(std_date);
}

short int CustomDate::get_second(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_sec;
}

short int CustomDate::get_weekday(void) const {
  struct tm* std_date = localtime(&unixtime);
  return std_date->tm_wday;
}

std::string CustomDate::get_weekday_string(bool flag) const {
  std::string week_days[] = { "DOMENICA", "LUNEDÌ", "MARTEDÌ", "MERCOLEDÌ", "GIOVEDÌ", "VENERDÌ", "SABATO" };
  return flag ? week_days[get_weekday()] : week_days[get_weekday()].substr(0,1);
}

bool CustomDate::is_holiday(void) const {
  struct tm* std_date = localtime(&unixtime);
  // check if it's Sunday
  if (std_date->tm_wday == 0) {
    return true;
  }
  // check fixed holidays
  for (int i = 0; i < HOLIDAYS_SIZE; i++) {
    if (std_date->tm_mon == HOLIDAYS[i].first && std_date->tm_mday == HOLIDAYS[i].second) {
      return true;
    }
  }
  // check Easter day and Easter Monday
  std::pair<short, short> easter = get_easter_day(std_date->tm_year + 1900);
  if (std_date->tm_mon == easter.first && (std_date->tm_mday == easter.second || std_date->tm_mday == easter.second + 1)) {
    return true;
  }
  if (std_date->tm_mon == (easter.first + 1) % 12 && std_date->tm_mday == 1 && easter.second == get_month_size(std_date->tm_year, easter.first)) {
    return true;
  }
  return false;
}

std::string CustomDate::get_formatted_string(DATE_FORMAT format) const {
  std::ostringstream str;
  std::string giorni[7] = { "Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab" };
  std::string mesi[12] = { "Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre" };
  struct tm* std_date = localtime(&unixtime);
  switch (format) {
  case DATE_FORMAT::xml:  // "%1:%2:%3 %4/%5/%6"
    str << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
    break;
  case DATE_FORMAT::sql:  // "%1-%2-%3 %4:%5:%6"
    str << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << "-" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "-" << std::setfill('0') << std::setw(2) << std_date->tm_mday << " " << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std::fixed << std::setprecision(3) << std_date->tm_sec;
    break;
  case DATE_FORMAT::human:	// "%1 %2/%3/%4  ore %5:%6:%7"
    str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << "  ore " << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min << ":" << std::setfill('0') << std::setw(2) << std_date->tm_sec;
    break;
  case DATE_FORMAT::label:	// "%1 %2/%3/%4"
    str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
    break;
  case DATE_FORMAT::print:	// "%1 %2/%3/%4 %5:%6"
    str << giorni[std_date->tm_wday] << " " << std::setfill('0') << std::setw(2) << std_date->tm_mday  << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900 << std::setfill('0') << std::setw(2) << std_date->tm_hour << ":" << std::setfill('0') << std::setw(2) << std_date->tm_min;
    break;
  case DATE_FORMAT::date:	// "gg/mm/aaaa"
    str << std::setfill('0') << std::setw(2) << std_date->tm_mday << "/" << std::setfill('0') << std::setw(2) << std_date->tm_mon + 1 << "/" << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
    break;
  case DATE_FORMAT::monthyear: // "mm aaaa"
    str << mesi[std_date->tm_mon] << " " << std::setfill('0') << std::setw(4) << std_date->tm_year + 1900;
    break;
  }
  return str.str();
}

short int CustomDate::get_month_size(void) const {
  int sz = 31;
  switch (get_month()) {
  case 3:
  case 5:
  case 8:
  case 10:
    sz = 30;
    break;
  case 1:
    if (is_leap_year(get_year() + 1900)) {
      sz = 29;
    } else {
      sz = 28;
    }
    break;
  }
  return sz;
}

short int CustomDate::get_weeks_in_month(void) const {
  short int n = 1;
  CustomDate cd { get_first_day_month(unixtime) };
  short int mn = cd.get_month();
  cd = get_last_day_week(cd.get_time_t());
  cd.add_days(1);
  while (cd.get_month() == mn) {
    n++;
    cd.add_days(7);
  }
  return n;
}

bool CustomDate::week_intersect_month(int mnt) const {
  CustomDate fdw { get_first_day_week(unixtime) };
  if (fdw.get_month() == mnt) {
    return true;
  }
  CustomDate ldw { get_last_day_week(unixtime) };
  return ldw.get_month() == mnt;
}

std::pair<short, short> CustomDate::get_easter_day(short int yr) {
  int a = yr % 19;
  int b = yr / 100;
  int c = yr % 100;
  int d = b / 4;
  int e = b % 4;
  int g = (8 * b + 13) / 25;
  int h = (19 * a + b - d - g + 15) % 30;
  int m = (a + 11 * h) / 319;
  int j = c / 4;
  int k = c % 4;
  int l = (2 * e + 2 * j - k - h + m + 32) % 7;
  int n = (h - m + l + 90) / 25;
  int p = (h - m + l + n + 19) % 32;
  std::pair<short, short> easter = {n - 1, p};
  return easter;
}


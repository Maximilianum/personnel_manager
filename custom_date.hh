/*
 * custom_date.hh
 * Copyright (C) 2018 - 2024 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CUSTOM_DATE_HH_
#define _CUSTOM_DATE_HH_

#include <iostream>
#include <iomanip>
#include <string>
#include <cmath>

#define ONE_DAY 86400

static const int HOLIDAYS_SIZE = 11;
static std::pair<short, short> HOLIDAYS[] = {{0, 1}, {0, 6}, {3, 25}, {4, 1}, {5, 2}, {5, 29}, {7, 15}, {10, 1}, {11, 8}, {11, 25}, {11, 26}};

enum class DATE_FORMAT { xml, sql, human, label, print, date, monthyear };

class CustomDate {
  friend std::ostream& operator<<(std::ostream& os, const CustomDate& cd);
public:
  /*
   * exception class for error handling
   */
  class CustomDateException
  {
  public:
    CustomDateException(const std::string& str) : message (str) {}
    std::string get_message(void) const { return message; }
  private:
    std::string message;
  };
  inline CustomDate(void) : unixtime (0) {}
  inline CustomDate(const time_t* time) : unixtime (*time) {}
  inline CustomDate(const CustomDate& cd) : unixtime (cd.unixtime) {}
  inline CustomDate(const char* str) : unixtime (atoll(str)) {}

  static CustomDate parse_string(const char* cs, DATE_FORMAT format = DATE_FORMAT::date);
  static CustomDate now();
  static CustomDate start_of_day(const CustomDate& cd);
  inline static CustomDate start_of_day(time_t ref_day) { return start_of_day(CustomDate(&ref_day)); }
  static CustomDate end_of_day(const CustomDate& cd);
  inline static CustomDate end_of_day(time_t ref_day) { return end_of_day(CustomDate(&ref_day)); }
  static CustomDate get_next_day(time_t ref_day);
  static CustomDate get_previous_day(time_t ref_day);
  static CustomDate get_first_day_week(time_t ref_day);
  static CustomDate get_last_day_week(time_t ref_day);
  static CustomDate get_first_day_month(time_t ref_day);
  static CustomDate get_last_day_month(time_t ref_day);
  static CustomDate get_first_day_year(time_t ref_day);
  static CustomDate get_last_day_year(time_t ref_day);
  static CustomDate get_day_of_week(time_t ref_day, int d);
  static CustomDate get_previous_month(const CustomDate& cd);
  static CustomDate get_next_month(const CustomDate& cd);
  static short int get_month_size(int year, int month);
  static bool is_leap_year(int year);
	
  inline CustomDate& operator=(const CustomDate &cd);
  inline bool operator<(const CustomDate &cd) const { return unixtime < cd.unixtime; }
  inline bool operator>(const CustomDate &cd) const { return unixtime > cd.unixtime; }
  inline bool operator==(const CustomDate &cd) const { return unixtime == cd.unixtime; }
  inline bool operator!=(const CustomDate &cd) const { return unixtime != cd.unixtime; }
  inline bool operator>=(const CustomDate &cd) const { return unixtime >= cd.unixtime; }
  inline bool operator<=(const CustomDate &cd) const { return unixtime <= cd.unixtime; }

  inline struct tm* get_struct_time(void) { return localtime(&unixtime); }
  void set_date_time(short int y, short int mn, short int d, short int h, short int m, short int s);
  void set_year(short int val);
  short int get_year(void) const;
  void set_month(short int val);
  short int get_month(void) const;
  void set_day(short int val);
  void add_days(short int val);
  short int get_day(void) const;
  inline bool is_same_day(const CustomDate& cd) { return get_day() == cd.get_day() && get_month() == cd.get_month(); }
  bool is_in_range(const CustomDate& cd, int days);
  void set_hour(short int val);
  short int get_hour(void) const;
  void set_minute(short int val);
  short int get_minute(void) const;
  void set_second(short int val);
  short int get_second(void) const;
  time_t get_time_t(void) const { return unixtime; }
  short int get_weekday(void) const;
  std::string get_weekday_string(bool flag = true) const;
  bool is_holiday(void) const;
  std::string get_formatted_string(DATE_FORMAT format) const;
  short int get_month_size(void) const;
  short int get_weeks_in_month(void) const;
  bool week_intersect_month(int mnt) const;
  inline double get_difference(const CustomDate& cd) const { return difftime(unixtime, cd.unixtime); }
  inline int get_difference_days(const CustomDate& cd) const { return std::trunc(get_difference(cd) / 86400); }
protected:
  static std::pair<short, short> get_easter_day(short int yr);
private:
  static int time_zone;
  time_t unixtime;
};

inline CustomDate& CustomDate::operator=(const CustomDate &cd) {
  if (this != &cd) {
    unixtime = cd.unixtime;
  }
  return *this;
}

#endif // _CUSTOM_DATE_HH_


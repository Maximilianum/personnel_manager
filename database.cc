/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * database.cc
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "database.hh"

int Database::db_version = 2;

Database::Database(const Glib::ustring& db_path, int ver) {
	// try to open an existing database
	int result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE, NULL);
	if (result != SQLITE_OK) {
		// create a new database
		result = sqlite3_open_v2(db_path.c_str(), &sqlt_db, SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE, NULL);
		if (result == SQLITE_OK) {
			origin = DATABASE_ORIGIN::created;
			create_database(sqlt_db);
			Glib::ustring sql = Glib::ustring::compose("PRAGMA user_version = %1;", ver);
			int rc = Database::exec(sqlt_db, sql, NULL, NULL);
			if (rc != SQLITE_OK) {
				std::cerr << "Error while setting user version: " << sqlite3_errmsg(sqlt_db) << std::endl;
			}
		} else {
			std::cerr << "Can't open database: " << sqlite3_errmsg(sqlt_db) << std::endl;
		}
	} else {
		origin = DATABASE_ORIGIN::opened;
		int v = Database::get_version(sqlt_db);
		if (v < db_version) {
			std::cout << "detected database version " << v << ", upgrading to version " << db_version << std::endl;
			upgrade_database(sqlt_db);
		}
	}
}

void Database::create_database(sqlite3* sqlt_db) {
	// create SQL TABLE RANK
	Glib::ustring sql = Glib::ustring("CREATE ") + Rank::get_sqlite_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table RANK: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE MEMBER
	sql = Glib::ustring("CREATE ") + Member::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table MEMBER: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE SERVICE
	sql = Glib::ustring("CREATE ") + Service::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table SERVICE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE LABEL
	sql = Glib::ustring("CREATE ") + Label::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LABEL: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE OFFWORK
	sql = Glib::ustring("CREATE ") + OffWork::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table OFFWORK: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE LICENSE_TYPE
	sql = Glib::ustring("CREATE ") + LicenseType::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LICENSE_TYPE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE LICENSE
	sql = Glib::ustring("CREATE ") + License::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LICENSE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE SHIFT
	sql = Glib::ustring("CREATE ") + Shift::get_sqlite_shift_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table SHIFT: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE MEMBER_SETTINGS
	sql = Glib::ustring("CREATE ") + Settings::get_sqlite_member_settings_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table MEMBER_SETTINGS: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE TIME_PRESET
	sql = Glib::ustring("CREATE ") + TimePreset::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table TIME_PRESET: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE SETTINGS
	sql = Glib::ustring("CREATE ") + Settings::get_sqlite_settings_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table SETTINGS: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE TEXTLINE
	sql = Glib::ustring("CREATE ") + TextLine::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table TEXTLINE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	// create SQL TABLE STR_SETTINGS
	sql = Glib::ustring("CREATE ") + Settings::get_sqlite_str_settings_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table STR_SETTINGS: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
}

/*
 * member functions
 */

void Database::upgrade_database(sqlite3* sqlt_db) {
	upgrade_rank(sqlt_db);
	upgrade_member(sqlt_db);
	upgrade_member_settings(sqlt_db);
	upgrade_label(sqlt_db);
	upgrade_shift(sqlt_db);
	upgrade_license(sqlt_db);
	upgrade_text_line(sqlt_db);
	Glib::ustring sql = Glib::ustring::compose("PRAGMA user_version = %1;", db_version);
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_rank(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + Rank::get_sqlite_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table RANK: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<Rank> ranks;
	sql = Glib::ustring("SELECT * from RANKS ORDER BY IDX ASC;");
	if (Database::exec(sqlt_db, sql, Rank::retrieve_rank, &ranks) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = ranks.cbegin(); it != ranks.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO RANK (FLABEL,SLABEL,RANK) VALUES ('%1','%2','%3'); ",get_sqlite_safe_string(it->get_full_label()),get_sqlite_safe_string(it->get_short_label()),it->get_index()));
	}
	sql.append(Glib::ustring("DROP TABLE RANKS;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_member(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + Member::get_sqlite_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table MEMBER: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	sql = Glib::ustring("CREATE ") + Service::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table SERVICE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<Member> members;
	sql = Glib::ustring("SELECT "		 \
						"MEMBERS.ID, "						\
						"MEMBERS.FNAME, "					\
						"MEMBERS.LNAME, "					\
						"RANK.RANK, "						\
						"RANK.FLABEL, "						 \
						"RANK.SLABEL, "						 \
						"MEMBERS.ENLISTMENT, "					\
						"MEMBERS.EXTRA, "								\
						"MEMBERS.WEEKPLAN from MEMBERS "					\
						"inner join RANK on MEMBERS.RANK = RANK.RANK "	\
						"ORDER BY RANK.RANK, MEMBERS.ENLISTMENT, MEMBERS.LNAME, MEMBERS.FNAME ASC;");
	if (Database::exec(sqlt_db, sql, Member::retrieve_member, &members) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = members.cbegin(); it != members.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO MEMBER (ID,FNAME,LNAME,RANK,ENLISTMENT,EXTRA,WEEKPLAN) VALUES ('%1','%2','%3','%4','%5','%6','%7');",get_sqlite_safe_string(it->get_id()),get_sqlite_safe_string(it->get_first_name()),get_sqlite_safe_string(it->get_last_name()),it->get_rank().get_index(),it->get_enlistment_date().get_time_t(),it->get_extra_years(),(it->get_working_week() == Workweek::six ? 6 : 5)));
	}
	sql.append(Glib::ustring("DROP TABLE MEMBERS;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = members.cbegin(); it != members.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO SERVICE (MEMBER,START) VALUES ('%1','%2');",get_sqlite_safe_string(it->get_id()),it->get_enlistment_date().get_time_t()));
	}
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_member_settings(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + Settings::get_sqlite_member_settings_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table MEMBER_SETTINGS: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<mbr_settings> member_settings;
	sql = Glib::ustring("SELECT * from MBR_SETTINGS;");
	if (Database::exec(sqlt_db, sql, Settings::retrieve_member_settings_complete, &member_settings) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = member_settings.cbegin(); it != member_settings.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO MEMBER_SETTINGS (ENTRY,MEMBER,VALUE) VALUES ('%1','%2','%3');",get_sqlite_safe_string(it->entry),get_sqlite_safe_string(it->mbr_id),get_sqlite_safe_string(it->value)));
	}
	sql.append(Glib::ustring("DROP TABLE MBR_SETTINGS;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_label(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + Label::get_sqlite_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LABEL: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	sql = Glib::ustring("CREATE ") + OffWork::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table OFFWORK: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<OffWork> offworks;
	sql = Glib::ustring("SELECT * from WORK_STATUS;");
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_offwork, &offworks) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = offworks.cbegin(); it != offworks.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO OFFWORK (LABEL,FLAGS) VALUES ('%1','%2');",get_sqlite_safe_string(it->get_full_label()),it->get_flags()));
		sql.append(Glib::ustring::compose("INSERT INTO LABEL (FULL,SHORT) VALUES ('%1','%2');",get_sqlite_safe_string(it->get_full_label()),get_sqlite_safe_string(it->get_short_label())));
	}
	sql.append(Glib::ustring("DROP TABLE WORK_STATUS;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_shift(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + Shift::get_sqlite_shift_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table SHIFT: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<Shift> shifts;
	sql = Glib::ustring("SELECT "										\
						"SHIFTS.MEMBER, "								\
						"SHIFTS.WORK_STATUS, "							\
						"LICENSES.REF_YEAR, "							\
						"SHIFTS.START, "								\
						"SHIFTS.FINISH from SHIFTS "					\
						"inner join LICENSES on SHIFTS.LICENSE = LICENSES.ID "	\
						"ORDER BY SHIFTS.START ASC;");
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = shifts.cbegin(); it != shifts.cend(); it++) {
		sql.append(compute_insert(*it));
	}
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
	shifts.clear();
	sql = Glib::ustring("SELECT * from SHIFTS where LICENSE='-1' order by SHIFTS.START asc;");
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = shifts.cbegin(); it != shifts.cend(); it++) {
		sql.append(compute_insert(*it));
	}
	sql.append(Glib::ustring("DROP TABLE SHIFTS;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_license(sqlite3* sqlt_db) {
	Glib::ustring sql { Glib::ustring("CREATE ") + LicenseType::get_sqlite_table() };
	int rc { Database::exec(sqlt_db, sql, NULL, NULL) };
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LICENSE_TYPE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	sql = Glib::ustring("CREATE ") + License::get_sqlite_table();
	rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table LICENSE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	sql = Glib::ustring("SELECT "	\
						"LABEL.FULL, "						\
						"LABEL.SHORT, "							\
						"LICENSES.REF_YEAR, "					\
						"OFFWORK.FLAGS from LICENSES "					\
						"inner join LABEL on LICENSES.LABEL=LABEL.FULL " \
						"inner join OFFWORK on LICENSES.LABEL=OFFWORK.LABEL " \
						"order by LICENSES.REF_YEAR asc;");
	std::set<LicenseType> types;
	if (Database::exec(sqlt_db, sql, LicenseType::retrieve_license_type_set, &types) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	std::set<Glib::ustring> visible;
	sql = Glib::ustring("SELECT LABEL from VISIBLE_LABEL;");
	if (Database::exec(sqlt_db, sql, Label::retrieve_string_set, &visible) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = types.begin(); it != types.end(); it++) {
		std::uint8_t flags { it->get_flags() };
		flags = flags >> 1;
		if (visible.count(it->get_full_label()) > 0) {
			flags = flags | LicenseType::mask4;
		}
		sql.append(Glib::ustring::compose("INSERT INTO LICENSE_TYPE (LABEL,FLAGS) VALUES ('%1','%2');",get_sqlite_safe_string(it->get_full_label()),flags));
	}
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
	sql = Glib::ustring("SELECT "	\
						"LICENSES.MEMBER, "					\
						"LABEL.FULL, "						\
						"LABEL.SHORT, "							\
						"LICENSES.REF_YEAR, "					\
						"LICENSES.START, "						\
						"LICENSES.DEADLINE, "					\
						"LICENSES.DAYS from LICENSES "								\
						"inner join LABEL on LICENSES.LABEL=LABEL.FULL " \
						"order by LICENSES.REF_YEAR asc;");
	std::vector<License> licenses;
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto it = licenses.cbegin(); it != licenses.cend(); it++) {
		sql.append(Glib::ustring::compose("INSERT INTO LICENSE (MEMBER,LABEL,REF_YEAR,START,DEADLINE,DAYS) VALUES ('%1','%2','%3','%4','%5','%6');",get_sqlite_safe_string(it->get_member()),get_sqlite_safe_string(it->get_full_label()),it->get_ref_year(),it->get_start_date().get_time_t(),it->get_deadline_date().get_time_t(),it->get_days()));
	}
	sql.append(Glib::ustring("DROP TABLE LICENSES;"));
	sql.append(Glib::ustring("DROP TABLE VISIBLE_LABEL;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

void Database::upgrade_text_line(sqlite3* sqlt_db) {
	Glib::ustring sql = Glib::ustring("CREATE ") + TextLine::get_sqlite_table();
	int rc = Database::exec(sqlt_db, sql, NULL, NULL);
	if (rc != SQLITE_OK) {
		std::cerr << "Error while creating table TEXTLINE: " << sqlite3_errmsg(sqlt_db) << std::endl;
	}
	std::vector<TextLine> lines;
	sql = Glib::ustring("SELECT * from LINE_TEXT;");
	if (Database::exec(sqlt_db, sql, TextLine::retrieve_text_line, &lines) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() exec failed!" } };
	}
	sql.assign(Glib::ustring());
	for (auto iter = lines.begin(); iter != lines.end(); iter++) {
		sql.append(Glib::ustring::compose("insert into TEXTLINE (ROW,TYPE,STRING,FSIZE) values ('%1','%2','%3','%4');",iter->get_row(), get_sqlite_safe_string(iter->get_type()),get_sqlite_safe_string(iter->get_text()),iter->get_font_size()));
	}
	sql.append(Glib::ustring("DROP TABLE LINE_TEXT;"));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::upgrade_database() Exec failed!" } };
	}
}

/**
 * rank
 */

std::vector<Rank> Database::get_ranks(void) const {
	std::vector<Rank> ranks;
	Glib::ustring sql = Glib::ustring("SELECT * from RANK ORDER BY RANK ASC;");
	if (Database::exec(sqlt_db, sql, Rank::retrieve_rank, &ranks) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_ranks() exec failed!" } };
	}
	return ranks;
}

std::vector<Glib::ustring> Database::get_rank_labels(void) const {
	std::vector<Glib::ustring> ranks;
	Glib::ustring sql = Glib::ustring("SELECT FLABEL from RANK ORDER BY RANK ASC;");
	if (Database::exec(sqlt_db, sql, Rank::retrieve_rank_label, &ranks) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_rank_labels() exec failed!" } };
	}
	return ranks;
}

Rank Database::get_rank_with_full(const Glib::ustring& str) const {
	std::vector<Rank> ranks;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from RANK where FLABEL='%1' ORDER BY RANK ASC;",get_sqlite_safe_string(str));
	if(Database::exec(sqlt_db, sql, Rank::retrieve_rank, &ranks) == SQLITE_OK) {
		if (ranks.size() > 0) {
			return ranks.front();
		} else {
			throw DBEmptyResultException { std::string { "Database::get_rank_with_full() couldn't find rank" } };
		}
	} else {
		throw DBError { std::string { "Database::get_rank_with_full() exec failed!" } };
	}
}

Rank Database::get_rank_with_short(const Glib::ustring& str) const {
	std::vector<Rank> ranks;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from RANK where SLABEL='%1' ORDER BY RANK ASC;",get_sqlite_safe_string(str));
	if(Database::exec(sqlt_db, sql, Rank::retrieve_rank, &ranks) == SQLITE_OK) {
		if (ranks.size() > 0) {
			return ranks.front();
		} else {
			throw DBEmptyResultException { std::string { "Database::get_rank_with_short() couldn't find rank" } };
		}
	} else {
		throw DBError { std::string { "Database::get_rank_with_full() exec failed!" } };
	}
}

void Database::insert(const Rank& rnk) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO RANK (FLABEL,SLABEL,RANK) VALUES ('%1','%2','%3');",get_sqlite_safe_string(rnk.get_full_label()),get_sqlite_safe_string(rnk.get_short_label()),rnk.get_index());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(rank) exec failed!" } };
	}
}

void Database::update_rank_full(const Rank& rnk, const Glib::ustring& str) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE RANK set FLABEL='%1' where RANK='%2';",get_sqlite_safe_string(str),rnk.get_index());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_rank_full() exec failed!" } };
	}
}

void Database::update_rank_short(const Rank& rnk, const Glib::ustring& str) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE RANK set SLABEL='%1' where RANK='%2';",get_sqlite_safe_string(str),rnk.get_index());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_rank_short() exec failed!" } };
	}
}

void Database::swap_rank(const Rank& rnk1, const Rank& rnk2) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from RANK where FLABEL='%1';",rnk1.get_full_label());
	sql.append(Glib::ustring::compose("DELETE from RANK where FLABEL='%1';",rnk2.get_full_label()));
	sql.append(Glib::ustring::compose("INSERT INTO RANK (FLABEL,SLABEL,RANK) VALUES ('%1','%2','%3');",get_sqlite_safe_string(rnk2.get_full_label()),get_sqlite_safe_string(rnk2.get_short_label()),rnk1.get_index()));
	sql.append(Glib::ustring::compose("INSERT INTO RANK (FLABEL,SLABEL,RANK) VALUES ('%1','%2','%3');",get_sqlite_safe_string(rnk1.get_full_label()),get_sqlite_safe_string(rnk1.get_short_label()),rnk2.get_index()));
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::swap_rank() exec failed!" } };
	}
}

void Database::delete_rank(short id) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from RANK where RANK='%1';",id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_rank() exec failed!" } };
	}
	std::vector<Rank> ranks;
	sql = Glib::ustring::compose("SELECT * from RANK where RANK>'%1' ORDER BY RANK ASC;",id);
	if (Database::exec(sqlt_db, sql, Rank::retrieve_rank, &ranks) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_rank() exec failed!" } };
	}
	int index = id;
	sql = Glib::ustring();
	for (auto iter = ranks.cbegin(); iter != ranks.cend(); iter++) {
		sql.append(Glib::ustring::compose("UPDATE RANK set RANK='%1' where RANK='%2';", index, index + 1));
		index++;
	}
	if (index > id) {
		if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
			throw DBError { std::string { "Database::delete_rank() exec failed!" } };
		}
	}
}

/**
 * member
 */

std::vector<Member> Database::get_members(void) const {
	std::vector<Member> members;
	Glib::ustring sql = Glib::ustring("SELECT "		 \
									  "MEMBER.ID, "	    \
									  "MEMBER.FNAME, "	    \
									  "MEMBER.LNAME, "	    \
									  "RANK.RANK, "	    \
									  "RANK.FLABEL, "	    \
									  "RANK.SLABEL, "		 \
									  "MEMBER.ENLISTMENT, "	 \
									  "MEMBER.EXTRA, "			\
									  "MEMBER.WEEKPLAN from MEMBER "	\
									  "inner join RANK on MEMBER.RANK = RANK.RANK " \
									  "ORDER BY RANK.RANK, MEMBER.ENLISTMENT, MEMBER.LNAME, MEMBER.FNAME ASC;");
	if (Database::exec(sqlt_db, sql, Member::retrieve_member, &members) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_members() exec failed!" } };
	}
	return members;
}

std::vector<Member> Database::get_active_members(const CustomDate& ref) const {
	std::vector<Member> members;
	Glib::ustring sql = Glib::ustring::compose("select " \
											   "MEMBER.ID, " \
											   "MEMBER.FNAME, " \
											   "MEMBER.LNAME, " \
											   "RANK.RANK, " \
											   "RANK.FLABEL, " \
											   "RANK.SLABEL, " \
											   "MEMBER.ENLISTMENT, " \
											   "MEMBER.EXTRA, "	\
											   "MEMBER.WEEKPLAN, " \
											   "SERVICE.START, " \
											   "SERVICE.END from MEMBER " \
											   "inner join RANK on MEMBER.RANK = RANK.RANK " \
											   "inner join SERVICE on MEMBER.ID = SERVICE.MEMBER " \
											   "where SERVICE.START<='%1' and (SERVICE.END is NULL or SERVICE.END>='%1') " \
											   "ORDER BY RANK.RANK, MEMBER.ENLISTMENT, MEMBER.LNAME, MEMBER.FNAME ASC;", ref.get_time_t());
	if (Database::exec(sqlt_db, sql, Member::retrieve_member, &members) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_active_members() exec failed!" } };
	}
	return members;
}

std::vector<Glib::ustring> Database::get_members_id(void) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring("SELECT ID from MEMBER order by ID asc;");
	if (Database::exec(sqlt_db, sql, Member::retrieve_member_id, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_members_id() exec failed!" } };
	}
	return strings;
}

Member Database::get_member(const Glib::ustring& mbr_id) const {
	std::vector<Member> members;
	Glib::ustring sql = Glib::ustring::compose("SELECT "	 \
											   "MEMBER.ID, " \
											   "MEMBER.FNAME, "	\
											   "MEMBER.LNAME, "	\
											   "RANK.RANK, "	\
											   "RANK.FLABEL, "	\
											   "RANK.SLABEL, "	 \
											   "MEMBER.ENLISTMENT, "	\
											   "MEMBER.EXTRA, "		\
											   "MEMBER.WEEKPLAN from MEMBER " \
											   "inner join RANK on MEMBER.RANK = RANK.RANK " \
											   "WHERE MEMBER.ID='%1' ORDER BY RANK.RANK, MEMBER.ID, MEMBER.LNAME, MEMBER.FNAME ASC;", mbr_id);
	if (Database::exec(sqlt_db, sql, Member::retrieve_member, &members) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_member() exec failed!" } };
	}
	if (members.size() > 0) {
		return members.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_member() couldn't find member!" } };
	}
}

void Database::insert(const Member& mbr) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO MEMBER (ID,FNAME,LNAME,RANK,ENLISTMENT,EXTRA,WEEKPLAN) VALUES ('%1','%2','%3','%4','%5','%6','%7');",get_sqlite_safe_string(mbr.get_id()),get_sqlite_safe_string(mbr.get_first_name()),get_sqlite_safe_string(mbr.get_last_name()),mbr.get_rank().get_index(),mbr.get_enlistment_date().get_time_t(),mbr.get_extra_years(),(mbr.get_working_week() == Workweek::six ? 6 : 5));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(member) exec failed!" } };
	}
}

void Database::update(const Member& mbr) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE MEMBER set FNAME='%1',LNAME='%2',RANK='%3',ENLISTMENT='%4',EXTRA='%5',WEEKPLAN='%6' where ID ='%7';",get_sqlite_safe_string(mbr.get_first_name()),get_sqlite_safe_string(mbr.get_last_name()),mbr.get_rank().get_index(),mbr.get_enlistment_date().get_time_t(),mbr.get_extra_years(),(mbr.get_working_week() == Workweek::six ? 6 : 5),get_sqlite_safe_string(mbr.get_id()));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update(member) exec failed!" } };
	}
}

void Database::delete_member(const Glib::ustring& id) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from MEMBER where ID='%1';",get_sqlite_safe_string(id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_member() exec failed!" } };
	}
}

/**
 * service
 */

std::vector<Service> Database::get_services(const Glib::ustring& id) const {
	std::vector<Service> services;
	Glib::ustring sql = Glib::ustring::compose("select * from SERVICE where MEMBER='%1' order by START asc", get_sqlite_safe_string(id));
	if (Database::exec(sqlt_db, sql, Service::retrieve_service, &services) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_services() exec failed!" } };
	}
	return services;
}

Service Database::get_service(const Glib::ustring& id, const CustomDate& st) const {
	std::vector<Service> services;
	Glib::ustring sql = Glib::ustring::compose("select * from SERVICE where MEMBER='%1' and START='%2' order by START asc", get_sqlite_safe_string(id), st.get_time_t());
	if (Database::exec(sqlt_db, sql, Service::retrieve_service, &services) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_services() exec failed!" } };
	}
	if (services.size() == 0) {
		throw DBEmptyResultException { std::string { "Database::get_service() couldn't find service!" } };
	}
	return services.front();
}

void Database::update_service_start(const Service& srv, const CustomDate& st) {
	Glib::ustring sql = Glib::ustring::compose("update SERVICE set START='%1' where MEMBER='%2' and START='%3';",st.get_time_t(),get_sqlite_safe_string(srv.get_member()),srv.get_start().get_time_t());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_service_start() exec failed!" } };
	}
}

void Database::update_service_end(const Service& srv, const CustomDate& st) {
	Glib::ustring sql = Glib::ustring::compose("update SERVICE set END='%1' where MEMBER='%2' and START='%3';",st.get_time_t(),get_sqlite_safe_string(srv.get_member()),srv.get_start().get_time_t());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_service_end() exec failed!" } };
	}
}

void Database::update_service_end(const Service& srv) {
	Glib::ustring sql = Glib::ustring::compose("update SERVICE set END=NULL where MEMBER='%1' and START='%2';",get_sqlite_safe_string(srv.get_member()),srv.get_start().get_time_t());
	if(Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_service_end() exec failed!" } };
	}
}

void Database::insert(const Service& srv) {
	Glib::ustring sql = Glib::ustring::compose("insert into SERVICE (MEMBER,START) values ('%1','%2');",get_sqlite_safe_string(srv.get_member()),srv.get_start().get_time_t());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(service) exec failed!" } };
	}
}

void Database::delete_services(const Glib::ustring& id) {
	Glib::ustring sql = Glib::ustring::compose("delete from SERVICE where MEMBER='%1';",get_sqlite_safe_string(id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_services() exec failed!" } };
	}
}

void Database::delete_service(const Glib::ustring& id, const CustomDate& st) {
	Glib::ustring sql = Glib::ustring::compose("delete from SERVICE where MEMBER='%1' and START='%2';",get_sqlite_safe_string(id),st.get_time_t());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_service() exec failed!" } };
	}
}

/**
 * label
 */

std::vector<Label> Database::get_labels(void) const {
	std::vector<Label> lbls;
	Glib::ustring sql = Glib::ustring("SELECT * from LABEL ORDER BY FULL ASC;");
	if (Database::exec(sqlt_db, sql, Label::retrieve_label, &lbls) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels() exec failed!" } };
	}
	return lbls;
}

std::set<Label> Database::get_labels_set(void) const {
	std::set<Label> lbls;
	Glib::ustring sql = Glib::ustring("SELECT * from LABEL ORDER BY FULL ASC;");
	if (Database::exec(sqlt_db, sql, Label::retrieve_label_set, &lbls) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels() exec failed!" } };
	}
	return lbls;
}

Label Database::get_label(const Glib::ustring& lbl) const {
	std::vector<Label> lbls;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from LABEL where FULL='%1' ORDER BY FULL ASC;",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, Label::retrieve_label, &lbls) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_label() exec failed!" } };
	}
	if (lbls.size() == 0) {
		throw DBEmptyResultException { std::string { "Database::get_label() couldn't find label!" } };
	}
	return lbls.front();
}

Label Database::get_short_label(const Glib::ustring& lbl) const {
	std::vector<Label> lbls;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from LABEL where SHORT='%1' ORDER BY FULL ASC;",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, Label::retrieve_label, &lbls) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_short_label() exec failed!" } };
	}
	if (lbls.size() == 0) {
		throw DBEmptyResultException { std::string { "Database::get_short_label() couldn't find label!" } };
	}
	return lbls.front();
}

std::vector<Glib::ustring> Database::get_labels_string() const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring("SELECT FULL from LABEL ORDER BY FULL ASC;");
	if (Database::exec(sqlt_db, sql, Label::retrieve_string, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_string() exec failed!" } };
	}
	return strings;
}

std::set<Glib::ustring> Database::get_labels_string_set() const {
	std::set<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring("SELECT FULL from LABEL ORDER BY FULL ASC;");
	if (Database::exec(sqlt_db, sql, Label::retrieve_string_set, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_string() exec failed!" } };
	}
	return strings;
}

void Database::insert(const Label& lbl) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO LABEL (FULL,SHORT) VALUES ('%1','%2');",get_sqlite_safe_string(lbl.get_full_label()),get_sqlite_safe_string(lbl.get_short_label()));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(label) exec failed!" } };
	}
}

void Database::update_label(const Glib::ustring& oldlbl, const Glib::ustring& newlbl) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE LABEL set FULL='%1' where FULL='%2';",get_sqlite_safe_string(newlbl),get_sqlite_safe_string(oldlbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_label() exec failed!" } };
	}
}

void Database::update_short_label(const Glib::ustring& oldlbl, const Glib::ustring& newlbl) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE LABEL set SHORT='%1' where FULL='%2';",get_sqlite_safe_string(newlbl),get_sqlite_safe_string(oldlbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_short_label() exec failed!" } };
	}
}

void Database::delete_label(const Glib::ustring& lbl) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from LABEL where FULL='%1';",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_label() exec failed!" } };
	}
}

/**
 * offwork
 */

std::vector<OffWork> Database::get_offworks(void) const {
	std::vector<OffWork> offwrks;
	Glib::ustring sql = Glib::ustring("SELECT " \
									  "OFFWORK.LABEL, "		   \
									  "LABEL.SHORT, "					\
									  "OFFWORK.FLAGS from OFFWORK "		\
									  "inner join LABEL on OFFWORK.LABEL=LABEL.FULL " \
									  "ORDER BY OFFWORK.LABEL ASC;");
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_offwork, &offwrks) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_offworks() exec failed!" } };
	}
	return offwrks;
}

OffWork Database::get_offwork(const Glib::ustring& lbl) const {
	std::vector<OffWork> offwrks;
	Glib::ustring sql = Glib::ustring::compose("SELECT " \
											   "OFFWORK.LABEL, " \
											   "LABEL.SHORT, " \
											   "OFFWORK.FLAGS from OFFWORK " \
											   "inner join LABEL on OFFWORK.LABEL=LABEL.FULL " \
											   "where OFFWORK.LABEL='%1' ORDER BY OFFWORK.LABEL ASC;",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_offwork, &offwrks) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_offwork() exec failed!" } };
	}
	if (offwrks.size() == 0) {
		throw DBEmptyResultException { std::string { "Database::get_offwork() couldn't find offwork!" } };
	}
	return offwrks.front();
}

std::vector<Glib::ustring> Database::get_offworks_label() const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring("select LABEL from OFFWORK order by LABEL asc;");
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_offworks_label() exec failed!" } };
	}
	return strings;
}

std::vector<Glib::ustring> Database::get_labels_str_with_absence(bool flag) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT LABEL from OFFWORK where FLAGS&1=%1 ORDER BY LABEL ASC;",flag);
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_str_with_absence() exec failed!" } };
	}
	return strings;
}

std::vector<Glib::ustring> Database::get_labels_str_with_weekday(bool flag) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT LABEL from OFFWORK where FLAGS&2=%1 ORDER BY LABEL ASC;",flag*2);
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_str_with_weekday() exec failed!" } };
	}
	return strings;
}

std::vector<Glib::ustring> Database::get_labels_str_with_saturday(bool flag) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT LABEL from OFFWORK where FLAGS&4=%1 ORDER BY LABEL ASC;",flag*4);
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_str_with_saturday() exec failed!" } };
	}
	return strings;
}

std::vector<Glib::ustring> Database::get_labels_str_with_sunday(bool flag) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT LABEL from OFFWORK where FLAGS&8=%1 ORDER BY LABEL ASC;",flag*8);
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_str_with_sunday() exec failed!" } };
	}
	return strings;
}

std::vector<Glib::ustring> Database::get_labels_str_with_holiday(bool flag) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT LABEL from OFFWORK where FLAGS&16=%1 ORDER BY LABEL ASC;",flag*16);
	if (Database::exec(sqlt_db, sql, OffWork::retrieve_label, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_labels_str_with_holiday() exec failed!" } };
	}
	return strings;
}

void Database::insert(const OffWork& ow) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO OFFWORK (LABEL,FLAGS) VALUES ('%1','%2');",get_sqlite_safe_string(ow.get_full_label()),ow.get_flags());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(offwork) exec failed!" } };
	}
}

void Database::update_offwork_label(const Glib::ustring& oldow, const Glib::ustring& newow) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE OFFWORK set LABEL='%1' where LABEL='%2';",get_sqlite_safe_string(newow),get_sqlite_safe_string(oldow));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_offwork() exec failed!" } };
	}
}

void Database::update_offwork_flags(const Glib::ustring& lbl, std::uint8_t flgs) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE OFFWORK set FLAGS='%1' where LABEL='%2';",flgs,get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_offwork_flags() exec failed!" } };
	}
}

void Database::delete_offwork(const Glib::ustring& lbl) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from OFFWORK where LABEL='%1';",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_offwork() exec failed!" } };
	}
}

/**
 * license type
 */

std::vector<LicenseType> Database::get_license_types(void) const {
	std::vector<LicenseType> licenses;
	Glib::ustring sql = Glib::ustring("SELECT * from LICENSE_TYPE ORDER BY LABEL ASC;");
	if (Database::exec(sqlt_db, sql, LicenseType::retrieve_license_type, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license_types() exec failed!" } };
	}
	return licenses;
}

std::vector<Glib::ustring> Database::get_license_types_label(void) const {
	std::vector<Glib::ustring> licenses;
	Glib::ustring sql = Glib::ustring("SELECT * from LICENSE_TYPE ORDER BY LABEL ASC;");
	if (Database::exec(sqlt_db, sql, LicenseType::retrieve_label, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license_types_label() exec failed!" } };
	}
	return licenses;
}

std::vector<LicenseType> Database::get_visible_license_types(void) const {
	std::vector<LicenseType> licenses;
	Glib::ustring sql = Glib::ustring("SELECT " \
									  "LICENSE_TYPE.LABEL, " \
									  "LABEL.SHORT, " \
									  "LICENSE_TYPE.FLAGS from LICENSE_TYPE " \
									  "inner join LABEL on LICENSE_TYPE.LABEL=LABEL.FULL " \
									  "where FLAGS&16=16 order by LABEL asc;");
	if (Database::exec(sqlt_db, sql, LicenseType::retrieve_license_type, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_visible_offworks() exec failed!" } };
	}
	return licenses;
}

LicenseType Database::get_license_type(const Glib::ustring& label) const {
	std::vector<LicenseType> licenses;
	Glib::ustring sql = Glib::ustring::compose("SELECT " \
											   "LICENSE_TYPE.LABEL, " \
											   "LABEL.SHORT, " \
											   "LICENSE_TYPE.FLAGS from LICENSE_TYPE " \
											   "inner join LABEL on LICENSE_TYPE.LABEL=LABEL.FULL " \
											   "where LABEL='%1' order by LABEL asc;", get_sqlite_safe_string(label));
	if (Database::exec(sqlt_db, sql, LicenseType::retrieve_license_type, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license_type() exec failed!" } };
	}
	if (licenses.size() > 0) {
		return licenses.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_license_type() couldn't find license!" } };
	}
}

void Database::insert(const LicenseType& lt) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO LICENSE_TYPE (LABEL,FLAGS) VALUES ('%1','%2');",get_sqlite_safe_string(lt.get_full_label()),lt.get_flags());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(LicenseType) exec failed!" } };
	}
}

void Database::update_license_type_flags(const Glib::ustring& lbl, std::uint8_t flgs) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE LICENSE_TYPE set FLAGS='%1' where LABEL='%2';",flgs,get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_license_type_flags() exec failed!" } };
	}
}

void Database::delete_license_type(const Glib::ustring& lbl) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from LICENSE_TYPE where LABEL='%1';",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_license_type() exec failed!" } };
	}
}

/**
 * license
 */

std::vector<License> Database::get_licenses(void) const {
	std::vector<License> licenses;
	Glib::ustring sql = Glib::ustring("SELECT * from LICENSE ORDER BY LABEL ASC;");
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_licenses() exec failed!" } };
	}
	return licenses;
}

std::vector<License> Database::get_licenses_for_member(const Glib::ustring& str) const {
	std::vector<License> licenses;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from LICENSE where MEMBER='%1' ORDER BY LABEL ASC;",get_sqlite_safe_string(str));
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_licenses_for_member() exec failed!" } };
	}
	return licenses;
}

std::vector<License> Database::get_licenses_with_label(const Glib::ustring& str) const {
	std::vector<License> licenses;
	Glib::ustring sql = Glib::ustring::compose("select * from LICENSE where LABEL='%1' order by LABEL asc;",get_sqlite_safe_string(str));
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_licenses_with_label() exec failed!" } };
	}
	return licenses;
}

std::vector<License> Database::get_valid_licenses_for_member(const Glib::ustring& id, const CustomDate& cd) const {
	std::vector<License> licenses;
	CustomDate sod = CustomDate::start_of_day(cd);
	Glib::ustring sql = Glib::ustring::compose("SELECT * from LICENSE where MEMBER='%1' and DEADLINE>='%2' and START<='%3' ORDER BY LABEL ASC;",get_sqlite_safe_string(id),sod.get_time_t(),sod.get_time_t());
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_valid_licenses_for_member() exec failed!" } };
	}
	return licenses;
}

std::vector<License> Database::get_visible_valid_licenses_for_member(const Glib::ustring& id, const CustomDate& cd) const {
	std::vector<License> licenses;
	CustomDate sod = CustomDate::start_of_day(cd);
	Glib::ustring sql = Glib::ustring::compose("SELECT "		   \
											   "LICENSE.MEMBER, "       \
											   "LICENSE.LABEL, "		\
											   "LICENSE.REF_YEAR, "	\
											   "LICENSE.START, "		\
											   "LICENSE.DEADLINE, "	\
											   "LICENSE.DAYS, " \
											   "LICENSE_TYPE.FLAGS from LICENSE "			\
											   "inner join LICENSE_TYPE on LICENSE.LABEL=LICENSE_TYPE.LABEL " \
											   "where MEMBER='%1' and DEADLINE>='%2' and START<='%3' and FLAGS&16=16 order by LICENSE.LABEL asc;",get_sqlite_safe_string(id),sod.get_time_t(),sod.get_time_t());
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_visible_valid_licenses_for_member() exec failed!" } };
	}
	return licenses;
}

std::vector<License> Database::get_visible_valid_licenses_for_member_with_label(const Glib::ustring& id, const CustomDate& cd, const Glib::ustring& lbl) const {
	std::vector<License> licenses;
	CustomDate sod = CustomDate::start_of_day(cd);
	Glib::ustring sql = Glib::ustring::compose("SELECT "		   \
											   "LICENSE.MEMBER, "       \
											   "LICENSE.LABEL, "		\
											   "LICENSE.REF_YEAR, "	\
											   "LICENSE.START, "		\
											   "LICENSE.DEADLINE, "	\
											   "LICENSE.DAYS from LICENSE " \
											   "inner join LICENSE_TYPE on LICENSE.LABEL=LICENSE_TYPE.LABEL " \
											   "where MEMBER='%1' and DEADLINE>='%2' and START<='%3' and LICENSE.LABEL='%4' ORDER BY LICENSE.LABEL ASC;",get_sqlite_safe_string(id),sod.get_time_t(),sod.get_time_t(),get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_visible_valid_licenses_for_member_with_label() exec failed!" } };
	}
	return licenses;
}

License Database::get_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year) const {
	std::vector<License> licenses;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from LICENSE where MEMBER='%1' and LABEL='%2' and REF_YEAR='%3' ORDER BY LABEL ASC;", get_sqlite_safe_string(id), get_sqlite_safe_string(label), ref_year);
	if (Database::exec(sqlt_db, sql, License::retrieve_license, &licenses) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license() exec failed!" } };
	}
	if (licenses.size() > 0) {
		return licenses.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_license() couldn't find license!" } };
	}
}

void Database::insert(const License& lcn) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO LICENSE (MEMBER,LABEL,REF_YEAR,START,DEADLINE,DAYS) VALUES ('%1','%2','%3','%4','%5','%6');",get_sqlite_safe_string(lcn.get_member()),get_sqlite_safe_string(lcn.get_full_label()),lcn.get_ref_year(),lcn.get_start_date().get_time_t(),lcn.get_deadline_date().get_time_t(),lcn.get_days());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(license) exec failed!" } };
	}
}

void Database::update_license(const License& lic, const Glib::ustring& member, const Glib::ustring& label, int ref_year) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE LICENSE set LABEL='%1', REF_YEAR='%2', START='%3', DEADLINE='%4', DAYS='%5' where MEMBER='%6' and LABEL='%7' and REF_YEAR='%8';",get_sqlite_safe_string(lic.get_full_label()),lic.get_ref_year(),lic.get_start_date().get_time_t(),lic.get_deadline_date().get_time_t(),lic.get_days(),get_sqlite_safe_string(member),get_sqlite_safe_string(label),ref_year);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_license() exec failed!" } };
	}
}

void Database::update_all_license_labels(const Glib::ustring& old_lbl, const Glib::ustring& new_lbl) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE LICENSE set LABEL='%1' where LABEL ='%2';",get_sqlite_safe_string(new_lbl),get_sqlite_safe_string(old_lbl));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_all_license_labels() exec failed!" } };
	}
}

void Database::delete_licenses(const Glib::ustring& id) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from LICENSE where MEMBER='%1';",get_sqlite_safe_string(id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_licenses() exec failed!" } };
	}
}

void Database::delete_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from LICENSE where MEMBER='%1' and LABEL='%2' and REF_YEAR='%3';",get_sqlite_safe_string(id),get_sqlite_safe_string(label),ref_year);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_license() exec failed!" } };
	}
}

/**
 * shifts
 */

Shift Database::get_shift(const Glib::ustring& id, const CustomDate& dt) const {
	CustomDate sod = CustomDate::start_of_day(dt);
	CustomDate eod = CustomDate::end_of_day(dt);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where MEMBER='%1' and START>='%2' and START<='%3' ORDER BY START ASC;",get_sqlite_safe_string(id),sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_shift() exec failed!" } };
	}
	if (shifts.size() > 0) {
		return shifts.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_shift() couldn't find any shift!" } };
	}
}

bool Database::has_shifts(const Glib::ustring& id, const CustomDate& dt, int days) const {
	const CustomDate* start;
	const CustomDate* end;
	CustomDate eor { dt };
	eor.add_days(days);
	if (dt < eor) {
		start = &dt;
		end = &eor;
	} else {
		start = &eor;
		end = &dt;
	}
	CustomDate sod = CustomDate::start_of_day(*start);
	CustomDate eod = CustomDate::end_of_day(*end);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where MEMBER='%1' and START>='%2' and START<='%3' ORDER BY START ASC;",get_sqlite_safe_string(id),sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::has_shifts() exec failed!" } };
	}
	return shifts.size() > 0;
}

std::vector<Shift> Database::get_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const {
	CustomDate sod = CustomDate::start_of_day(dts);
	CustomDate eod = CustomDate::end_of_day(dte);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where MEMBER='%1' and START>='%2' and START<='%3' ORDER BY START ASC;",get_sqlite_safe_string(str),sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_shifts_for_member() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_non_working_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const {
	CustomDate sod = CustomDate::start_of_day(dts);
	CustomDate eod = CustomDate::end_of_day(dte);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where LABEL is not NULL and MEMBER='%1' and START>='%2' and START<='%3' ORDER BY START ASC;",get_sqlite_safe_string(str),sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_non_working_shifts_for_member() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_shifts_with_offwork(const Glib::ustring& lbl) const {
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where LABEL='%1' order by START asc;",get_sqlite_safe_string(lbl));
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_shifts_with_offwork() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_working_shifts(const CustomDate& dt) const {
	CustomDate sod = CustomDate::start_of_day(dt);
	CustomDate eod = CustomDate::end_of_day(dt);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where LABEL is NULL and START>='%1' and START<='%2' ORDER BY START ASC;",sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_working_shifts() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_working_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const {
	CustomDate sod = CustomDate::start_of_day(dts);
	CustomDate eod = CustomDate::end_of_day(dte);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where LABEL is NULL and MEMBER='%1' and START>='%2' and START<='%3' ORDER BY START ASC;",get_sqlite_safe_string(str),sod.get_time_t(),eod.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_working_shifts_for_member() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_license_shifts_for_member(const Glib::ustring& id, const Glib::ustring& label, int ref_year, const CustomDate& ep) const {
	CustomDate eop = CustomDate::end_of_day(ep);
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("SELECT * from SHIFT where MEMBER='%1' and LABEL='%2' and LIC_YEAR='%3' and START<='%4' ORDER BY START ASC;", get_sqlite_safe_string(id), get_sqlite_safe_string(label), ref_year, eop.get_time_t());
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license_shifts_for_member() exec failed!" } };
	}
	return shifts;
}

std::vector<Shift> Database::get_license_shifts_for_member(const Glib::ustring& id, const Glib::ustring& label, int ref_year) const {
	std::vector<Shift> shifts;
	Glib::ustring sql = Glib::ustring::compose("select * from SHIFT where MEMBER='%1' and LABEL='%2' and LIC_YEAR='%3' order by START asc;", get_sqlite_safe_string(id), get_sqlite_safe_string(label), ref_year);
	if (Database::exec(sqlt_db, sql, Duty::retrieve_duty, &shifts) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_license_shifts_for_member() exec failed!" } };
	}
	return shifts;
}

void Database::insert(const Shift& sft) {
	Glib::ustring sql { compute_insert(sft) };
	execute(sql);
}

void Database::update(const Shift& sft, const Glib::ustring& id, const CustomDate& dt) {
	Glib::ustring sql { compute_update(sft, id, dt) };
	execute(sql);
}

void Database::update_shifts_labels(const Glib::ustring& old_lbl, const Glib::ustring& new_lbl) {
	Glib::ustring sql { Glib::ustring::compose("update SHIFT set LABEL='%1' where LABEL='%2';",get_sqlite_safe_string(new_lbl),get_sqlite_safe_string(old_lbl)) };
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_shifts_labels() exec failed!" } };
	}
}

void Database::update_shift_date(const Shift& sft, const CustomDate& start, const CustomDate& finish) {
	Glib::ustring sql { compute_update_shift_date(sft, start, finish) };
	execute(sql);
}

void Database::delete_shifts(const Glib::ustring& id) {
	Glib::ustring sql = Glib::ustring::compose("delete from SHIFT where MEMBER ='%1';",get_sqlite_safe_string(id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_shifts() exec failed!" } };
	}
}

void Database::delete_shift(const Glib::ustring& id, const CustomDate& dt) {
	Glib::ustring sql { compute_delete_shift(id, dt) };
	execute(sql);
}

void Database::delete_shifts_within(const CustomDate& start, const CustomDate& end) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from SHIFT where START>='%1' and START<='%2';",start.get_time_t(),end.get_time_t());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_shifts_within() exec failed!" } };
	}
}

/**
 * get string queries
 */

Glib::ustring Database::compute_insert(const Shift& sft) {
	std::uint8_t opts = 0;
	constexpr std::uint8_t mask0 { 1 << 0 };
	constexpr std::uint8_t mask1 { 1 << 1 };
	constexpr std::uint8_t mask2 { 1 << 2 };
	constexpr std::uint8_t mask3 { 1 << 3 };
	constexpr std::uint8_t mask4 { 1 << 4 };
	/*
	 * 0 LIC_YEAR
	 * 1 LABEL
	 * 2 NOTE
	 * 3 FLAGS
	 * 4 OBREAK
	 */
	Glib::ustring sql { "insert into SHIFT (MEMBER," };
	if (sft.get_label().compare(Glib::ustring()) != 0) {
		opts = opts | mask1;
		sql.append("LABEL,");
	}
	if (sft.get_license_year() >= 0) {
		opts = opts | mask0;
		sql.append("LIC_YEAR,");
	}
	sql.append("START");
	if ((opts&mask1) == 0) {
		sql.append(",FINISH");
	}
	if (sft.get_note().compare(Glib::ustring()) != 0) {
		opts = opts | mask2;
		sql.append(",NOTE");
	}
	std::uint8_t flags { sft.get_flags() };
	if (flags != 0) {
		opts = opts | mask3;
		sql.append(", FLAGS");
	}
	if (flags&mask2) {
		opts = opts | mask4;
		sql.append(", OBREAK");
	}
	sql.append(Glib::ustring::compose(") values ('%1',",get_sqlite_safe_string(sft.get_member())));
	if (opts&mask1) {
		sql.append(Glib::ustring::compose("'%1',",get_sqlite_safe_string(sft.get_label())));
	}
	if (opts&mask0) {
		sql.append(Glib::ustring::compose("'%1',",sft.get_license_year()));
	}
	sql.append(Glib::ustring::compose("'%1'",sft.get_start().get_time_t()));
	if ((opts&mask1) == 0) {
		sql.append(Glib::ustring::compose(",'%1'",sft.get_end().get_time_t()));
	}
	if (opts&mask2) {
		sql.append(Glib::ustring::compose(",'%1'",get_sqlite_safe_string(sft.get_note())));
	}
	if (opts&mask3) {
		sql.append(Glib::ustring::compose(",'%1'",sft.get_flags()));
	}
	if (opts&mask4) {
		sql.append(Glib::ustring::compose(",'%1'",sft.get_other_break_min()));
	}
	sql.append(");");
	return sql;
}

Glib::ustring Database::compute_update(const Shift& sft, const Glib::ustring& id, const CustomDate& dt) {
	Glib::ustring sql { Glib::ustring::compose("update SHIFT set MEMBER='%1',",get_sqlite_safe_string(sft.get_member())) };
	if (sft.is_working()) {
		sql.append(Glib::ustring("LABEL=NULL,"));
	} else {
		sql.append(Glib::ustring::compose("LABEL='%1',",get_sqlite_safe_string(sft.get_label())));
	}
	if (sft.get_license_year() >= 0) {
		sql.append(Glib::ustring::compose("LIC_YEAR='%1',",sft.get_license_year()));
	} else {
		sql.append(Glib::ustring::compose("LIC_YEAR=NULL,",sft.get_license_year()));
	}
	sql.append(Glib::ustring::compose("START='%1'",sft.get_start().get_time_t()));
	if (sft.get_label().compare(Glib::ustring()) == 0) {
		sql.append(Glib::ustring::compose(",FINISH='%1'",sft.get_end().get_time_t()));
	}
	sql.append(Glib::ustring::compose(",NOTE='%1'",get_sqlite_safe_string(sft.get_note())));
	sql.append(Glib::ustring::compose(",FLAGS='%1'",sft.get_flags()));
	sql.append(Glib::ustring::compose(",OBREAK='%1'",sft.get_other_break_min()));
	sql.append(Glib::ustring::compose(" where MEMBER ='%1' and START='%2';",get_sqlite_safe_string(id),dt.get_time_t()));
	return sql;
}

Glib::ustring Database::compute_update_shift_date(const Shift& sft, const CustomDate& start, const CustomDate& finish) {
	return Glib::ustring::compose("UPDATE SHIFT set START='%1', FINISH='%2' where MEMBER='%3' and START='%4';",start.get_time_t(),finish.get_time_t(),get_sqlite_safe_string(sft.get_member()),sft.get_start().get_time_t());
}

Glib::ustring Database::compute_delete_shift(const Glib::ustring& id, const CustomDate& dt) {
	CustomDate sod = CustomDate::start_of_day(dt);
	CustomDate eod = CustomDate::end_of_day(dt);
	return Glib::ustring::compose("DELETE from SHIFT where MEMBER ='%1' and START>='%2' and START<='%3';",get_sqlite_safe_string(id),sod.get_time_t(),eod.get_time_t());
}

/**
 * settings
 */

/*
 * member settings
 */

Glib::ustring Database::get_member_setting(const Glib::ustring& m_id, const Glib::ustring& entry) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from MEMBER_SETTINGS where ENTRY='%1' and MEMBER='%2';",get_sqlite_safe_string(entry),get_sqlite_safe_string(m_id));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_str_setting, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_member_setting() exec failed!" } };
	}
	if (strings.size() > 0) {
		return strings.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_member_setting() couldn't find member!" } };
	}
}

bool Database::check_member_setting(const Glib::ustring& m_id, const Glib::ustring& entry) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from MEMBER_SETTINGS where ENTRY='%1' and MEMBER='%2';",get_sqlite_safe_string(entry),get_sqlite_safe_string(m_id));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_str_setting, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_member_setting() exec failed!" } };
	}
	return strings.size() > 0;
}

void Database::insert(const mbr_settings& mbs) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO MEMBER_SETTINGS (ENTRY,MEMBER,VALUE) VALUES ('%1','%2','%3');",get_sqlite_safe_string(mbs.entry),get_sqlite_safe_string(mbs.mbr_id),get_sqlite_safe_string(mbs.value));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(member setting) exec failed!" } };
	}
}

void Database::update(const mbr_settings& mbs) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE MEMBER_SETTINGS set VALUE='%1' where ENTRY='%2' and MEMBER='%3';",get_sqlite_safe_string(mbs.value),get_sqlite_safe_string(mbs.entry),get_sqlite_safe_string(mbs.mbr_id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update(member setting) exec failed!" } };
	}
}

void Database::delete_member_settings(const Glib::ustring& m_id) {
	Glib::ustring sql = Glib::ustring::compose("delete from MEMBER_SETTINGS where MEMBER ='%1';",get_sqlite_safe_string(m_id));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_member_settings() exec failed!" } };
	}
}

/*
 * time preset settings
 */

std::vector<TimePreset> Database::get_time_presets() const {
	std::vector<TimePreset> time_presets;
	Glib::ustring sql = Glib::ustring("SELECT * from TIME_PRESET;");
	if (Database::exec(sqlt_db, sql, TimePreset::retrieve_time_preset, &time_presets) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_time_presets() exec failed!" } };
	}
	return time_presets;
}

void Database::insert(const TimePreset& tp) {
	std::pair<short,short> start { tp.get_start() };
	std::pair<short,short> finish { tp.get_finish() };
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO TIME_PRESET (START_H,START_M,FINISH_H,FINISH_M) VALUES ('%1','%2','%3','%4');",start.first,start.second,finish.first,finish.second);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(shift_time) exec failed!" } };
	}
}

void Database::update_start_h(int id, int val) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE TIME_PRESET set START_H='%1' where ID ='%2';",val,id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_start_h() exec failed!" } };
	}
}

void Database::update_start_m(int id, int val) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE TIME_PRESET set START_M='%1' where ID ='%2';",val,id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_start_m() exec failed!" } };
	}
}

void Database::update_finish_h(int id, int val) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE TIME_PRESET set FINISH_H='%1' where ID ='%2';",val,id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_finish_h() exec failed!" } };
	}
}

void Database::update_finish_m(int id, int val) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE TIME_PRESET set FINISH_M='%1' where ID ='%2';",val,id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_finish_m() exec failed!" } };
	}
}

void Database::delete_time_preset(int id) {
	Glib::ustring sql = Glib::ustring::compose("DELETE from TIME_PRESET where ID ='%1';",id);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_time_preset() exec failed!" } };
	}
}

/*
 * entry-value int settings
 */

int Database::get_setting(const Glib::ustring& entry) const {
	std::vector<int> integers;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from SETTINGS where ENTRY='%1';",get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_setting, &integers) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_setting() exec failed!" } };
	}
	if (integers.size() > 0) {
		return integers.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_setting() couldn't find setting!" } };
	}
}

bool Database::check_setting(const Glib::ustring& entry) const {
	std::vector<int> integers;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from SETTINGS where ENTRY='%1';",get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_setting, &integers) != SQLITE_OK) {
		throw DBError { std::string { "Database::check_setting() exec failed!" } };
	}
	return integers.size() > 0;
}

void Database::insert(const Glib::ustring& entry, int value) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO SETTINGS (ENTRY,VALUE) VALUES ('%1','%2');",get_sqlite_safe_string(entry),value);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(key-value) exec failed!" } };
	}
}

void Database::update(const Glib::ustring& entry, int value) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE SETTINGS set VALUE='%1' where ENTRY='%2';",value,get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update(key-value) exec failed!" } };
	}
}

/*
 * text line settings
 */

std::vector<TextLine> Database::get_text_lines(const Glib::ustring& type) const {
	std::vector<TextLine> lines;
	Glib::ustring sql = Glib::ustring::compose("select * from TEXTLINE where TYPE='%1' order by ROW asc;",get_sqlite_safe_string(type));
	if (Database::exec(sqlt_db, sql, TextLine::retrieve_text_line, &lines) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_text_line() exec failed!" } };
	}
	return lines;
}

void Database::update_text_line_string(const Glib::ustring& new_str, const Glib::ustring& type, const int row) {
	Glib::ustring sql = Glib::ustring::compose("update TEXTLINE set STRING='%1' where TYPE='%2' and ROW='%3';",get_sqlite_safe_string(new_str),get_sqlite_safe_string(type),row);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_text_line_string() exec failed!" } };
	}
}

void Database::update_text_line_fsize(double val, const Glib::ustring& type, const int row) {
	Glib::ustring sql = Glib::ustring::compose("update TEXTLINE set FSIZE='%1' where TYPE='%2' and ROW='%3';",val,get_sqlite_safe_string(type),row);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update_text_line_fsize() exec failed!" } };
	}
}

void Database::insert(const TextLine& txtl) {
	Glib::ustring sql = Glib::ustring::compose("insert into TEXTLINE (ROW,TYPE,STRING,FSIZE) values ('%1','%2','%3','%4');",txtl.get_row(),get_sqlite_safe_string(txtl.get_type()),get_sqlite_safe_string(txtl.get_text()),txtl.get_font_size());
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(TextLine) exec failed!" } };
	}
}

void Database::delete_text_line(const Glib::ustring& type, const int row) {
	Glib::ustring sql = Glib::ustring::compose("delete from TEXTLINE where TYPE ='%1' and ROW='%2';",get_sqlite_safe_string(type),row);
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::delete_text_line() exec failed!" } };
	}
}

/*
 * entry-value string settings
 */

Glib::ustring Database::get_str_setting(const Glib::ustring& entry) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from STR_SETTINGS where ENTRY='%1';",get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_str_setting, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::get_str_setting() exec failed!" } };
	}
	if (strings.size() > 0) {
		return strings.front();
	} else {
		throw DBEmptyResultException { std::string { "Database::get_str_setting() couldn't find setting!" } };
	}
}

bool Database::check_str_setting(const Glib::ustring& entry) const {
	std::vector<Glib::ustring> strings;
	Glib::ustring sql = Glib::ustring::compose("SELECT VALUE from STR_SETTINGS where ENTRY='%1';",get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, Settings::retrieve_str_setting, &strings) != SQLITE_OK) {
		throw DBError { std::string { "Database::check_str_setting() exec failed!" } };
	}
	return strings.size() > 0;
}

void Database::insert(const Glib::ustring& entry, const Glib::ustring& value) {
	Glib::ustring sql = Glib::ustring::compose("INSERT INTO STR_SETTINGS (ENTRY,VALUE) VALUES ('%1','%2');",get_sqlite_safe_string(entry),get_sqlite_safe_string(value));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::insert(key-value string) exec failed!" } };
	}
}

void Database::update(const Glib::ustring& entry, const Glib::ustring& value) {
	Glib::ustring sql = Glib::ustring::compose("UPDATE STR_SETTINGS set VALUE='%1' where ENTRY='%2';",get_sqlite_safe_string(value),get_sqlite_safe_string(entry));
	if (Database::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::update(key-value string) exec failed!" } };
	}
}

// colors

void Database::set_work_color(const Gdk::RGBA& col) {
	Duty::set_work_color(col);
	try {
		if (check_setting(WORK_CLR_RED)) {
			update(WORK_CLR_RED, col.get_red_u());
		} else {
			insert(WORK_CLR_RED, col.get_red_u());
		}
		if (check_setting(WORK_CLR_GREEN)) {
			update(WORK_CLR_GREEN, col.get_green_u());
		} else {
			insert(WORK_CLR_GREEN, col.get_green_u());
		}
		if (check_setting(WORK_CLR_BLUE)) {
			update(WORK_CLR_BLUE, col.get_blue_u());
		} else {
			insert(WORK_CLR_BLUE, col.get_blue_u());
		}
		if (check_setting(WORK_CLR_ALPHA)) {
			update(WORK_CLR_ALPHA, col.get_alpha_u());
		} else {
			insert(WORK_CLR_ALPHA, col.get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::set_work_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

Gdk::RGBA Database::get_work_color(void) const {
	Gdk::RGBA col;
	try {
		try {
			col.set_red_u(get_setting(WORK_CLR_RED));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_red_u(Gdk::RGBA("Black").get_red_u());
		}
		try {
			col.set_green_u(get_setting(WORK_CLR_GREEN));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_green_u(Gdk::RGBA("Black").get_green_u());
		}
		try {
			col.set_blue_u(get_setting(WORK_CLR_BLUE));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_blue_u(Gdk::RGBA("Black").get_blue_u());
		}
		try {
			col.set_alpha_u(get_setting(WORK_CLR_ALPHA));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_alpha_u(Gdk::RGBA("Black").get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::get_work_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	return col;
}

void Database::set_absent_color(const Gdk::RGBA& col) {
	Duty::set_absent_color(col);
	try {
		if (check_setting(ABSENT_CLR_RED)) {
			update(ABSENT_CLR_RED, col.get_red_u());
		} else {
			insert(ABSENT_CLR_RED, col.get_red_u());
		}
		if (check_setting(ABSENT_CLR_GREEN)) {
			update(ABSENT_CLR_GREEN, col.get_green_u());
		} else {
			insert(ABSENT_CLR_GREEN, col.get_green_u());
		}
		if (check_setting(ABSENT_CLR_BLUE)) {
			update(ABSENT_CLR_BLUE, col.get_blue_u());
		} else {
			insert(ABSENT_CLR_BLUE, col.get_blue_u());
		}
		if (check_setting(ABSENT_CLR_ALPHA)) {
			update(ABSENT_CLR_ALPHA, col.get_alpha_u());
		} else {
			insert(ABSENT_CLR_ALPHA, col.get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::set_absent_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

Gdk::RGBA Database::get_absent_color(void) const {
	Gdk::RGBA col;
	try {
		try {
			col.set_red_u(get_setting(ABSENT_CLR_RED));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_red_u(Gdk::RGBA("LightGreen").get_red_u());
		}
		try {
			col.set_green_u(get_setting(ABSENT_CLR_GREEN));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_green_u(Gdk::RGBA("LightGreen").get_green_u());
		}
		try {
			col.set_blue_u(get_setting(ABSENT_CLR_BLUE));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_blue_u(Gdk::RGBA("LightGreen").get_blue_u());
		}
		try {
			col.set_alpha_u(get_setting(ABSENT_CLR_ALPHA));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_alpha_u(Gdk::RGBA("LightGreen").get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::get_absent_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	return col;
}

void Database::set_license_color(const Gdk::RGBA& col) {
	Duty::set_license_color(col);
	try {
		if (check_setting(LICENSE_CLR_RED)) {
			update(LICENSE_CLR_RED, col.get_red_u());
		} else {
			insert(LICENSE_CLR_RED, col.get_red_u());
		}
		if (check_setting(LICENSE_CLR_GREEN)) {
			update(LICENSE_CLR_GREEN, col.get_green_u());
		} else {
			insert(LICENSE_CLR_GREEN, col.get_green_u());
		}
		if (check_setting(LICENSE_CLR_BLUE)) {
			update(LICENSE_CLR_BLUE, col.get_blue_u());
		} else {
			insert(LICENSE_CLR_BLUE, col.get_blue_u());
		}
		if (check_setting(LICENSE_CLR_ALPHA)) {
			update(LICENSE_CLR_ALPHA, col.get_alpha_u());
		} else {
			insert(LICENSE_CLR_ALPHA, col.get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::set_license_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

Gdk::RGBA Database::get_license_color(void) const {
	Gdk::RGBA col;
	try {
		try {
			col.set_red_u(get_setting(LICENSE_CLR_RED));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_red_u(Gdk::RGBA("Red").get_red_u());
		}
		try {
			col.set_green_u(get_setting(LICENSE_CLR_GREEN));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_green_u(Gdk::RGBA("Red").get_green_u());
		}
		try {
			col.set_blue_u(get_setting(LICENSE_CLR_BLUE));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_blue_u(Gdk::RGBA("Red").get_blue_u());
		}
		try {
			col.set_alpha_u(get_setting(LICENSE_CLR_ALPHA));
		} catch (const Database::DBEmptyResultException& dbe) {
			col.set_alpha_u(Gdk::RGBA("Red").get_alpha_u());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "Database::get_license_color() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	return col;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * database.hh
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DATABASE_HH_
#define _DATABASE_HH_

#include <iostream>
#include <set>
#include "db_engine.hh"
#include "shift.hh"
#include "service.hh"
#include "offwork.hh"
#include "text_line.hh"
#include "settings.hh"

#define WORK_CLR_RED			"WRK_CLR_RED"
#define WORK_CLR_GREEN			"WRK_CLR_GRN"
#define WORK_CLR_BLUE			"WRK_CLR_BLU"
#define WORK_CLR_ALPHA			"WRK_CLR_APH"
#define ABSENT_CLR_RED			"ABS_CLR_RED"
#define ABSENT_CLR_GREEN		"ABS_CLR_GRN"
#define ABSENT_CLR_BLUE			"ABS_CLR_BLU"
#define ABSENT_CLR_ALPHA		"ABS_CLR_APH"
#define LICENSE_CLR_RED			"LCN_CLR_RED"
#define LICENSE_CLR_GREEN		"LCN_CLR_GRN"
#define LICENSE_CLR_BLUE		"LCN_CLR_BLU"
#define LICENSE_CLR_ALPHA		"LCN_CLR_APH"
#define AUTOCOMP_WEEKDAY		"AUTC_WKD"
#define AUTOCOMP_SATURDAY 	    "AUTC_STD"
#define AUTOCOMP_SUNDAY			"AUTC_SND"
#define AUTOCOMP_HOLIDAY		"AUTC_HLD"

typedef struct {
	Glib::ustring entry;
	Glib::ustring mbr_id;
	Glib::ustring value;
} mbr_settings;

class Database : public DBEngine {
 public:
	Database(const Glib::ustring& db_path, int ver);
	inline ~Database() { sqlite3_close(sqlt_db); }
	static void create_database(sqlite3* sqlt_db);
	static void upgrade_database(sqlite3* sqlt_db);
	static void upgrade_rank(sqlite3* sqlt_db);
	static void upgrade_member(sqlite3* sqlt_db);
	static void upgrade_member_settings(sqlite3* sqlt_db);
	static void upgrade_label(sqlite3* sqlt_db);
	static void upgrade_shift(sqlite3* sqlt_db);
	static void upgrade_license(sqlite3* sqlt_db);
	static void upgrade_text_line(sqlite3* sqlt_db);
	/**
	 * rank
	 */
	std::vector<Rank> get_ranks(void) const;
	std::vector<Glib::ustring> get_rank_labels(void) const;
	Rank get_rank_with_full(const Glib::ustring& str) const;
	Rank get_rank_with_short(const Glib::ustring& str) const;
	void insert(const Rank& rnk);
	void update_rank_full(const Rank& rnk, const Glib::ustring& str);
	void update_rank_short(const Rank& rnk, const Glib::ustring& str);
	void swap_rank(const Rank& rnk1, const Rank& rnk2);
	void delete_rank(short id);
	/**
	 * member
	 */
	std::vector<Member> get_members(void) const;
	std::vector<Member> get_active_members(const CustomDate& ref) const;
	std::vector<Glib::ustring> get_members_id(void) const;
	Member get_member(const Glib::ustring& mbr_id) const;
	void insert(const Member& mbr);
	void update(const Member& mbr);
	void delete_member(const Glib::ustring& id);
	/**
	 * service
	 */
	std::vector<Service> get_services(const Glib::ustring& id) const;
	Service get_service(const Glib::ustring& id, const CustomDate& st) const;
	void update_service_start(const Service& srv, const CustomDate& st);
	void update_service_end(const Service& srv, const CustomDate& st);
	void update_service_end(const Service& srv);
	void insert(const Service& srv);
	void delete_services(const Glib::ustring& id);
	void delete_service(const Glib::ustring& id, const CustomDate& st);
	/**
	 * label
	 */
	std::vector<Label> get_labels(void) const;
	std::set<Label> get_labels_set(void) const;
	Label get_label(const Glib::ustring& lbl) const;
	Label get_short_label(const Glib::ustring& lbl) const;
	std::vector<Glib::ustring> get_labels_string() const;
	std::set<Glib::ustring> get_labels_string_set() const;
	void insert(const Label& lbl);
	void update_label(const Glib::ustring& oldlbl, const Glib::ustring& newlbl);
	void update_short_label(const Glib::ustring& oldlbl, const Glib::ustring& newlbl);
	void delete_label(const Glib::ustring& lbl);
	/**
	 * offwork
	 */
	std::vector<OffWork> get_offworks(void) const;
	OffWork get_offwork(const Glib::ustring& lbl) const;
	std::vector<Glib::ustring> get_offworks_label() const;
	std::vector<Glib::ustring> get_labels_str_with_absence(bool flag) const;
	std::vector<Glib::ustring> get_labels_str_with_weekday(bool flag) const;
	std::vector<Glib::ustring> get_labels_str_with_saturday(bool flag) const;
	std::vector<Glib::ustring> get_labels_str_with_sunday(bool flag) const;
	std::vector<Glib::ustring> get_labels_str_with_holiday(bool flag) const;
	void insert(const OffWork& ow);
	void update_offwork_label(const Glib::ustring& oldlbl, const Glib::ustring& newlbl);
	void update_offwork_flags(const Glib::ustring& lbl, std::uint8_t flgs);
	void delete_offwork(const Glib::ustring& lbl);
	/**
	 * license type
	 */
	std::vector<LicenseType> get_license_types(void) const;
	std::vector<Glib::ustring> get_license_types_label(void) const;
	std::vector<LicenseType> get_visible_license_types(void) const;
	LicenseType get_license_type(const Glib::ustring& label) const;
	void insert(const LicenseType& lt);
	void update_license_type_flags(const Glib::ustring& lbl, std::uint8_t flgs);
	void delete_license_type(const Glib::ustring& lbl);
	/**
	 * licenses
	 */
	std::vector<License> get_licenses(void) const;
	std::vector<License> get_licenses_for_member(const Glib::ustring& str) const;
	std::vector<License> get_licenses_with_label(const Glib::ustring& str) const;
	std::vector<License> get_valid_licenses_for_member(const Glib::ustring& id, const CustomDate& cd) const;
	std::vector<License> get_visible_valid_licenses_for_member(const Glib::ustring& id, const CustomDate& cd) const;
	std::vector<License> get_visible_valid_licenses_for_member_with_label(const Glib::ustring& id, const CustomDate& cd, const Glib::ustring& lbl) const;
	License get_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year) const;
	void insert(const License& lcn);
	void update_license(const License& lic, const Glib::ustring& member, const Glib::ustring& label, int ref_year);
	void update_all_license_labels(const Glib::ustring& old_lbl, const Glib::ustring& new_lbl);
	void delete_licenses(const Glib::ustring& id);
	void delete_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year);
	/**
	 * shift
	 */
	Shift get_shift(const Glib::ustring& id, const CustomDate& dt) const;
	bool has_shifts(const Glib::ustring& id, const CustomDate& dt, int days = 0) const;
	std::vector<Shift> get_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const;
	std::vector<Shift> get_non_working_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const;
	std::vector<Shift> get_shifts_with_offwork(const Glib::ustring& lbl) const;
	std::vector<Shift> get_working_shifts(const CustomDate& dt) const;
	std::vector<Shift> get_working_shifts_for_member(const Glib::ustring& str, const CustomDate& dts, const CustomDate& dte) const;
	std::vector<Shift> get_license_shifts_for_member(const Glib::ustring& id, const Glib::ustring& label, int ref_year, const CustomDate& ep) const;
	std::vector<Shift> get_license_shifts_for_member(const Glib::ustring& id, const Glib::ustring& label, int ref_year) const;
	void insert(const Shift& sft);
	void update(const Shift& sft, const Glib::ustring& id, const CustomDate& dt);
	void update_shifts_labels(const Glib::ustring& old_lbl, const Glib::ustring& new_lbl);
	void update_shift_date(const Shift& sft, const CustomDate& start, const CustomDate& finish);
	void delete_shifts(const Glib::ustring& id);
	void delete_shift(const Glib::ustring& id, const CustomDate& dt);
	void delete_shifts_within(const CustomDate& start, const CustomDate& end);
	/**
	 * get string queries
	 */
	static Glib::ustring compute_insert(const Shift& sft);
	static Glib::ustring compute_update(const Shift& sft, const Glib::ustring& id, const CustomDate& dt);
	static Glib::ustring compute_update_shift_date(const Shift& sft, const CustomDate& start, const CustomDate& finish);
	static Glib::ustring compute_delete_shift(const Glib::ustring& id, const CustomDate& dt);
	/**
	 * settings
	 */
	// member settings
	Glib::ustring get_member_setting(const Glib::ustring& m_id, const Glib::ustring& entry) const;
	bool check_member_setting(const Glib::ustring& m_id, const Glib::ustring& entry) const;
	void insert(const mbr_settings& mbs);
	void update(const mbr_settings& mbs);
	void delete_member_settings(const Glib::ustring& m_id);
	// time preset
	std::vector<TimePreset> get_time_presets() const;
	void insert(const TimePreset& tp);
	void update_start_h(int id, int val);
	void update_start_m(int id, int val);
	void update_finish_h(int id, int val);
	void update_finish_m(int id, int val);
	void delete_time_preset(int id);
	// entry-value int settings
	int get_setting(const Glib::ustring& entry) const;
	bool check_setting(const Glib::ustring& entry) const;
	void insert(const Glib::ustring& entry, int value);
	void update(const Glib::ustring& entry, int value);
	// text line settings
	std::vector<TextLine> get_text_lines(const Glib::ustring& type) const;
	void update_text_line_string(const Glib::ustring& new_str, const Glib::ustring& type, const int row);
	void update_text_line_fsize(double val, const Glib::ustring& type, const int row);
	void insert(const TextLine& txtl);
	void delete_text_line(const Glib::ustring& type, const int row);
	// entry-value string settings
	Glib::ustring get_str_setting(const Glib::ustring& entry) const;
	bool check_str_setting(const Glib::ustring& entry) const;
	void insert(const Glib::ustring& entry, const Glib::ustring& value);
	void update(const Glib::ustring& entry, const Glib::ustring& value);
	// colors
	void set_work_color(const Gdk::RGBA& col);
	Gdk::RGBA get_work_color(void) const;
	void set_absent_color(const Gdk::RGBA& col);
	Gdk::RGBA get_absent_color(void) const;
	void set_license_color(const Gdk::RGBA& col);
	Gdk::RGBA get_license_color(void) const;

	static int db_version;
};

#endif // _DATABASE_HH_

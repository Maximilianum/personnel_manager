/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * db_engine.cc
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "db_engine.hh"
#include <iostream>
#include <sstream>

int DBEngine::retrieve_data(void *data, int argc, char **argv, char **azColName) {
	int* value = static_cast<int*>(data);
	*value = atoi(*argv);
	return 0;
}

/*
 * member functions
 */

void DBEngine::execute(const std::string& sql) {
	if (DBEngine::exec(sqlt_db, sql, NULL, NULL) != SQLITE_OK) {
		throw DBError { std::string { "Database::exec failed! " } + sql };
	}
}

int DBEngine::count_rows(const std::string& name) {
	int result;
	std::stringstream ss;
	ss << "SELECT COUNT(*) FROM " << name << ";";
	std::string sql { ss.str() };
	if (DBEngine::exec(sqlt_db, sql, retrieve_data, &result) != SQLITE_OK) {
		throw DBError { std::string { "DBEngine::count_rows() exec failed" } };
	}
	return result;
}

/*
 * static functions
 */

std::string DBEngine::get_sqlite_safe_string(const std::string& str) {
	std::string safe = str;
	std::string::size_type pos = 0;
	std::string::size_type end = safe.size();
	try {
		while (pos < end) {
			std::string::value_type crt = safe[pos];
			switch ( crt ) {
			case '&':
				safe.replace(pos, 1, std::string("&&"));
				pos++;
				end = safe.size();
				break;
			case '\'':
				safe.replace(pos, 1, std::string("''"));
				pos++;
				end = safe.size();
				break;
			}
			pos++;
		}
	} catch(const std::exception& ex) {
		std::cerr << "standard exception: " << ex.what() << std::endl;
		std::cerr << "get_sqlite_safe_string:: " << str << std::endl;
	}
	return safe;
}

int DBEngine::exec(sqlite3* db, const std::string& sql, int (*callback)(void*,int,char**,char**), void* data) {
	char *zErrMsg = 0;
	int rc = sqlite3_exec(db, sql.c_str(), callback, data, &zErrMsg);
	if (rc != SQLITE_OK) {
		std::cerr << sql << " SQLite error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
	}
	return rc;
}

int DBEngine::get_version(sqlite3* db) {
	char *zErrMsg = 0;
	sqlite3_stmt* stmt_version;
	int db_version;
	if(sqlite3_prepare_v2(db, "PRAGMA user_version;", -1, &stmt_version, NULL) == SQLITE_OK) {
		while(sqlite3_step(stmt_version) == SQLITE_ROW) {
			db_version = sqlite3_column_int(stmt_version, 0);
		}
	} else {
		std::cerr << "ERROR Preparing: , " <<  sqlite3_errmsg(db) << std::endl;
	}
	sqlite3_finalize(stmt_version);
	return db_version;
}

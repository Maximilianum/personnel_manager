/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * db_engine.hh
 * Copyright (C) 2021 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DBENGINE_HH_
#define _DBENGINE_HH_

#include <string>
#include <sqlite3.h>

enum class DATABASE_ORIGIN { created, opened };

class DBEngine {
 public:
	/*
	 * exception class for error handling
	 */
	class DBError
	{
	public:
    DBError(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
  
	class DBEmptyResultException
	{
	public:
    DBEmptyResultException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};

	static int retrieve_data(void *data, int argc, char **argv, char **azColName);
	virtual inline ~DBEngine() { sqlite3_close(sqlt_db); }
	inline DATABASE_ORIGIN get_origin(void) const { return origin; };
	void execute(const std::string& sql);
	int count_rows(const std::string& name);
	static std::string get_sqlite_safe_string(const std::string& str);
	static int exec(sqlite3* db, const std::string& sql, int (*callback)(void*,int,char**,char**), void* data);
 protected:
	static int get_version(sqlite3* db);
	DATABASE_ORIGIN origin;
	sqlite3* sqlt_db;
};

#endif // _DBENGINE_HH_

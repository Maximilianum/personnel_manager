/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_document.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing_document.hh"

void DrawingDocument::parse_string(const Glib::ustring& str, std::vector<Glib::ustring>* vector, const Glib::ustring& separator) {
	Glib::ustring::size_type start { 0 };
	Glib::ustring::size_type pos = { 0 };
	do {
		pos = str.find(separator, start);
		vector->push_back(str.substr(start, pos));
		if (pos != Glib::ustring::npos) {
			start = pos + 1;
		}
	} while(pos != Glib::ustring::npos);
}

DrawingDocument::DrawingDocument(Database* db, const time_t ref_date) : DrawingArea(), database (db), page_width (580.0), page_height (820.0), font_size (11.0), left_margin (10.0), right_margin (10.0), top_margin (10.0), bottom_margin (10.0), svg_handle (nullptr) {
	set_has_window(true);
	calendata.set_reference_date(ref_date);
}

DrawingDocument::~DrawingDocument() {
	if (svg_handle) {
		g_object_unref(svg_handle);
	}
}

void DrawingDocument::init_font_and_colour(const Cairo::RefPtr<Cairo::Context>& cr) {
	cr->select_font_face("LiberationSerif", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
	cr->set_font_size(font_size);
	cr->set_source_rgb(0.0, 0.0, 0.0);
}

void DrawingDocument::on_size_allocate(Gtk::Allocation& allocation) {
	set_allocation(allocation);
	if(m_ref_window) {
		m_ref_window->move_resize(allocation.get_x(), allocation.get_y(), allocation.get_width(), allocation.get_height());
	}
}

void DrawingDocument::on_realize(void) {
	set_realized();
	if(!m_ref_window) {
		GdkWindowAttr attributes;
		memset(&attributes, 0, sizeof(attributes));
		Gtk::Allocation allocation = get_allocation();
		attributes.x = allocation.get_x();
		attributes.y = allocation.get_y();
		attributes.width = allocation.get_width();
		attributes.height = allocation.get_height();
		attributes.event_mask = get_events () | Gdk::EXPOSURE_MASK;
		attributes.window_type = GDK_WINDOW_CHILD;
		attributes.wclass = GDK_INPUT_OUTPUT;
		m_ref_window = Gdk::Window::create(get_parent_window(), &attributes, GDK_WA_X | GDK_WA_Y);
		set_window(m_ref_window);
		override_background_color(Gdk::RGBA("white"));
		override_color(Gdk::RGBA("black"));
		m_ref_window->set_user_data(gobj());
	}
}

bool DrawingDocument::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
	int pages = count_pages();
	cr->set_source_rgb(1.0, 1.0, 1.0);
	cr->rectangle(0.0, 0.0, page_width, page_height);
	cr->fill();
	init_font_and_colour(cr);
	for (int nrpg = 0; nrpg < pages; nrpg++) {
		draw_at_page(cr, nrpg, true);
	}
	return true;
}

void DrawingDocument::set_svg_logo(const Glib::ustring& file_path) {
	if (svg_handle) {
		g_object_unref(svg_handle);
	}
	GError **error = nullptr;
	svg_handle = rsvg_handle_new_from_file(file_path.c_str(), error);
	if (svg_handle) {
		rsvg_handle_set_dpi(svg_handle, 75.0);
	}
}

double DrawingDocument::draw_svg_logo(const Cairo::RefPtr<Cairo::Context>& cr, ORIENTATION page) {
	if (svg_handle) {
		// save current transformation matrix
		Cairo::Matrix current = cr->get_matrix();
		// get svg image dimensions
		gdouble svg_height, svg_width, alpha;
		rsvg_handle_get_intrinsic_size_in_pixels(svg_handle, &svg_width, &svg_height);
		RsvgRectangle viewport = {
			.x = 0.0,
			.y = 0.0,
			.width = svg_width,
			.height = svg_height
		};
		// apply matrix transformation to scale and put in position the image
		switch (page) {
		case ORIENTATION::horizontal:
			cr->translate(-(svg_height * 0.125) / 5.0, (page_height + (svg_width * 0.125)) / 2.0);
			alpha = 4.0;
			break;
		case ORIENTATION::vertical:
			cr->translate((page_width - (svg_width * 0.125)) / 2.0, -(svg_height * 0.125) / 5.0);
			alpha = 3.0;
			break;
		}
		cr->scale(0.125, 0.125);
		if (page == ORIENTATION::horizontal) {
			cr->rotate_degrees(-90.0);
		}
		// draw svg image
		rsvg_handle_render_document(svg_handle, cr->cobj(), &viewport, nullptr);
		// restore current transformation matrix to default values
		cr->set_matrix(current);
		return (svg_height * 0.125) / alpha;
	}
	return 0.0;
}

void DrawingDocument::rotate_and_draw_text(const Cairo::RefPtr<Cairo::Context>& cr, const Glib::ustring& str, double degree) {
	if (degree != 0.0 && str.compare(Glib::ustring()) != 0) {
		// save current transformation matrix
		Cairo::Matrix current = cr->get_matrix();
		// rotate current transformation matrix by 90 degrees
		cr->rotate_degrees(degree);
		// print given text at current position rotated by 90 degrees 
		cr->show_text(str.raw());
		// restore current transformation matrix to default values
		cr->set_matrix(current);
	} else {
		cr->show_text(str.raw());
	}
}

double DrawingDocument::draw_title(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double k, ORIENTATION page) {	
	// iter through title's strings
	for (auto iter_title = title.begin(), end_title = title.end(); iter_title != end_title; iter_title++) {
		cr->set_font_size(iter_title->second);
		// create a vector of strings and parse string to separate different lines of text
		std::vector<Glib::ustring> strings;
		parse_string(iter_title->first, &strings, Glib::ustring("\n"));
		Cairo::TextExtents te;
		double mx, my, alpha, degree;
		switch (page) {
		case ORIENTATION::horizontal:
			alpha = 1.1;
			degree = -90.0;
			break;
		case ORIENTATION::vertical:
			alpha = 0.8;
			degree = 0.0;
			break;
		}
		for (auto iter = strings.cbegin(); iter != strings.cend(); iter++) {
			cr->get_text_extents(iter->raw(), te);
			k += te.height * alpha;
			switch (page) {
			case ORIENTATION::horizontal:
				mx = k;
				my = ((page_height + te.width) / 2.0) + dy;
				break;
			case ORIENTATION::vertical:
				mx = ((page_width - te.width) / 2.0);
				my = k + dy;
			}
			cr->move_to(mx, my);
			rotate_and_draw_text(cr, *iter, degree);
		}
		k += (iter_title->second * 0.3);
	}
	return k;
}

double DrawingDocument::draw_sign(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double k, ORIENTATION page, int mpg) {
	// iter through sign's strings
	for (std::vector<std::pair<Glib::ustring, double> >::iterator iter_sign = sign.begin(), end_sign = sign.end(); iter_sign != end_sign; iter_sign++) {
		cr->set_font_size(iter_sign->second);
		// create a vector of strings and parse string to separate different lines of text
		std::vector<Glib::ustring> strings;
		parse_string(iter_sign->first, &strings, Glib::ustring("\n"));
		Cairo::TextExtents te;
		double mx, my, degree;
		// iterate through lines of text
		std::vector<Glib::ustring>::iterator iter = strings.begin();
		std::vector<Glib::ustring>::iterator end = strings.end();    
		while (iter != end)	{
			cr->get_text_extents(iter->raw(), te);
			k += te.height * 1.1;
			switch (page) {
			case ORIENTATION::horizontal:
				mx = k + ((c_width * 1.2) * mpg);
				my = ((page_height + te.width) / 3.0) + dy;
				degree = -90.0;
				break;
			case ORIENTATION::vertical:
				mx = (page_width - te.width) / 1.3;
				my = k + (c_height * 7.0) + dy;
				degree = 0.0;
			}
			cr->move_to(mx, my);
			rotate_and_draw_text(cr, *iter, degree);
			iter++;
		}
		k += (iter_sign->second * 0.3);
	}
	return k;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_document.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWING_DOCUMENT_HH_
#define _DRAWING_DOCUMENT_HH_

#include <gtkmm/drawingarea.h>
#include <librsvg-2.0/librsvg/rsvg.h>
#include "calendar_data.hh"

class DrawingDocument : public Gtk::DrawingArea {
 protected:
	enum class ORIENTATION { vertical, horizontal };
 public:
	static void parse_string(const Glib::ustring& str, std::vector<Glib::ustring>* vector, const Glib::ustring& separator);
	DrawingDocument(Database* db, const time_t ref_date);
	~DrawingDocument();
	inline void set_cell_width(double val) { c_width = val; }
	inline void set_cell_height(double val) { c_height = val; }
	inline void set_font_size(double val) { font_size = val; }
	void init_font_and_colour(const Cairo::RefPtr<Cairo::Context>& cr);
	inline void get_preferred_width_vfunc(int& minimum_width, int& natural_width) const { minimum_width = 500; natural_width = 580; }
	inline void get_preferred_height_vfunc(int& minimum_height, int& natural_height) const { minimum_height = 720; natural_height = 800; }
	inline void get_preferred_width_for_height_vfunc(int height, int& minimum_width, int& natural_width) const { minimum_width = 500; natural_width = 580; }
	inline void get_preferred_height_for_width_vfunc(int width, int& minimum_height, int& natural_height) const { minimum_height = 720; natural_height = 800; }
	virtual int count_pages() const = 0;
	void on_size_allocate(Gtk::Allocation& allocation);
	void on_realize(void);
	bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
	virtual void draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages = false) = 0;
	void set_svg_logo(const Glib::ustring& file_path);
	double draw_svg_logo(const Cairo::RefPtr<Cairo::Context>& cr, ORIENTATION page);
	void rotate_and_draw_text(const Cairo::RefPtr<Cairo::Context>& cr, const Glib::ustring& str, double degree);
	void add_title(const Glib::ustring& str, double val) { title.push_back(std::pair<Glib::ustring, double>(str, val)); }
	double draw_title(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double k, ORIENTATION page);
	inline void add_sign(const Glib::ustring& str, double val) { sign.push_back(std::pair<Glib::ustring, double>(str, val)); }
	double draw_sign(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double k, ORIENTATION page, int mpg = 7);
 protected:
	Glib::RefPtr<Gdk::Window> m_ref_window;
	Database* database;
	CalendarData calendata;
	double page_width;
	double page_height;
	double c_width;
	double c_height;
	double font_size;
	double left_margin;
	double right_margin;
	double top_margin;
	double bottom_margin;
	RsvgHandle* svg_handle;
	std::vector<std::pair<Glib::ustring, double> > title;
	std::vector<std::pair<Glib::ustring, double> > sign;
};

#endif // _DRAWING_DOCUMENT_HH_

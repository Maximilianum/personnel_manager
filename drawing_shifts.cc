/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_shifts.cc
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing_shifts.hh"

DrawingShifts::DrawingShifts(Database* db, const time_t ref_date) : DrawingDocument(db, ref_date), blank (false), members_per_page (7), extra_columns (3) {
	init_members_info();
	set_cell_width(40.0);
	set_cell_height(40.0);
}

unsigned long int DrawingShifts::get_number_of_rows_needed(int page) {
	// calculate how many rows must be drawn for this page
	unsigned long int member_rows { members_info.size() - (page * members_per_page) };
	if (member_rows > members_per_page) {
		member_rows = members_per_page;
	}
	return member_rows;
}

void DrawingShifts::draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages) {
	double dy { pages ? page_height * page : 0.0 };
	double x { left_margin };
	cr->set_line_width(1.5);
	// draw logo
	x += draw_svg_logo(cr, ORIENTATION::horizontal);
	// draw title
	x = draw_title(cr, dy, x, ORIENTATION::horizontal);
	unsigned long int member_rows { get_number_of_rows_needed(page) };
	// prepare font size and line width
	cr->set_font_size(font_size);
	cr->set_line_width(0.8);
	double x_grid { x + (c_width / 3.4) };
	double y_grid { page_height - bottom_margin - (c_height / 5.0) + dy };
	// draw ranks and names
	draw_ranks_and_names(cr, &x_grid, &y_grid, dy, &x, page);
	// print days number and weekdays
	draw_shifts(cr, &x_grid, &y_grid, dy, &x, page);
	// draw first row for days and weekdays
	cr->move_to(x, top_margin + dy);
	cr->line_to(x, page_height - bottom_margin + dy);
	cr->line_to(x + (c_width / 1.2), page_height - bottom_margin + dy);
	cr->line_to(x + (c_width / 1.2), top_margin + dy);
	cr->line_to(x, top_margin + dy);
	cr->move_to(x + (c_width / 2.4), top_margin + dy);
	cr->line_to(x + (c_width / 2.4), page_height - bottom_margin + dy);
	cr->stroke();
	// draw rows for members
	x += c_width;
	for(int n = 0; n < member_rows; n++) {
		cr->move_to(x + ((c_width * 1.2) * n), top_margin + dy);
		cr->line_to(x + ((c_width * 1.2) * n), page_height - bottom_margin + dy);
		cr->line_to(x + c_width + ((c_width * 1.2) * n), page_height - bottom_margin + dy);
		cr->line_to(x + c_width + ((c_width * 1.2) * n), top_margin + dy);
		cr->line_to(x + ((c_width * 1.2) * n), top_margin + dy);
	}
	cr->stroke();
	// draw sign
	x = draw_sign(cr, dy, x, ORIENTATION::horizontal);
}

void DrawingShifts::init_members_info(void) {
	members_info.clear();
	// create a vector filled with members info (rank, name and surname)
	try {
		std::vector<Member> members { database->get_active_members(CustomDate::get_first_day_month(calendata.get_reference_date())) };
		for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
			Member_info info;
			info.id = iter->get_id();
			info.rank = iter->get_rank().get_short_label();
			// create a vector of strings and parse string to handle multiple names
			std::vector<Glib::ustring> strings;
			DrawingShifts::parse_string(iter->get_first_name(), &strings, Glib::ustring(" "));
			info.name = strings[0];
			if (strings.size() > 1) {
				info.name.append(Glib::ustring(" ") + strings[1].substr(0, 1) + Glib::ustring("."));
			}
			info.surname = iter->get_last_name();
			members_info.push_back(info);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "DrawingShifts::init_members_info " << dbe.get_message() << std::endl;
	}
}

void DrawingShifts::draw_ranks_and_names(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page) {
	unsigned long int member_rows { get_number_of_rows_needed(page) };
	unsigned long int row_nr { 0 };
	std::vector<Member_info>::iterator iter = members_info.begin();
	std::vector<Member_info>::iterator end = members_info.end();
	// advance to the first member to print for current page
	std::advance(iter, page * members_per_page);
	// prepare column title
	Glib::ustring month_title = calendata.get_reference_cdate().get_formatted_string(DATE_FORMAT::monthyear);
	Cairo::TextExtents name_text_ext;
	Glib::ustring longest_name = month_title + Glib::ustring(".");
	cr->get_text_extents(longest_name, name_text_ext);
	// get the longest name
	while (iter != end && row_nr < member_rows)	{		
		Cairo::TextExtents test_text_ext;
		cr->get_text_extents(iter->name, test_text_ext);
		if (test_text_ext.width > name_text_ext.width) {
			name_text_ext = test_text_ext;
			longest_name.assign(iter->name);
		}
		cr->get_text_extents(iter->surname, test_text_ext);
		if (test_text_ext.width > name_text_ext.width) {
			name_text_ext = test_text_ext;
			longest_name.assign(iter->surname);
		}		
		iter++;
		row_nr++;
	}
	row_nr = 0;
	std::vector<Member_info>::iterator it { members_info.begin() };
	std::advance(it, page * members_per_page);
	std::vector<Member_info>::iterator en { members_info.end() };
	while (it != en && row_nr <= member_rows) {
		if (row_nr == 0) {
			// draw month and year
			cr->move_to(*x_grid, *y_grid);
			rotate_and_draw_text(cr, month_title, -90.0);
			// draw a line
			cr->move_to(*x, *y_grid - name_text_ext.width);
			cr->line_to(*x + (c_width / 1.2), *y_grid - name_text_ext.width);
			cr->stroke();
			row_nr++;
		}
		if (row_nr > 0)	{
			// print rank and names
			cr->move_to(*x + (c_width / 10.0) + ((c_width * 1.2) * row_nr), *y_grid);					
			rotate_and_draw_text(cr, it->rank, -90.0);
			cr->move_to(*x + (c_width / 10.0) + (font_size * 1.1) + ((c_width * 1.2) * row_nr), *y_grid);
			rotate_and_draw_text(cr, it->name, -90.0);
			cr->move_to(*x + (c_width / 10.0) + (font_size * 2.2) + ((c_width * 1.2) * row_nr), *y_grid);
			rotate_and_draw_text(cr, it->surname, -90.0);
			// draw a line
			cr->move_to(*x - (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - name_text_ext.width);
			cr->line_to(*x - (c_width / 5.0) + c_width + ((c_width * 1.2) * row_nr), *y_grid - name_text_ext.width);
			cr->stroke();
			row_nr++;
		}
		it++;
	}
	*y_grid -= name_text_ext.width;
}

void DrawingShifts::draw_shifts_column(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page, const CustomDate& cd_day) {
	unsigned long int member_rows { get_number_of_rows_needed(page) };
	unsigned long int row_nr { 0 };	
	std::vector<Member_info>::iterator iter { members_info.begin() };
	std::vector<Member_info>::iterator end { members_info.end() };
	// advance to the first member to print for current page
	std::advance(iter, page * members_per_page);
	Cairo::TextExtents shifts_text_ext;
	Glib::ustring longest_shift;
	Glib::ustring day, weekday;
	// prepare column title
	day = Glib::ustring::format(std::setfill(L'0'), std::setw(2), cd_day.get_day());
	weekday = cd_day.get_weekday_string(false);
	cr->get_text_extents(day, shifts_text_ext);
	longest_shift = day;
	// create a vector filled with shifts strings
	std::vector<Shift_strings> shifts_vec;
	while (iter != end && row_nr < member_rows)	{
		Shift_strings shift;
		shift.id = iter->id;		
		// create a vector of strings and parse shifts string to separate start/end time
		std::vector<Glib::ustring> strings;
		if (blank) {
			parse_string(Glib::ustring("\n"), &strings, Glib::ustring("\n"));
		} else {
			try {
				parse_string(calendata.get_shift_string(database, cd_day, shift.id), &strings, Glib::ustring("\n"));
			} catch (const Database::DBError& dbe) {
				std::cerr << "DrawingShifts::draw_shifts_column() " << dbe.get_message() << std::endl;
				throw dbe;
			}
		}
		shift.up = strings.size() > 0 ? strings[0] : Glib::ustring("");
		shift.down = strings.size() > 1 ? strings[1] : Glib::ustring("");
		// get the longest shift string size
		Cairo::TextExtents test_text_ext;
		cr->get_text_extents(shift.up, test_text_ext);
		if (test_text_ext.width > shifts_text_ext.width) {
			shifts_text_ext = test_text_ext;
			longest_shift.assign(shift.up);
		}
		cr->get_text_extents(shift.down, test_text_ext);
		if (test_text_ext.width > shifts_text_ext.width) {
			shifts_text_ext = test_text_ext;
			longest_shift.assign(shift.down);
		}
		shifts_vec.push_back(shift);
		row_nr++;
		iter++;
	}
	row_nr = 0;
	// calculate space for shifts
	Cairo::TextExtents cell_text_ext;
	cr->get_text_extents(longest_shift + Glib::ustring("_"), cell_text_ext);
	// set a limit to the cell width
	if (cell_text_ext.width > 19) {
		cell_text_ext.width = 19;
	}
	for (auto it = shifts_vec.cbegin(); it != shifts_vec.cend(); it++) {
		if (row_nr == 0) {
			// draw background color
			Gdk::RGBA color = calendata.get_background_color(cd_day);
			if (color != Gdk::RGBA("white")) {
				cr->set_source_rgba(color.get_red(), color.get_green(), color.get_blue(), color.get_alpha());
				cr->move_to(*x, *y_grid + dy);
				cr->line_to(*x, *y_grid - cell_text_ext.width + dy);
				cr->line_to(*x + (c_width / 1.2), *y_grid - cell_text_ext.width + dy);
				cr->line_to(*x + (c_width / 1.2), *y_grid + dy);
				cr->close_path();
				cr->fill();
			}
			// set color to white if background color is red
			if (color == Gdk::RGBA("red")) {
				cr->set_source_rgb(1.0, 1.0, 1.0);
			} else {
				cr->set_source_rgb(0.0, 0.0, 0.0);
			}			
			// print day number and weekday
			Cairo::TextExtents this_text_ext;
			cr->get_text_extents(day, this_text_ext);
			cr->move_to(*x_grid, *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, day, -90.0);
			cr->get_text_extents(weekday, this_text_ext);
			cr->move_to(*x_grid + (c_width / 2.4), *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, weekday, -90.0);
			cr->set_source_rgb(0.0, 0.0, 0.0);
			// draw a line
			cr->move_to(*x, *y_grid - cell_text_ext.width + dy);
			cr->line_to(*x + (c_width / 1.2), *y_grid - cell_text_ext.width + dy);
			cr->stroke();
			row_nr++;
		}
		// print shifts
		if (row_nr > 0)	{
			// draw background color
			Gdk::RGBA back_color = calendata.get_background_color(cd_day);
			if (back_color != Gdk::RGBA("white")) {
				cr->set_source_rgba(back_color.get_red(), back_color.get_green(), back_color.get_blue(), back_color.get_alpha());
				cr->move_to(*x - (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid + dy);
				cr->line_to(*x - (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
				cr->line_to(*x - (c_width / 5.0) + c_width + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
				cr->line_to(*x - (c_width / 5.0) + c_width + ((c_width * 1.2) * row_nr), *y_grid + dy);
				cr->close_path();
				cr->fill();
			}
			// set foreground color
			Gdk::RGBA fore_color = calendata.get_foreground_color(database, cd_day, it->id);
			cr->set_source_rgba(fore_color.get_red(), fore_color.get_green(), fore_color.get_blue(), fore_color.get_alpha());
			// set FontWeight
			bool font_bold = calendata.is_font_weight_bold(database, cd_day, it->id);
			if (font_bold) {
				cr->select_font_face("LiberationSerif", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_BOLD);
			}
			// draw shifts
			Cairo::TextExtents this_text_ext;
			cr->get_text_extents(it->up, this_text_ext);
			cr->move_to(*x + (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, it->up, -90.0);
			cr->get_text_extents(it->down, this_text_ext);
			cr->move_to(*x + (c_width / 5.0) + (font_size * 1.3) + ((c_width * 1.2) * row_nr), *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, it->down, -90.0);
			// set FontWeight back to normal
			if (font_bold) {
				cr->select_font_face("LiberationSerif", Cairo::FONT_SLANT_NORMAL, Cairo::FONT_WEIGHT_NORMAL);
			}
			// set foreground color to black
			cr->set_source_rgba(0.0, 0.0, 0.0, 1.0);
			// draw a line
			cr->move_to(*x - (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
			cr->line_to(*x - (c_width / 5.0) + c_width + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
			cr->stroke();
			row_nr++;
		}
	}
	*y_grid -= cell_text_ext.width;
}

void DrawingShifts::draw_extra_column(const Cairo::RefPtr<Cairo::Context>& cr, short type, double* x_grid, double* y_grid, const double dy, double* x, int page) {
	unsigned long int member_rows { get_number_of_rows_needed(page) };
	unsigned long int row_nr { 0 };
	std::vector<Member_info>::iterator m_iter { members_info.begin() };
	std::vector<Member_info>::iterator m_end { members_info.end() };
	// advance to the first member to print for current page
	std::advance(m_iter, page * members_per_page);
	std::vector<LicenseType> licenses { database->get_visible_license_types() };
	auto lt_it { licenses.begin() };
	auto lt_en { licenses.end() };
	if (type > 3) {
		std::advance(lt_it, type - 4);
	}	
	// prepare column title
	Glib::ustring title_up;
	Glib::ustring title_down;
	switch (type) {
	case 1:
		title_up = Glib::ustring("ore");
		title_down = Glib::ustring("eff.");
		break;
	case 2:
		title_up = Glib::ustring("ore");
		title_down = Glib::ustring("obb.");
		break;
	case 3:
		title_up = Glib::ustring("ore");
		title_down = Glib::ustring("str.");
		break;
	default:
		if (lt_it != lt_en)	{
			Glib::ustring lbl = lt_it->get_short_label();
			if (lbl.size() > 2)	{
				title_up.assign(lbl.substr(0, 2));
				title_down.assign(lbl.substr(2, std::string::npos));
			} else {
				title_up.assign(lbl);
			}
		}
		break;
	}
	Cairo::TextExtents extra_text_ext;
	cr->get_text_extents(title_down, extra_text_ext);
	Glib::ustring longest_string = title_down;
	// create a vector filled with value strings
	std::vector<Shift_strings> shifts_vec;
	while (m_iter != m_end && row_nr < member_rows)	{
		Shift_strings shift;
		shift.id = m_iter->id;
		try {
			CustomDate cd_day = calendata.get_reference_cdate();
			CustomDate firstday = CustomDate::get_first_day_month(cd_day.get_time_t());
			CustomDate lastday = CustomDate::get_last_day_month(cd_day.get_time_t());
			switch (type) {
			case 1:	// worked minutes
				if (!blank) {
					shift.up = Glib::ustring::format(calendata.get_worked_minutes(database, firstday, lastday, shift.id) / 60);
				}
				break;
			case 2:	// should work minutes
				shift.up = Glib::ustring::format(calendata.get_should_work_minutes(database, firstday, lastday, shift.id) / 60);
				break;
			case 3:	// difference between worked minutes and should work minutes
				if (!blank) {
					shift.up = Glib::ustring::format((calendata.get_worked_minutes(database, firstday, lastday, shift.id) - calendata.get_should_work_minutes(database, firstday, lastday, shift.id)) / 60);
				}
				break;
			default:
				if (lt_it != lt_en)	{
					int days_available = 0;
					std::vector<License> v_licenses { database->get_visible_valid_licenses_for_member_with_label(m_iter->id, firstday, lt_it->get_full_label()) };
					std::vector<License>::iterator vl_iter = v_licenses.begin();
					std::vector<License>::iterator vl_end = v_licenses.end();
					CustomDate lm { CustomDate::get_last_day_month(calendata.get_reference_date()) };
					while (vl_iter != vl_end && type < 9) {
						int days_used = calendata.get_license_amount(database, m_iter->id, vl_iter->get_full_label(), vl_iter->get_ref_year(), lm);
						days_available += vl_iter->get_days() - days_used;
						vl_iter++;
					}
					shift.up = Glib::ustring::format(days_available);
				}
				break;
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "DrawingShifts::draw_extra_column() " << dbe.get_message() << std::endl;
			throw dbe;
		}
		// get the longest shift string size
		Cairo::TextExtents test_text_ext;
		cr->get_text_extents(shift.up, test_text_ext);
		if (test_text_ext.width > extra_text_ext.width)	{
			extra_text_ext = test_text_ext;
			longest_string.assign(shift.up);
		}
		shifts_vec.push_back(shift);
		row_nr++;
		m_iter++;
	}
	row_nr = 0;
	// calculate space for shifts
	Cairo::TextExtents cell_text_ext;
	cr->get_text_extents(longest_string + Glib::ustring("_"), cell_text_ext);
	for (auto it = shifts_vec.cbegin(); it != shifts_vec.cend(); it++) {
		if (row_nr == 0) {
			// draw column title
			Cairo::TextExtents this_text_ext;
			cr->get_text_extents(title_up, this_text_ext);
			cr->move_to(*x_grid, *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, title_up, -90.0);
			cr->get_text_extents(title_down, this_text_ext);
			cr->move_to(*x_grid + (c_width / 2.4), *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, title_down, -90.0);
			// draw a line
			cr->move_to(*x, *y_grid - cell_text_ext.width + dy);
			cr->line_to(*x + (c_width / 1.2), *y_grid - cell_text_ext.width + dy);
			cr->stroke();
			row_nr++;
		}
		if (row_nr > 0) {			
			// draw values
			Cairo::TextExtents this_text_ext;
			cr->get_text_extents(it->up, this_text_ext);
			cr->move_to(*x + (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - ((cell_text_ext.width - this_text_ext.width) / 2.0) + dy);
			rotate_and_draw_text(cr, it->up, -90.0);
			// draw a line
			cr->move_to(*x - (c_width / 5.0) + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
			cr->line_to(*x - (c_width / 5.0) + c_width + ((c_width * 1.2) * row_nr), *y_grid - cell_text_ext.width + dy);
			cr->stroke();
			row_nr++;
		}
	}
	*y_grid -= cell_text_ext.width;
}

void DrawingShifts::draw_shifts(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page) {
	int col_nr { 1 };
	bool result;
	extra_columns = calculate_extra_columns();
	CustomDate cd_day { CustomDate::get_first_day_month(calendata.get_reference_date()) };
	int mnth { cd_day.get_month() };
	int mnthsize { cd_day.get_month_size() };
	do	{
		if (col_nr > 0 && col_nr <= mnthsize) {
			draw_shifts_column(cr, x_grid, y_grid, dy, x, page, cd_day);
		} else if (col_nr > mnthsize && col_nr <= (mnthsize + extra_columns)) {
			draw_extra_column(cr, col_nr - mnthsize, x_grid, y_grid, dy, x, page);
		}
		if (cd_day.get_month() == mnth || col_nr <= (mnthsize + extra_columns)) {
			if (col_nr > 0 && col_nr <= mnthsize) {
				cd_day.add_days(1);
			} else {
				mnth = -1;
			}
			col_nr++;
		}
	} while (cd_day.get_month() == mnth || col_nr <= (mnthsize + extra_columns));
}

int DrawingShifts::calculate_extra_columns(void) {
	try {
		std::vector<LicenseType> licenses { database->get_visible_license_types() };
		return licenses.size() + 3;
	} catch (const Database::DBError& dbe) {
		std::cerr << "DrawingShifts::calculate_extra_columns() " << dbe.get_message() << std::endl;
	}
	return 0;
}


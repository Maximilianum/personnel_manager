/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_shifts.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWING_SHIFTS_HH_
#define _DRAWING_SHIFTS_HH_

#include "drawing_document.hh"
#include "calendar_data.hh"

typedef struct {
	Glib::ustring id;
	Glib::ustring rank;
	Glib::ustring name;
	Glib::ustring surname;
} Member_info;

typedef struct {
	Glib::ustring id;
	Glib::ustring up;
	Glib::ustring down;
} Shift_strings;

class DrawingShifts: public DrawingDocument {
 public:
	DrawingShifts(Database* db, const time_t ref_date);
	inline void set_blank(bool flag) { blank = flag; }
	inline void set_members_per_page(int val) { members_per_page = val; }
	inline int count_pages() const { return members_info.size() > 0 ? (members_info.size() / members_per_page) + ((members_info.size() % members_per_page) > 0) : 0; }
	unsigned long int get_number_of_rows_needed(int page);
	void draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages = false);
 protected:
	void init_members_info(void);
	void draw_ranks_and_names(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page);
	void draw_shifts_column(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page, const CustomDate& cd_day);
	void draw_extra_column(const Cairo::RefPtr<Cairo::Context>& cr, short type, double* x_grid, double* y_grid, const double dy, double* x, int page);
	void draw_shifts(const Cairo::RefPtr<Cairo::Context>& cr, double* x_grid, double* y_grid, const double dy, double* x, int page);
	int calculate_extra_columns(void);
 private:
	std::vector<Member_info> members_info;
	bool blank;
	int members_per_page;
	int extra_columns;
};

#endif // _DRAWING_SHIFTS_HH_


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_siris.cc
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing_siris.hh"
#include "database.hh"

#define SHORT_WIDTH_CELL	0.60f
#define SHORT_HEIGHT_CELL   0.30f
#define LINE_HEIGHT			12.0f

DrawingSiris::DrawingSiris(Database* db, const time_t ref_date) : DrawingDocument(db, ref_date) {
	init_work_status();
	set_cell_width(120.0);
	set_cell_height(60.0);
}

void DrawingSiris::set_member(const Member& mbr) {
	member = mbr;
	std::vector<Glib::ustring> strings;
	Glib::ustring rank = member.get_rank().get_short_label();
	parse_string(member.get_first_name(), &strings, Glib::ustring(" "));
	Glib::ustring name = strings[0];
	if (strings.size() > 1) {
		name.append(Glib::ustring(" ") + strings[1].substr(0, 1) + Glib::ustring("."));
	}
	Glib::ustring surname = member.get_last_name();
	Glib::ustring complete = Glib::ustring::compose("%1 %2 %3", rank, name, surname);
	member_str.assign(complete);
}

int DrawingSiris::count_pages() const {
	CustomDate cd_day { calendata.get_reference_cdate() };
	int pages = cd_day.get_weeks_in_month();
	pages = (pages / 2) + (pages % 2);
	return pages;
}

void DrawingSiris::draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages) {
	double dy = pages ? page_height * page : 0.0;
	double y = top_margin;
	cr->set_line_width(1.5);
	// draw title
	y = draw_title(cr, dy, y, ORIENTATION::vertical);
	// prepare font size and line width
	cr->set_font_size(font_size);
	cr->set_line_width(0.8);
	// draw days
	draw_days_at_page(cr, dy + y, page);
	// draw first row for headers
	cr->move_to(left_margin, y + dy);
	cr->line_to(left_margin, y + dy + (c_height * SHORT_HEIGHT_CELL));
	cr->line_to(page_width - right_margin, y + dy + (c_height * SHORT_HEIGHT_CELL));
	cr->line_to(page_width - right_margin, y + dy);
	cr->line_to(left_margin, y + dy);
	cr->stroke();
	y += c_height * SHORT_HEIGHT_CELL;
}

void DrawingSiris::compute_siris_month(void) {
	CustomDate cd_cm { CustomDate::get_first_day_month(calendata.get_reference_date()) };
	srsmonth.set_month(cd_cm.get_month());
	int pages = count_pages();
	for (int nrpg = 0; nrpg < pages; nrpg++) {
		for (short int i = 0; i < 2; i++) {
			SirisWeek sweek;
			sweek.set_month(srsmonth.get_month());
			sweek.set_romc(srsmonth.get_romc());
			sweek.set_romp(srsmonth.get_romp());
			sweek.set_pb(srsmonth.get_pb());
			sweek.set_rrs(srsmonth.get_rrs());
			sweek.set_rri(srsmonth.get_rri());
			int weekn = (nrpg * 2) + i;
			CustomDate cd_day { CustomDate::get_day_of_week(cd_cm.get_time_t(), 1) };
			cd_day.add_days(7 * weekn);
			if (cd_day.get_month() != cd_cm.get_month() + 1) {
				calendata.collect_siris_week_data(database, cd_day, member.get_id(), &sweek);
				if (sweek.is_ready()) {
					sweek.compute();
				}
				srsmonth.set_romc(sweek.get_romc());
				srsmonth.set_romp(sweek.get_romp());
				srsmonth.set_pb(sweek.get_pb());
				srsmonth.set_rrs(sweek.get_rrs());
				srsmonth.set_rri(sweek.get_rri());
				srsmonth.add_week(sweek);
			}
		}
	}
}

void DrawingSiris::init_work_status(void) {
	wrk_sts.clear();
	wrk_sts = database->get_labels();
}

void DrawingSiris::draw_days_at_page(const Cairo::RefPtr<Cairo::Context>& cr, double y, int page) {
	int y_up = y;
	// draw headers
	Glib::ustring headers[] = {Glib::ustring("DATA"), Glib::ustring("PROGR.TO"), Glib::ustring("EFFETTUATO"), Glib::ustring("NOTE")};
	double sc_width = c_width * SHORT_WIDTH_CELL; // shorter cell width
	double x = left_margin;
	for (int i = 0; i < 4; i++)	{
		double width = i < 1 ? sc_width : c_width;
		Cairo::TextExtents te;
		cr->get_text_extents(headers[i].raw(), te);
		cr->move_to(x + ((width - te.width) / 2.0), y + (((c_height * SHORT_HEIGHT_CELL) + te.height) / 2.0f));
		cr->show_text(headers[i].raw());
		x += width;
	}
	y += c_height * SHORT_HEIGHT_CELL;
	int i = 0;
	int weekn = 0;
	while (srsmonth.has_week(weekn) && i < 2) {
		SirisWeek* sweek = srsmonth.get_week(weekn);
		y = draw_week(cr, y, sweek);
		i++;
		weekn = (page * 2) + i;
	}
	// draw vertical lines
	cr->move_to(left_margin, y_up);
	cr->line_to(left_margin, y);
	cr->move_to(left_margin + sc_width, y_up);
	cr->line_to(left_margin + sc_width, y);
	cr->move_to(left_margin + sc_width + c_width, y_up);
	cr->line_to(left_margin + sc_width + c_width, y);
	cr->move_to(left_margin + sc_width + (c_width * 2.0), y_up);
	cr->line_to(left_margin + sc_width + (c_width * 2.0), y);
	cr->move_to(page_width - right_margin, y_up);
	cr->line_to(page_width - right_margin, y);
	cr->stroke();
}

double DrawingSiris::draw_week(const Cairo::RefPtr<Cairo::Context>& cr, double y, SirisWeek* sweek) {
	double sc_width = c_width * SHORT_WIDTH_CELL; // shorter cell width
	for (int i = 0; i < 7; i++)	{
		int index = i + 1;
		if (index == 7) {
			index = 0;
		}
		CustomDate cd { sweek->get_date(index) };
		// print week day
		Cairo::TextExtents te;
		if (cd.is_holiday()) {
			cr->set_source_rgba(1.0f, 0.0f, 0.0f, 1.0f);
		} else {
			cr->set_source_rgba(0.0f, 0.0f, 0.0f, 1.0f);
		}
		cr->get_text_extents(cd.get_weekday_string(), te);
		cr->move_to(left_margin + ((sc_width - te.width) / 2.0f), y + LINE_HEIGHT);
		cr->show_text(cd.get_weekday_string());
		// print date
		cr->get_text_extents(cd.get_formatted_string(DATE_FORMAT::date), te);
		cr->move_to(left_margin + ((sc_width - te.width) / 2.0f), y + (LINE_HEIGHT * 2.0f));
		cr->show_text(cd.get_formatted_string(DATE_FORMAT::date));
		// print programmed shifts
		try {
			Shift shift { calendata.get_shift(database, cd, member.get_id(), false) };
			if (calendata.is_working(database, cd, member.get_id()) || calendata.is_absence(database, shift)) {
				cr->set_source_rgba(0.0f, 0.0f, 0.0f, 1.0f);
			} else {
				cr->set_source_rgba(1.0f, 0.0f, 0.0f, 1.0f);
			}
		} catch (const Database::DBEmptyResultException& dbe) {
			cr->set_source_rgba(0.0f, 0.0f, 0.0f, 1.0f);
		}
		cr->get_text_extents(sweek->get_programmed(index).raw(), te);
		cr->move_to(left_margin + sc_width + ((c_width - te.width) / 2.0f), y + LINE_HEIGHT);
		cr->show_text(sweek->get_programmed(index).raw());
		if (sweek->get_executed(index).size() > 0) {
			std::vector<Glib::ustring> exes;
			parse_string(sweek->get_executed(index), &exes, Glib::ustring("\n")); // parse multiple lines in executed
			std::vector<Glib::ustring>::iterator it_exes = exes.begin();
			std::vector<Glib::ustring>::iterator en_exes = exes.end();
			while (it_exes != en_exes) {
				// print each executed shifts lines
				cr->get_text_extents(it_exes->raw(), te);
				cr->move_to(left_margin + sc_width + c_width + ((c_width - te.width) / 2.0f), y + LINE_HEIGHT);
				cr->show_text(it_exes->raw());
				y += LINE_HEIGHT;
				it_exes++;
			}
			if (exes.size() < 2) {
				y += LINE_HEIGHT * 2.0f;
			}
		} else {
			y += LINE_HEIGHT * 3.0f;
		}
		y += 2.0f;
		// draw horizontal line
		cr->set_source_rgba(0.0f, 0.0f, 0.0f, 1.0f);
		cr->move_to(left_margin, y);
		cr->line_to(page_width - right_margin, y);
		cr->stroke();
	}
	return y;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_siris.hh
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWING_SIRIS_HH_
#define _DRAWING_SIRIS_HH_

#include "drawing_document.hh"
#include "calendar_data.hh"

class DrawingSiris: public DrawingDocument {
 public:
	DrawingSiris(Database* db, const time_t ref_date);
	void set_member(const Member& mbr);
	int count_pages() const;
	void set_up_week_table(const Cairo::RefPtr<Cairo::Context>& cr);
	void draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages = false);
	void compute_siris_month(void);
 protected:
	void init_work_status(void);
	void draw_days_at_page(const Cairo::RefPtr<Cairo::Context>& cr, double y, int page);
	double draw_week(const Cairo::RefPtr<Cairo::Context>& cr, double y, SirisWeek* sweek);
 private:
	Member member;
	Glib::ustring member_str;
	std::vector<Label> wrk_sts;
	SirisMonth srsmonth;
};

#endif // _DRAWING_SIRIS_HH_


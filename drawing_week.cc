/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_week.cc
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "drawing_week.hh"
#include "database.hh"

DrawingWeek::DrawingWeek(Database* db, const time_t ref_date) : DrawingDocument(db, ref_date), show_shift (false), start_day(0) {
	init_members(CustomDate::get_first_day_month(calendata.get_reference_date()));
	init_work_status();
	set_cell_width(130.0);
	set_cell_height(84.0);
}

int DrawingWeek::count_pages() const {
	CustomDate cd_day { calendata.get_reference_cdate() };
	return cd_day.get_weeks_in_month();
}

void DrawingWeek::draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages) {
	double dy { pages ? page_height * page : 0.0 };
	double y { top_margin };
	cr->set_line_width(1.5);
	// draw logo
	y += draw_svg_logo(cr, ORIENTATION::vertical);
	// draw title
	y = draw_title(cr, dy, y, ORIENTATION::vertical);
	// prepare font size and line width
	cr->set_font_size(font_size);
	cr->set_line_width(0.8);
	// draw object
	Cairo::TextExtents te;
	Glib::ustring obj_str = description + Glib::ustring(" dal");
	cr->get_text_extents(obj_str.raw(), te);
	y += te.height * 2.0;
	cr->move_to(((page_width - te.width) / 3.0), y + dy);
	cr->show_text(obj_str.raw());
	double y_obj { y };
	y += te.height * 0.4;
	// draw days
	draw_days_at_page(cr, dy, y, y_obj, page);
	int grid_rows { 7 };
	double x_grid { left_margin + (c_width / 5.0) };
	double y_grid { y + dy + (c_height / 3.0) };
	// draw first row for headers
	cr->move_to(left_margin, y + dy);
	cr->line_to(left_margin, y + dy + (c_height / 3.0));
	cr->line_to(page_width - right_margin, y + dy + (c_height / 3.0));
	cr->line_to(page_width - right_margin, y + dy);
	cr->line_to(left_margin, y + dy);
	cr->stroke();
	// draw rows
	y += c_height / 3.0;
	for(int n = 0; n < grid_rows; n++) {
		cr->move_to(left_margin, y + dy + (c_height * n));
		cr->line_to(left_margin, y + dy + (c_height * n) + c_height);
		cr->line_to(page_width - right_margin, y + dy + (c_height * n) + c_height);
		cr->line_to(page_width - right_margin, y + dy + (c_height * n));
		cr->line_to(left_margin, y + dy + (c_height * n));
	}
	cr->stroke();
	// draw vertical lines
	double sc_width = c_width * SHORT_CELL; // shorter cell width
	cr->move_to(left_margin + sc_width, y - (c_height / 3.0f) + dy);
	cr->line_to(left_margin + sc_width, y + dy + (c_height * grid_rows));
	cr->move_to(left_margin + (sc_width * 2.0f), y - (c_height / 3.0f) + dy);
	cr->line_to(left_margin + (sc_width * 2.0f), y + dy + (c_height * grid_rows));
	cr->get_text_extents(members_str[longest_name].raw(), te);
	cr->move_to(left_margin + (sc_width * 2.0f) + (te.width * 1.1f), y - (c_height / 3.0f) + dy);
	cr->line_to(left_margin + (sc_width * 2.0f) + (te.width * 1.1f), y + dy + (c_height * grid_rows));
	cr->stroke();
	// draw horizontal lines
	for(int i = 0; i < grid_rows; i++) {
		for(int n = 1; n < members.size(); n++) {
			cr->move_to(left_margin + (sc_width * 2.0f) + (te.width * 1.1f), y + dy + (c_height * i) + ((c_height / members.size()) * n));
			cr->line_to(page_width - right_margin, y + dy + (c_height * i) + ((c_height / members.size()) * n));
		}
	}
	cr->stroke();
	// draw sign
	y += c_height / 5.0;
	y = draw_sign(cr, dy, y, ORIENTATION::vertical);
}

void DrawingWeek::init_members(const CustomDate& ref) {
	members.clear();
	try {
		members = database->get_active_members(ref);
	} catch (const Database::DBError& dbe) {
		std::cerr << "DrawingWeek::init_members " << dbe.get_message() << std::endl;
	}
	members_str.clear();
	int n { 0 };
	Glib::ustring::size_type max_str { 0 };
	for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
		std::vector<Glib::ustring> strings;
		Glib::ustring rank = iter->get_rank().get_short_label();
		parse_string(iter->get_first_name(), &strings, Glib::ustring(" "));
		Glib::ustring name = strings[0];
		if (strings.size() > 1) {
			name.append(Glib::ustring(" ") + strings[1].substr(0, 1) + Glib::ustring("."));
		}
		Glib::ustring surname = iter->get_last_name();
		Glib::ustring complete = Glib::ustring::compose("%1 %2 %3", rank, name, surname);
		if (complete.size() > max_str) {
			max_str = complete.size();
			longest_name = n;
		}
		members_str.push_back(complete);
		n++;
	}
}

void DrawingWeek::init_work_status(void) {
	wrk_sts.clear();
	wrk_sts = database->get_labels();
}

void DrawingWeek::parse_string(const Glib::ustring& str, std::vector<Glib::ustring>* vector, const Glib::ustring& separator) {
	Glib::ustring::size_type start { 0 };
	Glib::ustring::size_type pos { 0 };
	do {
		pos = str.find(separator, start);
		vector->push_back(str.substr(start, pos - start));
		if (pos != Glib::ustring::npos) {
			start = pos + 1;
		}
	}
	while(pos != Glib::ustring::npos);
}

void DrawingWeek::draw_days_at_page(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double y, double y_obj, int page) {
	// draw headers
	Glib::ustring headers[] = {Glib::ustring("DATA"), Glib::ustring("TURNO"), Glib::ustring("NOME"), Glib::ustring("ANNOTAZIONI")};
	double sc_width { c_width * SHORT_CELL }; // shorter cell width
	double x { left_margin };
	for (int i = 0; i < 4; i++)	{
		double width;
		Cairo::TextExtents te;
		switch (i) {
			case 0:
			case 1:
				width = sc_width;
				break;
			case 2:
				cr->get_text_extents(members_str[longest_name].raw(), te);
				width = te.width * 1.1f;
				break;
			default:
				width = page_width - x - right_margin;
				break;
		}
		cr->get_text_extents(headers[i].raw(), te);
		cr->move_to(x + ((width - te.width) / 2.0f), y + dy + (((c_height / 3.0f) + (te.height * 1.1f)) / 2.0f));
		cr->show_text(headers[i].raw());
		x += width;
	}
	y += c_height / 3.0;
	CustomDate cd_day { CustomDate::get_day_of_week(CustomDate::get_first_day_month(calendata.get_reference_date()).get_time_t(), start_day) };
	cd_day.add_days(7 * page);
	// print subject
	if (cd_day.get_weekday() == start_day) {
		Cairo::TextExtents te;
		Glib::ustring obj_str = cd_day.get_formatted_string(DATE_FORMAT::date) + Glib::ustring(" al");
		cr->get_text_extents(obj_str.raw(), te);
		cr->move_to(((page_width - te.width) / 1.75), y_obj + dy);
		cr->show_text(obj_str.raw());
	}
	for (int n = 0; n < 7; n++) {
		draw_day(cr, dy, &y, y_obj, page, cd_day);
		cd_day.add_days(1);
	}
	cd_day.add_days(-1);
	Cairo::TextExtents te;
	Glib::ustring obj_str = cd_day.get_formatted_string(DATE_FORMAT::date);
	cr->get_text_extents(obj_str.raw(), te);
	cr->move_to(((page_width - te.width) / 1.47f), y_obj + dy);
	cr->show_text(obj_str.raw());
}

void DrawingWeek::draw_day(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double* y, double y_obj, int page, const CustomDate& cd_day) {
	init_members(CustomDate::start_of_day(cd_day));
	// print week day
	double sc_width { c_width * 0.75 }; // shorter cell width
	Glib::ustring wday_str { cd_day.get_weekday_string() };
	Cairo::TextExtents te;
	cr->get_text_extents(wday_str.raw(), te);
	cr->move_to(left_margin + ((sc_width - te.width) / 2.0f), *y + dy + (((c_height / 2.0f) + (te.height * 1.1f)) / 2.0f));
	cr->show_text(wday_str.raw());
	// print date
	Glib::ustring date_str = cd_day.get_formatted_string(DATE_FORMAT::date);
	cr->get_text_extents(date_str.raw(), te);
	cr->move_to(left_margin + ((sc_width - te.width) / 2.0f), *y + dy + (((c_height / 2.0) + (te.height * 1.1)) / 2.0) + (te.height * 2.0));
	cr->show_text(date_str.raw());
	// print shifts
	int n { 0 };
	for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
		std::vector<Glib::ustring> strings;
		parse_string(calendata.get_shift_string(database, cd_day, iter->get_id(), false, false, true), &strings, Glib::ustring("\n"));
		if (strings.size() >= 2 && strings[0] != Glib::ustring()) {
			Glib::ustring shift_str;
			std::string stds = strings[0].substr(strings[0].size() - 1, 1).raw();
			if (std::isdigit(stds[stds.size() - 1])) {
				if (show_shift) {
					shift_str = strings[0] + Glib::ustring("/") + strings[1];
				} else {
					shift_str = Glib::ustring("P");
				}
			} else {
				shift_str = strings[0] + strings[1];
			}
			cr->get_text_extents(shift_str.raw(), te);
			cr->move_to(left_margin + sc_width + ((sc_width - te.width) / 2.0f), *y + dy + ((c_height / members.size()) * n) + (c_height / (members.size() * 1.3f)));
			cr->show_text(shift_str.raw());
		}
		Glib::ustring note { calendata.get_shift_note(database, cd_day, iter->get_id(), false) };
		cr->get_text_extents(members_str[longest_name].raw(), te);
		cr->move_to(left_margin + (sc_width * 2.0) + (te.width * 1.12f), *y + dy + ((c_height / members.size()) * n) + (c_height / (members.size() * 1.3f)));
		cr->show_text(note.raw());
		n++;
	}
	// print members
	n = 0;
	for (auto iter = members_str.cbegin(); iter != members_str.cend(); iter++) {
		cr->get_text_extents(iter->raw(), te);
		cr->move_to(left_margin + (sc_width * 2.0f) + 1.5f, *y + dy + ((c_height / members.size()) * n) + (c_height / (members.size() * 1.3f)));
		cr->show_text(iter->raw());
		n++;
	}
	*y += c_height;			
}

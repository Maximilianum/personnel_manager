/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * drawing_week.hh
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DRAWING_WEEK_H_
#define _DRAWING_WEEK_H_

#include "drawing_document.hh"
#include "calendar_data.hh"
#include "member.hh"

#define SHORT_CELL  0.75f

class DrawingWeek: public DrawingDocument {
 public:
	DrawingWeek(Database* db, const time_t ref_date);
	inline void set_description(const Glib::ustring& str) { description.assign(str); }
	inline void set_start_day(int val) { start_day = val; }
	inline void set_show_shift(bool flag) { show_shift = flag; }
	int count_pages() const;
	void draw_at_page(const Cairo::RefPtr<Cairo::Context>& cr, int page, bool pages = false);
 protected:
	void init_members(const CustomDate& ref);
	void init_work_status(void);
	void parse_string(const Glib::ustring& str, std::vector<Glib::ustring>* vector, const Glib::ustring& separator);
	void draw_days_at_page(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double y, double y_obj, int page);
	void draw_day(const Cairo::RefPtr<Cairo::Context>& cr, const double dy, double* y, double y_obj, int page, const CustomDate& cd_day);
 private:
	std::vector<Member> members;
	std::vector<Label> wrk_sts;
	std::vector<Glib::ustring> members_str;
	bool show_shift;
	int longest_name;
	int start_day;
	Glib::ustring description;
};

#endif // _DRAWING_WEEK_H_


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * dual_string_tmcr.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DUAL_STRING_TREE_MODEL_COLUMN_RECORD_HH_
#define _DUAL_STRING_TREE_MODEL_COLUMN_RECORD_HH_

#include <gtkmm/treemodelcolumn.h>

class DualStringTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	DualStringTreeModelColumnRecord() { add(column1); add(column2); }
	Gtk::TreeModelColumn<Glib::ustring> column1;
	Gtk::TreeModelColumn<Glib::ustring> column2;
};

#endif // _DUAL_STRING_TREE_MODEL_COLUMN_RECORD_HH_

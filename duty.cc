/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * duty.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "duty.hh"

int Duty::max_work_length = DEFAULT_MAX_WORK;
int Duty::work_time_start = DEFAULT_WORK_TIME_START;
int Duty::work_time_end = DEFAULT_WORK_TIME_END;
bool Duty::work_time_saturday_close = false;
bool Duty::work_time_saturday_enabled = false;
int Duty::work_time_saturday_start = DEFAULT_WORK_TIME_SATURDAY_START;
int Duty::work_time_saturday_end = DEFAULT_WORK_TIME_SATURDAY_END;
bool Duty::work_time_holiday_close = false;
bool Duty::work_time_holiday_enabled = false;
int Duty::work_time_holiday_start = DEFAULT_WORK_TIME_HOLIDAY_START;
int Duty::work_time_holiday_end = DEFAULT_WORK_TIME_HOLIDAY_END;
int Duty::meal_break = DEFAULT_MEAL_BREAK;
int Duty::lunch_start = DEFAULT_LUNCH_START;
int Duty::lunch_end = DEFAULT_LUNCH_END;
int Duty::dinner_start = DEFAULT_DINNER_START;
int Duty::dinner_end = DEFAULT_DINNER_END;
Gdk::RGBA Duty::work_color = DEFAULT_WORK_COLOUR;
Gdk::RGBA Duty::absent_color = DEFAULT_ABSENT_COLOUR;
Gdk::RGBA Duty::license_color = DEFAULT_LICENSE_COLOUR;
const Glib::ustring Duty::sqlite_shift_table { Glib::ustring("TABLE SHIFT (" \
															 "MEMBER TEXT NOT NULL, " \
															 "LABEL TEXT, " \
															 "LIC_YEAR INTEGER, " \
															 "START INTEGER NOT NULL, " \
															 "FINISH INTEGER, " \
															 "NOTE TEXT, " \
															 "FLAGS INTEGER, " \
															 "OBREAK INTEGER, " \
															 "PRIMARY KEY (MEMBER, START), " \
															 "FOREIGN KEY(MEMBER) REFERENCES MEMBERS(ID), " \
															 "FOREIGN KEY(LABEL) REFERENCES LABEL(FULL));") };

void Duty::set_meal_break(int val) {
	meal_break = val;
}

int Duty::get_meal_break() {
	return meal_break;
}

void Duty::set_lunch_start(int val) {
	lunch_start = val;
}

int Duty::get_lunch_start() {
	return lunch_start;
}

void Duty::set_lunch_end(int val) {
	lunch_end = val;
}

int Duty::get_lunch_end() {
	return lunch_end;
}

void Duty::set_dinner_start(int val) {
	dinner_start = val;
}

int Duty::get_dinner_start() {
	return dinner_start;
}

void Duty::set_dinner_end(int val) {
	dinner_end = val;
}

int Duty::get_dinner_end() {
	return dinner_end;
}

void Duty::set_max_work_length(int val) {
	max_work_length = val;
}

int Duty::get_max_work_length() {
	return max_work_length;
}

void Duty::set_work_time_start(int val) {
	work_time_start = val;
}

int Duty::get_work_time_start() {
	return work_time_start;
}

void Duty::set_work_time_end(int val) {
	work_time_end = val;
}

int Duty::get_work_time_end() {
	return work_time_end;
}

void Duty::set_work_time_saturday_close(bool flag) {
	work_time_saturday_close = flag;
}

bool Duty::get_work_time_saturday_close() {
	return work_time_saturday_close;
}

void Duty::set_work_time_saturday_enabled(bool flag) {
	work_time_saturday_enabled = flag;
}

bool Duty::get_work_time_saturday_enabled() {
	return work_time_saturday_enabled;
}

void Duty::set_work_time_saturday_start(int val) {
	work_time_saturday_start = val;
}

int Duty::get_work_time_saturday_start() {
	return work_time_saturday_start;
}

void Duty::set_work_time_saturday_end(int val) {
	work_time_saturday_end = val;
}

int Duty::get_work_time_saturday_end() {
	return work_time_saturday_end;
}

void Duty::set_work_time_holiday_close(bool flag) {
	work_time_holiday_close = flag;
}

bool Duty::get_work_time_holiday_close() {
	return work_time_holiday_close;
}

void Duty::set_work_time_holiday_enabled(bool flag) {
	work_time_holiday_enabled = flag;
}

bool Duty::get_work_time_holiday_enabled() {
	return work_time_holiday_enabled;
}

void Duty::set_work_time_holiday_start(int val) {
	work_time_holiday_start = val;
}

int Duty::get_work_time_holiday_start() {
	return work_time_holiday_start;
}

void Duty::set_work_time_holiday_end(int val) {
	work_time_holiday_end = val;
}

int Duty::get_work_time_holiday_end() {
	return work_time_holiday_end;
}

void Duty::set_work_color(Gdk::RGBA col) {
	work_color = col;
}

Gdk::RGBA Duty::get_work_color() {
	return work_color;
}

void Duty::set_absent_color(Gdk::RGBA col) {
	absent_color = col;
}

Gdk::RGBA Duty::get_absent_color() {
	return absent_color;
}

void Duty::set_license_color(Gdk::RGBA col) {
	license_color = col;
}

Gdk::RGBA Duty::get_license_color() {
	return license_color;
}

int Duty::retrieve_duty(void *data, int argc, char **argv, char **azColName) {
	Duty dty;
	std::vector<Duty>* duties { static_cast<std::vector<Duty>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("MEMBER") && argv[i]) {
			dty.set_member(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("WORK_STATUS")) == 0) && argv[i]) {
			dty.set_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LIC_YEAR")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("REF_YEAR")) == 0) && argv[i]) {
			dty.set_license_year(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("START") && argv[i]) {
			dty.set_start(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FINISH") && argv[i]) {
			dty.set_end(argv[i]);
		} else if (Glib::ustring(azColName[i]).compare(Glib::ustring("NOTE")) == 0 && argv[i]) {
			dty.set_note(argv[i]);
		} else if (Glib::ustring(azColName[i]).compare(Glib::ustring("FLAGS")) == 0 && argv[i]) {
			dty.set_flags(argv[i]);
		} else if (Glib::ustring(azColName[i]).compare(Glib::ustring("OBREAK")) == 0 && argv[i]) {
			dty.set_other_break_min(argv[i]);
		}
	}
	duties->push_back(dty);
	return 0;
}

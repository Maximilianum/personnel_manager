/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * duty.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _DUTY_HH_
#define _DUTY_HH_

#define DEFAULT_MAX_WORK		    -1
#define DEFAULT_WORK_TIME_START		    0
#define DEFAULT_WORK_TIME_END		    1440
#define DEFAULT_WORK_TIME_SATURDAY_START    0
#define DEFAULT_WORK_TIME_SATURDAY_END	    1440
#define DEFAULT_WORK_TIME_HOLIDAY_START	    0
#define DEFAULT_WORK_TIME_HOLIDAY_END	    1440
#define DEFAULT_MEAL_BREAK		    30
#define DEFAULT_LUNCH_START		    660
#define DEFAULT_LUNCH_END		    900
#define DEFAULT_DINNER_START	            1080
#define DEFAULT_DINNER_END		    1260
#define DEFAULT_WORK_COLOUR                 Gdk::RGBA("Black")
#define DEFAULT_ABSENT_COLOUR               Gdk::RGBA("LightGreen")
#define DEFAULT_LICENSE_COLOUR              Gdk::RGBA("Red")

#include <cstdint>
#include <glibmm/ustring.h>
#include <gdkmm/rgba.h>
#include "custom_date.hh"

class Duty {
protected:
  static int meal_break;
  static int lunch_start;
  static int lunch_end;
  static int dinner_start;
  static int dinner_end;
  static Gdk::RGBA work_color;
  static Gdk::RGBA absent_color;
  static Gdk::RGBA license_color;
  static const Glib::ustring sqlite_shift_table;
  
  Glib::ustring member;
  CustomDate start;
  CustomDate end;
  Glib::ustring label;
  int lic_year;
  Glib::ustring note;
  /*
	 * flags bit mask
	 * 0 lunch break enabled
	 * 1 dinner break enabled
	 * 2 other break enabled
	 */
  std::uint8_t flags;
  int obreak;
  
private:
  static int max_work_length; // not used
  static int work_time_start;
  static int work_time_end;
  static bool work_time_saturday_close;
  static bool work_time_saturday_enabled;
  static int work_time_saturday_start;
  static int work_time_saturday_end;
  static bool work_time_holiday_close;
  static bool work_time_holiday_enabled;
  static int work_time_holiday_start;
  static int work_time_holiday_end;
  
public:
  static constexpr std::uint8_t mask0 { 1 << 0 };
  static constexpr std::uint8_t mask1 { 1 << 1 };
  static constexpr std::uint8_t mask2 { 1 << 2 };
	
  static void set_meal_break(int val);
  static int get_meal_break();
  static void set_lunch_start(int val);
  static int get_lunch_start();
  static void set_lunch_end(int val);
  static int get_lunch_end();
  static void set_dinner_start(int val);
  static int get_dinner_start();
  static void set_dinner_end(int val);
  static int get_dinner_end();
  static void set_max_work_length(int val);
  static int get_max_work_length();
  static void set_work_time_start(int val);
  static int get_work_time_start();
  static void set_work_time_end(int val);
  static int get_work_time_end();
  static void set_work_time_saturday_close(bool flag);
  static bool get_work_time_saturday_close();
  static void set_work_time_saturday_enabled(bool flag);
  static bool get_work_time_saturday_enabled();
  static void set_work_time_saturday_start(int val);
  static int get_work_time_saturday_start();
  static void set_work_time_saturday_end(int val);
  static int get_work_time_saturday_end();
  static void set_work_time_holiday_close(bool flag);
  static bool get_work_time_holiday_close();
  static void set_work_time_holiday_enabled(bool flag);
  static bool get_work_time_holiday_enabled();
  static void set_work_time_holiday_start(int val);
  static int get_work_time_holiday_start();
  static void set_work_time_holiday_end(int val);
  static int get_work_time_holiday_end();
  static void set_work_color(Gdk::RGBA col);
  static Gdk::RGBA get_work_color();
  static void set_absent_color(Gdk::RGBA col);
  static Gdk::RGBA get_absent_color();
  static void set_license_color(Gdk::RGBA col);
  static Gdk::RGBA get_license_color();
  static inline const Glib::ustring get_sqlite_shift_table(void) { return sqlite_shift_table; }
  static int retrieve_duty(void *data, int argc, char **argv, char **azColName);

  inline Duty() : lic_year (-1), flags (0b00000011) , obreak (0) {}
  inline Duty(const Duty& dt) : member (dt.member), start (dt.start), end (dt.end), label (dt.label), lic_year (dt.lic_year), note (dt.note), flags (dt.flags), obreak (dt.obreak) {}
  inline Duty(const CustomDate& cds, const CustomDate& cde, const Glib::ustring &id) : start (cds), end (cde), member (id), lic_year (-1), flags (0b00000011), obreak (0) {}
  inline Duty(const Glib::ustring &lbl, const Glib::ustring &id, const CustomDate& cd) : member(id), start(cd), label (lbl), lic_year (-1), flags (0), obreak (0) {}
  inline void set_member(const Glib::ustring &str) { member.assign(str); }
  inline Glib::ustring get_member() const { return member; }
  inline void set_start(const CustomDate& cd) { start = cd; }
  inline void set_start(const char* cstr) { start = CustomDate { cstr }; }
  inline CustomDate get_start() const { return start; }
  inline void set_end(const CustomDate& cd) { end = cd; }
  inline void set_end(const char* cstr) { end = CustomDate { cstr }; }
  inline CustomDate get_end() const { return end; }
  inline void set_label(const Glib::ustring& str) { label.assign(str); }
  inline Glib::ustring get_label() const { return label; }
  inline void set_license_year(int val) { lic_year = val; }
  inline void set_license_year(const char* cstr) { lic_year = atoi(cstr); }
  inline int get_license_year() const { return lic_year; }
  inline bool is_working() const { return label.compare(Glib::ustring()) == 0; }
  inline void set_note(const Glib::ustring& str) { note.assign(str); }
  inline Glib::ustring get_note() const { return note; }
  inline void set_flags(std::uint8_t flgs) { flags = flgs; }
  inline void set_flags(const char* flgs) { flags = atoi(flgs); }
  inline std::uint8_t get_flags() const { return flags; }
  inline bool is_lunch_break_enabled() const { return flags & mask0; }
  inline void set_lunch_break(const bool flg) { flags = flg ? flags | mask0 : flags & ~mask0; }
  inline bool is_dinner_break_enabled() const { return flags & mask1; }
  inline void set_dinner_break(const bool flg) { flags = flg ? flags | mask1 : flags & ~mask1; }
  inline bool is_other_break_enabled() const { return flags & mask2; }
  inline void set_other_break(const bool flg) { flags = flg ? flags | mask2 : flags & ~mask2; }
  inline void set_other_break_min(const int val) { obreak = val; }
  inline void set_other_break_min(const char* cstr) { obreak = atoi(cstr); }
  inline int get_other_break_min() const { return obreak; }
  
};

#endif // _DUTY_HH_

/*
 * errors_dialog.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "errors_dialog.hh"

ErrorsDialog::ErrorsDialog(Gtk::Window& win) : Gtk::Dialog(Glib::ustring("Errori rilevati"), win, true) {
  Gtk::Box* box = get_content_area();
  text_view.set_visible(true);
  text_view.set_cursor_visible(false);
  text_view.set_editable(false);
  scrolled_win.set_hexpand(true);
  scrolled_win.set_visible(true);
  scrolled_win.add(text_view);
  box->pack_start(scrolled_win, true, true);
  box->show_all_children();
  add_button(Glib::ustring("Ok"),Gtk::ResponseType::RESPONSE_OK);
}

void ErrorsDialog::set_message(const Glib::ustring& str) {
  Glib::RefPtr<Gtk::TextBuffer> buffer { text_view.get_buffer() };
  buffer->set_text(str);
  int lines = buffer->get_line_count();
  int characters = buffer->get_char_count();
  resize((characters / lines) * 12,lines * 20);
}

/*
 * errors_dialog.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _ERRORS_DIALOG_HH_
#define _ERRORS_DIALOG_HH_

#include <gtkmm/dialog.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/textview.h>

class ErrorsDialog : public Gtk::Dialog {
public:
  ErrorsDialog(Gtk::Window& win);
  void set_message(const Glib::ustring& str);
private:
  Gtk::ScrolledWindow scrolled_win;
  Gtk::TextView text_view;
  Gtk::Button ok_button;
};

#endif // _ERRORS_DIALOG_HH_

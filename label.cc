/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * label.cc
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "label.hh"
#include <vector>
#include <set>
Glib::ustring Label::sqlite_table { Glib::ustring("TABLE LABEL (" \
												  "FULL TEXT PRIMARY KEY NOT NULL, " \
												  "SHORT TEXT NOT NULL, "		\
												  "UNIQUE(SHORT)); ") };

int Label::retrieve_label(void *data, int argc, char **argv, char **azColName) {
	Label lbl;
	std::vector<Label>* vec { static_cast<std::vector<Label>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			lbl.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			lbl.set_short_label(argv[i]);
		}
	}
	vec->push_back(lbl);
	return 0;
}

int Label::retrieve_label_set(void *data, int argc, char **argv, char **azColName) {
	Label lbl;
	std::set<Label>* lblset { static_cast<std::set<Label>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			lbl.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			lbl.set_short_label(argv[i]);
		}
	}
	lblset->insert(lblset->end(),lbl);
	return 0;
}

int Label::retrieve_string(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring str;
	std::vector<Glib::ustring>* vec { static_cast<std::vector<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			str.assign(argv[i]);
		}
	}
	vec->push_back(str);
	return 0;
}

int Label::retrieve_string_set(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring str;
	std::set<Glib::ustring>* strset { static_cast<std::set<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			str.assign(argv[i]);
		}
	}
	strset->insert(strset->end(),str);
	return 0;
}

Label& Label::operator=(const Label& lb) {
	if (this != &lb) {
		flabel = lb.flabel;
		slabel = lb.slabel;
	}
	return *this;
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * label.hh
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LABEL_HH_
#define _LABEL_HH_

#include <glibmm/ustring.h>
#include <iostream>

class Label {
 public:
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_label(void *data, int argc, char **argv, char **azColName);
	static int retrieve_label_set(void *data, int argc, char **argv, char **azColName);
	static int retrieve_string(void *data, int argc, char **argv, char **azColName);
	static int retrieve_string_set(void *data, int argc, char **argv, char **azColName);
	inline Label() {};
	inline Label(const Glib::ustring& str1, const Glib::ustring& str2) : flabel (str1), slabel (str2) {}
	Label& operator=(const Label& lb);
	inline bool operator<(const Label& lb) const { return flabel < lb.flabel; }
	inline bool operator==(const Label& ws) const { return flabel.compare(ws.flabel) == 0 && slabel.compare(ws.slabel) == 0; }
	inline void set_full_label(const Glib::ustring& str) { flabel.assign(str); }
	inline Glib::ustring get_full_label(void) const { return flabel; }
	inline void set_short_label(const Glib::ustring& str) { slabel.assign(str); }
	inline Glib::ustring get_short_label(void) const { return slabel; }
 protected:
	Glib::ustring flabel;
	Glib::ustring slabel;
 private:
	static Glib::ustring sqlite_table;
};

#endif // _LABEL_HH_


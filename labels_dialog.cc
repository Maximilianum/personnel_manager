/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * labels_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "labels_dialog.hh"
#include <gtkmm/messagedialog.h>

void LabelsDialog::build_gui(LabelsDialog* ld) {
	Gtk::Box* box = ld->get_content_area();
	ld->bldr->get_widget("labels_box", ld->labels_box);
	ld->bldr->get_widget("labels_treeview", ld->labels_treeview);
	ld->labels_liststore = Gtk::ListStore::create(ld->label_tmcr);
	ld->labels_treeview->set_model(ld->labels_liststore);
	ld->label_short_treeviewcolumn = ld->labels_treeview->get_column(0);
	ld->label_short_cellrenderertext = (Gtk::CellRendererText*)ld->label_short_treeviewcolumn->get_first_cell();
	ld->label_full_treeviewcolumn = ld->labels_treeview->get_column(1);
	ld->label_full_cellrenderertext = (Gtk::CellRendererText*)ld->label_full_treeviewcolumn->get_first_cell();
	ld->labels_treeselection = ld->labels_treeview->get_selection();
	ld->labels_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	ld->bldr->get_widget("add_label_button", ld->add_label_button);
	ld->bldr->get_widget("remove_label_button", ld->remove_label_button);
	box->pack_start(*(ld->labels_box));
	/*
	 * load values
	 */
	ld->load_labels_in_treeview();
	/*
	 * signals connections
	 */
	ld->labels_treeselection->signal_changed().connect(sigc::mem_fun(ld, &LabelsDialog::on_labels_treeselection_changed));
	ld->add_label_button->signal_clicked().connect(sigc::mem_fun(ld, &LabelsDialog::on_add_label_button_clicked));
	ld->label_short_cellrenderertext->signal_edited().connect(sigc::mem_fun(ld, &LabelsDialog::on_label_short_edited));
	ld->label_full_cellrenderertext->signal_edited().connect(sigc::mem_fun(ld, &LabelsDialog::on_label_full_edited));
	ld->remove_label_button->signal_clicked().connect(sigc::mem_fun(ld, &LabelsDialog::on_remove_label_button_clicked));
}

void LabelsDialog::load_labels_in_treeview(void) {
	labels_liststore->clear();
	try {
		std::vector<Label> lbls { database->get_labels() };
		for (auto iter = lbls.cbegin(); iter != lbls.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { labels_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[label_tmcr.short_str] = iter->get_short_label();
			row[label_tmcr.full_str] = iter->get_full_label();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::load_data_in_label_treeview() " << dbe.get_message() << std::endl;
	}
}

void LabelsDialog::on_add_label_button_clicked(void) {
	Glib::ustring new_label { Glib::ustring("senza nome") };
	Glib::ustring new_short_label { Glib::ustring("s.n.") };
	unsigned int ns { 0 };
	unsigned int nf { 0 };
	bool duplicated;
	try {
		std::vector<Label> labels { database->get_labels() };
		do {
			duplicated = false;
			for (auto iter = labels.cbegin(); iter != labels.cend(); iter++) {
				if (iter->get_short_label() == new_short_label) {
					if (ns == 0) {
						new_short_label.append(Glib::ustring::format(ns));
					} else {
						new_short_label.replace(4, new_short_label.size() - 4, Glib::ustring::format(ns));
					}
					ns++;
					duplicated = true;
				}
				if (iter->get_full_label() == new_label) {
					if (nf == 0) {
						new_label.append(Glib::ustring::format(nf));
					} else {
						new_label.replace(10, new_label.size() - 10, Glib::ustring::format(nf));
					}
					nf++;
					duplicated = true;
					break;
				}
			}
		} while (duplicated);
		Label label { new_label, new_short_label };
		database->insert(label);
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_add_label_button_clicked() " << dbe.get_message() << std::endl;
	}
	load_labels_in_treeview();
}

void LabelsDialog::on_label_short_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> labels_model { labels_treeview->get_model() };
	Gtk::TreeModel::iterator iter { labels_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		database->update_short_label(row[label_tmcr.full_str], new_text);
		row[label_tmcr.short_str] = new_text;
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_label_short_label_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LabelsDialog::on_label_short_label_edited() " << dbe.get_message() << std::endl;
	}
}

void LabelsDialog::on_label_full_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::ustring old_text;
	Glib::RefPtr<Gtk::TreeModel> labels_model { labels_treeview->get_model() };
	Gtk::TreeModel::iterator iter { labels_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		database->update_label(row[label_tmcr.full_str], new_text);
		database->update_shifts_labels(row[label_tmcr.full_str], new_text);
		database->update_all_license_labels(row[label_tmcr.full_str], new_text);
		row[label_tmcr.full_str] = new_text;
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_label_full_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LabelsDialog::on_label_full_edited() " << dbe.get_message() << std::endl;
	}
}

void LabelsDialog::on_remove_label_button_clicked(void) {
	Glib::RefPtr<Gtk::TreeModel> labels_treemodel { labels_treeview->get_model() };
	std::vector<Gtk::TreeModel::Path> selected { labels_treeselection->get_selected_rows() };
	auto iter { selected.cbegin() };
	Gtk::TreeModel::iterator iter_tm { labels_treemodel->get_iter(*iter) };
	Gtk::TreeModel::Row row { *iter_tm };
	bool offwork = true;
	try {
		OffWork ow { database->get_offwork(row[label_tmcr.full_str]) };
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_remove_label_button_clicked() " << dbe.get_message() << std::endl;
		return;
	} catch (const Database::DBEmptyResultException& dbe) {
		offwork = false;
	}
	bool license = true;
	try {
		LicenseType lt { database->get_license_type(row[label_tmcr.full_str]) };
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_remove_label_button_clicked() " << dbe.get_message() << std::endl;
		return;
	} catch (const Database::DBEmptyResultException& dbe) {
		license = false;
	}
	if (offwork || license) {
		Gtk::MessageDialog alert_dialog { *this, "L'etichetta selezionata non può essere eliminata perché è in uso.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_CANCEL, true };
		alert_dialog.run();
		return;
	}
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {	
		try {	
			database->delete_label(row[label_tmcr.full_str]);
			labels_liststore->erase(iter_tm);
		} catch (const Database::DBError& dbe) {
			std::cerr << "LabelsDialog::on_remove_label_button_clicked() " << dbe.get_message() << std::endl;
			return;
		} catch (const Database::DBEmptyResultException& dbe) {
			std::cerr << "LabelsDialog::on_remove_label_button_clicked() " << dbe.get_message() << std::endl;
		}
	}
}

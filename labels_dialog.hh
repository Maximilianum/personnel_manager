/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * labels_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LABELS_DIALOG_HH_
#define _LABELS_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "label_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class LabelsDialog : public ControlDialog {
 public:
	inline LabelsDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline LabelsDialog(const LabelsDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(LabelsDialog* sd);
	void load_labels_in_treeview(void);
	inline void on_labels_treeselection_changed(void) { remove_label_button->set_sensitive(labels_treeselection->count_selected_rows() > 0); }
	void on_add_label_button_clicked(void);
	void on_label_short_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_label_full_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_remove_label_button_clicked(void);

	Gtk::Box* labels_box;
	Gtk::TreeView* labels_treeview;
	Glib::RefPtr<Gtk::ListStore> labels_liststore;
	Glib::RefPtr<Gtk::TreeSelection> labels_treeselection;
	Gtk::TreeViewColumn* label_short_treeviewcolumn;
	Gtk::CellRendererText* label_short_cellrenderertext;
	Gtk::TreeViewColumn* label_full_treeviewcolumn;
	Gtk::CellRendererText* label_full_cellrenderertext;
	Gtk::Button* add_label_button;
	Gtk::Button* remove_label_button;
	LabelTreeModelColumnRecord label_tmcr;
	Database* database;
};

#endif // _LABELS_DIALOG_HH_

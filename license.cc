/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license.cc
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "license.hh"

Glib::ustring License::sqlite_table { Glib::ustring("TABLE LICENSE ("	\
													"MEMBER TEXT NOT NULL, " \
													"LABEL TEXT NOT NULL, " \
													"REF_YEAR INTEGER NOT NULL," \
													"START INTEGER NOT NULL, " \
													"DEADLINE INTEGER NOT NULL, " \
													"DAYS INTEGER NOT NULL, " \
													"PRIMARY KEY (MEMBER, LABEL, REF_YEAR), " \
													"FOREIGN KEY(MEMBER) REFERENCES MEMBER(ID), " \
													"FOREIGN KEY(LABEL) REFERENCES LABEL(FULL));") };

int License::retrieve_license(void *data, int argc, char **argv, char **azColName) {
	License lcn;
	std::vector<License>* licenses { static_cast<std::vector<License>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("MEMBER") && argv[i]) {
			lcn.set_member(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			lcn.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			lcn.set_short_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("REF_YEAR") && argv[i]) {
			lcn.set_ref_year(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("START") && argv[i]) {
			lcn.set_start_date(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("DEADLINE") && argv[i]) {
			lcn.set_deadline_date(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("DAYS") && argv[i]) {
			lcn.set_days(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLAGS") && argv[i]) {
			lcn.set_flags(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("WEEKDAY") && argv[i]) {
			lcn.set_weekday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SATURDAY") && argv[i]) {
			lcn.set_saturday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SUNDAY") && argv[i]) {
			lcn.set_sunday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("HOLIDAY") && argv[i]) {
			lcn.set_holiday(atoi(argv[i]));
		}
	}
	licenses->push_back(lcn);
	return 0;
}

/**
 * 
 * operators
 * 
 */

License& License::operator=(const License &lcn) {
	if (this != &lcn) {
		member = lcn.member;
		flabel = lcn.flabel;
		slabel = lcn.slabel;
		ref_year = lcn.ref_year;
		start_date = lcn.start_date;
		deadline_date = lcn.deadline_date;
		days = lcn.days;
		flags = lcn.flags;
	}
	return *this;
}

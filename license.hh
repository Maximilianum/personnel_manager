/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license.hh
 * Copyright (C) 2016 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LICENSE_HH_
#define _LICENSE_HH_

#include <vector>
#include <glibmm/ustring.h>
#include "license_type.hh"
#include "custom_date.hh"

class License : public LicenseType {
 public:
	// static class functions
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_license(void *data, int argc, char **argv, char **azColName);
	// constructors
	inline License() : LicenseType(), ref_year (-1) {}
	inline License(const Glib::ustring &str1, const Glib::ustring &str2, const Glib::ustring &str3, int ry, const CustomDate& sd, const CustomDate& dd, int val, std::uint8_t flgs): LicenseType(str2, str3, flgs), member (str1), ref_year (ry), start_date (sd), deadline_date (dd), days (val) {}
	// operators
	License& operator=(const License& lcn);
	inline bool operator<(const License& lcn) const { return flabel < lcn.flabel || (flabel == lcn.flabel && ref_year < lcn.ref_year); }
	inline bool operator==(const License& lcn) const { return flabel == lcn.flabel && member == lcn.member && ref_year == lcn.ref_year; }
	// class member functions
	inline void set_member(const Glib::ustring& str) { member.assign(str); }
	inline Glib::ustring get_member(void) const { return member; }
	inline void set_ref_year(int val) { ref_year = val; }
	inline int get_ref_year(void) const { return ref_year; }
	inline void set_start_date(const CustomDate& cd) { start_date = cd; }
	inline void set_start_date(const char* str) { start_date = CustomDate { str }; }
	inline CustomDate get_start_date(void) const { return start_date; }
	inline void set_deadline_date(const CustomDate& cd) { deadline_date = cd; }
	inline void set_deadline_date(const char* str) { deadline_date = CustomDate { str }; }
	inline CustomDate get_deadline_date(void) const { return deadline_date; }
	inline void set_days(int val) { days = val; }
	inline int get_days(void) const { return days; }
 private:
	static Glib::ustring sqlite_table;
	Glib::ustring member;
	int ref_year;
	CustomDate start_date;
	CustomDate deadline_date;
	int days;
};

#endif // _LICENSE_HH_


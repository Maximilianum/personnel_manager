/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_dialog.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "license_dialog.hh"
#include <gtkmm/messagedialog.h>

void LicenseDialog::build_gui(LicenseDialog* ld) {
	Gtk::Box* box = ld->get_content_area();
	ld->bldr->get_widget("license_box", ld->license_box);
	ld->license_label_liststore = Gtk::ListStore::create(ld->combobox_tmcr);
	ld->bldr->get_widget("license_label_combobox", ld->license_label_combobox);
	ld->license_label_combobox->set_model(ld->license_label_liststore);
	ld->bldr->get_widget("license_year_entry", ld->license_year_entry);
	ld->bldr->get_widget("license_start_entry", ld->license_start_entry);
	ld->bldr->get_widget("license_deadline_entry", ld->license_deadline_entry);
	ld->bldr->get_widget("license_days_entry", ld->license_days_entry);
	ld->bldr->get_widget("cancel_license_button", ld->cancel_license_button);
	ld->bldr->get_widget("confirm_license_button", ld->confirm_license_button);
	box->pack_start(*(ld->license_box));
	/*
	 * load values
	 */
	std::vector<Glib::ustring> labels;
	try {
		labels = ld->database->get_license_types_label();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseDialog::build_gui() " << dbe.get_message() << std::endl;
	}
	if (labels.size() > 0) {
		ld->load_data_combobox(labels.cbegin(), labels.cend(), ld->license_label_liststore);
		ld->license_label_combobox->set_active(0);
	} else {
		ld->confirm_license_button->set_sensitive(false);
	}
	/*
	 * signals connections
	 */
	ld->confirm_license_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseDialog::on_confirm_button_clicked));
	ld->cancel_license_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseDialog::on_cancel_button_clicked));
}

void LicenseDialog::set_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year) {
	License license;
	if (id.compare(Glib::ustring()) != 0) {
		try {
			license = database->get_license(id, label, ref_year);
		} catch (const Database::DBError& dbe) {
			std::cerr << "LicenseDialog::set_license " << dbe.get_message() << std::endl;
		} catch (const Database::DBEmptyResultException& dbe) {}
		if (license.get_full_label().compare(Glib::ustring()) == 0) {
			license_year_entry->set_text(Glib::ustring::format(ref_year));
			license_start_entry->set_text(Glib::ustring::compose("01/01/%1",ref_year));
			license_deadline_entry->set_text(Glib::ustring::compose("31/12/%1",ref_year));
			license_days_entry->set_text(Glib::ustring::format(32));
		} else {
			Gtk::TreeModel::iterator iter { get_combobox_item(license_label_liststore, license.get_full_label()) };
			license_label_combobox->set_active(iter);
			license_year_entry->set_text(Glib::ustring::format(license.get_ref_year()));
			license_start_entry->set_text(license.get_start_date().get_formatted_string(DATE_FORMAT::date));
			license_deadline_entry->set_text(license.get_deadline_date().get_formatted_string(DATE_FORMAT::date));
			license_days_entry->set_text(Glib::ustring::format(license.get_days()));
		}
		m_id.assign(id);
	}
}

License LicenseDialog::get_license() const {
	if (!check_consistency()) {
		throw CDEmptyFieldException { std::string { "LicenseDialog::get_license() license's data unconsistent!" } };
	}
	Gtk::TreeModel::iterator itr { license_label_combobox->get_active() };
	Gtk::TreeModel::Row row { *itr };
	Glib::ustring label { row[combobox_tmcr.column] };
	int year = atoi(license_year_entry->get_text().c_str());
	CustomDate cds, cdd;
	try {
		cds = CustomDate::parse_string(license_start_entry->get_text().c_str());
		cdd = CustomDate::parse_string(license_deadline_entry->get_text().c_str());
	} catch (const CustomDate::CustomDateException& cde) {
		std::cerr << "LicenseDialog::get_license() " << cde.get_message() << std::endl;
	}
	int days = atoi(license_days_entry->get_text().c_str());
	std::uint8_t flags;
	Glib::ustring slabel;
	try {
		LicenseType lt { database->get_license_type(label) };
		flags = lt.get_flags();
		slabel = lt.get_short_label();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseDialog::get_license() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseDialog::get_license() " << dbe.get_message() << std::endl;
	}
	License license { m_id, label, slabel, year, cds, cdd, days, flags };
	return license;
}

bool LicenseDialog::check_consistency() const {
	if (license_label_combobox->get_active_row_number() == -1) {
		return false;
	}
	if (license_year_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (license_start_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (license_deadline_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (license_days_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	return true;
}

void LicenseDialog::on_confirm_button_clicked(void) {
	bool success = true;
	License license;
	try {
		license = get_license();
	} catch (const LicenseDialog::CDEmptyFieldException& ef) {
		Gtk::MessageDialog alert_dialog { *this, "Tutti i campi devono essere compilati!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		alert_dialog.run();
		success = false;
	}
	if (success) {
		License lic;
		bool id_exist = false;
		try {
			lic = database->get_license(m_id,license.get_full_label(),license.get_ref_year());
			id_exist = true;
		} catch (const Database::DBError& dbe) {
			std::cerr << "LicenseDialog::on_confirm_button_clicked() " << dbe.get_message() << std::endl;
			return;
		} catch (const Database::DBEmptyResultException& dbe) {
			id_exist = false;
		}
		if (id_exist) {
			Gtk::MessageDialog alert_dialog { *this, "Questa licenza è già presente, vuoi aggiornarla con questi dati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
			int res_id = alert_dialog.run();
			if (res_id == Gtk::RESPONSE_YES) {
				try {
					database->update_license(license, m_id, license.get_full_label(), license.get_ref_year());
				} catch (const Database::DBError& dbe) {
					std::cerr << "LicenseDialog::on_confirm_button_clicked() " << dbe.get_message() << std::endl;
					return;
				}
			}
		} else {
			try {
				database->insert(license);
			} catch (const Database::DBError& dbe) {
				std::cerr << "LicenseDialog::on_confirm_button_clicked() " << dbe.get_message() << std::endl;
				return;
			}
		}
	}
	response(Gtk::RESPONSE_APPLY);
}

void LicenseDialog::on_cancel_button_clicked(void) {
	response(Gtk::RESPONSE_CANCEL);
}

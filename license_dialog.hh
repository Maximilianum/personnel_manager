/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_dialog.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LICENSE_DIALOG_HH_
#define _LICENSE_DIALOG_HH_

#include "control_dialog.hh"
#include <gtkmm/combobox.h>
#include <gtkmm/entry.h>
#include "database.hh"

class LicenseDialog : public ControlDialog {
 public:
	inline LicenseDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline LicenseDialog(const LicenseDialog& ld) : ControlDialog(ld) { build_gui(this); }
	void set_license(const Glib::ustring& id, const Glib::ustring& label, int ref_year);
	inline void set_ref_year(int val) { license_year_entry->set_text(Glib::ustring::format(val)); }
	inline void set_start_date(const CustomDate& cd) { license_start_entry->set_text(cd.get_formatted_string(DATE_FORMAT::date)); }
	inline void set_deadline_date(const CustomDate& cd) { license_deadline_entry->set_text(cd.get_formatted_string(DATE_FORMAT::date)); }
	License get_license() const;
	bool check_consistency() const;
	void on_confirm_button_clicked();
	void on_cancel_button_clicked();
 private:
	static void build_gui(LicenseDialog* ld);

	Gtk::Box* license_box;
	Gtk::ComboBox* license_label_combobox;
	Glib::RefPtr<Gtk::ListStore> license_label_liststore;
	Gtk::Entry* license_year_entry;
	Gtk::Entry* license_start_entry;
	Gtk::Entry* license_deadline_entry;
	Gtk::Entry* license_days_entry;
	Gtk::Button* cancel_license_button;
	Gtk::Button* confirm_license_button;
	Database* database;
	Glib::ustring m_id;
};

#endif // _LICENSE_DIALOG_HH_

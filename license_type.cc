/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_type.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "license_type.hh"
#include <vector>
#include <set>

Glib::ustring LicenseType::sqlite_table { Glib::ustring("TABLE LICENSE_TYPE (" \
													"LABEL TEXT PRIMARY KEY NOT NULL, " \
													"FLAGS INTEGER NOT NULL, "	\
													"FOREIGN KEY(LABEL) REFERENCES LABEL(FULL));") };

int LicenseType::retrieve_license_type(void *data, int argc, char **argv, char **azColName) {
	LicenseType lt;
	std::vector<LicenseType>* licenses { static_cast<std::vector<LicenseType>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			lt.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			lt.set_short_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLAGS") && argv[i]) {
			lt.set_flags(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("WEEKDAY") && argv[i]) {
			lt.set_weekday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SATURDAY") && argv[i]) {
			lt.set_saturday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SUNDAY") && argv[i]) {
			lt.set_sunday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("HOLIDAY") && argv[i]) {
			lt.set_holiday(atoi(argv[i]));
		}
	}
	licenses->push_back(lt);
	return 0;
}

int LicenseType::retrieve_license_type_set(void *data, int argc, char **argv, char **azColName) {
	LicenseType lt;
	std::set<LicenseType>* licenses { static_cast<std::set<LicenseType>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			lt.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			lt.set_short_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLAGS") && argv[i]) {
			lt.set_flags(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("WEEKDAY") && argv[i]) {
			lt.set_weekday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SATURDAY") && argv[i]) {
			lt.set_saturday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SUNDAY") && argv[i]) {
			lt.set_sunday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("HOLIDAY") && argv[i]) {
			lt.set_holiday(atoi(argv[i]));
		}
	}
	licenses->insert(licenses->end(),lt);
	return 0;
}

int LicenseType::retrieve_label(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring str;
	std::vector<Glib::ustring>* labels { static_cast<std::vector<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			str.assign(argv[i]);
		}
	}
	labels->push_back(str);
	return 0;
}

LicenseType& LicenseType::operator=(const LicenseType& lt) {
	if (this != &lt) {
		flabel = lt.flabel;
		slabel = lt.slabel;
		flags = lt.flags;
	}
	return *this;
}

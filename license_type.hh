/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_type.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LICENSE_TYPE_HH_
#define _LICENSE_TYPE_HH_

#include <cstdint>
#include "label.hh"

class LicenseType : public Label {
 public:
	/*
	 * flags bit mask
	 * 0 weekday
	 * 1 saturday
	 * 2 sunday
	 * 3 holyday
	 * 4 visible
	 */
	static constexpr std::uint8_t mask0 { 1 << 0 };
	static constexpr std::uint8_t mask1 { 1 << 1 };
	static constexpr std::uint8_t mask2 { 1 << 2 };
	static constexpr std::uint8_t mask3 { 1 << 3 };
	static constexpr std::uint8_t mask4 { 1 << 4 };
	
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_license_type(void *data, int argc, char **argv, char **azColName);
	static int retrieve_license_type_set(void *data, int argc, char **argv, char **azColName);
	static int retrieve_label(void *data, int argc, char **argv, char **azColName);
	inline LicenseType() : flags (0) {};
	inline LicenseType(const LicenseType& lt) : Label(lt.flabel, lt.slabel), flags (lt.flags) {}
	inline LicenseType(const Glib::ustring& str1, const Glib::ustring& str2, std::uint8_t flgs) : Label(str1, str2), flags (flgs) {}
	LicenseType& operator=(const LicenseType& lt);
	inline bool operator<(const LicenseType& lt) const { return flabel < lt.flabel; }
	inline bool operator==(const LicenseType& lt) const { return flabel.compare(lt.flabel) == 0 && slabel.compare(lt.slabel) == 0 && flags == lt.flags; }
	inline void set_flags(std::uint8_t flgs) { flags = flgs; }
	inline std::uint8_t get_flags() const { return flags; }
	inline void set_weekday(bool flg) { flags = flg ? flags | mask0 : flags & ~mask0; }
	inline bool get_weekday(void) const { return flags & mask0; }
	inline void set_saturday(bool flg) { flags = flg ? flags | mask1 : flags & ~mask1; }
	inline bool get_saturday(void) const { return flags & mask1; }
	inline void set_sunday(bool flg) { flags = flg ? flags | mask2 : flags & ~mask2; }
	inline bool get_sunday(void) const { return flags & mask2; }
	inline void set_holiday(bool flg) { flags = flg ? flags | mask3 : flags & ~mask3; }
	inline bool get_holiday(void) const { return flags & mask3; }
	inline void set_visible(bool flg) { flags = flg ? flags | mask4 : flags & ~mask4; }
	inline bool get_visible(void) const { return flags & mask4; }
 protected:
	std::uint8_t flags;
 private:
	static Glib::ustring sqlite_table;
	
};

#endif // _LICENSE_TYPE_HH_


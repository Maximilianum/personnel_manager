/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_type_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "license_type_dialog.hh"
#include <gtkmm/messagedialog.h>

void LicenseTypeDialog::load_data_combobox(std::set<Glib::ustring>::const_iterator start, std::set<Glib::ustring>::const_iterator end, Glib::RefPtr<Gtk::ListStore> liststore) {
	liststore->clear();
	for (auto iter = start; iter != end; iter++) {
		Gtk::TreeModel::iterator tmi = liststore->append();
		Gtk::TreeModel::Row row = *tmi;
		row[combobox_tmcr.column] = *iter;
	}
}

void LicenseTypeDialog::build_gui(LicenseTypeDialog* ld) {
	Gtk::Box* main_box = ld->get_content_area();
	ld->bldr->get_widget("box", ld->box);
	ld->bldr->get_widget("label_combobox", ld->label_combobox);
	ld->label_liststore = Gtk::ListStore::create(ld->combobox_tmcr);
	ld->label_combobox->set_model(ld->label_liststore);
	ld->bldr->get_widget("frame_label", ld->frame_label);
	ld->bldr->get_widget("confirm_button", ld->confirm_button);
	ld->bldr->get_widget("cancel_button", ld->cancel_button);
	main_box->pack_start(*(ld->box));
	/*
	 * load values
	 */
	ld->frame_label->set_text("Etichetta");
	std::set<Glib::ustring> labels;
	std::vector<Glib::ustring> offworks;
	std::vector<Glib::ustring> licenses;
	try {
		labels = ld->database->get_labels_string_set();
		offworks = ld->database->get_offworks_label();
		licenses = ld->database->get_license_types_label();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypeDialog::build_gui() " << dbe.get_message() << std::endl;
	}
	for (auto iter = offworks.cbegin(); iter != offworks.cend(); iter++) {
		labels.erase(*iter);
	}
	for (auto iter = licenses.cbegin(); iter != licenses.cend(); iter++) {
		labels.erase(*iter);
	}
	if (labels.size() > 0) {
		ld->load_data_combobox(labels.cbegin(), labels.cend(), ld->label_liststore);
		ld->label_combobox->set_active(0);
	} else {
		ld->confirm_button->set_sensitive(false);
	}
	/*
	 * signals connections
	 */
	ld->confirm_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseTypeDialog::on_confirm_button_clicked));
	ld->cancel_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseTypeDialog::on_cancel_button_clicked));
}

void LicenseTypeDialog::on_confirm_button_clicked(void) {
	Gtk::TreeModel::iterator it { label_combobox->get_active() };
	Gtk::TreeModel::Row row { *it };
	try {
		Label label { database->get_label(row[combobox_tmcr.column]) };
		LicenseType lt { label.get_full_label(), label.get_short_label(), 0 };
		database->insert(lt);
		response(Gtk::RESPONSE_APPLY);
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypeDialog::on_confirm_button_clicked() " << dbe.get_message() << std::endl;
		return;
	}
}

void LicenseTypeDialog::on_cancel_button_clicked(void) {
	response(Gtk::RESPONSE_CANCEL);
}

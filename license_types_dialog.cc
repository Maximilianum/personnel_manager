/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_types_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "license_types_dialog.hh"
#include <gtkmm/messagedialog.h>

void LicenseTypesDialog::build_gui(LicenseTypesDialog* ld) {
	Gtk::Box* box = ld->get_content_area();
	ld->bldr->get_widget("license_types_box", ld->license_types_box);
	ld->bldr->get_widget("license_types_treeview", ld->license_types_treeview);
	ld->license_types_liststore = Gtk::ListStore::create(ld->license_type_tmcr);
	ld->license_types_treeview->set_model(ld->license_types_liststore);
	ld->license_type_label_treeviewcolumn = ld->license_types_treeview->get_column(0);
	ld->license_type_label_cellrenderertext = (Gtk::CellRendererText*)ld->license_type_label_treeviewcolumn->get_first_cell();
	ld->license_types_treeview->append_column_editable(Glib::ustring("Fer."), ld->license_type_tmcr.weekday);
	ld->license_type_weekday_treeviewcolumn = ld->license_types_treeview->get_column(1);
	ld->license_type_weekday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->license_type_weekday_treeviewcolumn->get_first_cell();
	ld->license_types_treeview->append_column_editable(Glib::ustring("Sab."), ld->license_type_tmcr.saturday);
	ld->license_type_saturday_treeviewcolumn = ld->license_types_treeview->get_column(2);
	ld->license_type_saturday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->license_type_saturday_treeviewcolumn->get_first_cell();
	ld->license_types_treeview->append_column_editable(Glib::ustring("Dom."), ld->license_type_tmcr.sunday);
	ld->license_type_sunday_treeviewcolumn = ld->license_types_treeview->get_column(3);
	ld->license_type_sunday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->license_type_sunday_treeviewcolumn->get_first_cell();
	ld->license_types_treeview->append_column_editable(Glib::ustring("Fest."), ld->license_type_tmcr.holiday);
	ld->license_type_holiday_treeviewcolumn = ld->license_types_treeview->get_column(4);
	ld->license_type_holiday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->license_type_holiday_treeviewcolumn->get_first_cell();
	ld->license_types_treeview->append_column_editable(Glib::ustring("Visibile"), ld->license_type_tmcr.visible);
	ld->license_type_visible_treeviewcolumn = ld->license_types_treeview->get_column(5);
	ld->license_type_visible_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->license_type_visible_treeviewcolumn->get_first_cell();
	ld->license_types_treeselection = ld->license_types_treeview->get_selection();
	ld->license_types_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	ld->bldr->get_widget("add_license_type_button", ld->add_license_type_button);
	ld->bldr->get_widget("remove_license_type_button", ld->remove_license_type_button);
	box->pack_start(*(ld->license_types_box));
	/*
	 * load values
	 */
	ld->load_license_types_in_treeview();
	/*
	 * signals connections
	 */
	ld->license_types_treeselection->signal_changed().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_license_types_treeselection_changed));
	ld->add_license_type_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_add_license_type_button_clicked));
	ld->license_type_weekday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_weekday_toggled));
	ld->license_type_saturday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_saturday_toggled));
	ld->license_type_sunday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_sunday_toggled));
	ld->license_type_holiday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_holiday_toggled));
	ld->license_type_visible_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_visible_toggled));
	ld->remove_license_type_button->signal_clicked().connect(sigc::mem_fun(ld, &LicenseTypesDialog::on_remove_license_type_button_clicked));
}

void LicenseTypesDialog::load_license_types_in_treeview(void) {
	license_types_liststore->clear();
	try {
		std::vector<LicenseType> license_types { database->get_license_types() };
		for (auto iter = license_types.cbegin(); iter != license_types.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { license_types_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[license_type_tmcr.label] = iter->get_full_label();
			row[license_type_tmcr.weekday] = iter->get_weekday();
			row[license_type_tmcr.saturday] = iter->get_saturday();
			row[license_type_tmcr.sunday] = iter->get_sunday();
			row[license_type_tmcr.holiday] = iter->get_holiday();
			row[license_type_tmcr.visible] = iter->get_visible();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::load_data_in_license_type_treeview() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_add_license_type_button_clicked(void) {
	LicenseTypeDialog license_type_dialog { LicenseTypeDialog(Glib::ustring::compose("%1item_picker_dialog.ui",ui_path), *this, database) };
	int res_id { license_type_dialog.run() };
	if (res_id == Gtk::RESPONSE_APPLY) {
		load_license_types_in_treeview();
	}
}

void LicenseTypesDialog::on_weekday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> license_types_model { license_types_treeview->get_model() };
	Gtk::TreeModel::iterator iter { license_types_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		LicenseType lt { database->get_license_type(row[license_type_tmcr.label]) };
		lt.set_weekday(!lt.get_weekday());
		std::uint8_t flags { lt.get_flags() };
		database->update_license_type_flags(row[license_type_tmcr.label],flags);
		load_license_types_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_weekday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_weekday_toggled() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_saturday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> license_types_model { license_types_treeview->get_model() };
	Gtk::TreeModel::iterator iter { license_types_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		LicenseType lt { database->get_license_type(row[license_type_tmcr.label]) };
		lt.set_saturday(!lt.get_saturday());
		std::uint8_t flags { lt.get_flags() };
		database->update_license_type_flags(row[license_type_tmcr.label],flags);
		load_license_types_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_saturday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_saturday_toggled() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_sunday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> license_types_model { license_types_treeview->get_model() };
	Gtk::TreeModel::iterator iter { license_types_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		LicenseType lt { database->get_license_type(row[license_type_tmcr.label]) };
		lt.set_sunday(!lt.get_sunday());
		std::uint8_t flags { lt.get_flags() };
		database->update_license_type_flags(row[license_type_tmcr.label],flags);
		load_license_types_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_sunday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_sunday_toggled() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_holiday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> license_types_model { license_types_treeview->get_model() };
	Gtk::TreeModel::iterator iter { license_types_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		LicenseType lt { database->get_license_type(row[license_type_tmcr.label]) };
		lt.set_holiday(!lt.get_holiday());
		std::uint8_t flags { lt.get_flags() };
		database->update_license_type_flags(row[license_type_tmcr.label],flags);
		load_license_types_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_holiday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_holiday_toggled() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_visible_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> license_types_model { license_types_treeview->get_model() };
	Gtk::TreeModel::iterator iter { license_types_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		LicenseType lt { database->get_license_type(row[license_type_tmcr.label]) };
		lt.set_visible(!lt.get_visible());
		std::uint8_t flags { lt.get_flags() };
		database->update_license_type_flags(row[license_type_tmcr.label],flags);
		load_license_types_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_visible_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicenseTypesDialog::on_license_type_visible_toggled() " << dbe.get_message() << std::endl;
	}
}

void LicenseTypesDialog::on_remove_license_type_button_clicked(void) {
	Glib::RefPtr<Gtk::TreeModel> license_types_treemodel { license_types_treeview->get_model() };
	std::vector<Gtk::TreeModel::Path> selected { license_types_treeselection->get_selected_rows() };
	auto iter { selected.cbegin() };
	Gtk::TreeModel::iterator iter_tm { license_types_treemodel->get_iter(*iter) };
	Gtk::TreeModel::Row row { *iter_tm };
	std::vector<License> licenses;
	try {
		licenses = database->get_licenses_with_label(row[license_type_tmcr.label]);
	} catch (const Database::DBError& dbe) {
		std::cerr << "LabelsDialog::on_remove_label_button_clicked() " << dbe.get_message() << std::endl;
		return;
	}
	if (licenses.size() > 0) {
		Gtk::MessageDialog alert_dialog { *this, "La licenza selezionata non può essere eliminata perché è in uso.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_CANCEL, true };
		alert_dialog.run();
		return;
	} else {
		Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
		int res_id { alert_dialog.run() };
		if (res_id == Gtk::RESPONSE_YES) {
			try {
				database->delete_license_type(row[license_type_tmcr.label]);
				license_types_liststore->erase(iter_tm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "LicenseTypesDialog::on_remove_license_type_button_clicked() " << dbe.get_message() << std::endl;
				return;
			} catch (const Database::DBEmptyResultException& dbe) {
				std::cerr << "LicenseTypesDialog::on_remove_license_type_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

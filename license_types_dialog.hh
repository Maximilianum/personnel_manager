/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * license_types_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LICENSE_TYPES_DIALOG_HH_
#define _LICENSE_TYPES_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "license_type_dialog.hh"
#include "license_type_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class LicenseTypesDialog : public ControlDialog {
 public:
	inline LicenseTypesDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline LicenseTypesDialog(const LicenseTypesDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(LicenseTypesDialog* sd);
	void load_license_types_in_treeview(void);
	inline void on_license_types_treeselection_changed(void) { remove_license_type_button->set_sensitive(license_types_treeselection->count_selected_rows() > 0); }
	void on_add_license_type_button_clicked(void);
	void on_weekday_toggled(const Glib::ustring& path);
	void on_saturday_toggled(const Glib::ustring& path);
	void on_sunday_toggled(const Glib::ustring& path);
	void on_holiday_toggled(const Glib::ustring& path);
	void on_visible_toggled(const Glib::ustring& path);
	void on_remove_license_type_button_clicked(void);

	Gtk::Box* license_types_box;
	Gtk::TreeView* license_types_treeview;
	Glib::RefPtr<Gtk::ListStore> license_types_liststore;
	Glib::RefPtr<Gtk::TreeSelection> license_types_treeselection;
	Gtk::TreeViewColumn* license_type_label_treeviewcolumn;
	Gtk::CellRendererText* license_type_label_cellrenderertext;
	Gtk::TreeViewColumn* license_type_weekday_treeviewcolumn;
	Gtk::CellRendererToggle* license_type_weekday_cellrenderertoggle;
	Gtk::TreeViewColumn* license_type_saturday_treeviewcolumn;
	Gtk::CellRendererToggle* license_type_saturday_cellrenderertoggle;
	Gtk::TreeViewColumn* license_type_sunday_treeviewcolumn;
	Gtk::CellRendererToggle* license_type_sunday_cellrenderertoggle;
	Gtk::TreeViewColumn* license_type_holiday_treeviewcolumn;
	Gtk::CellRendererToggle* license_type_holiday_cellrenderertoggle;
	Gtk::TreeViewColumn* license_type_visible_treeviewcolumn;
	Gtk::CellRendererToggle* license_type_visible_cellrenderertoggle;
	Gtk::Button* add_license_type_button;
	Gtk::Button* remove_license_type_button;
	LicenseTypeTreeModelColumnRecord license_type_tmcr;
	Database* database;
};

#endif // _LICENSE_TYPES_DIALOG_HH_

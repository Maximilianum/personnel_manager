/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * licenses_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "licenses_dialog.hh"
#include <gtkmm/messagedialog.h>

void LicensesDialog::build_gui(LicensesDialog* ld) {
	Gtk::Box* box = ld->get_content_area();
	ld->member_liststore = Gtk::ListStore::create(ld->combobox2_tmcr);
	ld->bldr->get_widget("member_combobox", ld->member_combobox);
	ld->member_combobox->set_model(ld->member_liststore);
	ld->bldr->get_widget("licenses_box", ld->licenses_box);
	ld->license_liststore = Gtk::ListStore::create(ld->license_tmcr);
	ld->bldr->get_widget("license_treeview", ld->license_treeview);
	ld->license_treeview->set_model(ld->license_liststore);
	ld->license_treeselection = ld->license_treeview->get_selection();
	ld->license_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	ld->bldr->get_widget("add_license_button", ld->add_license_button);
	ld->bldr->get_widget("remove_license_button", ld->remove_license_button);
	box->pack_start(*(ld->licenses_box));
	/*
	 * load values
	 */
	ld->load_members_in_combobox();
	ld->member_combobox->set_active(0);
	ld->load_license_in_treeview();
	/*
	 * signals connections
	 */
	ld->member_combobox->signal_changed().connect(sigc::mem_fun(ld, &LicensesDialog::load_license_in_treeview));
	ld->license_treeselection->signal_changed().connect(sigc::mem_fun(ld, &LicensesDialog::on_license_treeselection_changed));
	ld->license_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(ld, &LicensesDialog::on_license_treeview_clicked));
	ld->add_license_button->signal_clicked().connect(sigc::mem_fun(ld, &LicensesDialog::on_add_license_button_clicked));
	ld->remove_license_button->signal_clicked().connect(sigc::mem_fun(ld, &LicensesDialog::on_remove_license_button_clicked));
}

void LicensesDialog::load_members_in_combobox() const {
	member_liststore->clear();
	try {
		auto members { database->get_members() };
		for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { member_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[combobox2_tmcr.column1] = Glib::ustring::compose("%1 %2 %3",iter->get_rank().get_short_label(),iter->get_first_name(),iter->get_last_name());
			row[combobox2_tmcr.column2] = iter->get_id();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicensesDialog::load_members_in_combobox() " << dbe.get_message() << std::endl;
	}
}

Glib::ustring LicensesDialog::get_selected_member() const {
	Gtk::TreeModel::iterator iter { member_combobox->get_active() };
	if (iter != member_liststore->children().end()) {
		Gtk::TreeModel::Row row { *iter };
		return row[combobox2_tmcr.column2];
	}
	return Glib::ustring();
}

void LicensesDialog::load_license_in_treeview() const {
	license_liststore->clear();
	Glib::ustring id { get_selected_member() };
	if (id.compare(Glib::ustring()) != 0) {
		try {
			std::vector<License> licenses { database->get_licenses_for_member(id) };
			for (auto lm_iter = licenses.begin(); lm_iter != licenses.end(); lm_iter++) {
				Gtk::TreeModel::iterator iter_list = license_liststore->append();
				Gtk::TreeModel::Row row = *iter_list;
				row[license_tmcr.label] = lm_iter->get_full_label();
				row[license_tmcr.year] = Glib::ustring::format(lm_iter->get_ref_year());
				row[license_tmcr.start] = Glib::ustring(lm_iter->get_start_date().get_formatted_string(DATE_FORMAT::date));
				row[license_tmcr.deadline] = Glib::ustring(lm_iter->get_deadline_date().get_formatted_string(DATE_FORMAT::date));
				row[license_tmcr.days] = Glib::ustring::format(lm_iter->get_days());
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "LicensesDialog::load_license_in_treeview() " << dbe.get_message() << std::endl;
		} catch (const Database::DBEmptyResultException& dbe) {}
	}
}

void LicensesDialog::on_license_treeview_clicked(GdkEventButton *event) {
	if (event->button == 1 && event->type == GDK_2BUTTON_PRESS)	{
		Glib::ustring id { get_selected_member() };
		try {
			Gtk::TreeModel::iterator iter_tm { get_selected_license() };
			Gtk::TreeModel::Row row { *iter_tm };
			LicenseDialog license_dialog { LicenseDialog(Glib::ustring::compose("%1license_dialog.ui",ui_path), *this, database) };
			license_dialog.set_license(id, row[license_tmcr.label], atoi(Glib::ustring(row[license_tmcr.year]).c_str()));
			int res_id { license_dialog.run() };
			if (res_id == Gtk::RESPONSE_APPLY) {
				load_license_in_treeview();
			}
		} catch (IteratorNotFoundException infe) {}
	}
}

Gtk::TreeModel::iterator LicensesDialog::get_selected_license() {
	int nr_sel = license_treeselection->count_selected_rows();
	Glib::RefPtr<Gtk::TreeModel> license_member_treemodel = license_treeview->get_model();
	if (nr_sel > 0)	{
		std::vector<Gtk::TreePath> selected = license_treeselection->get_selected_rows();
		std::vector<Gtk::TreePath>::const_iterator iter = selected.begin();
		return license_member_treemodel->get_iter(*iter);
	} else {
		throw IteratorNotFoundException { std::string { "LicensesDialog::get_selected_license() iterator not found." } };
	}
}

void LicensesDialog::on_add_license_button_clicked(void) {
	LicenseDialog license_dialog { LicenseDialog(Glib::ustring::compose("%1license_dialog.ui",ui_path), *this, database) };
	time_t dt = time(nullptr);
	CustomDate date { &dt };
	license_dialog.set_license(get_selected_member(),Glib::ustring(),1900 + date.get_year());
	int res_id { license_dialog.run() };
	if (res_id == Gtk::RESPONSE_APPLY) {
		load_license_in_treeview();
	}
}

void LicensesDialog::on_remove_license_button_clicked(void) {
	Glib::ustring id { get_selected_member() };
	Gtk::TreeModel::iterator iter_tm;
	Gtk::TreeModel::Row row;
	try {
		iter_tm = get_selected_license();
		row = *iter_tm;
		std::vector<Shift> shifts { database ->get_license_shifts_for_member(id, row[license_tmcr.label], atoi(Glib::ustring(row[license_tmcr.year]).c_str())) };
		if (shifts.size() > 0) {
			Gtk::MessageDialog alert_dialog { *this, "La licenza selezionata non può essere eliminata perché è in uso.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_CANCEL, true };
			alert_dialog.run();
			return;
		}
	} catch (IteratorNotFoundException infe) {
		return;
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicensesDialog::on_remove_license_button_clicked() " << dbe.get_message() << std::endl;
		return;
	}
	try {
		Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
		int res_id { alert_dialog.run() };
		if (res_id == Gtk::RESPONSE_YES) {
			database->delete_license(id, row[license_tmcr.label], atoi(Glib::ustring(row[license_tmcr.year]).c_str()));
			license_liststore->erase(iter_tm);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "LicensesDialog::on_remove_license_button_clicked() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "LicensesDialog::on_remove_license_button_clicked() " << dbe.get_message() << std::endl;
	}
}

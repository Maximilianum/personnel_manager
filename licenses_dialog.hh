/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * licenses_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LICENSES_DIALOG_HH_
#define _LICENSES_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/combobox.h>
#include <gtkmm/button.h>
#include "dual_string_tmcr.hh"
#include "license_dialog.hh"
#include "license_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class LicensesDialog : public ControlDialog {
 public:
	class IteratorNotFoundException
	{
	public:
    IteratorNotFoundException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
	inline LicensesDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline LicensesDialog(const LicensesDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(LicensesDialog* ld);
	void load_members_in_combobox() const;
	Glib::ustring get_selected_member() const;
	void load_license_in_treeview() const;
	inline void on_license_treeselection_changed(void) { remove_license_button->set_sensitive(license_treeselection->count_selected_rows() > 0); }
	void on_license_treeview_clicked(GdkEventButton *event);
	Gtk::TreeModel::iterator get_selected_license();
	void on_add_license_button_clicked(void);
	void on_remove_license_button_clicked(void);

	Gtk::Box* licenses_box;
	Gtk::ComboBox* member_combobox;
	Glib::RefPtr<Gtk::ListStore> member_liststore;
	Glib::RefPtr<Gtk::ListStore> license_liststore;
	Gtk::TreeView* license_treeview;
	Glib::RefPtr<Gtk::TreeSelection> license_treeselection;
	Gtk::Button* add_license_button;
	Gtk::Button* remove_license_button;
	DualStringTreeModelColumnRecord combobox2_tmcr;
	LicenseTreeModelColumnRecord license_tmcr;
	Database* database;
};

#endif // _LICENSES_DIALOG_HH_

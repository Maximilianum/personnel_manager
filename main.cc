/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main.cc
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <gtkmm.h>
#include <iostream>
#include "main_window.hh"

#ifdef ENABLE_NLS
#include <libintl.h>
#endif

void copy_file(const Glib::ustring& filename, unsigned int src, unsigned int dst) {
	Glib::ustring src_filename { src == 0 ? filename : Glib::ustring::compose("%1.%2",filename,Glib::ustring::format(src)) };
	// retrieve informations from source file
	struct stat src_stat;
	int result = stat(src_filename.c_str(),&src_stat);
	if (result != 0) {
		perror("copy_file: ");
		return;
	}
	Glib::ustring dst_filename { Glib::ustring::compose("%1.%2",filename,Glib::ustring::format(dst)) };
	// check if destination file exists
	if (access(dst_filename.c_str(), F_OK) == 0) {
		// retrieve informations from destination file
		struct stat dst_stat;
		result = stat(dst_filename.c_str(),&dst_stat);
		if (result != 0) {
			perror("copy_file: ");
			return;
		}
		// copy to the next level of backup only if source file is newer than destination file
		if (src_stat.st_mtime > dst_stat.st_mtime) {
			if (dst < 3) {
				copy_file(filename, src + 1, dst + 1);
			}
		}
	}
	system(Glib::ustring::compose("cp -u %1 %2", src_filename, dst_filename).c_str());
}

void backup_db(const Glib::ustring& dbfile) {
	if (access(dbfile.c_str(), F_OK) == 0) {
		copy_file(dbfile, 0, 1);
	}
}

int main (int argc, char *argv[]) {
	Glib::RefPtr<Gtk::Application> app { Gtk::Application::create(Glib::ustring("org.maniscalco.") + APP_FILE_NAME, Gio::APPLICATION_FLAGS_NONE) };
  Glib::ustring data_path { Glib::get_home_dir() + Glib::ustring("/.local/share/") + APP_FILE_NAME };
  Glib::RefPtr<Gtk::Builder> bldr;
  Glib::ustring ui_path { Glib::ustring::compose("/usr/share/%1/ui/", APP_FILE_NAME) };
  for (int i = 1; i < argc; i++) {
    Glib::ustring option { argv[i] };
    if (option.compare(Glib::ustring("--devel")) == 0 || option.compare(Glib::ustring("-d")) == 0) {
      ui_path.assign(Glib::ustring());
    } else if (option.compare(Glib::ustring("--ramdisk")) == 0 || option.compare(Glib::ustring("-r")) == 0) {
      data_path.assign(Glib::ustring("/tmp/ramdisk/") + APP_FILE_NAME);
    }
  }
  Glib::RefPtr<Gio::File> data_file { Gio::File::create_for_path(data_path) };
  // try to create the main directory in .local/share/
  try {
    data_file->make_directory();
  } catch (const Gio::Error& error) {
    if (error.code() != Gio::Error::EXISTS) {
      std::cerr << error.what() << std::endl;
	  return -1;
    }
  }
  backup_db(data_path + DB_FILENAME);
  MainWindow::set_data_path(data_path);
  try {
	  bldr = Gtk::Builder::create_from_file(ui_path + Glib::ustring::compose("%1.ui", APP_FILE_NAME));
  } catch (const Glib::FileError& ex) {
    std::cerr << "CtrlEngine:build_gui() " << ex.what() << std::endl;
    return 1;
  } catch(const Gtk::BuilderError& ex) {
    std::cerr << "CtrlEngine:build_gui() " << ex.what() << std::endl;
    return 2;
  }
  MainWindow::set_resources_path(ui_path);
  MainWindow::set_app(app);
  MainWindow* ctrl;
  bldr->get_widget_derived("main_window", ctrl);
  if (ctrl) {
    app->run(*ctrl);
  }
  delete ctrl;
  return 0;
}

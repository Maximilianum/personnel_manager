/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main_tree_model_column_record.hh
 * Copyright (C) 2015 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MAIN_TREE_MODEL_COLUMN_RECORD_H_
#define _MAIN_TREE_MODEL_COLUMN_RECORD_H_

class MainTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
	 public:
		MainTreeModelColumnRecord() { add(id); add(rank); add(name);
									  add(day1); add(day2); add(day3); add(day4); add(day5); add(day6); add(day7); add(day8); add(day9); add(day10); add(day11); add(day12); add(day13); add(day14); add(day15); add(day16); add(day17); add(day18); add(day19); add(day20); add(day21); add(day22); add(day23); add(day24); add(day25); add(day26); add(day27); add(day28); add(day29); add(day30); add(day31); add(day32); add(day33); add(day34); add(day35); add(day36); add(day37); add(day38); add(day39); add(day40); add(day41); add(day42);
									  add(hours_work); add(hours_month); add(hours_extra);
									  add(bcolor1); add(bcolor2); add(bcolor3); add(bcolor4); add(bcolor5); add(bcolor6); add(bcolor7); add(bcolor8); add(bcolor9); add(bcolor10); add(bcolor11); add(bcolor12); add(bcolor13); add(bcolor14); add(bcolor15); add(bcolor16); add(bcolor17); add(bcolor18); add(bcolor19); add(bcolor20); add(bcolor21); add(bcolor22); add(bcolor23); add(bcolor24); add(bcolor25); add(bcolor26); add(bcolor27); add(bcolor28); add(bcolor29); add(bcolor30); add(bcolor31); add(bcolor32); add(bcolor33); add(bcolor34); add(bcolor35); add(bcolor36); add(bcolor37); add(bcolor38); add(bcolor39); add(bcolor40); add(bcolor41); add(bcolor42);
									  add(fcolor_rank); add(fcolor_name);
									  add(fcolor1); add(fcolor2); add(fcolor3); add(fcolor4); add(fcolor5); add(fcolor6); add(fcolor7); add(fcolor8); add(fcolor9); add(fcolor10); add(fcolor11); add(fcolor12); add(fcolor13); add(fcolor14); add(fcolor15); add(fcolor16); add(fcolor17); add(fcolor18); add(fcolor19); add(fcolor20); add(fcolor21); add(fcolor22); add(fcolor23); add(fcolor24); add(fcolor25); add(fcolor26); add(fcolor27); add(fcolor28); add(fcolor29); add(fcolor30); add(fcolor31); add(fcolor32); add(fcolor33); add(fcolor34); add(fcolor35); add(fcolor36); add(fcolor37); add(fcolor38); add(fcolor39); add(fcolor40); add(fcolor41); add(fcolor42);
									  add(fontsize_rank); add(fontsize_name);
									  add(fontsize1); add(fontsize2); add(fontsize3); add(fontsize4); add(fontsize5); add(fontsize6); add(fontsize7); add(fontsize8); add(fontsize9); add(fontsize10); add(fontsize11); add(fontsize12); add(fontsize13); add(fontsize14); add(fontsize15); add(fontsize16); add(fontsize17); add(fontsize18); add(fontsize19); add(fontsize20); add(fontsize21); add(fontsize22); add(fontsize23); add(fontsize24); add(fontsize25); add(fontsize26); add(fontsize27); add(fontsize28); add(fontsize29); add(fontsize30); add(fontsize31); add(fontsize32); add(fontsize33); add(fontsize34); add(fontsize35); add(fontsize36); add(fontsize37); add(fontsize38); add(fontsize39); add(fontsize40); add(fontsize41); add(fontsize42);
									  add(fontsize_hours_work); add(fontsize_hours_month); add(fontsize_hours_extra);
									  add(fontsize_extra_1); add(fontsize_extra_2); add(fontsize_extra_3); add(fontsize_extra_4); add(fontsize_extra_5);
									  add(extra_1); add(extra_2); add(extra_3); add(extra_4); add(extra_5); }
		Gtk::TreeModelColumn<Glib::ustring> id;			// 0
		Gtk::TreeModelColumn<Glib::ustring> rank;
		Gtk::TreeModelColumn<Glib::ustring> name;
		Gtk::TreeModelColumn<Glib::ustring> day1;		// 3
		Gtk::TreeModelColumn<Glib::ustring> day2;
		Gtk::TreeModelColumn<Glib::ustring> day3;
		Gtk::TreeModelColumn<Glib::ustring> day4;
		Gtk::TreeModelColumn<Glib::ustring> day5;
		Gtk::TreeModelColumn<Glib::ustring> day6;
		Gtk::TreeModelColumn<Glib::ustring> day7;
		Gtk::TreeModelColumn<Glib::ustring> day8;
		Gtk::TreeModelColumn<Glib::ustring> day9;
		Gtk::TreeModelColumn<Glib::ustring> day10;
		Gtk::TreeModelColumn<Glib::ustring> day11;
		Gtk::TreeModelColumn<Glib::ustring> day12;
		Gtk::TreeModelColumn<Glib::ustring> day13;
		Gtk::TreeModelColumn<Glib::ustring> day14;
		Gtk::TreeModelColumn<Glib::ustring> day15;
		Gtk::TreeModelColumn<Glib::ustring> day16;
		Gtk::TreeModelColumn<Glib::ustring> day17;
		Gtk::TreeModelColumn<Glib::ustring> day18;
		Gtk::TreeModelColumn<Glib::ustring> day19;
		Gtk::TreeModelColumn<Glib::ustring> day20;
		Gtk::TreeModelColumn<Glib::ustring> day21;
		Gtk::TreeModelColumn<Glib::ustring> day22;
		Gtk::TreeModelColumn<Glib::ustring> day23;
		Gtk::TreeModelColumn<Glib::ustring> day24;
		Gtk::TreeModelColumn<Glib::ustring> day25;
		Gtk::TreeModelColumn<Glib::ustring> day26;
		Gtk::TreeModelColumn<Glib::ustring> day27;
		Gtk::TreeModelColumn<Glib::ustring> day28;
		Gtk::TreeModelColumn<Glib::ustring> day29;
		Gtk::TreeModelColumn<Glib::ustring> day30;
		Gtk::TreeModelColumn<Glib::ustring> day31;
		Gtk::TreeModelColumn<Glib::ustring> day32;
		Gtk::TreeModelColumn<Glib::ustring> day33;
		Gtk::TreeModelColumn<Glib::ustring> day34;
		Gtk::TreeModelColumn<Glib::ustring> day35;
		Gtk::TreeModelColumn<Glib::ustring> day36;
		Gtk::TreeModelColumn<Glib::ustring> day37;
		Gtk::TreeModelColumn<Glib::ustring> day38;
		Gtk::TreeModelColumn<Glib::ustring> day39;
		Gtk::TreeModelColumn<Glib::ustring> day40;
		Gtk::TreeModelColumn<Glib::ustring> day41;
		Gtk::TreeModelColumn<Glib::ustring> day42;
		Gtk::TreeModelColumn<Glib::ustring>hours_work;	// 45
		Gtk::TreeModelColumn<Glib::ustring>hours_month;
		Gtk::TreeModelColumn<Glib::ustring>hours_extra;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor1;		// 48
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor2;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor3;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor4;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor5;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor6;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor7;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor8;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor9;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor10;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor11;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor12;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor13;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor14;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor15;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor16;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor17;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor18;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor19;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor20;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor21;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor22;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor23;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor24;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor25;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor26;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor27;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor28;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor29;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor30;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor31;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor32;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor33;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor34;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor35;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor36;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor37;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor38;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor39;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor40;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor41;
		Gtk::TreeModelColumn<Gdk::RGBA> bcolor42;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor_rank;	// 90
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor_name;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor1;		// 92
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor2;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor3;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor4;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor5;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor6;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor7;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor8;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor9;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor10;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor11;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor12;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor13;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor14;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor15;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor16;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor17;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor18;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor19;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor20;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor21;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor22;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor23;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor24;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor25;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor26;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor27;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor28;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor29;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor30;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor31;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor32;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor33;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor34;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor35;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor36;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor37;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor38;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor39;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor40;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor41;
		Gtk::TreeModelColumn<Gdk::RGBA> fcolor42;
		Gtk::TreeModelColumn<double> fontsize_rank;	// 134
		Gtk::TreeModelColumn<double> fontsize_name;
		Gtk::TreeModelColumn<double> fontsize1;		// 136
		Gtk::TreeModelColumn<double> fontsize2;
		Gtk::TreeModelColumn<double> fontsize3;
		Gtk::TreeModelColumn<double> fontsize4;
		Gtk::TreeModelColumn<double> fontsize5;
		Gtk::TreeModelColumn<double> fontsize6;
		Gtk::TreeModelColumn<double> fontsize7;
		Gtk::TreeModelColumn<double> fontsize8;
		Gtk::TreeModelColumn<double> fontsize9;
		Gtk::TreeModelColumn<double> fontsize10;
		Gtk::TreeModelColumn<double> fontsize11;
		Gtk::TreeModelColumn<double> fontsize12;
		Gtk::TreeModelColumn<double> fontsize13;
		Gtk::TreeModelColumn<double> fontsize14;
		Gtk::TreeModelColumn<double> fontsize15;
		Gtk::TreeModelColumn<double> fontsize16;
		Gtk::TreeModelColumn<double> fontsize17;
		Gtk::TreeModelColumn<double> fontsize18;
		Gtk::TreeModelColumn<double> fontsize19;
		Gtk::TreeModelColumn<double> fontsize20;
		Gtk::TreeModelColumn<double> fontsize21;
		Gtk::TreeModelColumn<double> fontsize22;
		Gtk::TreeModelColumn<double> fontsize23;
		Gtk::TreeModelColumn<double> fontsize24;
		Gtk::TreeModelColumn<double> fontsize25;
		Gtk::TreeModelColumn<double> fontsize26;
		Gtk::TreeModelColumn<double> fontsize27;
		Gtk::TreeModelColumn<double> fontsize28;
		Gtk::TreeModelColumn<double> fontsize29;
		Gtk::TreeModelColumn<double> fontsize30;
		Gtk::TreeModelColumn<double> fontsize31;
		Gtk::TreeModelColumn<double> fontsize32;
		Gtk::TreeModelColumn<double> fontsize33;
		Gtk::TreeModelColumn<double> fontsize34;
		Gtk::TreeModelColumn<double> fontsize35;
		Gtk::TreeModelColumn<double> fontsize36;
		Gtk::TreeModelColumn<double> fontsize37;
		Gtk::TreeModelColumn<double> fontsize38;
		Gtk::TreeModelColumn<double> fontsize39;
		Gtk::TreeModelColumn<double> fontsize40;
		Gtk::TreeModelColumn<double> fontsize41;
		Gtk::TreeModelColumn<double> fontsize42;
		Gtk::TreeModelColumn<double> fontsize_hours_work;	// 178
		Gtk::TreeModelColumn<double> fontsize_hours_month;
		Gtk::TreeModelColumn<double> fontsize_hours_extra;
		Gtk::TreeModelColumn<double> fontsize_extra_1;		// 181
		Gtk::TreeModelColumn<double> fontsize_extra_2;
		Gtk::TreeModelColumn<double> fontsize_extra_3;
		Gtk::TreeModelColumn<double> fontsize_extra_4;
		Gtk::TreeModelColumn<double> fontsize_extra_5;
		Gtk::TreeModelColumn<Glib::ustring>extra_1;			// 186
		Gtk::TreeModelColumn<Glib::ustring>extra_2;
		Gtk::TreeModelColumn<Glib::ustring>extra_3;
		Gtk::TreeModelColumn<Glib::ustring>extra_4;
		Gtk::TreeModelColumn<Glib::ustring>extra_5;
};

#endif // _MAIN_TREE_MODEL_COLUMN_RECORD_H_


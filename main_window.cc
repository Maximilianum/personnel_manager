/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main_window.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main_window.hh"
#include <iomanip>
#include "drawing_shifts.hh"
#include "drawing_week.hh"
#include "drawing_siris.hh"
#include "printable_window.hh"
#include "errors_dialog.hh"

Glib::ustring MainWindow::data_path;

void MainWindow::set_data_path(const Glib::ustring& path) {
	data_path.assign(path);
}

MainWindow::MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder) : CtrlAppEngine(cobject, builder) {
	CtrlAppEngine::app_filename = APP_FILE_NAME;
	CtrlAppEngine::app_name = APP_NAME;
	CtrlAppEngine::version = VERSION;
	CtrlAppEngine::build_number = BUILD_NUMBER;
	CtrlAppEngine::copyright = COPYRIGHT;
	CtrlAppEngine::project_website = APP_WEBSITE;
	app_status = APP_STATUS::starting;
	calendata = new CalendarData();
	months_label_strings = { " Gennaio  ", " Febbraio ", "  Marzo   ", "  Aprile  ", "  Maggio  ", "  Giugno  ", "  Luglio  ", "  Agosto  ", "Settembre ", " Ottobre  ", " Novembre ", " Dicembre " };
	// database
	database = new Database(data_path + Glib::ustring(DB_FILENAME), Database::db_version);
	update_shift_settings();
	/**
	 * data files path
	 */
	page_setup_path = data_path + Glib::ustring("/printer.page_setup");
	/**
	 * main window
	 */
	bldr->get_widget("main_statusbar", main_statusbar);
	main_status_cntxtid = main_statusbar->get_context_id(Glib::ustring("GENERAL"));
	main_error_cntxtid = main_statusbar->get_context_id(Glib::ustring("ERROR"));
	/**
	 * main menu
	 */
	bldr->get_widget("main_menubar", main_menubar);
	bldr->get_widget ("about_menuitem", about_menuitem);
	bldr->get_widget ("undo_menuitem", undo_menuitem);
	bldr->get_widget ("redo_menuitem", redo_menuitem);
	bldr->get_widget ("upfontsize_menuitem", upfontsize_menuitem);
	bldr->get_widget ("downfontsize_menuitem", downfontsize_menuitem);
	bldr->get_widget ("members_menuitem", members_menuitem);
	bldr->get_widget ("licenses_menuitem", licenses_menuitem);
	bldr->get_widget ("ranks_menuitem", ranks_menuitem);
	bldr->get_widget ("stats_menuitem", stats_menuitem);
	bldr->get_widget ("autocomplete_menuitem", autocomplete_menuitem);
	bldr->get_widget ("time_preset_menuitem", time_preset_menuitem);
	bldr->get_widget ("meal_break_menuitem", meal_break_menuitem);
	bldr->get_widget ("work_time_menuitem", work_time_menuitem);
	bldr->get_widget ("labels_menuitem", labels_menuitem);
	bldr->get_widget ("offworks_menuitem", offworks_menuitem);
	bldr->get_widget ("license_type_menuitem", license_type_menuitem);
	bldr->get_widget ("view_menuitem", view_menuitem);
	bldr->get_widget ("title_menuitem", title_menuitem);
	bldr->get_widget ("sign_menuitem", sign_menuitem);
	bldr->get_widget ("print_menuitem", print_menuitem);
	bldr->get_widget ("quit_menuitem", quit_menuitem);

	main_treestore = Gtk::TreeStore::create(main_tmcr);
	bldr->get_widget("main_treeview", main_treeview);
	main_treeview->set_model(main_treestore);
	main_treeselection = main_treeview->get_selection();
	main_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	main_treeview_fontsize = 11;
	columns_days = { main_tmcr.day1, main_tmcr.day2, main_tmcr.day3, main_tmcr.day4, main_tmcr.day5, main_tmcr.day6, main_tmcr.day7, main_tmcr.day8, main_tmcr.day9, main_tmcr.day10,
					 main_tmcr.day11, main_tmcr.day12, main_tmcr.day13, main_tmcr.day14, main_tmcr.day15, main_tmcr.day16, main_tmcr.day17, main_tmcr.day18, main_tmcr.day19, main_tmcr.day20, main_tmcr.day21,
					 main_tmcr.day22, main_tmcr.day23, main_tmcr.day24, main_tmcr.day25, main_tmcr.day26, main_tmcr.day27, main_tmcr.day28, main_tmcr.day29, main_tmcr.day30, main_tmcr.day31, main_tmcr.day32,
					 main_tmcr.day33, main_tmcr.day34, main_tmcr.day35, main_tmcr.day36, main_tmcr.day37, main_tmcr.day38, main_tmcr.day39, main_tmcr.day40, main_tmcr.day41, main_tmcr.day42 };
	// calendar toolbar
	bldr->get_widget("calendar_toolbar", calendar_toolbar);
	bldr->get_widget("toolbar_month_label", toolbar_month_label);
	bldr->get_widget("previous_month_toolbutton", previous_month_toolbutton);
	bldr->get_widget("next_month_toolbutton", next_month_toolbutton);
	bldr->get_widget("toolbar_year_label", toolbar_year_label);
	bldr->get_widget("previous_year_toolbutton", previous_year_toolbutton);
	bldr->get_widget("next_year_toolbutton", next_year_toolbutton);
	bldr->get_widget("complete_member_toolbutton", complete_member_toolbutton);
	bldr->get_widget("complete_month_toolbutton", complete_month_toolbutton);
	bldr->get_widget("check_month_toggletoolbutton", check_month_toggletoolbutton);
	bldr->get_widget("clear_month_toolbutton", clear_month_toolbutton);
	bldr->get_widget("print_month_toolbutton", print_month_toolbutton);
	bldr->get_widget("print_blank_month_toolbutton", print_blank_month_toolbutton);
	bldr->get_widget("print_week_toolbutton", print_week_toolbutton);
	bldr->get_widget("print_siris_toolbutton", print_siris_toolbutton);
	// edit toolbar
	bldr->get_widget("edit_toolbar", edit_toolbar);
	bldr->get_widget("edit_add_toolbutton", edit_add_toolbutton);
	bldr->get_widget("edit_modify_toolbutton", edit_modify_toolbutton);
	bldr->get_widget("edit_remove_toolbutton", edit_remove_toolbutton);
	bldr->get_widget("edit_shift_forward_toolbutton", edit_shift_forward_toolbutton);
	bldr->get_widget("edit_shift_backward_toolbutton", edit_shift_backward_toolbutton);
	
	connect_signals();
}

MainWindow::~MainWindow(void) {
	delete database;
	delete calendata;
}

void MainWindow::update_shift_settings(void) {
	try {
		try {
			Duty::set_meal_break(database->get_setting(MEAL_BREAK));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(MEAL_BREAK, Duty::get_meal_break());
		}
		try {
			Duty::set_lunch_start(database->get_setting(LUNCH_START));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(LUNCH_START, Duty::get_lunch_start());
		}
		try {
		Duty::set_lunch_end(database->get_setting(LUNCH_END));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(LUNCH_END, Duty::get_lunch_end());
		}
		try {
			Duty::set_dinner_start(database->get_setting(DINNER_START));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(DINNER_START, Duty::get_dinner_start());
		}
		try {
			Duty::set_dinner_end(database->get_setting(DINNER_END));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(DINNER_END, Duty::get_dinner_end());
		}
		try {
			Duty::set_max_work_length(database->get_setting(MAX_WRK_LENGTH));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(MAX_WRK_LENGTH, Duty::get_max_work_length());
		}
		try {
			Duty::set_work_time_start(database->get_setting(WORK_TIME_START));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WORK_TIME_START, Duty::get_work_time_start());
		}
		try {
			Duty::set_work_time_end(database->get_setting(WORK_TIME_END));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WORK_TIME_END, Duty::get_work_time_end());
		}
		try {
			Duty::set_work_time_saturday_close(database->get_setting(WRKTM_SATURDAY_CLOSE));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_SATURDAY_CLOSE, Duty::get_work_time_saturday_close());
		}
		try {
			Duty::set_work_time_saturday_enabled(database->get_setting(WRKTM_SATURDAY_ENABLE));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_SATURDAY_ENABLE, Duty::get_work_time_saturday_enabled());
		}
		try {
			Duty::set_work_time_saturday_start(database->get_setting(WRKTM_SATURDAY_START));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_SATURDAY_START, Duty::get_work_time_saturday_start());
		}
		try {
			Duty::set_work_time_saturday_end(database->get_setting(WRKTM_SATURDAY_END));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_SATURDAY_END, Duty::get_work_time_saturday_end());
		}
		try {
			Duty::set_work_time_holiday_close(database->get_setting(WRKTM_HOLIDAY_CLOSE));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_HOLIDAY_CLOSE, Duty::get_work_time_holiday_close());
		}
		try {
			Duty::set_work_time_holiday_enabled(database->get_setting(WRKTM_HOLIDAY_ENABLE));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_HOLIDAY_ENABLE, Duty::get_work_time_holiday_enabled());
		}
		try {
			Duty::set_work_time_holiday_start(database->get_setting(WRKTM_HOLIDAY_START));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_HOLIDAY_START, Duty::get_work_time_holiday_start());
		}
		try {
			Duty::set_work_time_holiday_end(database->get_setting(WRKTM_HOLIDAY_END));
		} catch (const Database::DBEmptyResultException& dber) {
			database->insert(WRKTM_HOLIDAY_END, Duty::get_work_time_holiday_end());
		}
		Duty::set_work_color(database->get_work_color());
		Duty::set_absent_color(database->get_absent_color());
		Duty::set_license_color(database->get_license_color());
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::set_shift_settings() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

/**
 * connect all needed signals to the relative functions
 */
void MainWindow::connect_signals(void) {
	// main menu
	about_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::about));
	undo_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_undo_app_action));
	redo_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_redo_app_action));
	upfontsize_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_upfontsize_action));
	downfontsize_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_downfontsize_action));
	downfontsize_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_downfontsize_action));
	members_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_members_action));
	licenses_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_licenses_action));
	ranks_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_ranks_action));
	stats_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_stats_action));
	autocomplete_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_autocomplete_action));
	time_preset_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_time_preset_action));
	meal_break_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_meal_break_action));
	work_time_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_work_time_action));
	labels_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_labels_action));
	offworks_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_offworks_action));
	license_type_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_license_type_action));
	view_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_view_action));
	title_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_title_action));
	sign_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_sign_action));
	print_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_print_action));
	quit_menuitem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::quit));
	// main window
	signal_focus_in_event().connect(sigc::mem_fun(this, &MainWindow::on_window_focus_in));
	signal_focus_out_event().connect(sigc::mem_fun(this, &MainWindow::on_window_focus_out));
	/**
	 * main page
	 */
	main_treeview->signal_row_expanded().connect(sigc::mem_fun(this, &MainWindow::on_main_treeview_row_expanded));
	main_treeview->signal_row_collapsed().connect(sigc::mem_fun(this, &MainWindow::on_main_treeview_row_collapsed));
	main_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(this, &MainWindow::on_main_treeview_clicked));
	main_treeselection_connection = main_treeselection->signal_changed().connect(sigc::mem_fun(this, &MainWindow::on_main_treeselection_changed));
	main_treeview->signal_key_press_event().connect(sigc::mem_fun(this, &MainWindow::on_main_treeview_key_pressed),false);
	// edit toolbar
	edit_add_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_add_shift_clicked));
	edit_remove_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_remove_shift_clicked));
	edit_modify_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_edit_shift_clicked));
	edit_shift_backward_toolbutton->signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(this, &MainWindow::on_move_shift_clicked),-1));
	edit_shift_forward_toolbutton->signal_clicked().connect(sigc::bind<int>(sigc::mem_fun(this, &MainWindow::on_move_shift_clicked),1));
	// calendar toolbar
	next_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_next_month_toolbutton_clicked));
	previous_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_previous_month_toolbutton_clicked));
	next_year_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_next_year_toolbutton_clicked));
	previous_year_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_previous_year_toolbutton_clicked));
	complete_member_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_complete_member_toolbutton_clicked));
	complete_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_complete_month_toolbutton_clicked));
	check_month_toggletoolbutton->signal_toggled().connect(sigc::mem_fun(this, &MainWindow::on_check_month_toggletoolbutton_toggled));
	clear_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_clear_month_toolbutton_clicked));
	print_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_print_month_toolbutton_clicked));
	print_blank_month_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_print_blank_month_toolbutton_clicked));
	print_week_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_print_week_toolbutton_clicked));
	print_siris_toolbutton->signal_clicked().connect(sigc::mem_fun(this, &MainWindow::on_print_siris_toolbutton_clicked));
}

/**
 * main menu
 */

void MainWindow::on_undo_app_action(void) {
	try {
		calendata->undo(database);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::on_undo_app_action() DBError" << std::endl;
	}
	update_edit_toolbar();
	load_data_in_main_treeview();
	update_main_menu();
}

void MainWindow::on_redo_app_action(void) {
	calendata->redo(database);
	update_edit_toolbar();
	load_data_in_main_treeview();
	update_main_menu();
}

void MainWindow::on_upfontsize_action(void) {
	if (main_treeview_fontsize < 60.0) {
		main_treeview_fontsize += 0.5;
		load_data_in_main_treeview();
	}
}

void MainWindow::on_downfontsize_action(void) {
	if (main_treeview_fontsize > 8.0) {
		main_treeview_fontsize -= 0.5;
		load_data_in_main_treeview();
	}
}

void MainWindow::on_members_action(void) {
	MembersDialog members_dialog { MembersDialog(Glib::ustring::compose("%1members_dialog.ui",resources_path), *this, database) };
	int res_id { members_dialog.run() };
	load_data_in_main_treeview();
}

void MainWindow::on_licenses_action(void) {
	LicensesDialog licenses_dialog { LicensesDialog(Glib::ustring::compose("%1licenses_dialog.ui",resources_path), *this, database) };
	int res_id { licenses_dialog.run() };
	load_data_in_main_treeview();
}

void MainWindow::on_ranks_action(void) {
	RanksDialog ranks_dialog { RanksDialog(Glib::ustring::compose("%1ranks_dialog.ui",resources_path), *this, database) };
	int res_id { ranks_dialog.run() };
	load_data_in_main_treeview();
}

void MainWindow::on_stats_action(void) {
	StatsDialog::set_calendar_data(calendata);
	StatsDialog stats_dialog { StatsDialog(Glib::ustring::compose("%1stats_dialog.ui",resources_path), *this, database) };
	stats_dialog.run();
}

void MainWindow::on_autocomplete_action(void) {
	AutocompleteDialog autocomplete_dialog { AutocompleteDialog(Glib::ustring::compose("%1autocomplete_dialog.ui",resources_path), *this, database) };
	int res_id { autocomplete_dialog.run() };
}

void MainWindow::on_time_preset_action(void) {
	TimePresetDialog time_preset_dialog { TimePresetDialog(Glib::ustring::compose("%1time_preset_dialog.ui",resources_path), *this, database) };
	int res_id { time_preset_dialog.run() };
}

void MainWindow::on_meal_break_action(void) {
	MealBreakDialog meal_break_dialog { MealBreakDialog(Glib::ustring::compose("%1meal_break_dialog.ui",resources_path), *this, database) };
	int res_id { meal_break_dialog.run() };
}

void MainWindow::on_work_time_action(void) {
	WorkTimeDialog work_time_dialog { WorkTimeDialog(Glib::ustring::compose("%1work_time_dialog.ui",resources_path), *this, database) };
	int res_id { work_time_dialog.run() };
}

void MainWindow::on_labels_action(void) {
	LabelsDialog labels_dialog { LabelsDialog(Glib::ustring::compose("%1labels_dialog.ui",resources_path), *this, database) };
	int res_id { labels_dialog.run() };
}

void MainWindow::on_offworks_action(void) {
	OffWorksDialog offworks_dialog { OffWorksDialog(Glib::ustring::compose("%1offworks_dialog.ui",resources_path), *this, database) };
	int res_id { offworks_dialog.run() };
}

void MainWindow::on_license_type_action(void) {
	LicenseTypesDialog license_types_dialog { LicenseTypesDialog(Glib::ustring::compose("%1license_types_dialog.ui",resources_path), *this, database) };
	int res_id { license_types_dialog.run() };
	update_main_treeview_columns();
	load_data_in_main_treeview();
}

void MainWindow::on_view_action(void) {
	ViewDialog view_dialog { ViewDialog(Glib::ustring::compose("%1view_dialog.ui",resources_path), *this, database) };
	int res_id { view_dialog.run() };
	update_main_treeview_columns();
	load_data_in_main_treeview();
}

void MainWindow::on_title_action(void) {
	TitleDialog title_dialog { TitleDialog(Glib::ustring::compose("%1title_dialog.ui",resources_path), *this, database) };
	int res_id { title_dialog.run() };
}

void MainWindow::on_sign_action(void) {
	SignDialog sign_dialog { SignDialog(Glib::ustring::compose("%1sign_dialog.ui",resources_path), *this, database) };
	int res_id { sign_dialog.run() };
}

void MainWindow::on_print_action(void) {
	PrintDialog print_dialog { PrintDialog(Glib::ustring::compose("%1print_dialog.ui",resources_path), *this, database) };
	int res_id { print_dialog.run() };
}

void MainWindow::update_main_menu(void) {
	undo_menuitem->set_sensitive(calendata->is_undo_available());
	redo_menuitem->set_sensitive(calendata->is_redo_available());
}

bool MainWindow::on_window_focus_in(GdkEventFocus *event) {
	if (app_status == APP_STATUS::starting)	{
		int width;
		get_size(width, saved_height);
		initial_height = saved_height;
		set_up_printer();
		update_toolbar();
		update_main_treeview_columns();
		load_data_in_main_treeview();
	}
	calendar_toolbar->set_sensitive(true);
	app_status = APP_STATUS::running;
	main_treeview->grab_focus();
	return false;
}

bool MainWindow::on_window_focus_out(GdkEventFocus *event) {
	calendar_toolbar->set_sensitive(false);
	return false;
}

/**
 * main page
 */

void MainWindow::update_main_treeview_columns(void) {
	short int extra_col = 0;
	try {
		// enable an extra column for each visible license (5 max)
		std::vector<LicenseType> licenses { database->get_visible_license_types() };
		for (auto iter = licenses.cbegin(); iter != licenses.cend() && extra_col < 5; iter++) {
			Gtk::TreeViewColumn* treeviewcolumn = main_treeview->get_column(47 + extra_col);
			treeviewcolumn->set_visible(true);
			treeviewcolumn->set_title(iter->get_short_label());
			extra_col++;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::update_main_treeview_columns() " << dbe.get_message() << std::endl;
		return;
	}
	// hide unused extra columns
	for (short i = extra_col; i < 5; i++) {
		main_treeview->get_column(36 + i)->set_visible(false);
	}
}

void MainWindow::load_data_in_main_treeview(void) {
	std::vector<Gtk::TreeModelColumn<Gdk::RGBA>> col_bcolors = { main_tmcr.bcolor1, main_tmcr.bcolor2, main_tmcr.bcolor3, main_tmcr.bcolor4, main_tmcr.bcolor5, main_tmcr.bcolor6, main_tmcr.bcolor7, main_tmcr.bcolor8, main_tmcr.bcolor9, main_tmcr.bcolor10,
																 main_tmcr.bcolor11, main_tmcr.bcolor12, main_tmcr.bcolor13, main_tmcr.bcolor14, main_tmcr.bcolor15, main_tmcr.bcolor16, main_tmcr.bcolor17, main_tmcr.bcolor18, main_tmcr.bcolor19, main_tmcr.bcolor20, main_tmcr.bcolor21,
																 main_tmcr.bcolor22, main_tmcr.bcolor23, main_tmcr.bcolor24, main_tmcr.bcolor25, main_tmcr.bcolor26, main_tmcr.bcolor27, main_tmcr.bcolor28, main_tmcr.bcolor29, main_tmcr.bcolor30, main_tmcr.bcolor31, main_tmcr.bcolor32,
																 main_tmcr.bcolor33, main_tmcr.bcolor34, main_tmcr.bcolor35, main_tmcr.bcolor36, main_tmcr.bcolor37, main_tmcr.bcolor38, main_tmcr.bcolor39, main_tmcr.bcolor40, main_tmcr.bcolor41, main_tmcr.bcolor42 };
	std::vector<Gtk::TreeModelColumn<Gdk::RGBA>> col_fcolors = { main_tmcr.fcolor1, main_tmcr.fcolor2, main_tmcr.fcolor3, main_tmcr.fcolor4, main_tmcr.fcolor5, main_tmcr.fcolor6, main_tmcr.fcolor7, main_tmcr.fcolor8, main_tmcr.fcolor9, main_tmcr.fcolor10,
																 main_tmcr.fcolor11, main_tmcr.fcolor12, main_tmcr.fcolor13, main_tmcr.fcolor14, main_tmcr.fcolor15, main_tmcr.fcolor16, main_tmcr.fcolor17, main_tmcr.fcolor18, main_tmcr.fcolor19, main_tmcr.fcolor20, main_tmcr.fcolor21,
																 main_tmcr.fcolor22, main_tmcr.fcolor23, main_tmcr.fcolor24, main_tmcr.fcolor25, main_tmcr.fcolor26, main_tmcr.fcolor27, main_tmcr.fcolor28, main_tmcr.fcolor29, main_tmcr.fcolor30, main_tmcr.fcolor31, main_tmcr.fcolor32,
																 main_tmcr.fcolor33, main_tmcr.fcolor34, main_tmcr.fcolor35, main_tmcr.fcolor36, main_tmcr.fcolor37, main_tmcr.fcolor38, main_tmcr.fcolor39, main_tmcr.fcolor40, main_tmcr.fcolor41, main_tmcr.fcolor42 };
	std::vector<Gtk::TreeModelColumn<double>> col_fsizes = { main_tmcr.fontsize1, main_tmcr.fontsize2, main_tmcr.fontsize3, main_tmcr.fontsize4, main_tmcr.fontsize5, main_tmcr.fontsize6, main_tmcr.fontsize7, main_tmcr.fontsize8, main_tmcr.fontsize9, main_tmcr.fontsize10,
															 main_tmcr.fontsize11, main_tmcr.fontsize12, main_tmcr.fontsize13, main_tmcr.fontsize14, main_tmcr.fontsize15, main_tmcr.fontsize16, main_tmcr.fontsize17, main_tmcr.fontsize18, main_tmcr.fontsize19, main_tmcr.fontsize20, main_tmcr.fontsize21,
															 main_tmcr.fontsize22, main_tmcr.fontsize23, main_tmcr.fontsize24, main_tmcr.fontsize25, main_tmcr.fontsize26, main_tmcr.fontsize27, main_tmcr.fontsize28, main_tmcr.fontsize29, main_tmcr.fontsize30, main_tmcr.fontsize31, main_tmcr.fontsize32, 
															 main_tmcr.fontsize33, main_tmcr.fontsize34, main_tmcr.fontsize35, main_tmcr.fontsize36, main_tmcr.fontsize37, main_tmcr.fontsize38, main_tmcr.fontsize39, main_tmcr.fontsize40, main_tmcr.fontsize41, main_tmcr.fontsize42 };
	std::vector<Gtk::TreeModelColumn<Glib::ustring>> col_extras = { main_tmcr.extra_1, main_tmcr.extra_2, main_tmcr.extra_3, main_tmcr.extra_4, main_tmcr.extra_5 };
	std::vector<Gtk::TreeModelColumn<double>> col_extra_fsizes = { main_tmcr.fontsize_extra_1, main_tmcr.fontsize_extra_2, main_tmcr.fontsize_extra_3, main_tmcr.fontsize_extra_4, main_tmcr.fontsize_extra_5 };
	main_treestore->clear();
	load_first_headers_in_main_treeview(columns_days, col_bcolors, col_fcolors, col_fsizes);
	load_second_headers_in_main_treeview(columns_days, col_bcolors, col_fcolors, col_fsizes);
	try {
		calendata->update_offworks_cache(database);
		std::vector<Member> members { database->get_active_members(CustomDate::get_first_day_month(calendata->get_reference_date())) };
		std::vector<Member>::iterator m_iter = members.begin();
		std::vector<Member>::iterator m_end = members.end();
		for (auto m_iter = members.begin(); m_iter != members.end(); m_iter++) {
			Gtk::TreeModel::iterator iter_list = main_treestore->append();
			Gtk::TreeModel::Row row = *iter_list;
			Gtk::TreeModel::iterator it_child_obb = main_treestore->append(row.children());
			Gtk::TreeModel::Row c_row_obb = *it_child_obb;
			Gtk::TreeModel::iterator it_child_eff = main_treestore->append(row.children());
			Gtk::TreeModel::Row c_row_eff = *it_child_eff;
			Gtk::TreeModel::iterator it_child_diff = main_treestore->append(row.children());
			Gtk::TreeModel::Row c_row_diff = *it_child_diff;
			Gtk::TreeModel::iterator it_child_week = main_treestore->append(row.children());
			Gtk::TreeModel::Row c_row_week = *it_child_week;
			row[main_tmcr.rank] = m_iter->get_rank().get_short_label();
			row[main_tmcr.fontsize_rank] = main_treeview_fontsize;
			row[main_tmcr.name] = Glib::ustring::compose("%1\n%2",m_iter->get_first_name(),m_iter->get_last_name());
			row[main_tmcr.fontsize_name] = main_treeview_fontsize;
			if (members_in_error.count(m_iter->get_id().raw()) > 0)	{
				row[main_tmcr.fcolor_rank] = Gdk::RGBA("Red");
				row[main_tmcr.fcolor_name] = Gdk::RGBA("Red");
			}
			c_row_obb[main_tmcr.rank] = m_iter->get_id();
			c_row_obb[main_tmcr.fontsize_rank] = main_treeview_fontsize;
			c_row_obb[main_tmcr.name] = Glib::ustring("ore d'obbligo");
			c_row_obb[main_tmcr.fontsize_name] = main_treeview_fontsize;
			c_row_eff[main_tmcr.name] = Glib::ustring("ore effettuate");
			c_row_eff[main_tmcr.fontsize_name] = main_treeview_fontsize;
			c_row_diff[main_tmcr.name] = Glib::ustring("differenza ore");
			c_row_diff[main_tmcr.fontsize_name] = main_treeview_fontsize;
			c_row_week[main_tmcr.name] = Glib::ustring("ore settimanali");
			c_row_week[main_tmcr.fontsize_name] = main_treeview_fontsize;
			CustomDate cd_day { CustomDate::get_first_day_month(calendata->get_reference_date()) };
			CustomDate ldaym { CustomDate::get_last_day_month(calendata->get_reference_date()) };
			CustomDate cd_end { CustomDate::get_last_day_week(ldaym.get_time_t()) };
			int mnth { cd_day.get_month() };
			cd_day = CustomDate::get_first_day_week(cd_day.get_time_t());
			calendata->update_shifts_cache(database, m_iter->get_id(), cd_day, cd_end);
			short int index { 0 };
			while (cd_day.get_month() == mnth || cd_day.week_intersect_month(mnth)) {
				bool outside = cd_day.get_month() != mnth && cd_day.week_intersect_month(mnth);
				load_day_data_in_row(row, c_row_obb, c_row_eff, c_row_diff, c_row_week, columns_days[index], col_bcolors[index], col_fcolors[index], col_fsizes[index], cd_day, m_iter->get_id(), outside);
				cd_day.add_days(1);
				index++;
			}
			cd_day = CustomDate::get_first_day_month(calendata->get_reference_date());
			row[main_tmcr.id] = m_iter->get_id();
			if (expanded_ids.count(m_iter->get_id().raw()) == 1) {
				main_treeview->expand_to_path(main_treeview->get_model()->get_path(iter_list));
			}
			int worked { calendata->get_worked_minutes(database, cd_day, ldaym, m_iter->get_id()) };
			int swm { calendata->get_should_work_minutes(database, cd_day, ldaym, m_iter->get_id(), true) };
			row[main_tmcr.hours_work] = Glib::ustring::format(worked / 60);
			row[main_tmcr.fontsize_hours_work] = main_treeview_fontsize;
			row[main_tmcr.hours_month] = Glib::ustring::format(swm / 60);
			row[main_tmcr.fontsize_hours_month] = main_treeview_fontsize;
			row[main_tmcr.hours_extra] = Glib::ustring::format((worked - swm) / 60);
			row[main_tmcr.fontsize_hours_extra] = main_treeview_fontsize;
			short int extra_col { 0 };
			std::vector<License> v_licenses { database->get_visible_valid_licenses_for_member(m_iter->get_id(), cd_day) }; 
			Glib::ustring last_label { Glib::ustring() };
			CustomDate last_month { CustomDate::get_last_day_month(calendata->get_reference_date()) };
			
			for (auto iter = v_licenses.cbegin(); iter != v_licenses.cend() && extra_col < 5; iter++) {
				int days_used { calendata->get_license_amount(database, m_iter->get_id(), iter->get_full_label(), iter->get_ref_year(), last_month) };
				int days_available { iter->get_days() - days_used };
				if (iter->get_full_label() == last_label) {
					extra_col--;
					Glib::ustring av_str { row[col_extras[extra_col]] };
					days_available += atoi(av_str.c_str());
				}
				row[col_extras[extra_col]] = Glib::ustring::format(days_available);
				row[col_extra_fsizes[extra_col]] = main_treeview_fontsize;
				last_label = iter->get_full_label();
				extra_col++;
			}
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::load_data_in_main_treeview() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void MainWindow::load_day_data_in_row(Gtk::TreeModel::Row row, Gtk::TreeModel::Row c_row_obb, Gtk::TreeModel::Row c_row_eff, Gtk::TreeModel::Row c_row_diff, Gtk::TreeModel::Row c_row_week, Gtk::TreeModelColumn<Glib::ustring>& day_rcd, Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, const Glib::ustring& id, bool outside) {
	try {
		row[day_rcd] = calendata->get_shift_string(database, cd_day, id, true);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::load_day_data_in_row() " << dbe.get_message() << std::endl;
	}
	if (calendata->is_selected(cd_day.get_time_t(), id)) {
		row[bcolor_rcd] = Gdk::RGBA("Black");
		row[fcolor_rcd] = Gdk::RGBA("White");
	} else {
		Gdk::RGBA bcolour { calendata->get_background_color(cd_day, id) };
		Gdk::RGBA fcolour { calendata->get_foreground_color(database, cd_day, id) };
		if (outside) {
			if (bcolour == Gdk::RGBA("White")) {
				bcolour.set_grey(0.9);
			} else {
				bcolour.set_alpha(0.5);
			}
			if (fcolour != Gdk::RGBA("White")) {
				fcolour.set_alpha(0.5);
			} else {
				fcolour.set_grey(0.9);
			}
		}
		row[bcolor_rcd] = bcolour;
		row[fcolor_rcd] = fcolour;
	}
	row[fsize_rcd] = main_treeview_fontsize;
	int should_work { calendata->get_should_work_minutes(database, cd_day, cd_day, id, true) / 60 };
	c_row_obb[day_rcd] = Glib::ustring::format(should_work);
	c_row_obb[fsize_rcd] = main_treeview_fontsize;
	int worked { calendata->get_worked_minutes(database, cd_day, cd_day, id) / 60 };
	c_row_eff[day_rcd] = Glib::ustring::format(worked);
	c_row_eff[fsize_rcd] = main_treeview_fontsize;
	int diff { worked - should_work };
	c_row_diff[day_rcd] = diff > 0 ? Glib::ustring::compose("+%1", Glib::ustring::format(diff)) : Glib::ustring::format(diff);
	c_row_diff[fsize_rcd] = main_treeview_fontsize;
	CustomDate fdayw { CustomDate::get_first_day_week(cd_day.get_time_t()) };
	CustomDate ldayw { CustomDate::get_last_day_week(cd_day.get_time_t()) };
	int wd { cd_day.get_weekday() };
	switch (wd) {
	case 4:
		c_row_week[day_rcd] = Glib::ustring::compose("eff.\n%1", Glib::ustring::format(calendata->get_worked_minutes(database, fdayw, ldayw, id) / 60));
		c_row_week[fsize_rcd] = main_treeview_fontsize;
		break;
	case 5:
		c_row_week[day_rcd] = Glib::ustring::compose("obb.\n%1", Glib::ustring::format(calendata->get_should_work_minutes(database, fdayw, ldayw, id, true) / 60));
		c_row_week[fsize_rcd] = main_treeview_fontsize;
		break;
	case 6:
		diff = (calendata->get_worked_minutes(database, fdayw, ldayw, id) - calendata->get_should_work_minutes(database, fdayw, ldayw, id, true)) / 60;
		c_row_week[day_rcd] = diff > 0 ? Glib::ustring::compose("diff.\n+%1", Glib::ustring::format(diff)) : Glib::ustring::compose("diff.\n%1", Glib::ustring::format(diff));
		c_row_week[fsize_rcd] = main_treeview_fontsize;
		break;
	case 0:
		CustomDate startcd = CustomDate::get_first_day_month(calendata->get_reference_date());
		diff = (calendata->get_worked_minutes(database, startcd, cd_day, id) - calendata->get_should_work_minutes(database, startcd, cd_day, id, true)) / 60;
		if (diff < 0) {
			c_row_week[fcolor_rcd] = Gdk::RGBA("Red");
		}
		c_row_week[day_rcd] = diff > 0 ? Glib::ustring::compose("str.\n+%1", Glib::ustring::format(diff)) : Glib::ustring::compose("str.\n%1", Glib::ustring::format(diff));
		c_row_week[fsize_rcd] = main_treeview_fontsize;
		break;
	}
}

void MainWindow::load_first_headers_in_main_treeview(const std::vector<Gtk::TreeModelColumn<Glib::ustring>>& col_days, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_bcolors, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_fcolors, const std::vector<Gtk::TreeModelColumn<double>>& col_fsizes) {
	Gtk::TreeModel::iterator iter_list = main_treestore->append();
	Gtk::TreeModel::Row row = *iter_list;
	row[main_tmcr.rank] = Glib::ustring(" ");
	row[main_tmcr.fontsize_rank] = main_treeview_fontsize;
	row[main_tmcr.name] = Glib::ustring(" ");
	row[main_tmcr.fontsize_name] = main_treeview_fontsize;
	CustomDate cd_day { CustomDate::get_first_day_month(calendata->get_reference_date()) };
	int mnth { cd_day.get_month() };
	cd_day = CustomDate::get_first_day_week(cd_day.get_time_t());
	short int index { 0 };
	while (cd_day.get_month() == mnth || cd_day.week_intersect_month(mnth)) {
		if (index > 27) {
			Gtk::TreeViewColumn* treeviewcolumn = main_treeview->get_column(2 + index);
			treeviewcolumn->set_visible(true);
		}
		bool outside { cd_day.get_month() != mnth && cd_day.week_intersect_month(mnth) };
		load_first_headers_in_row(row, col_days[index], col_bcolors[index], col_fcolors[index], col_fsizes[index], cd_day, outside);
		cd_day.add_days(1);
		index++;
	}
	while (index < 42) {
		Gtk::TreeViewColumn* treeviewcolumn = main_treeview->get_column(2 + index);
		treeviewcolumn->set_visible(false);
		index++;
	}
	row[main_tmcr.id] = Glib::ustring();
	row[main_tmcr.hours_work] = Glib::ustring("ore");
	row[main_tmcr.fontsize_hours_work] = main_treeview_fontsize;
	row[main_tmcr.hours_month] = Glib::ustring("ore");
	row[main_tmcr.fontsize_hours_month] = main_treeview_fontsize;
	row[main_tmcr.hours_extra] = Glib::ustring("ore");
	row[main_tmcr.fontsize_hours_extra] = main_treeview_fontsize;
	std::vector<Gtk::TreeModelColumn<Glib::ustring>> col_extras = { main_tmcr.extra_1, main_tmcr.extra_2, main_tmcr.extra_3, main_tmcr.extra_4, main_tmcr.extra_5 };
	std::vector<Gtk::TreeModelColumn<double>> col_extra_fsizes = { main_tmcr.fontsize_extra_1, main_tmcr.fontsize_extra_2, main_tmcr.fontsize_extra_3, main_tmcr.fontsize_extra_4, main_tmcr.fontsize_extra_5 };
	for (int i = 0; i < 5; i++) {
		Gtk::TreeViewColumn* treeviewcolumn = main_treeview->get_column(47 + i);
		if (treeviewcolumn->get_visible()) {
			row[col_extras[i]] = treeviewcolumn->get_title();
			row[col_extra_fsizes[i]] = main_treeview_fontsize;
		}
	}
}

void MainWindow::load_first_headers_in_row(Gtk::TreeModel::Row row, const Gtk::TreeModelColumn<Glib::ustring>& day_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, const Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, bool outside) {
	row[day_rcd] = Glib::ustring::format(cd_day.get_day());
	Gdk::RGBA bcolour { calendata->get_background_color(cd_day) };
	Gdk::RGBA fcolour { calendata->get_foreground_color(cd_day) };
	if (outside) {
		if (bcolour == Gdk::RGBA("White")) {
			bcolour.set_grey(0.9);
		} else {
			bcolour.set_alpha(0.5);
		}
		if (fcolour != Gdk::RGBA("White")) {
			fcolour.set_alpha(0.5);
		} else {
			fcolour.set_grey(0.9);
		}
	}
	row[bcolor_rcd] = bcolour;
	row[fcolor_rcd] = fcolour;
	row[fsize_rcd] = main_treeview_fontsize;
}

void MainWindow::load_second_headers_in_main_treeview(const std::vector<Gtk::TreeModelColumn<Glib::ustring>>& col_days, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_bcolors, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_fcolors, const std::vector<Gtk::TreeModelColumn<double>>& col_fsizes) {
	Gtk::TreeModel::iterator iter_tm { main_treestore->append() };
	Gtk::TreeModel::Row row { *iter_tm };
	row[main_tmcr.rank] = Glib::ustring("Grado");
	row[main_tmcr.fontsize_rank] = main_treeview_fontsize;
	row[main_tmcr.name] = Glib::ustring("Nome");
	row[main_tmcr.fontsize_name] = main_treeview_fontsize;
	CustomDate cd_day { CustomDate::get_first_day_month(calendata->get_reference_date()) };
	int mnth { cd_day.get_month() };
	cd_day = CustomDate::get_first_day_week(cd_day.get_time_t());
	short int index { 0 };
	while (cd_day.get_month() == mnth || cd_day.week_intersect_month(mnth)) {
		bool outside { cd_day.get_month() != mnth && cd_day.week_intersect_month(mnth) };
		load_second_headers_in_row(row, col_days[index], col_bcolors[index], col_fcolors[index], col_fsizes[index], cd_day, outside);
		cd_day.add_days(1);
		index++;
	}
	row[main_tmcr.id] = Glib::ustring();
	row[main_tmcr.hours_work] = Glib::ustring("eff.");
	row[main_tmcr.fontsize_hours_work] = main_treeview_fontsize;
	row[main_tmcr.hours_month] = Glib::ustring("obb.");
	row[main_tmcr.fontsize_hours_month] = main_treeview_fontsize;
	row[main_tmcr.hours_extra] = Glib::ustring("str.");
	row[main_tmcr.fontsize_hours_extra] = main_treeview_fontsize;
}

void MainWindow::load_second_headers_in_row(Gtk::TreeModel::Row row, const Gtk::TreeModelColumn<Glib::ustring>& day_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, const Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, bool outside) {
	row[day_rcd] = cd_day.get_weekday_string(false);
	Gdk::RGBA bcolour { calendata->get_background_color(cd_day) };
	Gdk::RGBA fcolour { calendata->get_foreground_color(cd_day) };
	if (outside) {
		if (bcolour == Gdk::RGBA("White")) {
			bcolour.set_grey(0.9);
		} else {
			bcolour.set_alpha(0.5);
		}
		if (fcolour != Gdk::RGBA("White")) {
			fcolour.set_alpha(0.5);
		} else {
			fcolour.set_grey(0.9);
		}
	}
	row[bcolor_rcd] = bcolour;
	row[fcolor_rcd] = fcolour;
	row[fsize_rcd] = main_treeview_fontsize;
}

void MainWindow::on_main_treeview_row_expanded(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path) {
	Gtk::TreeModel::Row row { *iter };
	Glib::ustring id { row[main_tmcr.id] };
	expanded_ids.insert(id.raw());
}

void MainWindow::on_main_treeview_row_collapsed(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path) {
	Gtk::TreeModel::Row row { *iter };
	Glib::ustring id { row[main_tmcr.id] };
	expanded_ids.erase(id.raw());
}

int MainWindow::get_clicked_column_index(GdkEventButton *event) {
	unsigned int index;
	unsigned int nr { main_treeview->get_n_columns() };
	// determine which column has been clicked
	for (index = 0; index < nr; index++) {
		Gtk::TreeViewColumn* column { main_treeview->get_column(index) };
		int x_offset { column->get_x_offset() };
		if (event->x >= x_offset && event->x <= x_offset + column->get_width()) {
			return index;
		}
	}
	return -1;
}

Glib::ustring MainWindow::get_member_id_of_clicked_row(GdkEventButton *event) {
	Glib::ustring mid;
	// get TreeModel path at click
	Gtk::TreeModel::Path path;
	Gtk::TreeViewColumn* column;
	int cell_x, cell_y;	
	if (main_treeview->get_path_at_pos(event->x, event->y, path, column, cell_x, cell_y)) {
		// get member id of selected row
		Glib::RefPtr<Gtk::TreeModel> main_treemodel { main_treeview->get_model() };
		Gtk::TreeModel::iterator iter { main_treemodel->get_iter(path) };
		Gtk::TreeModel::Row row { *iter };
		mid = row[main_tmcr.id];
	}
	return mid;
}

void MainWindow::on_main_treeview_clicked(GdkEventButton *event) {
	if (event->type == GDK_BUTTON_PRESS || event->type == GDK_2BUTTON_PRESS) {
		int index = get_clicked_column_index(event);
		Glib::ustring clicked_member { get_member_id_of_clicked_row(event) };
		if (index > 1) {
			complete_member_toolbutton->set_sensitive(false);
			print_siris_toolbutton->set_sensitive(false);
			main_treeselection_connection.disconnect();	
			if (clicked_member.compare(Glib::ustring()) != 0) {
				CustomDate cd_day = calendata->get_reference_cdate();
				Glib::RefPtr<Gtk::TreeModel> main_treemodel = main_treeview->get_model();
				Gtk::TreeModel::iterator iter = main_treemodel->children().begin();
				Gtk::TreeModel::Row row = *iter;
				Glib::ustring daystr { row[columns_days[index - 2]] };
				std::stringstream ss { daystr.raw() };
				int dayn;
				ss >> dayn;
				if (index > dayn && index - dayn < 10) {
					cd_day.set_day(dayn);
				} else if (dayn > index) {
					cd_day = CustomDate::get_previous_month(cd_day);
					cd_day.set_day(dayn);
				} else if (index > dayn && index - dayn > 9) {
					cd_day = CustomDate::get_next_month(cd_day);
					cd_day.set_day(dayn);
				}
				if (event->state & GDK_SHIFT_MASK && clicked_member.compare(calendata->get_selection().note) == 0) {
					Selection sel { calendata->get_selection() };
					CustomDate sd { &sel.date };
					sel.days = cd_day.get_difference_days(sd);
					calendata->set_selection(sel);
				} else {
					calendata->set_selection({ cd_day.get_time_t(), 0, clicked_member} );
					if (event->button == 3) {
						// update treeview in order to visualize changed selection before starting editing
						load_data_in_main_treeview();
						on_edit_shift_clicked();
					}
				}
			} else {
				calendata->clear_selection();
			}
			update_edit_toolbar();
			load_data_in_main_treeview();
			main_treeselection_connection = main_treeselection->signal_changed().connect(sigc::mem_fun(this, &MainWindow::on_main_treeselection_changed));
		} else {
			calendata->set_selection({ 0, 0, clicked_member} );
		}
	}
}

void MainWindow::on_main_treeselection_changed(void) {
	int nr_sel = main_treeselection->count_selected_rows();
	Glib::RefPtr<Gtk::TreeModel> main_treemodel = main_treeview->get_model();
	if (nr_sel > 0)	{
		Selection sel { calendata->get_selection() };
		if (sel.note.compare(Glib::ustring()) != 0) {
			complete_member_toolbutton->set_sensitive(true);
			print_siris_toolbutton->set_sensitive(true);
		} else {
			complete_member_toolbutton->set_sensitive(false);
			print_siris_toolbutton->set_sensitive(false);
		}
		if (sel.note.compare(Glib::ustring()) == 0 || sel.date > 0) {
			main_treeselection_connection.disconnect();
			main_treeselection->unselect_all();
			main_treeselection_connection = main_treeselection->signal_changed().connect(sigc::mem_fun(this, &MainWindow::on_main_treeselection_changed));
		}
	}
}

bool MainWindow::on_main_treeview_key_pressed(GdkEventKey* kevent) {
	if (kevent->keyval == GDK_KEY_Delete || kevent->keyval == GDK_KEY_BackSpace) {
		on_remove_shift_clicked();
		return false;
	} else if (calendata->is_valid_selection()) {
		Selection sel { calendata->get_selection() };
		if (kevent->keyval == GDK_KEY_Up) {
			try {
				std::vector<Member> members { database->get_active_members(CustomDate::get_first_day_month(calendata->get_reference_date())) };
				int index { 0 };
				for (auto m_iter = members.begin(); m_iter != members.end(); m_iter++) {
					if (m_iter->get_id().compare(sel.note) == 0) {
						break;
					}
					index++;
				}
				if (index > 0) {
					sel.note = members.at(index - 1).get_id();
					calendata->set_selection(sel);
					load_data_in_main_treeview();
					update_edit_toolbar();
				}
			} catch (const Database::DBError& dbe) {
				std::cerr << "MainWindow::on_main_treeview_key_pressed() " << dbe.get_message() << std::endl;
				throw dbe;
			}
		} else if (kevent->keyval == GDK_KEY_Down) {
			try {
				std::vector<Member> members { database->get_active_members(CustomDate::get_first_day_month(calendata->get_reference_date())) };
				int index { 0 };
				for (auto m_iter = members.begin(); m_iter != members.end(); m_iter++) {
					if (m_iter->get_id().compare(sel.note) == 0) {
						break;
					}
					index++;
				}
				if (index < members.size() - 1) {
					sel.note = members.at(index + 1).get_id();
					calendata->set_selection(sel);
					load_data_in_main_treeview();
					update_edit_toolbar();
				}
			} catch (const Database::DBError& dbe) {
				std::cerr << "MainWindow::on_main_treeview_key_pressed() " << dbe.get_message() << std::endl;
				throw dbe;
			}
		} else if (kevent->keyval == GDK_KEY_Right) {
			if (kevent->state & GDK_SHIFT_MASK) {
				sel.days++;
			} else {
				struct tm* std_date { localtime(&(sel.date)) };
				int rm { std_date->tm_mon };
				CustomDate cd { CustomDate::get_next_day(sel.date) };
				sel.date = cd.get_time_t();
				if (rm != cd.get_month()) {
					calendata->set_next_month();
					update_toolbar();
				}
			}
			calendata->set_selection(sel);
			load_data_in_main_treeview();
			update_edit_toolbar();
		} else if (kevent->keyval == GDK_KEY_Left) {
			if (kevent->state & GDK_SHIFT_MASK) {
				sel.days--;
			} else {
				struct tm* std_date { localtime(&(sel.date)) };
				int rm { std_date->tm_mon };
				CustomDate cd { CustomDate::get_previous_day(sel.date) };
				sel.date = cd.get_time_t();
				if (rm != cd.get_month()) {
					calendata->set_previous_month();
					update_toolbar();
				}
			}
			calendata->set_selection(sel);
			load_data_in_main_treeview();
			update_edit_toolbar();
		} else if (kevent->keyval == GDK_KEY_Return) {
			if (calendata->has_shifts(database, sel)) {
				on_edit_shift_clicked();
			} else {
				on_add_shift_clicked();
			}
		}
	}
	return true;
}

// popup menu

void MainWindow::on_add_shift_clicked(void) {
	if (calendata->is_valid_selection()) {
		Selection sel { calendata->get_selection() };
		CustomDate start { &sel.date };
		try {
			if (!calendata->has_shifts(database, sel)) {
				Shift shift { start, start, sel.note};
				ShiftDialog shift_dialog { ShiftDialog(Glib::ustring::compose("%1shift_dialog.ui",resources_path), *this, database, calendata) };
				shift_dialog.set_shift(shift);
				shift_dialog.run();
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::on_add_shift_clicked() " << dbe.get_message() << std::endl;
			throw dbe;
		} catch (const Database::DBEmptyResultException& dbe) {
			std::cerr << "MainWindow::on_add_shift_clicked() " << dbe.get_message() << std::endl;
			throw dbe;
		}
		load_data_in_main_treeview();
		update_edit_toolbar();
		update_main_menu();
	}
}

void MainWindow::on_remove_shift_clicked(void) {
	if (calendata->is_valid_selection()) {
		Selection sel { calendata->get_selection() };
		try {
			if (calendata->has_shifts(database, sel)) {
				remove_selected_shift();
			} else {
				Gtk::MessageDialog alert_dialog { *this, "Non ci sono turni da eliminare.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
				int res_id = alert_dialog.run();
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::on_remove_shift_clicked() " << dbe.get_message() << std::endl;
			throw dbe;
		}
	}
}

void MainWindow::remove_selected_shift(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente i turni selezionati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id = alert_dialog.run();
	if (res_id == Gtk::RESPONSE_YES) {
		try {
			calendata->remove_selected_shifts(database);
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::remove_selected_shift() " << dbe.get_message() << std::endl;
		}
		load_data_in_main_treeview();
		update_edit_toolbar();
		update_main_menu();
	}
}

void MainWindow::on_edit_shift_clicked(void) {
	if (calendata->is_valid_selection() && calendata->is_selected_member_active(database)) {
		try {
			Shift shift { calendata->get_selected_shift(database) };
			ShiftDialog shift_dialog { ShiftDialog(Glib::ustring::compose("%1shift_dialog.ui",resources_path), *this, database, calendata) };
			shift_dialog.set_shift(shift);
			shift_dialog.run();
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::on_edit_shift_clicked() " << dbe.get_message() << std::endl;
			throw dbe;
		} catch (const Database::DBEmptyResultException& dbe) {
			on_add_shift_clicked();
		}
		load_data_in_main_treeview();
		update_edit_toolbar();
		update_main_menu();
	}
}

void MainWindow::on_move_shift_clicked(int direction) {
	if (calendata->is_valid_selection()) {
		if (calendata->move_shift(database, direction)) {
			load_data_in_main_treeview();
			update_main_menu();
		} else {
			Glib::ustring message { "Errore durante lo spostamento del turno, operazione abortita." };
			main_statusbar->pop(main_error_cntxtid);
			main_statusbar->push(message, main_error_cntxtid);
		}
	}
}

/**
 * edit toolbar
 */

void MainWindow::update_edit_toolbar(void) {
	if (calendata->is_valid_selection()) {
		Selection sel { calendata->get_selection() };
		try {
			bool flag = calendata->has_shifts(database, sel);
			edit_add_toolbutton->set_sensitive(!flag || sel.days > 0);
			edit_modify_toolbutton->set_sensitive(flag && sel.days == 0);
			edit_remove_toolbutton->set_sensitive(flag);
			edit_shift_forward_toolbutton->set_sensitive(flag && sel.days == 0);
			edit_shift_backward_toolbutton->set_sensitive(flag && sel.days == 0);
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::update_edit_toolbar() " << dbe.get_message() << std::endl;
			throw dbe;
		}
	} else {
		edit_add_toolbutton->set_sensitive(false);
		edit_modify_toolbutton->set_sensitive(false);
		edit_remove_toolbutton->set_sensitive(false);
		edit_shift_forward_toolbutton->set_sensitive(false);
		edit_shift_backward_toolbutton->set_sensitive(false);
	}
}

/**
 * calendar toolbar
 */

void MainWindow::update_toolbar(void) {
	CustomDate cd_day { calendata->get_reference_cdate() };
	toolbar_month_label->set_text(months_label_strings.at(cd_day.get_month()));
	toolbar_year_label->set_text(Glib::ustring::format(cd_day.get_year() + 1900));
}

void MainWindow::on_next_month_toolbutton_clicked(void) {
	if (check_month_toggletoolbutton->get_active()) {
		check_month_toggletoolbutton->set_active(false);
	}
	calendata->set_next_month();
	load_data_in_main_treeview();
	update_toolbar();
}

void MainWindow::on_previous_month_toolbutton_clicked(void) {
	if (check_month_toggletoolbutton->get_active()) {
		check_month_toggletoolbutton->set_active(false);
	}
	calendata->set_previous_month();
	load_data_in_main_treeview();
	update_toolbar();
}

void MainWindow::on_next_year_toolbutton_clicked(void) {
	if (check_month_toggletoolbutton->get_active()) {
		check_month_toggletoolbutton->set_active(false);
	}
	calendata->set_next_year();
	load_data_in_main_treeview();
	update_toolbar();
}

void MainWindow::on_previous_year_toolbutton_clicked(void) {
	if (check_month_toggletoolbutton->get_active()) {
		check_month_toggletoolbutton->set_active(false);
	}
	calendata->set_previous_year();
	load_data_in_main_treeview();
	update_toolbar();
}

void MainWindow::on_complete_member_toolbutton_clicked(void) {
	calendata->add_shifts_for_selected_member(database);
	load_data_in_main_treeview();
	update_main_menu();
}

void MainWindow::on_complete_month_toolbutton_clicked(void) {
	if (check_month_toggletoolbutton->get_active()) {
		check_month_toggletoolbutton->set_active(false);
	}
	calendata->add_shifts_for_all_members(database);
	load_data_in_main_treeview();
	update_main_menu();
}

void MainWindow::on_check_month_toggletoolbutton_toggled(void) {
	if (check_month_toggletoolbutton->get_active())	{
		std::vector<shift_error> errors;
		Glib::ustring message;
		try {
			CustomDate fdaym { CustomDate::get_first_day_month(calendata->get_reference_date()) };
			CustomDate ldaym { CustomDate::get_last_day_month(calendata->get_reference_date()) };
			// check if all days are full time covered
			calendata->check_time_coverage(database, &errors);
			// create a vector of members id
			std::vector<Member> members { database->get_active_members(ldaym) };
			// check if all members has shifts
			calendata->check_shifts_for_members(database, members, &errors);
			// check the difference between worked minutes and planned time
			calendata->check_worked_time_for_members(database, members, &errors);
			// check if Siris computation is possible in first and last week of month
			calendata->check_siris_for_members(database, fdaym, members, &errors);
			calendata->check_siris_for_members(database, ldaym, members, &errors);
			if (errors.size() == 1) {
				message.assign(Glib::ustring("E' stato rilevato un errore."));
			} else if (errors.size() > 1) {
				message.assign(Glib::ustring::compose("Sono stati rilevati %1 errori.", Glib::ustring::format(errors.size())));
			} else {
				message.assign(Glib::ustring("Verifica superata senza errori."));
			}
			main_statusbar->pop(main_error_cntxtid);
			main_statusbar->push(message, main_error_cntxtid);
			members_in_error.clear();
			Glib::ustring errors_txt;
			Glib::ustring numero[] = { Glib::ustring("prima"), Glib::ustring("seconda"), Glib::ustring("terza"), Glib::ustring("quarta"), Glib::ustring("quinta") };
			for (auto it_err = errors.begin(); it_err != errors.end(); it_err++) {
				// if error struct has a member id, it will record it in an unordered set
				if (it_err->id.compare(Glib::ustring()) != 0) {
					members_in_error.insert(it_err->id.raw());
				}
				switch (it_err->type) {
				case ERRTYPE::missing:
					errors_txt.append(Glib::ustring::compose("Errore: \"nessuna indicazione del turno effettuato\" - matricola: %1 - giorno: %2\n", it_err->id, Glib::ustring::format(it_err->day)));
					break;
				case ERRTYPE::out_of_time:
					errors_txt.append(Glib::ustring::compose("Errore: \"lavoro al di fuori dell'orario di apertura\" - giorno: %1\n", Glib::ustring::format(it_err->day)));
					break;
				case ERRTYPE::not_covered:
					errors_txt.append(Glib::ustring::compose("Errore: \"giorno non completamente coperto dal personale\" - giorno: %1\n", Glib::ustring::format(it_err->day)));
					break;
				case ERRTYPE::not_enough:
					errors_txt.append(Glib::ustring::compose("Errore: \"ore di lavoro settimanali non sufficienti nella %1 settimana\" - matricola: %2\n", numero[it_err->day], it_err->id, Glib::ustring::format(it_err->day)));
					break;
				case ERRTYPE::siris:
					errors_txt.append(Glib::ustring::compose("Errore: \"impossibile compilare correttamente il prospetto Siris\" - matricola: %1 - giorno: %2\n", it_err->id, Glib::ustring::format(it_err->day)));
					break;
				}
			}
			if (errors.size() > 0) {
				ErrorsDialog dialog { ErrorsDialog(*this) };
				dialog.set_message(errors_txt);
				dialog.run();
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "MainWindow::on_check_month_toggletoolbutton_toggled() " << dbe.get_message() << std::endl;
			throw dbe;
		}
	} else {
		clear_errors();
	}
	load_data_in_main_treeview();
}

void MainWindow::clear_errors(void) {
	members_in_error.clear();
	calendata->clear_errors();
	main_statusbar->pop(main_error_cntxtid);
}

void MainWindow::on_clear_month_toolbutton_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Questa operazione cancellerà tutti i turni del mese, sei sicuro di volerlo fare?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id = alert_dialog.run();
	if (res_id == Gtk::RESPONSE_YES) {
		if (check_month_toggletoolbutton->get_active()) {
			check_month_toggletoolbutton->set_active(false);
		}
		calendata->clear_shifts(database);
		load_data_in_main_treeview();
		update_main_menu();
	}
}

/*
 * print
 */

void MainWindow::print_current_month(bool blank) {
	// deselect previous selections
	calendata->clear_selection();
	DrawingShifts* drawing_area = new DrawingShifts(database, calendata->get_reference_date());
	PrintableWindow *print_win = new PrintableWindow(drawing_area);
	print_win->set_transient_for(*this);
	drawing_area->set_blank(blank);
	try {
		drawing_area->set_font_size(11.0);
		drawing_area->set_members_per_page(database->get_setting(MEMBERS_PER_PAGE));
		std::vector<TextLine> title_lines { database->get_text_lines(TITLE_LINE) };
		for (auto iter = title_lines.begin(); iter != title_lines.end(); iter++) {
			drawing_area->add_title(iter->get_text(), iter->get_font_size());
		}
		std::vector<TextLine> sign_lines { database->get_text_lines(SIGN_LINE) };
		for (auto iter = sign_lines.begin(); iter != sign_lines.end(); iter++) {
			drawing_area->add_sign(iter->get_text(), iter->get_font_size());
		}
		std::vector<TextLine> sign_name { database->get_text_lines(SIGN_NAME) };
		if (sign_name.size() > 0) {
			drawing_area->add_sign(Glib::ustring::compose("(%1)",sign_name.front().get_text()), sign_name.front().get_font_size());
		}
		drawing_area->set_size_request(580.0, 800.0);
		drawing_area->set_svg_logo(database->get_str_setting(LOGO_FILE_PATH));
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::print_current_month() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	print_win->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &MainWindow::on_window_hide), print_win));
	print_win->present();
}

void MainWindow::print_week(void) {
	DrawingWeek* drawing_area = new DrawingWeek(database, calendata->get_reference_date());
	PrintableWindow *print_win = new PrintableWindow(drawing_area);
	print_win->set_transient_for(*this);
	try {
		drawing_area->set_font_size(11.0);
		std::vector<TextLine> title_lines { database->get_text_lines(TITLE_LINE) };
		for (auto iter = title_lines.begin(); iter != title_lines.end(); iter++) {
			drawing_area->add_title(iter->get_text(), iter->get_font_size());
		}
		std::vector<TextLine> sign_lines { database->get_text_lines(SIGN_LINE) };
		for (auto iter = sign_lines.begin(); iter != sign_lines.end(); iter++) {
			drawing_area->add_sign(iter->get_text(), iter->get_font_size());
		}
		std::vector<TextLine> sign_name { database->get_text_lines(SIGN_NAME) };
		if (sign_name.size() > 0) {
			drawing_area->add_sign(Glib::ustring::compose("(%1)",sign_name.front().get_text()), sign_name.front().get_font_size());
		}
		drawing_area->set_size_request(580.0, 800.0);
		drawing_area->set_svg_logo(database->get_str_setting(LOGO_FILE_PATH));
		drawing_area->set_description(database->get_str_setting(PRINT_WEEK_DESCR));
		drawing_area->set_start_day(database->get_setting(WEEK_START));
		drawing_area->set_show_shift(database->get_setting(PRINT_WEEK_VIEW));
		print_win->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &MainWindow::on_window_hide), print_win));
		print_win->present();
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::print_week() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void MainWindow::print_siris(const Glib::ustring& member_id) {
	DrawingSiris* drawing_area = new DrawingSiris(database, calendata->get_reference_date());
	PrintableWindow *print_win = new PrintableWindow(drawing_area);
	print_win->set_transient_for(*this);
	try {
		Member mbr = database->get_member(calendata->get_selection().note);
		drawing_area->set_member(mbr);
		drawing_area->compute_siris_month();
		drawing_area->set_font_size(11.0);
		drawing_area->set_size_request(580.0, 800.0);
		print_win->signal_hide().connect(sigc::bind<Gtk::Window*>(sigc::mem_fun(this, &MainWindow::on_window_hide), print_win));
		print_win->present();
	} catch (const Database::DBError& dbe) {
		std::cerr << "MainWindow::print_siris() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "MainWindow::print_siris() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

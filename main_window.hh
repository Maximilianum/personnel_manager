/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * main_window.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MAIN_WINDOW_HH_
#define _MAIN_WINDOW_HH_

#define APP_FILE_NAME "personnel_manager"
#define APP_NAME "Personnel Manager"
#define DB_FILENAME "/personnelmanager.db"
#define VERSION "2.0.2"
#define BUILD_NUMBER "698"
#define COPYRIGHT "Copyright © 2015 - 2024 Massimiliano Maniscalco"
#define APP_WEBSITE "https://gitlab.com/Maximilianum/personnel_manager"

#include <gtkmm.h>
#include <giomm/file.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include "ctrl_app_engine.hh"
// data
#include "calendar_data.hh"
// dialogs
#include "members_dialog.hh"
#include "member_dialog.hh"
#include "licenses_dialog.hh"
#include "license_dialog.hh"
#include "shift_dialog.hh"
#include "autocomplete_dialog.hh"
#include "ranks_dialog.hh"
#include "stats_dialog.hh"
#include "time_preset_dialog.hh"
#include "meal_break_dialog.hh"
#include "work_time_dialog.hh"
#include "labels_dialog.hh"
#include "offworks_dialog.hh"
#include "license_types_dialog.hh"
#include "view_dialog.hh"
#include "title_dialog.hh"
#include "sign_dialog.hh"
#include "print_dialog.hh"

// treeview model
#include "main_tree_model_column_record.hh"
// combobox model
#include "license_combobox_tree_model_column_record.hh"

class MainWindow : public CtrlAppEngine {
 public:
	static void set_data_path(const Glib::ustring& path);
	MainWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder);
	~MainWindow(void);
 protected:
	void update_shift_settings(void);
	/**
	 * general user interface functions
	 */
	void connect_signals(void);
	/**
	 * main menu
	 */
	void on_undo_app_action(void);
	void on_redo_app_action(void);
	void on_upfontsize_action(void);
	void on_downfontsize_action(void);
	void on_members_action(void);
	void on_licenses_action(void);
	void on_ranks_action(void);
	void on_stats_action(void);
	void on_autocomplete_action(void);
	void on_time_preset_action(void);
	void on_meal_break_action(void);
	void on_work_time_action(void);
	void on_labels_action(void);
	void on_offworks_action(void);
	void on_license_type_action(void);
	void on_view_action(void);
	void on_title_action(void);
	void on_sign_action(void);
	void on_print_action(void);
	void update_main_menu(void);
	/**
	 * main window
	 */
	bool on_window_focus_in(GdkEventFocus *event);
	bool on_window_focus_out(GdkEventFocus *event);
	/**
	 * main page
	 */
	void update_main_treeview_columns(void);
	void load_data_in_main_treeview(void);
	void load_day_data_in_row(Gtk::TreeModel::Row row, Gtk::TreeModel::Row c_row_obb, Gtk::TreeModel::Row c_row_eff, Gtk::TreeModel::Row c_row_diff, Gtk::TreeModel::Row c_row_week, Gtk::TreeModelColumn<Glib::ustring>& day_rcd, Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, const Glib::ustring& id, bool outside);
	void load_first_headers_in_main_treeview(const std::vector<Gtk::TreeModelColumn<Glib::ustring>>& col_days, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_bcolors, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_fcolors, const std::vector<Gtk::TreeModelColumn<double>>& col_fsizes);
	void load_first_headers_in_row(Gtk::TreeModel::Row row, const Gtk::TreeModelColumn<Glib::ustring>& day_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, const Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, bool outside);
	void load_second_headers_in_main_treeview(const std::vector<Gtk::TreeModelColumn<Glib::ustring>>& col_days, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_bcolors, const std::vector<Gtk::TreeModelColumn<Gdk::RGBA>>& col_fcolors, const std::vector<Gtk::TreeModelColumn<double>>& col_fsizes);
	void load_second_headers_in_row(Gtk::TreeModel::Row row, const Gtk::TreeModelColumn<Glib::ustring>& day_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& bcolor_rcd, const Gtk::TreeModelColumn<Gdk::RGBA>& fcolor_rcd, const Gtk::TreeModelColumn<double>& fsize_rcd, const CustomDate& cd_day, bool outside);
	void on_main_treeview_row_expanded(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path);
	void on_main_treeview_row_collapsed(const Gtk::TreeModel::iterator& iter, const Gtk::TreeModel::Path& path);
	int get_clicked_column_index(GdkEventButton *event);
	Glib::ustring get_member_id_of_clicked_row(GdkEventButton *event);
	void on_main_treeview_clicked(GdkEventButton *event);
	void on_main_treeselection_changed(void);
	bool on_main_treeview_key_pressed(GdkEventKey* kevent);
	// edit toolbar
	void on_add_shift_clicked(void);
	void on_remove_shift_clicked(void);
	void remove_selected_shift(void);
	void on_edit_shift_clicked(void);
	void on_move_shift_clicked(int direction);
	void update_edit_toolbar(void);
	// calendar toolbar
	void update_toolbar(void);
	void on_next_month_toolbutton_clicked(void);
	void on_previous_month_toolbutton_clicked(void);
	void on_next_year_toolbutton_clicked(void);
	void on_previous_year_toolbutton_clicked(void);
	void on_complete_member_toolbutton_clicked(void);
	void on_complete_month_toolbutton_clicked(void);
	void on_check_month_toggletoolbutton_toggled(void);
	void clear_errors(void);
	void on_clear_month_toolbutton_clicked(void);
	inline void on_print_month_toolbutton_clicked(void) { print_current_month(); }
	inline void on_print_blank_month_toolbutton_clicked(void) { print_current_month(true); }
	inline void on_print_week_toolbutton_clicked(void) { print_week(); }
	inline void on_print_siris_toolbutton_clicked(void) { print_siris(calendata->get_selection().note); }
	// print
	void print_current_month(bool blank = false);
	void print_week(void);
	void print_siris(const Glib::ustring& member_id);
 private:
	static Glib::ustring data_path;
	Database* database;
	CalendarData* calendata;
	Settings settings;
	std::unordered_set<std::string> members_in_error;
	/**
	 * main menu
	 */
	Gtk::MenuBar* main_menubar;
	Gtk::MenuItem* about_menuitem;
	Gtk::MenuItem* undo_menuitem;
	Gtk::MenuItem* redo_menuitem;
	Gtk::MenuItem* upfontsize_menuitem;
	Gtk::MenuItem* downfontsize_menuitem;
	Gtk::MenuItem* members_menuitem;
	Gtk::MenuItem* licenses_menuitem;
	Gtk::MenuItem* ranks_menuitem;
	Gtk::MenuItem* stats_menuitem;
	Gtk::MenuItem* autocomplete_menuitem;
	Gtk::MenuItem* time_preset_menuitem;
	Gtk::MenuItem* meal_break_menuitem;
	Gtk::MenuItem* work_time_menuitem;
	Gtk::MenuItem* labels_menuitem;
	Gtk::MenuItem* offworks_menuitem;
	Gtk::MenuItem* license_type_menuitem;
	Gtk::MenuItem* view_menuitem;
	Gtk::MenuItem* title_menuitem;
	Gtk::MenuItem* sign_menuitem;
	Gtk::MenuItem* print_menuitem;
	Gtk::MenuItem* quit_menuitem;
	/**
	 * main window
	 */
	int saved_height;
	int initial_height;
	Gtk::Statusbar* main_statusbar;
	int main_status_cntxtid;
	int main_error_cntxtid;
	/**
	 * trees column record
	 */
	MainTreeModelColumnRecord main_tmcr;
	/**
	 * main page
	 */
	Glib::RefPtr<Gtk::TreeStore> main_treestore;
	Gtk::TreeView* main_treeview;
	Glib::RefPtr<Gtk::TreeSelection> main_treeselection;
	sigc::connection main_treeselection_connection;
	double main_treeview_fontsize;
	std::unordered_set<std::string> expanded_ids;
	std::vector<Gtk::TreeModelColumn<Glib::ustring>> columns_days;
	// calendar toolbar
	Gtk::Toolbar* calendar_toolbar;
	Gtk::Label* toolbar_month_label;
	std::vector<Glib::ustring> months_label_strings;
	Gtk::ToolButton* previous_month_toolbutton;
	Gtk::ToolButton* next_month_toolbutton;
	Gtk::Label* toolbar_year_label;
	Gtk::ToolButton* previous_year_toolbutton;
	Gtk::ToolButton* next_year_toolbutton;
	Gtk::ToolButton* complete_member_toolbutton;
	Gtk::ToolButton* complete_month_toolbutton;
	Gtk::ToggleToolButton* check_month_toggletoolbutton;
	Gtk::ToolButton* clear_month_toolbutton;
	Gtk::ToolButton* print_month_toolbutton;
	Gtk::ToolButton* print_blank_month_toolbutton;
	Gtk::ToolButton* print_week_toolbutton;
	Gtk::ToolButton* print_siris_toolbutton;
	// edit toolbar
	Gtk::Toolbar* edit_toolbar;
	Gtk::ToolButton* edit_add_toolbutton;
	Gtk::ToolButton* edit_modify_toolbutton;
	Gtk::ToolButton* edit_remove_toolbutton;
	Gtk::ToolButton* edit_shift_forward_toolbutton;
	Gtk::ToolButton* edit_shift_backward_toolbutton;
};

#endif // _MAIN_WINDOW_HH_


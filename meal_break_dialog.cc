/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * meal_break_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "meal_break_dialog.hh"

void MealBreakDialog::build_gui(MealBreakDialog* mb) {
	Gtk::Box* box = mb->get_content_area();
	mb->bldr->get_widget("meal_break_box", mb->meal_break_box);
	mb->bldr->get_widget("meal_break_minutes_scale", mb->meal_break_minutes_scale);
	mb->bldr->get_widget("meal_break_lunch_from_hour_combobox", mb->meal_break_lunch_from_hour_combobox);
	mb->meal_break_lunch_from_hour_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_lunch_from_hour_combobox->set_model(mb->meal_break_lunch_from_hour_liststore);
	mb->meal_break_lunch_from_hour_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_lunch_from_minutes_combobox", mb->meal_break_lunch_from_minutes_combobox);
	mb->meal_break_lunch_from_minutes_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_lunch_from_minutes_combobox->set_model(mb->meal_break_lunch_from_minutes_liststore);
	mb->meal_break_lunch_from_minutes_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_lunch_to_hour_combobox", mb->meal_break_lunch_to_hour_combobox);
	mb->meal_break_lunch_to_hour_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_lunch_to_hour_combobox->set_model(mb->meal_break_lunch_to_hour_liststore);
	mb->meal_break_lunch_to_hour_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_lunch_to_minutes_combobox", mb->meal_break_lunch_to_minutes_combobox);
	mb->meal_break_lunch_to_minutes_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_lunch_to_minutes_combobox->set_model(mb->meal_break_lunch_to_minutes_liststore);
	mb->meal_break_lunch_to_minutes_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_dinner_from_hour_combobox", mb->meal_break_dinner_from_hour_combobox);
	mb->meal_break_dinner_from_hour_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_dinner_from_hour_combobox->set_model(mb->meal_break_dinner_from_hour_liststore);
	mb->meal_break_dinner_from_hour_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_dinner_from_minutes_combobox", mb->meal_break_dinner_from_minutes_combobox);
	mb->meal_break_dinner_from_minutes_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_dinner_from_minutes_combobox->set_model(mb->meal_break_dinner_from_minutes_liststore);
	mb->meal_break_dinner_from_minutes_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_dinner_to_hour_combobox", mb->meal_break_dinner_to_hour_combobox);
	mb->meal_break_dinner_to_hour_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_dinner_to_hour_combobox->set_model(mb->meal_break_dinner_to_hour_liststore);
	mb->meal_break_dinner_to_hour_combobox->set_active(0);
	mb->bldr->get_widget("meal_break_dinner_to_minutes_combobox", mb->meal_break_dinner_to_minutes_combobox);
	mb->meal_break_dinner_to_minutes_liststore = Gtk::ListStore::create(mb->combobox_tmcr);
	mb->meal_break_dinner_to_minutes_combobox->set_model(mb->meal_break_dinner_to_minutes_liststore);
	mb->meal_break_dinner_to_minutes_combobox->set_active(0);
	box->pack_start(*(mb->meal_break_box));
	/*
	 * load values
	 */
	std::vector<Glib::ustring> hour {};
	for (short i = 0; i < 25; i++) {
		hour.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
	}
	std::vector<Glib::ustring> minute {};
	for (short i = 0; i < 60; i += 10) {
		minute.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
	}
	mb->load_data_combobox(hour.cbegin() + 9, hour.cend() - 10, mb->meal_break_lunch_from_hour_liststore);
	mb->load_data_combobox(minute.cbegin(), minute.cend(), mb->meal_break_lunch_from_minutes_liststore);
	mb->load_data_combobox(hour.cbegin() + 12, hour.cend() - 6, mb->meal_break_lunch_to_hour_liststore);
	mb->load_data_combobox(minute.cbegin(), minute.cend(), mb->meal_break_lunch_to_minutes_liststore);
	mb->load_data_combobox(hour.cbegin() + 17, hour.cend() - 2, mb->meal_break_dinner_from_hour_liststore);
	mb->load_data_combobox(minute.cbegin(), minute.cend(), mb->meal_break_dinner_from_minutes_liststore);
	mb->load_data_combobox(hour.cbegin() + 20, hour.cend() - 1, mb->meal_break_dinner_to_hour_liststore);
	mb->load_data_combobox(minute.cbegin(), minute.cend(), mb->meal_break_dinner_to_minutes_liststore);
	mb->meal_break_minutes_scale->set_value(mb->get_meal_break());
	Glib::ustring string_value { Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_lunch_start() / 60) };
	int index { mb->get_row_index_for_value_in_combobox(mb->meal_break_lunch_from_hour_liststore, string_value) };
	mb->meal_break_lunch_from_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_lunch_start() % 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_lunch_from_minutes_liststore, string_value);
	mb->meal_break_lunch_from_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_lunch_end() / 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_lunch_to_hour_liststore, string_value);
	mb->meal_break_lunch_to_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_lunch_end() % 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_lunch_to_minutes_liststore, string_value);
	mb->meal_break_lunch_to_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_dinner_start() / 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_dinner_from_hour_liststore, string_value);
	mb->meal_break_dinner_from_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_dinner_start() % 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_dinner_from_minutes_liststore, string_value);
	mb->meal_break_dinner_from_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_dinner_end() / 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_dinner_to_hour_liststore, string_value);
	mb->meal_break_dinner_to_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), mb->get_dinner_end() % 60);
	index = mb->get_row_index_for_value_in_combobox(mb->meal_break_dinner_to_minutes_liststore, string_value);
	mb->meal_break_dinner_to_minutes_combobox->set_active(index);
	/*
	 * signals connections
	 */
	mb->meal_break_minutes_scale->signal_value_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_meal_break_minutes_value_changed));	
	mb->meal_break_lunch_from_hour_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_lunch_from_value_changed));
	mb->meal_break_lunch_from_minutes_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_lunch_from_value_changed));
	mb->meal_break_lunch_to_hour_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_lunch_to_value_changed));
	mb->meal_break_lunch_to_minutes_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_lunch_to_value_changed));
	mb->meal_break_dinner_from_hour_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_dinner_from_value_changed));
	mb->meal_break_dinner_from_minutes_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_dinner_from_value_changed));
	mb->meal_break_dinner_to_hour_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_dinner_to_value_changed));
	mb->meal_break_dinner_to_minutes_combobox->signal_changed().connect(sigc::mem_fun(mb, &MealBreakDialog::on_dinner_to_value_changed));
}

void MealBreakDialog::on_meal_break_minutes_value_changed(void) {
	try {
		set_meal_break(meal_break_minutes_scale->get_value());
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::on_meal_break_minutes_value_changed() " << dbe.get_message() << std::endl;
	}
}

void MealBreakDialog::on_lunch_from_value_changed(void) {
	Gtk::TreeModel::iterator iter_h { meal_break_lunch_from_hour_combobox->get_active() };
	Gtk::TreeModel::Row row_h { *iter_h };
	Glib::ustring value_str_h { row_h[combobox_tmcr.column] };
	Gtk::TreeModel::iterator iter_m { meal_break_lunch_from_minutes_combobox->get_active() };
	Gtk::TreeModel::Row row_m { *iter_m };
	Glib::ustring value_str_m { row_m[combobox_tmcr.column] };
	int value { (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str()) };
	try {
		set_lunch_start(value);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::on_lunch_from_value_changed() " << dbe.get_message() << std::endl;
	}
}

void MealBreakDialog::on_lunch_to_value_changed(void) {
	Gtk::TreeModel::iterator iter_h { meal_break_lunch_to_hour_combobox->get_active() };
	Gtk::TreeModel::Row row_h { *iter_h };
	Glib::ustring value_str_h { row_h[combobox_tmcr.column] };
	Gtk::TreeModel::iterator iter_m { meal_break_lunch_to_minutes_combobox->get_active() };
	Gtk::TreeModel::Row row_m { *iter_m };
	Glib::ustring value_str_m { row_m[combobox_tmcr.column] };
	int value { (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str()) };
	try {
		set_lunch_end(value);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::on_lunch_to_value_changed() " << dbe.get_message() << std::endl;
	}
}

void MealBreakDialog::on_dinner_from_value_changed(void) {
	Gtk::TreeModel::iterator iter_h { meal_break_dinner_from_hour_combobox->get_active() };
	Gtk::TreeModel::Row row_h { *iter_h };
	Glib::ustring value_str_h { row_h[combobox_tmcr.column] };
	Gtk::TreeModel::iterator iter_m { meal_break_dinner_from_minutes_combobox->get_active() };
	Gtk::TreeModel::Row row_m { *iter_m };
	Glib::ustring value_str_m { row_m[combobox_tmcr.column] };
	int value { (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str()) };
	try {
		set_dinner_start(value);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::on_dinner_from_value_changed() " << dbe.get_message() << std::endl;
	}
}

void MealBreakDialog::on_dinner_to_value_changed(void) {
	Gtk::TreeModel::iterator iter_h { meal_break_dinner_to_hour_combobox->get_active() };
	Gtk::TreeModel::Row row_h { *iter_h };
	Glib::ustring value_str_h { row_h[combobox_tmcr.column] };
	Gtk::TreeModel::iterator iter_m { meal_break_dinner_to_minutes_combobox->get_active() };
	Gtk::TreeModel::Row row_m { *iter_m };
	Glib::ustring value_str_m { row_m[combobox_tmcr.column] };
	int value { (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str()) };
	try {
		set_dinner_end(value);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::on_dinner_to_value_changed() " << dbe.get_message() << std::endl;
	}
}

void MealBreakDialog::set_meal_break(int val) {
	Shift::set_meal_break(val);
	try {
		if (database->check_setting(MEAL_BREAK)) {
			database->update(MEAL_BREAK, val);
		} else {
			database->insert(MEAL_BREAK, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::set_meal_break() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int MealBreakDialog::get_meal_break(void) const {
	try {
		return database->get_setting(MEAL_BREAK);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::get_meal_break() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_MEAL_BREAK;
}

void MealBreakDialog::set_lunch_start(int val) {
	Shift::set_lunch_start(val);
	try {
		if (database->check_setting(LUNCH_START)) {
			database->update(LUNCH_START, val);
		} else {
			database->insert(LUNCH_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::set_lunch_start() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int MealBreakDialog::get_lunch_start(void) const {
	try {
		return database->get_setting(LUNCH_START);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::get_lunch_start() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_LUNCH_START;
}

void MealBreakDialog::set_lunch_end(int val) {
	Shift::set_lunch_end(val);
	try {
		if (database->check_setting(LUNCH_END)) {
			database->update(LUNCH_END, val);
		} else {
			database->insert(LUNCH_END, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::set_lunch_end() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int MealBreakDialog::get_lunch_end(void) const {
	try {
		return database->get_setting(LUNCH_END);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::get_lunch_end() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_LUNCH_END;
}

void MealBreakDialog::set_dinner_start(int val) {
	Shift::set_dinner_start(val);
	try {
		if (database->check_setting(DINNER_START)) {
			database->update(DINNER_START, val);
		} else {
			database->insert(DINNER_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::set_dinner_start() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int MealBreakDialog::get_dinner_start(void) const {
	try {
		return database->get_setting(DINNER_START);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::get_dinner_start() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_DINNER_START;
}

void MealBreakDialog::set_dinner_end(int val) {
	Shift::set_dinner_end(val);
	try {
		if (database->check_setting(DINNER_END)) {
			database->update(DINNER_END, val);
		} else {
			database->insert(DINNER_END, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::set_dinner_end() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int MealBreakDialog::get_dinner_end(void) const {
	try {
		return database->get_setting(DINNER_END);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MealBreakDialog::get_dinner_end() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_DINNER_END;
}

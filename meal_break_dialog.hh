/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * meal_break_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MEAL_BREAK_DIALOG_HH_
#define _MEAL_BREAK_DIALOG_HH_

#include <gtkmm/combobox.h>
#include <gtkmm/scale.h>
#include "control_dialog.hh"
#include "database.hh"

#define MEAL_BREAK				"MEAL_BRK"
#define LUNCH_START				"LUNCH_STRT"
#define LUNCH_END				"LUNCH_END"
#define DINNER_START			"DINN_STRT"
#define DINNER_END				"DINN_END"

class MealBreakDialog : public ControlDialog {
 public:
	inline MealBreakDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline MealBreakDialog(const MealBreakDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(MealBreakDialog* mb);
	void on_meal_break_minutes_value_changed(void);
	void on_lunch_from_value_changed(void);
	void on_lunch_to_value_changed(void);
	void on_dinner_from_value_changed(void);
	void on_dinner_to_value_changed(void);
	void set_meal_break(int val);
	int get_meal_break(void) const;
	void set_lunch_start(int val);
	int get_lunch_start(void) const;
	void set_lunch_end(int val);
	int get_lunch_end(void) const;
	void set_dinner_start(int val);
	int get_dinner_start(void) const;
	void set_dinner_end(int val);
	int get_dinner_end(void) const;

	Gtk::Box* meal_break_box;
	Gtk::Scale* meal_break_minutes_scale;
	Gtk::ComboBox* meal_break_lunch_from_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_lunch_from_hour_liststore;
	Gtk::ComboBox* meal_break_lunch_from_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_lunch_from_minutes_liststore;
	Gtk::ComboBox* meal_break_lunch_to_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_lunch_to_hour_liststore;
	Gtk::ComboBox* meal_break_lunch_to_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_lunch_to_minutes_liststore;
	Gtk::ComboBox* meal_break_dinner_from_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_dinner_from_hour_liststore;
	Gtk::ComboBox* meal_break_dinner_from_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_dinner_from_minutes_liststore;
	Gtk::ComboBox* meal_break_dinner_to_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_dinner_to_hour_liststore;
	Gtk::ComboBox* meal_break_dinner_to_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> meal_break_dinner_to_minutes_liststore;
	Database* database;
};

#endif // _MEAL_BREAK_DIALOG_HH_

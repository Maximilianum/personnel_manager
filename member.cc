/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * member.cc
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "member.hh"

Glib::ustring Member::sqlite_table { Glib::ustring("TABLE MEMBER (" \
												   "ID TEXT PRIMARY KEY NOT NULL, " \
												   "FNAME TEXT NOT NULL, " \
												   "LNAME TEXT NOT NULL, " \
												   "RANK INTEGER NOT NULL, " \
												   "ENLISTMENT INTEGER NOT NULL, " \
												   "EXTRA INTEGER, " \
												   "WEEKPLAN INTEGER, " \
												   "FOREIGN KEY(RANK) REFERENCES RANK(RANK), " \
												   "UNIQUE(ID));") };

int Member::retrieve_member(void *data, int argc, char **argv, char **azColName) {
	Member mbr;
	Rank rnk;
	std::vector<Member>* members { static_cast<std::vector<Member>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("ID") && argv[i]) {
			mbr.set_id(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FNAME") && argv[i]) {
			mbr.set_first_name(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("LNAME") && argv[i]) {
			mbr.set_last_name(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("RANK") && argv[i]) {
			rnk.set_index(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLABEL") && argv[i]) {
			rnk.set_full_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SLABEL") && argv[i]) {
			rnk.set_short_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("ENLISTMENT") && argv[i]) {
			mbr.set_enlistment_date(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("EXTRA") && argv[i]) {
			mbr.set_extra_years(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("WEEKPLAN") && argv[i]) {
			if (atoi(argv[i]) == 6) {
				mbr.set_working_week(Workweek::six);
			} else if (atoi(argv[i]) == 5) {
				mbr.set_working_week(Workweek::five);
			}
		}
	}
	mbr.set_rank(rnk);
	members->push_back(mbr);
	return 0;
}

int Member::retrieve_member_id(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring str;
	std::vector<std::string>* vec { static_cast<std::vector<std::string>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("ID") && argv[i]) {
			str.assign(argv[i]);
		}
	}
	vec->push_back(str);
	return 0;
}

/**
 * 
 * operators
 * 
 */

Member& Member::operator=(const Member &mbr) {
	if (this != &mbr) {
		id = mbr.id;
		fname = mbr.fname;
		lname = mbr.lname;
		rank = mbr.rank;
		enlistment_date = mbr.enlistment_date;
		extra = mbr.extra;
		working_week = mbr.working_week;
	}
	return *this;
}

	bool Member::operator<( const Member &mbr) const {
		if (rank < mbr.rank) {
			return true;
		} else if (rank == mbr.rank && enlistment_date < mbr.enlistment_date) {
			return true;
		} else if (rank == mbr.rank && enlistment_date == mbr.enlistment_date && id.raw() < mbr.id.raw()) {
			return true;
		}
		return false;
	}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * member.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MEMBER_HH_
#define _MEMBER_HH_

#include <glibmm/ustring.h>
#include <vector>
#include "rank.hh"
#include "custom_date.hh"
#include "license.hh"

enum class Workweek { six, five };

class Member {
 public:
	// static class functions
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_member(void *data, int argc, char **argv, char **azColName);
	static int retrieve_member_id(void *data, int argc, char **argv, char **azColName);
	// constructors
	inline Member() : id (Glib::ustring()), fname(Glib::ustring()), lname(Glib::ustring()), enlistment_date(CustomDate()), extra (0), working_week (Workweek::six) {}
	inline Member(const Glib::ustring& str1, const Glib::ustring& str2, const Glib::ustring& str3, Rank rnk, const CustomDate& cd, short val, Workweek wwk): id (str1), fname(str2), lname(str3), rank(rnk), enlistment_date(cd), extra (val), working_week (wwk) {}
	// operators
	Member& operator=(const Member &mbr);
	bool operator<(const Member &mbr) const;
	inline bool operator==(const Member &mbr) const { return id.raw() == mbr.id.raw(); }
	// class member functions
	inline void set_id(const Glib::ustring &str) { id.assign(str); }
	inline Glib::ustring get_id(void) const { return id; }
	inline void set_first_name(const Glib::ustring &str) { fname.assign(str); }
	inline Glib::ustring get_first_name(void) const { return fname; }
	inline void set_last_name(const Glib::ustring& str) { lname.assign(str); }
	inline Glib::ustring get_last_name(void) const { return lname; }
	inline void set_extra_years(short int val) { extra = val; }
	inline short int get_extra_years(void) const { return extra; }
	inline void set_rank(const Rank& rnk) { rank = rnk; }
	inline Rank get_rank(void) const { return rank; }
	inline void set_enlistment_date(const CustomDate& cd) { enlistment_date = cd; }
	inline void set_enlistment_date(const char* str) { enlistment_date = CustomDate { str }; }
	inline CustomDate get_enlistment_date(void) const { return enlistment_date; }
	inline void set_working_week(Workweek wwk) { working_week = wwk; }
	inline Workweek get_working_week(void) const { return working_week; }
	inline int get_years_of_service_at(const int val) const { return extra; /* to do: implement calculation */ }
 private:
	static Glib::ustring sqlite_table;
	Glib::ustring id;
	Glib::ustring fname;
	Glib::ustring lname;
	Rank rank;
	CustomDate enlistment_date;
	short int extra;
	Workweek working_week;
};

#endif // _MEMBER_HH_


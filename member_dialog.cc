/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * member_dialog.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "member_dialog.hh"
#include <gtkmm/messagedialog.h>

void MemberDialog::build_gui(MemberDialog* md) {
	Gtk::Box* box = md->get_content_area();
	md->bldr->get_widget("member_box", md->member_box);
	md->member_rank_liststore = Gtk::ListStore::create(md->combobox_tmcr);
	md->bldr->get_widget("member_rank_combobox", md->member_rank_combobox);
	md->member_rank_combobox->set_model(md->member_rank_liststore);
	md->bldr->get_widget("member_id_entry", md->member_id_entry);
	md->bldr->get_widget("member_name_entry", md->member_name_entry);
	md->bldr->get_widget("member_surname_entry", md->member_surname_entry);
	md->bldr->get_widget("member_enlistment_entry", md->member_enlistment_entry);
	md->bldr->get_widget("member_extra_entry", md->member_extra_entry);
	md->bldr->get_widget("member_weekplan_six_radiobutton", md->member_weekplan_six_radiobutton);
	md->bldr->get_widget("member_weekplan_five_radiobutton", md->member_weekplan_five_radiobutton);
	md->bldr->get_widget("confirm_member_button", md->confirm_member_button);
	md->bldr->get_widget("cancel_member_button", md->cancel_member_button);
	md->service_liststore = Gtk::ListStore::create(md->combobox2_tmcr);
	md->bldr->get_widget("service_treeview", md->service_treeview);
	md->service_treeview->set_model(md->service_liststore);
	md->service_treeselection = md->service_treeview->get_selection();
	md->service_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	md->start_cellrenderertext = (Gtk::CellRendererText*)md->service_treeview->get_column(0)->get_first_cell();
	md->end_cellrenderertext = (Gtk::CellRendererText*)md->service_treeview->get_column(1)->get_first_cell();
	md->bldr->get_widget("add_button", md->add_button);
	md->bldr->get_widget("remove_button", md->remove_button);
	box->pack_start(*(md->member_box));
	/*
	 * load values
	 */
	std::vector<Glib::ustring> ranks;
	try {
		ranks = md->database->get_rank_labels();
	} catch (const Database::DBError& dbe) {
		std::cerr << "MemberDialog::build_gui() " << dbe.get_message() << std::endl;
	}
	md->load_data_combobox(ranks.cbegin(), ranks.cend(), md->member_rank_liststore);
	/*
	 * signals connections
	 */
	md->confirm_member_button->signal_clicked().connect(sigc::mem_fun(md, &MemberDialog::on_confirm_member_button_clicked));
	md->cancel_member_button->signal_clicked().connect(sigc::mem_fun(md, &MemberDialog::on_cancel_member_button_clicked));
	md->add_button->signal_clicked().connect(sigc::mem_fun(md, &MemberDialog::on_add_button_clicked));
	md->remove_button->signal_clicked().connect(sigc::mem_fun(md, &MemberDialog::on_remove_button_clicked));
}

void MemberDialog::set_member(const Glib::ustring& id) {
	Member member;
	if (id.compare(Glib::ustring()) != 0) {
		try {
			member = database->get_member(id);
		} catch (const Database::DBError& dbe) {
			std::cerr << "MemberDialog::set_member " << dbe.get_message() << std::endl;
		} catch (const Database::DBEmptyResultException& dbe) {
			std::cerr << "MemberDialog::set_member " << dbe.get_message() << std::endl;
		}
		Gtk::TreeModel::iterator iter { get_combobox_item(member_rank_liststore, member.get_rank().get_full_label()) };
		member_rank_combobox->set_active(iter);
	}
	member_id_entry->set_text(member.get_id());
	member_name_entry->set_text(member.get_first_name());
	member_surname_entry->set_text(member.get_last_name());
	member_enlistment_entry->set_text(member.get_enlistment_date().get_formatted_string(DATE_FORMAT::date));
	member_extra_entry->set_text(Glib::ustring::format(member.get_extra_years()));
	switch (member.get_working_week()) {
	case Workweek::six:
		member_weekplan_six_radiobutton->set_active(true);
		break;
	case Workweek::five:
		member_weekplan_five_radiobutton->set_active(true);
		break;
	}
	load_service_in_treeview(member.get_id());
	service_treeselection->signal_changed().connect(sigc::mem_fun(this, &MemberDialog::on_service_treeselection_changed));
	start_cellrenderertext->signal_edited().connect(sigc::mem_fun(this, &MemberDialog::on_start_edited));
	end_cellrenderertext->signal_edited().connect(sigc::mem_fun(this, &MemberDialog::on_end_edited));
}

Member MemberDialog::get_member() const {
	if (!check_consistency()) {
		throw CDEmptyFieldException { std::string { "MemberDialog::get_member() member's data unconsistent!" } };
	}
	Gtk::TreeModel::iterator itr { member_rank_combobox->get_active() };
	Gtk::TreeModel::Row row { *itr };
	Rank rank;
	CustomDate cd;
	try {
		rank = database->get_rank_with_full(row[combobox_tmcr.column]);
		cd = CustomDate::parse_string(member_enlistment_entry->get_text().c_str());
	} catch (Database::DBError& dbe) {
		std::cerr << "MemberDialog::get_member() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "MemberDialog::get_member() " << dbe.get_message() << std::endl;
	} catch (const CustomDate::CustomDateException& cde) {
		std::cerr << "MemberDialog::get_member() " << cde.get_message() << std::endl;
	}
	short int extra = atoi(member_extra_entry->get_text().c_str());
	Workweek wwk = member_weekplan_six_radiobutton->get_active() ? Workweek::six : Workweek::five;
	Member mbr { member_id_entry->get_text(), member_name_entry->get_text(),member_surname_entry->get_text(),rank,cd,extra,wwk };
	return mbr;
}

bool MemberDialog::check_consistency() const {
	if (member_id_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (member_name_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (member_surname_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (member_rank_combobox->get_active_row_number() == -1) {
		return false;
	}
	if (member_enlistment_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	if (member_extra_entry->get_text().compare(Glib::ustring()) == 0) {
		return false;
	}
	return true;
}

void MemberDialog::on_confirm_member_button_clicked(void) {
	bool success = true;
	Member member;
	try {
		member = get_member();
	} catch (const MemberDialog::CDEmptyFieldException& ef) {
		Gtk::MessageDialog alert_dialog { *this, "Tutti i campi devono essere compilati!", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		alert_dialog.run();
		success = false;
	}
	if (success) {
		Member mbr;
		bool id_exist = false;
		try {
			mbr = database->get_member(member.get_id());
			id_exist = true;
		} catch (const Database::DBError& dbe) {
			std::cerr << "MemberDialog::on_confirm_member_button_clicked() " << dbe.get_message() << std::endl;
			return;
		} catch (const Database::DBEmptyResultException& dbe) {
			id_exist = false;
		}
		if (id_exist) {
			Gtk::MessageDialog alert_dialog { *this, "Questo membro è già presente, vuoi aggiornarlo con questi dati?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
			int res_id = alert_dialog.run();
			if (res_id == Gtk::RESPONSE_YES) {
				try {
					database->update(member);
				} catch (const Database::DBError& dbe) {
					std::cerr << "MemberDialog::on_confirm_member_button_clicked() " << dbe.get_message() << std::endl;
					return;
				}
			}
		} else {
			try {
				database->insert(member);
			} catch (const Database::DBError& dbe) {
				std::cerr << "MemberDialog::on_confirm_member_button_clicked() " << dbe.get_message() << std::endl;
				return;
			}
		}
	}
	response(Gtk::RESPONSE_APPLY);
}

void MemberDialog::on_cancel_member_button_clicked(void) {
	response(Gtk::RESPONSE_CANCEL);
}

void MemberDialog::load_service_in_treeview(const Glib::ustring& id) {
	service_liststore->clear();
	std::vector<Service> services;
	try {
		services = database->get_services(id);
	} catch (const Database::DBError& dbe) {
		std::cerr << "MembersDialog::load_members_in_treeview() " << dbe.get_message() << std::endl;
	}
	for (auto iter = services.cbegin(); iter != services.cend(); iter++) {
		Gtk::TreeModel::iterator iter_list = service_liststore->append();
		Gtk::TreeModel::Row row = *iter_list;
		row[combobox2_tmcr.column1] = Glib::ustring(iter->get_start().get_formatted_string(DATE_FORMAT::date));
		Glib::ustring str { iter->is_end_null() ? "" : iter->get_end().get_formatted_string(DATE_FORMAT::date) };
		row[combobox2_tmcr.column2] = str;
	}
}

void MemberDialog::on_service_treeselection_changed(void) {
	remove_button->set_sensitive(service_treeselection->count_selected_rows() > 0);
}

void MemberDialog::on_start_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> service_model { service_treeview->get_model() };
	Gtk::TreeModel::iterator iter { service_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		CustomDate bfr { CustomDate::parse_string(Glib::ustring(row[combobox2_tmcr.column1]).c_str(), DATE_FORMAT::date) };
		CustomDate aft { CustomDate::parse_string(new_text.c_str(), DATE_FORMAT::date) };
		Service srv { database->get_service(member_id_entry->get_text(), bfr) };
		database->update_service_start(srv, aft);
		row[combobox2_tmcr.column1] = aft.get_formatted_string(DATE_FORMAT::date);
	} catch (Database::DBError& dbe) {
		std::cerr << "MemberDialog::on_start_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "MemberDialog::on_start_edited() " << dbe.get_message() << std::endl;
	} catch (const CustomDate::CustomDateException& cde) {
		std::cerr << "Unknown date format!\n";
	}
}

void MemberDialog::on_end_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> service_model { service_treeview->get_model() };
	Gtk::TreeModel::iterator iter { service_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		Glib::ustring old_text { row[combobox2_tmcr.column2] };
		bool end_was_null { old_text.compare(Glib::ustring()) == 0 };
		CustomDate bfr { end_was_null ? CustomDate() : CustomDate::parse_string(old_text.c_str(), DATE_FORMAT::date) };
		bool end_is_null { new_text.compare(Glib::ustring()) == 0 };
		CustomDate aft { end_is_null ? CustomDate() : CustomDate::parse_string(new_text.c_str(), DATE_FORMAT::date) };
		CustomDate start { CustomDate::parse_string(Glib::ustring(row[combobox2_tmcr.column1]).c_str(), DATE_FORMAT::date) };
		Service srv { database->get_service(member_id_entry->get_text(), start) };
		if (end_is_null) {
			database->update_service_end(srv);
			row[combobox2_tmcr.column2] = new_text;
		} else {
			database->update_service_end(srv, aft);
			row[combobox2_tmcr.column2] = aft.get_formatted_string(DATE_FORMAT::date);
		}
	} catch (Database::DBError& dbe) {
		std::cerr << "MemberDialog::on_end_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "MemberDialog::on_end_edited() " << dbe.get_message() << std::endl;
	} catch (const CustomDate::CustomDateException& cde) {
		std::cerr << "Unknown date format!\n";
	}
}

void MemberDialog::on_add_button_clicked(void) {
	if (service_liststore->children().size() > 0) {
		bool reject = false;
		Gtk::TreeModel::iterator iter = service_liststore->children().end();
		iter--;
		Gtk::TreeModel::Row row = *iter;
		Glib::ustring str { row[combobox2_tmcr.column2] };
		if (str.compare(Glib::ustring()) == 0) {
			reject = true;
		} else {
			CustomDate cd { CustomDate::parse_string(str.c_str(), DATE_FORMAT::date) };
			if (cd > CustomDate::now()) {
				reject = true;
			}
		}
		if (reject) {
			Gtk::MessageDialog alert_dialog { *this, "Non puoi aggiungere un periodo di servizio perché l'ultimo è ancora in corso.", true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			alert_dialog.run();
			return;
		}
	} else {
		Glib::ustring str { member_id_entry->get_text() };
		try {
			Member mbr { database->get_member(str) };
		} catch (Database::DBError& dbe) {
			std::cerr << "MemberDialog::on_add_button_clicked() " << dbe.get_message() << std::endl;
		} catch (Database::DBEmptyResultException& dber) {
			return;
		}
	}
	Service srv;
	srv.set_member(member_id_entry->get_text());
	Gtk::TreeModel::iterator iter_list = service_liststore->append();
	Gtk::TreeModel::Row row = *iter_list;
	row[combobox2_tmcr.column1] = Glib::ustring(srv.get_start().get_formatted_string(DATE_FORMAT::date));
	row[combobox2_tmcr.column2] = Glib::ustring();
	try {
		database->insert(srv);
	} catch (Database::DBError& dbe) {
		std::cerr << "MemberDialog::on_add_button_clicked() " << dbe.get_message() << std::endl;
	}
}

void MemberDialog::on_remove_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Sei sicure di voler eliminare l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> service_treemodel { service_treeview->get_model() };
		std::vector<Gtk::TreePath> selected = service_treeselection->get_selected_rows();
		std::vector<Gtk::TreePath>::const_iterator iter = selected.begin();
		Gtk::TreeModel::iterator it { service_treemodel->get_iter(*iter) };
		Gtk::TreeModel::Row row { *it };
		try {
			CustomDate start { CustomDate::start_of_day(CustomDate::parse_string(Glib::ustring(row[combobox2_tmcr.column1]).c_str(), DATE_FORMAT::date)) };
			database->delete_service(member_id_entry->get_text(), start);
			service_liststore->erase(it);
		} catch (Database::DBError& dbe) {
			std::cerr << "MemberDialog::on_remove_button_clicked() " << dbe.get_message() << std::endl;
		} catch (const CustomDate::CustomDateException& cde) {
			std::cerr << "Unknown date format!\n";
		}
	}
}

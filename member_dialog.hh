/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * member_dialog.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MEMBER_DIALOG_HH_
#define _MEMBER_DIALOG_HH_

#include <gtkmm/entry.h>
#include <gtkmm/combobox.h>
#include <gtkmm/radiobutton.h>
#include "dual_string_tmcr.hh"
#include "control_dialog.hh"
#include "database.hh"

class MemberDialog : public ControlDialog {
 public:
	inline MemberDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline MemberDialog(const MemberDialog& md) : ControlDialog(md) { build_gui(this); }
	void set_member(const Glib::ustring& id);
	Member get_member() const;
	bool check_consistency() const;
	void on_confirm_member_button_clicked(void);
	void on_cancel_member_button_clicked(void);
	void load_service_in_treeview(const Glib::ustring& id);
	void on_service_treeselection_changed(void);
	void on_start_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_end_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_add_button_clicked(void);
	void on_remove_button_clicked(void);
 private:
	static void build_gui(MemberDialog* md);
	Gtk::Box* member_box;
	Gtk::Entry* member_id_entry;
	Gtk::ComboBox* member_rank_combobox;
	Glib::RefPtr<Gtk::ListStore> member_rank_liststore;
	Gtk::Entry* member_name_entry;
	Gtk::Entry* member_surname_entry;
	Gtk::Entry* member_enlistment_entry;
	Gtk::Entry* member_extra_entry;
	Gtk::RadioButton* member_weekplan_six_radiobutton;
	Gtk::RadioButton* member_weekplan_five_radiobutton;
	Gtk::Button* confirm_member_button;
	Gtk::Button* cancel_member_button;
	Glib::RefPtr<Gtk::ListStore> service_liststore;
	Gtk::TreeView* service_treeview;
	Glib::RefPtr<Gtk::TreeSelection> service_treeselection;
	Gtk::CellRendererText* start_cellrenderertext;
	Gtk::CellRendererText* end_cellrenderertext;
	Gtk::Button* add_button;
	Gtk::Button* remove_button;
	DualStringTreeModelColumnRecord combobox2_tmcr;
	Database* database;
};

#endif // _MEMBER_DIALOG_HH_

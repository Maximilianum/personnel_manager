/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * members_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "members_dialog.hh"
#include <gtkmm/messagedialog.h>

void MembersDialog::build_gui(MembersDialog* md) {
	Gtk::Box* box = md->get_content_area();
	md->bldr->get_widget("members_box", md->members_box);
	md->members_liststore = Gtk::ListStore::create(md->members_tmcr);
	md->bldr->get_widget("members_treeview", md->members_treeview);
	md->members_treeview->set_model(md->members_liststore);
	md->members_treeselection = md->members_treeview->get_selection();
	md->members_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	md->bldr->get_widget("add_member_button", md->add_member_button);
	md->bldr->get_widget("remove_member_button", md->remove_member_button);
	box->pack_start(*(md->members_box));
	/*
	 * load values
	 */
	md->load_members_in_treeview();
	/*
	 * signals connections
	 */
	md->members_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(md, &MembersDialog::on_members_treeview_clicked));
	md->members_treeselection->signal_changed().connect(sigc::mem_fun(md, &MembersDialog::on_members_treeselection_changed));
	md->add_member_button->signal_clicked().connect(sigc::mem_fun(md, &MembersDialog::on_add_member_button_clicked));
	md->remove_member_button->signal_clicked().connect(sigc::mem_fun(md, &MembersDialog::on_remove_member_button_clicked));
}

void MembersDialog::load_members_in_treeview(void) {
	members_liststore->clear();
	try {
		std::vector<Member> members { database->get_members() };
		for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
			Gtk::TreeModel::iterator iter_list = members_liststore->append();
			Gtk::TreeModel::Row row = *iter_list;
			row[members_tmcr.id] = iter->get_id();
			row[members_tmcr.rank] = iter->get_rank().get_short_label();
			row[members_tmcr.name] = iter->get_first_name();
			row[members_tmcr.surname] = iter->get_last_name();
			row[members_tmcr.enlistment] = Glib::ustring(iter->get_enlistment_date().get_formatted_string(DATE_FORMAT::date));
			row[members_tmcr.extra] = Glib::ustring::format(iter->get_extra_years());
			Glib::ustring wp = iter->get_working_week() == Workweek::six ? "sei giorni" : "cinque giorni";
			row[members_tmcr.weekplan] = wp;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "MembersDialog::load_members_in_treeview() " << dbe.get_message() << std::endl;
	}
}

void MembersDialog::on_members_treeview_clicked(GdkEventButton *event) {
	if (event->button == 1 && event->type == GDK_2BUTTON_PRESS)	{
		MemberDialog member_dialog { MemberDialog(Glib::ustring::compose("%1member_dialog.ui",ui_path), *this, database) };
		try {
			Glib::ustring m_id = get_selected_member_id();
			member_dialog.set_member(m_id);
			int res_id = member_dialog.run();
			if (res_id == Gtk::RESPONSE_APPLY) {
				load_members_in_treeview();
			}
		} catch (IteratorNotFoundException infe) {}
	}
}

void MembersDialog::on_members_treeselection_changed(void) {
	int n { members_treeselection->count_selected_rows() };
	remove_member_button->set_sensitive(n > 0);
}

Gtk::TreeModel::iterator MembersDialog::get_selected_member_iter() {
	int nr_sel = members_treeselection->count_selected_rows();
	Glib::RefPtr<Gtk::TreeModel> members_treemodel = members_treeview->get_model();
	if (nr_sel > 0) {
		std::vector<Gtk::TreePath> selected = members_treeselection->get_selected_rows();
		std::vector<Gtk::TreePath>::const_iterator iter = selected.begin();
		return members_treemodel->get_iter(*iter);
	} else {
		throw IteratorNotFoundException { std::string { "MembersDialog::get_selected_member_iter() iterator not found." } };
	}
}

Glib::ustring MembersDialog::get_selected_member_id(void) {
	Gtk::TreeModel::iterator itv { get_selected_member_iter() };
	Gtk::TreeModel::Row mrow = *itv;
	Glib::ustring m_id = mrow[members_tmcr.id];
	return m_id;
}

void MembersDialog::on_add_member_button_clicked(void) {
	MemberDialog member_dialog { MemberDialog(Glib::ustring::compose("%1member_dialog.ui",ui_path), *this, database) };
	member_dialog.set_member(Glib::ustring());
	int res_id = member_dialog.run();
	if (res_id == Gtk::RESPONSE_APPLY) {
		load_members_in_treeview();
	}
}

void MembersDialog::on_remove_member_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "L'eliminazione dell'elemento selezionato comporterà la cancellazione delle sue impostazioni e di tutti i suoi turni, sei sicuro di procedere?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id = alert_dialog.run();
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> members_treemodel { members_treeview->get_model() };
		std::vector<Gtk::TreePath> selected { members_treeselection->get_selected_rows() };
		for (auto iter = selected.cbegin(); iter != selected.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { members_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			try {
				database->delete_services(row[members_tmcr.id]);
				database->delete_member_settings(row[members_tmcr.id]);
				database->delete_shifts(row[members_tmcr.id]);
				database->delete_licenses(row[members_tmcr.id]);
				database->delete_member(row[members_tmcr.id]);
				members_liststore->erase(iter_tm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "MembersDialog::on_remove_member_button_clicked() " << dbe.get_message() << std::endl;
				return;
			}
		}
	}
}


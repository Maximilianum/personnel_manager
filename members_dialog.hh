/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * members_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _MEMBERS_DIALOG_HH_
#define _MEMBERS_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "member_dialog.hh"
#include "members_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class MembersDialog : public ControlDialog {
 public:
	class IteratorNotFoundException
	{
	public:
    IteratorNotFoundException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
	inline MembersDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline MembersDialog(const MembersDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(MembersDialog* md);
	void load_members_in_treeview(void);
	void on_members_treeview_clicked(GdkEventButton *event);
	void on_members_treeselection_changed(void);
	Gtk::TreeModel::iterator get_selected_member_iter();
	Glib::ustring get_selected_member_id(void);
	void on_add_member_button_clicked(void);
	void on_remove_member_button_clicked(void);
	void on_licenses_member_button_clicked(void);

	Gtk::Box* members_box;
	Glib::RefPtr<Gtk::ListStore> members_liststore;
	Gtk::TreeView* members_treeview;
	Glib::RefPtr<Gtk::TreeSelection> members_treeselection;
	Gtk::Button* add_member_button;
	Gtk::Button* remove_member_button;
	MembersTreeModelColumnRecord members_tmcr;
	Database* database;
};

#endif // _MEMBERS_DIALOG_HH_

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offwork.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "offwork.hh"
#include <vector>

Glib::ustring OffWork::sqlite_table { Glib::ustring("TABLE OFFWORK (" \
													"LABEL TEXT PRIMARY KEY NOT NULL, " \
													"FLAGS INTEGER NOT NULL, "	\
													"FOREIGN KEY(LABEL) REFERENCES LABEL(FULL));") };

int OffWork::retrieve_offwork(void *data, int argc, char **argv, char **azColName) {
	OffWork ow;
	std::vector<OffWork>* offwrk { static_cast<std::vector<OffWork>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			ow.set_full_label(argv[i]);
		} else if ((Glib::ustring(azColName[i]).compare(Glib::ustring("SLABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("SHORT")) == 0) && argv[i]) {
			ow.set_short_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLAGS") && argv[i]) {
			ow.set_flags(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("ABSENCE") && argv[i]) {
			ow.set_absence(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("WEEKDAY") && argv[i]) {
			ow.set_weekday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SATURDAY") && argv[i]) {
			ow.set_saturday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SUNDAY") && argv[i]) {
			ow.set_sunday(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("HOLIDAY") && argv[i]) {
			ow.set_holiday(atoi(argv[i]));
		}
	}
	offwrk->push_back(ow);
	return 0;
}

int OffWork::retrieve_label(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring str;
	std::vector<Glib::ustring>* labels { static_cast<std::vector<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("LABEL")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("FULL")) == 0) && argv[i]) {
			str.assign(argv[i]);
		}
	}
	labels->push_back(str);
	return 0;
}

OffWork& OffWork::operator=(const OffWork& lb) {
	if (this != &lb) {
		flabel = lb.flabel;
		slabel = lb.slabel;
		flags = lb.flags;
	}
	return *this;
}

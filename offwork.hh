/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offwork.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OFFWORK_HH_
#define _OFFWORK_HH_

#include <cstdint>
#include "label.hh"

class OffWork : public Label {
 public:
	/*
	 * flags bit mask
	 * 0 absence
	 * 1 weekday
	 * 2 saturday
	 * 3 sunday
	 * 4 holyday
	 */
	static constexpr std::uint8_t mask0 { 1 << 0 };
	static constexpr std::uint8_t mask1 { 1 << 1 };
	static constexpr std::uint8_t mask2 { 1 << 2 };
	static constexpr std::uint8_t mask3 { 1 << 3 };
	static constexpr std::uint8_t mask4 { 1 << 4 };
	
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_offwork(void *data, int argc, char **argv, char **azColName);
	static int retrieve_label(void *data, int argc, char **argv, char **azColName);
	inline OffWork() : flags (0) {};
	inline OffWork(const OffWork& ow) : Label(ow.flabel, ow.slabel), flags (ow.flags) {}
	inline OffWork(const Glib::ustring& str1, const Glib::ustring& str2, std::uint8_t flgs) : Label(str1, str2), flags (flgs) {}
	OffWork& operator=(const OffWork& lb);
	inline bool operator<(const OffWork& lb) const { return flabel < lb.flabel; }
	inline bool operator==(const OffWork& ws) const { return flabel.compare(ws.flabel) == 0 && slabel.compare(ws.slabel) == 0 && flags == ws.flags; }
	inline void set_flags(std::uint8_t flgs) { flags = flgs; }
	inline std::uint8_t get_flags() const { return flags; }
	inline void set_absence(bool flg) { flags = flg ? flags | mask0 : flags & ~mask0; }
	inline bool get_absence(void) const { return flags & mask0; }
	inline void set_weekday(bool flg) { flags = flg ? flags | mask1 : flags & ~mask1; }
	inline bool get_weekday(void) const { return flags & mask1; }
	inline void set_saturday(bool flg) { flags = flg ? flags | mask2 : flags & ~mask2; }
	inline bool get_saturday(void) const { return flags & mask2; }
	inline void set_sunday(bool flg) { flags = flg ? flags | mask3 : flags & ~mask3; }
	inline bool get_sunday(void) const { return flags & mask3; }
	inline void set_holiday(bool flg) { flags = flg ? flags | mask4 : flags & ~mask4; }
	inline bool get_holiday(void) const { return flags & mask4; }
 protected:
	std::uint8_t flags;
 private:
	static Glib::ustring sqlite_table;
	
};

#endif // _OFFWORK_HH_


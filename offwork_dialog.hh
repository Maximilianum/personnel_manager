/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offwork_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OFFWORK_DIALOG_HH_
#define _OFFWORK_DIALOG_HH_

#include <gtkmm/combobox.h>
#include <gtkmm/button.h>
#include <set>
#include "control_dialog.hh"
#include "database.hh"

class OffWorkDialog : public ControlDialog {
 public:
	inline OffWorkDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline OffWorkDialog(const OffWorkDialog& sd) : ControlDialog(sd) { build_gui(this); }
	void init(void);
 protected:
	void load_data_combobox(std::set<Glib::ustring>::const_iterator start, std::set<Glib::ustring>::const_iterator end, Glib::RefPtr<Gtk::ListStore> list);
	static void build_gui(OffWorkDialog* sd);
	void on_confirm_button_clicked(void);
	void on_cancel_button_clicked(void);

	Gtk::Box* box;
	Gtk::ComboBox* label_combobox;
	Glib::RefPtr<Gtk::ListStore> label_liststore;
	Gtk::Label* frame_label;
	Gtk::Button* confirm_button;
	Gtk::Button* cancel_button;
	Database* database;
};

#endif // _OFFWORK_DIALOG_HH_

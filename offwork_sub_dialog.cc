/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offwork_sub_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "offwork_sub_dialog.hh"
#include <gtkmm/messagedialog.h>

void OffWorkSubDialog::init(const Glib::ustring lbl) {
	original_label.assign(lbl);
	frame_label->set_text("Giorno Libero");
	std::vector<Glib::ustring> offworks;
	try {
		offworks = database->get_offworks_label();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorkSubDialog::init() " << dbe.get_message() << std::endl;
	}
	if (offworks.size() > 0) {
		for (auto iter = offworks.begin(); iter != offworks.end(); iter++) {
			if (iter->compare(original_label) == 0) {
				offworks.erase(iter);
				break;
			}
		}
		ControlDialog::load_data_combobox(offworks.cbegin(), offworks.cend(), label_liststore);
		label_combobox->set_active(0);
	} else {
		confirm_button->set_sensitive(false);
	}
	/*
	 * signals connections
	 */
	confirm_button->signal_clicked().connect(sigc::mem_fun(this, &OffWorkSubDialog::on_confirm_button_clicked));
	cancel_button->signal_clicked().connect(sigc::mem_fun(this, &OffWorkSubDialog::on_cancel_button_clicked));
}

void OffWorkSubDialog::on_confirm_button_clicked(void) {
	Gtk::TreeModel::iterator it { label_combobox->get_active() };
	Gtk::TreeModel::Row row { *it };
	try {
		database->update_shifts_labels(original_label,row[combobox_tmcr.column]);
		response(Gtk::RESPONSE_APPLY);
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorkSubDialog::on_confirm_button_clicked() " << dbe.get_message() << std::endl;
		return;
	}
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offwork_sub_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OFFWORK_SUB_DIALOG_HH_
#define _OFFWORK_SUB_DIALOG_HH_

#include "offwork_dialog.hh"

class OffWorkSubDialog : public OffWorkDialog {
 public:
	inline OffWorkSubDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : OffWorkDialog(path, parent, db) {}
	inline OffWorkSubDialog(const OffWorkSubDialog& sd) : OffWorkDialog(sd) {}
	void init(const Glib::ustring lbl);
 protected:
	void on_confirm_button_clicked(void);
	Glib::ustring original_label;
};

#endif // _OFFWORK_SUB_DIALOG_HH_

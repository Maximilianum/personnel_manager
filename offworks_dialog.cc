/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offworks_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "offworks_dialog.hh"
#include "offwork_dialog.hh"
#include "offwork_sub_dialog.hh"
#include <gtkmm/messagedialog.h>

void OffWorksDialog::build_gui(OffWorksDialog* ld) {
	Gtk::Box* box = ld->get_content_area();
	ld->bldr->get_widget("offworks_box", ld->offworks_box);
	ld->bldr->get_widget("offworks_treeview", ld->offworks_treeview);
	ld->offworks_liststore = Gtk::ListStore::create(ld->offwork_tmcr);
	ld->offworks_treeview->set_model(ld->offworks_liststore);
	ld->offwork_label_treeviewcolumn = ld->offworks_treeview->get_column(0);
	ld->offwork_label_cellrenderertext = (Gtk::CellRendererText*)ld->offwork_label_treeviewcolumn->get_first_cell();
	ld->offworks_treeview->append_column_editable(Glib::ustring("Ass."), ld->offwork_tmcr.absence);
	ld->offwork_absence_treeviewcolumn = ld->offworks_treeview->get_column(1);
	ld->offwork_absence_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->offwork_absence_treeviewcolumn->get_first_cell();
	ld->offworks_treeview->append_column_editable(Glib::ustring("Fer."), ld->offwork_tmcr.weekday);
	ld->offwork_weekday_treeviewcolumn = ld->offworks_treeview->get_column(2);
	ld->offwork_weekday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->offwork_weekday_treeviewcolumn->get_first_cell();
	ld->offworks_treeview->append_column_editable(Glib::ustring("Sab."), ld->offwork_tmcr.saturday);
	ld->offwork_saturday_treeviewcolumn = ld->offworks_treeview->get_column(3);
	ld->offwork_saturday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->offwork_saturday_treeviewcolumn->get_first_cell();
	ld->offworks_treeview->append_column_editable(Glib::ustring("Dom."), ld->offwork_tmcr.sunday);
	ld->offwork_sunday_treeviewcolumn = ld->offworks_treeview->get_column(4);
	ld->offwork_sunday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->offwork_sunday_treeviewcolumn->get_first_cell();
	ld->offworks_treeview->append_column_editable(Glib::ustring("Fest."), ld->offwork_tmcr.holiday);
	ld->offwork_holiday_treeviewcolumn = ld->offworks_treeview->get_column(5);
	ld->offwork_holiday_cellrenderertoggle = (Gtk::CellRendererToggle*)ld->offwork_holiday_treeviewcolumn->get_first_cell();
	ld->offworks_treeselection = ld->offworks_treeview->get_selection();
	ld->offworks_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	ld->bldr->get_widget("add_offwork_button", ld->add_offwork_button);
	ld->bldr->get_widget("remove_offwork_button", ld->remove_offwork_button);
	box->pack_start(*(ld->offworks_box));
	/*
	 * load values
	 */
	ld->load_offworks_in_treeview();
	/*
	 * signals connections
	 */
	ld->offworks_treeselection->signal_changed().connect(sigc::mem_fun(ld, &OffWorksDialog::on_offworks_treeselection_changed));
	ld->add_offwork_button->signal_clicked().connect(sigc::mem_fun(ld, &OffWorksDialog::on_add_offwork_button_clicked));
	ld->offwork_absence_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &OffWorksDialog::on_absence_toggled));
	ld->offwork_weekday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &OffWorksDialog::on_weekday_toggled));
	ld->offwork_saturday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &OffWorksDialog::on_saturday_toggled));
	ld->offwork_sunday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &OffWorksDialog::on_sunday_toggled));
	ld->offwork_holiday_cellrenderertoggle->signal_toggled().connect(sigc::mem_fun(ld, &OffWorksDialog::on_holiday_toggled));
	ld->remove_offwork_button->signal_clicked().connect(sigc::mem_fun(ld, &OffWorksDialog::on_remove_offwork_button_clicked));
}

void OffWorksDialog::load_offworks_in_treeview(void) {
	offworks_liststore->clear();
	try {
		std::vector<OffWork> offworks { database->get_offworks() };
		for (auto iter = offworks.cbegin(); iter != offworks.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { offworks_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[offwork_tmcr.label] = iter->get_full_label();
			row[offwork_tmcr.absence] = iter->get_absence();
			row[offwork_tmcr.weekday] = iter->get_weekday();
			row[offwork_tmcr.saturday] = iter->get_saturday();
			row[offwork_tmcr.sunday] = iter->get_sunday();
			row[offwork_tmcr.holiday] = iter->get_holiday();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::load_data_in_offwork_treeview() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_add_offwork_button_clicked(void) {
	OffWorkDialog offwork_dialog { OffWorkDialog(Glib::ustring::compose("%1item_picker_dialog.ui",ui_path), *this, database) };
	offwork_dialog.init();
	int res_id { offwork_dialog.run() };
	if (res_id == Gtk::RESPONSE_APPLY) {
		load_offworks_in_treeview();
	}
}

void OffWorksDialog::on_absence_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> offworks_model { offworks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { offworks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		OffWork ow { database->get_offwork(row[offwork_tmcr.label]) };
		ow.set_absence(!ow.get_absence());
		std::uint8_t flags { ow.get_flags() };
		database->update_offwork_flags(row[offwork_tmcr.label],flags);
		load_offworks_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_absence_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_absence_toggled() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_weekday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> offworks_model { offworks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { offworks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		OffWork ow { database->get_offwork(row[offwork_tmcr.label]) };
		ow.set_weekday(!ow.get_weekday());
		std::uint8_t flags { ow.get_flags() };
		database->update_offwork_flags(row[offwork_tmcr.label],flags);
		load_offworks_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_weekday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_weekday_toggled() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_saturday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> offworks_model { offworks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { offworks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		OffWork ow { database->get_offwork(row[offwork_tmcr.label]) };
		ow.set_saturday(!ow.get_saturday());
		std::uint8_t flags { ow.get_flags() };
		database->update_offwork_flags(row[offwork_tmcr.label],flags);
		load_offworks_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_saturday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_saturday_toggled() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_sunday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> offworks_model { offworks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { offworks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		OffWork ow { database->get_offwork(row[offwork_tmcr.label]) };
		ow.set_sunday(!ow.get_sunday());
		std::uint8_t flags { ow.get_flags() };
		database->update_offwork_flags(row[offwork_tmcr.label],flags);
		load_offworks_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_sunday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_sunday_toggled() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_holiday_toggled(const Glib::ustring& path) {
	Glib::RefPtr<Gtk::TreeModel> offworks_model { offworks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { offworks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		OffWork ow { database->get_offwork(row[offwork_tmcr.label]) };
		ow.set_holiday(!ow.get_holiday());
		std::uint8_t flags { ow.get_flags() };
		database->update_offwork_flags(row[offwork_tmcr.label],flags);
		load_offworks_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_holiday_toggled() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "OffWorksDialog::on_offwork_holiday_toggled() " << dbe.get_message() << std::endl;
	}
}

void OffWorksDialog::on_remove_offwork_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> offworks_treemodel { offworks_treeview->get_model() };
		std::vector<Gtk::TreeModel::Path> selected { offworks_treeselection->get_selected_rows() };
		for (auto iter = selected.cbegin(); iter != selected.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { offworks_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			try {
				std::vector<Shift> shifts { database->get_shifts_with_offwork(row[offwork_tmcr.label]) };
				if (shifts.size() > 0) {
					Gtk::MessageDialog alert2_dialog { *this, "Sono stati rilevati dei turni che dovranno essere modificati, vuoi proseguire con l'operazione?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
					int res_id { alert2_dialog.run() };
					if (res_id == Gtk::RESPONSE_YES) {
						OffWorkSubDialog offwork_sub_dialog { OffWorkSubDialog(Glib::ustring::compose("%1item_picker_dialog.ui",ui_path), *this, database) };
						offwork_sub_dialog.init(row[offwork_tmcr.label]);
						int res_id { offwork_sub_dialog.run() };
						if (res_id == Gtk::RESPONSE_APPLY) {
							database->delete_offwork(row[offwork_tmcr.label]);
							offworks_liststore->erase(iter_tm);
						}
					}
				} else {
					database->delete_offwork(row[offwork_tmcr.label]);
					offworks_liststore->erase(iter_tm);
				}
			} catch (const Database::DBError& dbe) {
				std::cerr << "OffWorksDialog::on_remove_offwork_button_clicked() " << dbe.get_message() << std::endl;
				return;
			} catch (const Database::DBEmptyResultException& dbe) {
				std::cerr << "OffWorksDialog::on_remove_offwork_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * offworks_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _OFFWORKS_DIALOG_HH_
#define _OFFWORKS_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "offwork_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class OffWorksDialog : public ControlDialog {
 public:
	inline OffWorksDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline OffWorksDialog(const OffWorksDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(OffWorksDialog* sd);
	void load_offworks_in_treeview(void);
	inline void on_offworks_treeselection_changed(void) { remove_offwork_button->set_sensitive(offworks_treeselection->count_selected_rows() > 0); }
	void on_add_offwork_button_clicked(void);
	void on_absence_toggled(const Glib::ustring& path);
	void on_weekday_toggled(const Glib::ustring& path);
	void on_saturday_toggled(const Glib::ustring& path);
	void on_sunday_toggled(const Glib::ustring& path);
	void on_holiday_toggled(const Glib::ustring& path);
	void on_remove_offwork_button_clicked(void);

	Gtk::Box* offworks_box;
	Gtk::TreeView* offworks_treeview;
	Glib::RefPtr<Gtk::ListStore> offworks_liststore;
	Glib::RefPtr<Gtk::TreeSelection> offworks_treeselection;
	Gtk::TreeViewColumn* offwork_label_treeviewcolumn;
	Gtk::CellRendererText* offwork_label_cellrenderertext;
	Gtk::TreeViewColumn* offwork_absence_treeviewcolumn;
	Gtk::CellRendererToggle* offwork_absence_cellrenderertoggle;
	Gtk::TreeViewColumn* offwork_weekday_treeviewcolumn;
	Gtk::CellRendererToggle* offwork_weekday_cellrenderertoggle;
	Gtk::TreeViewColumn* offwork_saturday_treeviewcolumn;
	Gtk::CellRendererToggle* offwork_saturday_cellrenderertoggle;
	Gtk::TreeViewColumn* offwork_sunday_treeviewcolumn;
	Gtk::CellRendererToggle* offwork_sunday_cellrenderertoggle;
	Gtk::TreeViewColumn* offwork_holiday_treeviewcolumn;
	Gtk::CellRendererToggle* offwork_holiday_cellrenderertoggle;
	Gtk::Button* add_offwork_button;
	Gtk::Button* remove_offwork_button;
	OffWorkTreeModelColumnRecord offwork_tmcr;
	Database* database;
};

#endif // _OFFWORKS_DIALOG_HH_

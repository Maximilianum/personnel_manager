/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * view_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "print_dialog.hh"

void PrintDialog::build_gui(PrintDialog* pd) {
	Gtk::Box* box = pd->get_content_area();
	pd->bldr->get_widget("print_box", pd->print_box);
	pd->bldr->get_widget("members_per_page_entry", pd->members_per_page_entry);
	pd->bldr->get_widget("logo_filechooserbutton", pd->logo_filechooserbutton);
	pd->bldr->get_widget("week_description_entry", pd->week_description_entry);
	pd->bldr->get_widget("weekday_combobox", pd->weekday_combobox);
	pd->weekday_liststore = Gtk::ListStore::create(pd->combobox_tmcr);
	pd->weekday_combobox->set_model(pd->weekday_liststore);
	std::vector<Glib::ustring> weekdays = { Glib::ustring("Domenica"), Glib::ustring("Lunedì"), Glib::ustring("Martedì"), Glib::ustring("Mercoledì"), Glib::ustring("Giovedì"), Glib::ustring("Venerdì"), Glib::ustring("Sabato") };
	pd->load_data_combobox(weekdays.begin(), weekdays.end(), pd->weekday_liststore);
	pd->weekday_combobox->set_active(0);
	pd->bldr->get_widget("week_shift_button", pd->week_shift_button);
	pd->bldr->get_widget("week_presence_button", pd->week_presence_button);
	box->pack_start(*(pd->print_box));
	/*
	 * load values
	 */
	pd->members_per_page_entry->set_text(Glib::ustring::format(pd->database->get_setting(MEMBERS_PER_PAGE)));
	Glib::RefPtr<Gio::File> logo_file { Gio::File::create_for_path(pd->database->get_str_setting(LOGO_FILE_PATH)) };
	if (logo_file->query_exists()) {
		try {
			pd->logo_filechooserbutton->set_filename(pd->database->get_str_setting(LOGO_FILE_PATH));
		} catch (const Database::DBError& dbe) {
			std::cerr << "PrintDialog::build_gui() " << dbe.get_message() << std::endl;
			throw dbe;
		} catch (const Database::DBEmptyResultException& dbe) {
			pd->logo_filechooserbutton->set_filename(Glib::ustring());
		}
	}
	try {
		pd->week_description_entry->set_text(pd->database->get_str_setting(PRINT_WEEK_DESCR));
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::build_gui() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		pd->week_description_entry->set_text(Glib::ustring());
	}
	try {
		pd->weekday_combobox->set_active(pd->database->get_setting(WEEK_START));
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::build_gui() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		pd->weekday_combobox->set_active(0);
	}
	bool pwv = false;
	try {
		pwv = pd->database->get_setting(PRINT_WEEK_VIEW);
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::build_gui() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {
		pd->weekday_combobox->set_active(0);
	}
	if (pwv) {
		pd->week_shift_button->set_active(true);
	} else {
		pd->week_presence_button->set_active(true);
	}
	/*
	 * signals connections
	 */
	pd->members_per_page_entry->signal_activate().connect(sigc::mem_fun(pd, &PrintDialog::on_members_per_page_edited));
	pd->logo_filechooserbutton->signal_file_set().connect(sigc::mem_fun(pd, &PrintDialog::on_logo_filechooser_set));
	pd->week_description_entry->signal_activate().connect(sigc::mem_fun(pd, &PrintDialog::on_week_description_edited));
	pd->weekday_combobox->signal_changed().connect(sigc::mem_fun(pd, &PrintDialog::on_week_start_selection_changed));
	pd->week_shift_button->signal_toggled().connect(sigc::mem_fun(pd, &PrintDialog::on_week_view_toggled));
}

void PrintDialog::on_members_per_page_edited(void) {
	int number { atoi(members_per_page_entry->get_text().c_str()) };
	members_per_page_entry->set_text(Glib::ustring::format(number));
	try {
		if (database->check_setting(MEMBERS_PER_PAGE)) {
			database->update(MEMBERS_PER_PAGE, number);
		} else {
			database->insert(MEMBERS_PER_PAGE, number);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::on_members_per_page_edited() " << dbe.get_message() << std::endl;
	}
}

void PrintDialog::on_logo_filechooser_set(void) {
	try {
		Glib::ustring path { logo_filechooserbutton->get_filename() };
		if (database->check_setting(LOGO_FILE_PATH)) {
			database->update(LOGO_FILE_PATH, path);
		} else {
			database->insert(LOGO_FILE_PATH, path);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::on_logo_filechooser_set() " << dbe.get_message() << std::endl;
	} 
}

void PrintDialog::on_week_description_edited(void) {
	try {
		Glib::ustring str { week_description_entry->get_text() };
		if (database->check_str_setting(PRINT_WEEK_DESCR)) {
			database->update(PRINT_WEEK_DESCR, str);
		} else {
			database->insert(PRINT_WEEK_DESCR, str);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::on_week_description_edited() " << dbe.get_message() << std::endl;
	}
}

void PrintDialog::on_week_start_selection_changed(void) {
	try {
		int val { weekday_combobox->get_active_row_number() };
		if (database->check_setting(WEEK_START)) {
			database->update(WEEK_START, val);
		} else {
			database->insert(WEEK_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::on_week_start_selection_changed() " << dbe.get_message() << std::endl;
	}
}

void PrintDialog::on_week_view_toggled(void) {
	try {
		bool flag { week_shift_button->get_active() };
		if (database->check_setting(PRINT_WEEK_VIEW)) {
			database->update(PRINT_WEEK_VIEW, flag);
		} else {
			database->insert(PRINT_WEEK_VIEW, flag);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "PrintDialog::on_week_view_toggled() " << dbe.get_message() << std::endl;
	}
}

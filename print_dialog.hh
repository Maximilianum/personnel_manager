/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * print_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRINT_DIALOG_HH_
#define _PRINT_DIALOG_HH_

#include <gtkmm/entry.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/filechooserbutton.h>
#include <gtkmm/combobox.h>
#include "control_dialog.hh"
#include "database.hh"

#define MEMBERS_PER_PAGE		"MBRS_PPAGE"
#define LOGO_FILE_PATH			"LOGO_FPATH"
#define PRINT_WEEK_DESCR		"PRNT_WKDSCR"
#define WEEK_START				"WK_STRT"
#define PRINT_WEEK_VIEW			"PRNT_WKVW"

class PrintDialog : public ControlDialog {
 public:
	inline PrintDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline PrintDialog(const PrintDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(PrintDialog* pd);
	void on_members_per_page_edited(void);
	void on_logo_filechooser_set(void);
	void on_week_description_edited(void);
	void on_week_start_selection_changed(void);
	void on_week_view_toggled(void);

	Gtk::Box* print_box;
	Gtk::Entry* members_per_page_entry;
	Gtk::FileChooserButton* logo_filechooserbutton;
	Gtk::Entry* week_description_entry;
	Gtk::ComboBox* weekday_combobox;
	Glib::RefPtr<Gtk::ListStore> weekday_liststore;
	Gtk::RadioButton* week_shift_button;
	Gtk::RadioButton* week_presence_button;
	Database* database;
};

#endif // _PRINT_DIALOG_HH_

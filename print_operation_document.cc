/*
 * print_operation_document.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "print_operation_document.hh"

void PrintOperationDocument::on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& print_context) {
  Cairo::RefPtr<Cairo::Context> cr = print_context->get_cairo_context();
  set_n_pages(drawing_area->count_pages());
  drawing_area->init_font_and_colour(cr);
}

void PrintOperationDocument::on_draw_page(const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr) {
  Cairo::RefPtr<Cairo::Context> cr = context->get_cairo_context();
  drawing_area->draw_at_page(cr, page_nr);
}

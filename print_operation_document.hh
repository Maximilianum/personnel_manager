/*
 * print_operation_document.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRINT_OPERATION_DOCUMENT_HH_
#define _PRINT_OPERATION_DOCUMENT_HH_

#include <gtkmm/printoperation.h>
#include "drawing_document.hh"

class PrintOperationDocument: public Gtk::PrintOperation {
public:
  inline static Glib::RefPtr<PrintOperationDocument> create(void) { return Glib::RefPtr<PrintOperationDocument> (new PrintOperationDocument()); }
  inline virtual ~PrintOperationDocument() {}
  inline void set_drawing_area(DrawingDocument* dd) { drawing_area = dd; }
protected:
  inline PrintOperationDocument() {}
  virtual void on_begin_print(const Glib::RefPtr<Gtk::PrintContext>& context);
  virtual void on_draw_page(const Glib::RefPtr<Gtk::PrintContext>& context, int page_nr);
private:
  DrawingDocument* drawing_area;
};

#endif // _PRINT_OPERATION_DOCUMENT_HH_


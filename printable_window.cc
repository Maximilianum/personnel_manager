/*
 * printable_window.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "printable_window.hh"
#include <gtkmm/stock.h>
#include <gtkmm/messagedialog.h>

PrintableWindow::PrintableWindow(DrawingDocument* dd): m_box (Gtk::ORIENTATION_VERTICAL), drawing_area (dd) {
  set_default_size(595.0, 880.0);
  set_title(Glib::ustring("Turni del mese corrente"));
  set_modal(true);
  scrolledwin.add(*drawing_area);
  m_box.pack_end(scrolledwin, true, true, 0);
  add(m_box);
  build_menu();
  show_all_children();
}

PrintableWindow::~PrintableWindow() {
  delete drawing_area;
}

void PrintableWindow::build_menu(void) {
  //Create actions for menus and toolbars:
  m_ref_actiongroup = Gtk::ActionGroup::create();
  //File menu:
  m_ref_actiongroup->add(Gtk::Action::create("FileMenu", "_File"));
  m_ref_actiongroup->add(Gtk::Action::create("Print", Gtk::Stock::PRINT), sigc::mem_fun(*this, &PrintableWindow::onMenuPrint));
  m_ref_actiongroup->add(Gtk::Action::create("Close", Gtk::Stock::CLOSE), sigc::mem_fun(*this, &PrintableWindow::onMenuClose));
  m_ref_uimanager = Gtk::UIManager::create();
  m_ref_uimanager->insert_action_group(m_ref_actiongroup);
  add_accel_group(m_ref_uimanager->get_accel_group());
  //Layout the actions in a menubar and toolbar:
  Glib::ustring ui_info =
    "<ui>"
    "  <menubar name='MenuBar'>"
    "    <menu action='FileMenu'>"
    "      <menuitem action='Print'/>"
    "      <separator/>"
    "      <menuitem action='Close'/>"
    "    </menu>"
    "  </menubar>"
    "</ui>";
  try	{
    m_ref_uimanager->add_ui_from_string(ui_info);
  } catch(const Glib::Error& ex) {
    std::cerr << "building menus failed: " << ex.what();
  }
  //Get the menubar and toolbar widgets, and add them to a container widget:
  Gtk::Widget* pMenubar = m_ref_uimanager->get_widget("/MenuBar");
  if(pMenubar) {
    m_box.pack_start(*pMenubar, Gtk::PACK_SHRINK);
  }
}

void PrintableWindow::onMenuPrint(void) {
  print_or_preview(Gtk::PRINT_OPERATION_ACTION_PRINT_DIALOG);
}

void PrintableWindow::onMenuClose(void) {
  hide();
}

void PrintableWindow::print_or_preview(Gtk::PrintOperationAction print_action) {
  Glib::RefPtr<PrintOperationDocument> print = PrintOperationDocument::create();
  print->set_drawing_area(drawing_area);
  print->set_track_print_status();
  print->set_default_page_setup(m_ref_pagesetup);
  print->set_print_settings(m_ref_settings);
  print->signal_done().connect(sigc::bind (sigc::mem_fun(*this, &PrintableWindow::on_printOperation_done), print));
  try	{
    print->run(print_action, *this);
  } catch (const Gtk::PrintError& ex)	{
    std::cerr << "An error occurred while trying to run a print operation:" << ex.what() << std::endl;
  }
}

void PrintableWindow::on_printOperation_status_changed(const Glib::RefPtr<PrintOperationDocument>& operation) {
  Glib::ustring status_msg;
  if ( operation->is_finished()) {
    status_msg = "Print job completed.";
  } else {
    status_msg = operation->get_status_string ();
  }
  std::cout << status_msg.raw() << std::endl;
}

void PrintableWindow::on_printOperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<PrintOperationDocument>& operation) {
  if (result == Gtk::PRINT_OPERATION_RESULT_ERROR) {
    Gtk::MessageDialog err_dialog(*this, "Error printing month", false, Gtk::MESSAGE_ERROR, Gtk::BUTTONS_OK, true);
    err_dialog.run();
  } else if (result == Gtk::PRINT_OPERATION_RESULT_APPLY) {
    m_ref_settings = operation->get_print_settings();
  }
  if (!operation->is_finished()) {
    operation->signal_status_changed().connect(sigc::bind(sigc::mem_fun( *this, &PrintableWindow::on_printOperation_status_changed), operation));
  }
}

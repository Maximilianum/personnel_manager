/*
 * printable_window.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _PRINTABLE_WINDOW_HH_
#define _PRINTABLE_WINDOW_HH_

#include <gdkmm/window.h>
#include <gtkmm/box.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/uimanager.h>
#include "print_operation_document.hh"

class PrintableWindow: public Gtk::Window {
public:
  PrintableWindow(DrawingDocument* dd);
  virtual ~PrintableWindow();
protected:
  void build_menu(void);
  void onMenuPrint(void);
  void onMenuClose(void);
  void print_or_preview(Gtk::PrintOperationAction print_action);
  void on_printOperation_status_changed(const Glib::RefPtr<PrintOperationDocument>& operation);
  void on_printOperation_done(Gtk::PrintOperationResult result, const Glib::RefPtr<PrintOperationDocument>& operation);
  Glib::RefPtr<Gtk::PageSetup> m_ref_pagesetup;
  Glib::RefPtr<Gtk::PrintSettings> m_ref_settings;
  Gtk::Box m_box;
  Gtk::ScrolledWindow scrolledwin;
  DrawingDocument* drawing_area;
  Glib::RefPtr<Gtk::UIManager> m_ref_uimanager;
  Glib::RefPtr<Gtk::ActionGroup> m_ref_actiongroup;
};

#endif // _PRINTABLE_WINDOW_HH_


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * rank.cc
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rank.hh"

std::ostream& operator<<(std::ostream& os, const Rank& rnk) {
	return os << "\'" << rnk.get_full_label().raw() << "\', \'" << rnk.get_short_label().raw() << "\', " << rnk.get_index();
}

Glib::ustring Rank::sqlite_table { Glib::ustring("TABLE RANK (" \
                                                 "FLABEL TEXT PRIMARY KEY NOT NULL, " \
                                                 "SLABEL TEXT NOT NULL, " \
												 "RANK INTEGER NOT NULL, " \
												 "UNIQUE(FLABEL), " \
												 "UNIQUE(SLABEL), " \
												 "UNIQUE(RANK));") };

int Rank::retrieve_rank(void *data, int argc, char **argv, char **azColName) {
	Rank rnk;
	std::vector<Rank>* ranks { static_cast<std::vector<Rank>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("RANK")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("IDX")) == 0) && argv[i]) {
			rnk.set_index(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FLABEL") && argv[i]) {
			rnk.set_full_label(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("SLABEL") && argv[i]) {
			rnk.set_short_label(argv[i]);
		}
	}
	ranks->push_back(rnk);
	return 0;
}

int Rank::retrieve_rank_label(void *data, int argc, char **argv, char **azColName) {
	std::vector<Glib::ustring>* ranks { static_cast<std::vector<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("FLABEL") && argv[i]) {
			ranks->push_back(argv[i]);
		}
	}
	return 0;
}

Rank& Rank::operator=(const Rank& rnk){
	if (this != &rnk) {
		index = rnk.index;
		full_label = rnk.full_label;
		short_label = rnk.short_label;
	}
	return *this;
}


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * rank.hh
 * Copyright (C) 2019 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RANK_HH_
#define _RANK_HH_

#include <vector>
#include <glibmm/ustring.h>

class Rank {
	friend std::ostream& operator<<(std::ostream& os, const Rank& rnk);
 public:
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_rank(void *data, int argc, char **argv, char **azColName);
	static int retrieve_rank_label(void *data, int argc, char **argv, char **azColName);
	inline Rank() {}
	inline Rank(unsigned int val, const Glib::ustring& fl, const Glib::ustring& sl) : index (val), full_label (fl), short_label (sl) {}
	Rank& operator=(const Rank& rnk);
	inline bool operator<(const Rank& rnk) const { return index < rnk.index; }
	inline bool operator==(const Rank& rnk) const { return index == rnk.index && full_label.compare(rnk.full_label) == 0 && short_label.compare(rnk.short_label) == 0; }
	inline void set_index(unsigned int val) { index = val; }
	inline unsigned int get_index(void) const { return index; }
	inline void set_full_label(const Glib::ustring& str) { full_label.assign(str); }
	inline Glib::ustring get_full_label(void) const { return full_label; }
	inline void set_short_label(const Glib::ustring& str) { short_label.assign(str); }
	inline Glib::ustring get_short_label(void) const { return short_label; }
 private:
	static Glib::ustring sqlite_table;
	unsigned int index;
	Glib::ustring full_label;
	Glib::ustring short_label;
};

#endif // _RANK_HH_


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * rank_tree_model_column_record.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RANK_TREE_MODEL_COLUMN_RECORD_H_
#define _RANK_TREE_MODEL_COLUMN_RECORD_H_

class RankTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
	 public:
		RankTreeModelColumnRecord() { add(short_str); add(full_str); }
		Gtk::TreeModelColumn<Glib::ustring> short_str;
		Gtk::TreeModelColumn<Glib::ustring> full_str;
};

#endif // _RANK_TREE_MODEL_COLUMN_RECORD_H_


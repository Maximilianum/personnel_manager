/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ranks_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ranks_dialog.hh"
#include <gtkmm/messagedialog.h>

void RanksDialog::build_gui(RanksDialog* rd) {
	Gtk::Box* box = rd->get_content_area();
	rd->bldr->get_widget("ranks_box", rd->ranks_box);
	rd->ranks_liststore = Gtk::ListStore::create(rd->rank_tmcr);
	rd->bldr->get_widget("ranks_treeview", rd->ranks_treeview);
	rd->ranks_treeview->set_model(rd->ranks_liststore);
	rd->ranks_treeselection = rd->ranks_treeview->get_selection();
	rd->ranks_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	rd->rank_short_cellrenderertext = (Gtk::CellRendererText*)rd->ranks_treeview->get_column(0)->get_first_cell();
	rd->rank_full_cellrenderertext = (Gtk::CellRendererText*)rd->ranks_treeview->get_column(1)->get_first_cell();
	rd->bldr->get_widget("add_rank_button", rd->add_rank_button);
	rd->bldr->get_widget("remove_rank_button", rd->remove_rank_button);
	rd->bldr->get_widget("move_up_rank_button", rd->move_up_rank_button);
	rd->bldr->get_widget("move_down_rank_button", rd->move_down_rank_button);
	box->pack_start(*(rd->ranks_box));
	/*
	 * load values
	 */
	rd->load_ranks_in_treeview();
	/*
	 * signals connections
	 */
	rd->ranks_treeselection->signal_changed().connect(sigc::mem_fun(rd, &RanksDialog::on_ranks_treeselection_changed));
	rd->add_rank_button->signal_clicked().connect(sigc::mem_fun(rd, &RanksDialog::on_add_rank_button_clicked));
	rd->move_up_rank_button->signal_clicked().connect(sigc::mem_fun(rd, &RanksDialog::on_move_up_rank_button_clicked));
	rd->move_down_rank_button->signal_clicked().connect(sigc::mem_fun(rd, &RanksDialog::on_move_down_rank_button_clicked));
	rd->remove_rank_button->signal_clicked().connect(sigc::mem_fun(rd, &RanksDialog::on_remove_rank_button_clicked));
	rd->rank_short_cellrenderertext->signal_edited().connect(sigc::mem_fun(rd, &RanksDialog::on_rank_short_edited));
	rd->rank_full_cellrenderertext->signal_edited().connect(sigc::mem_fun(rd, &RanksDialog::on_rank_full_edited));
}

void RanksDialog::load_ranks_in_treeview(void) {
	ranks_liststore->clear();
	try {
		std::vector<Rank> ranks { database->get_ranks() };
		for (auto iter = ranks.cbegin(); iter != ranks.cend(); iter++) {
			Gtk::TreeModel::iterator iter_list = ranks_liststore->append();
			Gtk::TreeModel::Row row = *iter_list;
			row[rank_tmcr.short_str] = iter->get_short_label();
			row[rank_tmcr.full_str] = iter->get_full_label();
		}
	} catch (Database::DBError& dbe) {
		std::cerr << "RanksDialog::load_ranks_in_treeview() " << dbe.get_message() << std::endl;
	}
}

void RanksDialog::on_ranks_treeselection_changed(void) {
	bool flag = ranks_treeselection->count_selected_rows() > 0;
	remove_rank_button->set_sensitive(flag);
	if (flag) {
		std::vector<Gtk::TreePath> selected = ranks_treeselection->get_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> ranks_treemodel { ranks_treeview->get_model() };
		std::vector<Gtk::TreePath>::const_iterator iter = selected.begin();
		Gtk::TreeModel::iterator iter_tm = ranks_treemodel->get_iter(*iter);
		bool flag2 { ranks_liststore->children().begin() == iter_tm };
		move_up_rank_button->set_sensitive(!flag2);
		Gtk::TreeModel::iterator it_last { ranks_liststore->children().end() };
		it_last--;
		bool flag3 { it_last == iter_tm };
		move_down_rank_button->set_sensitive(!flag3);
	} else {
		move_up_rank_button->set_sensitive(false);
		move_down_rank_button->set_sensitive(false);
	}
}

void RanksDialog::get_unique_rank(Rank& rnk) {
	Glib::ustring std_full_str { rnk.get_full_label() };
	Glib::ustring std_short_str { rnk.get_short_label() };
	unsigned int ns = 0;
	unsigned int nf = 0;
	bool duplicated;
	do {
		duplicated = false;
		for (auto iter = ranks_liststore->children().begin(); iter != ranks_liststore->children().end(); iter++) {
			Gtk::TreeModel::Row row = *iter;
			if (row[rank_tmcr.short_str] == rnk.get_short_label()) {
				if (ns == 0) {
					rnk.set_short_label(rnk.get_short_label() + Glib::ustring::format(ns));
				} else {
					rnk.set_short_label(rnk.get_short_label().replace(std_short_str.size(), rnk.get_short_label().size() - std_short_str.size(), Glib::ustring::format(ns)));
				}
				ns++;
				duplicated = true;
			}
			if (row[rank_tmcr.full_str] == rnk.get_full_label()) {
				if (nf == 0) {
					rnk.set_full_label(rnk.get_full_label().append(Glib::ustring::format(nf)));
				} else {
					rnk.set_full_label(rnk.get_full_label().replace(std_full_str.size(), rnk.get_full_label().size() - std_full_str.size(), Glib::ustring::format(nf)));
				}
				nf++;
				duplicated = true;
				break;
			}
		}
	} while (duplicated);
}

void RanksDialog::on_add_rank_button_clicked(void) {
	Glib::ustring std_full_str { "nuovo grado" };
	Glib::ustring std_short_str { "n.g." };
	unsigned int index { ranks_liststore->children().size() + 1 };
	Rank rnk { index, std_full_str, std_short_str };
	get_unique_rank(rnk);
	try {
		database->insert(rnk);
	} catch (const Database::DBError& dbe) {
		std::cerr << "RanksDialog::on_add_rank_button_clicked() " << dbe.get_message() << std::endl;
	}
	load_ranks_in_treeview();
}

void RanksDialog::on_move_up_rank_button_clicked(void) {
	std::vector<Gtk::TreePath> selected { ranks_treeselection->get_selected_rows() };
	Glib::RefPtr<Gtk::TreeModel> ranks_treemodel { ranks_treeview->get_model() };
	std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
	Gtk::TreeModel::iterator iter_tm { ranks_treemodel->get_iter(*iter) };
	Gtk::TreeModel::Row row { *iter_tm };
	try {
		Rank rnk_down { database->get_rank_with_full(row[rank_tmcr.full_str]) };
		if (ranks_liststore->children().begin() != iter_tm) {
			iter_tm--;
			Gtk::TreeModel::Row row_up { *iter_tm };
			Rank rnk_up { database->get_rank_with_full(row_up[rank_tmcr.full_str]) };
			database->swap_rank(rnk_down, rnk_up);
			load_ranks_in_treeview();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "RanksDialog::on_move_up_rank_button_clicked() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "RanksDialog::on_move_up_rank_button_clicked() " << dbe.get_message() << std::endl;
	}
}

void RanksDialog::on_move_down_rank_button_clicked(void) {
	std::vector<Gtk::TreePath> selected { ranks_treeselection->get_selected_rows() };
	Glib::RefPtr<Gtk::TreeModel> ranks_treemodel { ranks_treeview->get_model() };
	std::vector<Gtk::TreePath>::const_iterator iter { selected.begin() };
	Gtk::TreeModel::iterator iter_tm { ranks_treemodel->get_iter(*iter) };
	Gtk::TreeModel::Row row { *iter_tm };
	try {
		Rank rnk_up { database->get_rank_with_full(row[rank_tmcr.full_str]) };
		iter_tm++;
		if (iter_tm != ranks_liststore->children().end()) {
			Gtk::TreeModel::Row row_down { *iter_tm };
			Rank rnk_down { database->get_rank_with_full(row_down[rank_tmcr.full_str]) };
			database->swap_rank(rnk_up, rnk_down);
			load_ranks_in_treeview();
		}
	} catch (Database::DBError& dbe) {
		std::cerr << "RanksDialog::on_move_down_rank_button_clicked() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "RanksDialog::on_move_down_rank_button_clicked() " << dbe.get_message() << std::endl;
	}
}

void RanksDialog::on_remove_rank_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> ranks_treemodel { ranks_treeview->get_model() };
		std::vector<Gtk::TreeModel::Path> selected { ranks_treeselection->get_selected_rows() };
		for (auto iter = selected.begin(); iter != selected.end(); iter++) {
			Gtk::TreeModel::iterator iter_tm { ranks_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			try {
				Rank rank { database->get_rank_with_full(row[rank_tmcr.full_str]) };
				database->delete_rank(rank.get_index());
				load_ranks_in_treeview();
			} catch (Database::DBError& dbe) {
				std::cerr << "RanksDialog::on_remove_rank_button_clicked() " << dbe.get_message() << std::endl;
			} catch (const Database::DBEmptyResultException& dbe) {
				std::cerr << "RanksDialog::on_remove_rank_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void RanksDialog::on_rank_short_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> ranks_model { ranks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { ranks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		Rank rnk { database->get_rank_with_full(row[rank_tmcr.full_str]) };
		database->update_rank_short(rnk, new_text);
		row[rank_tmcr.short_str] = new_text;
	} catch (Database::DBError& dbe) {
		std::cerr << "RanksDialog::on_rank_short_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "RanksDialog::on_rank_short_edited() " << dbe.get_message() << std::endl;
	}
}

void RanksDialog::on_rank_full_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> ranks_model { ranks_treeview->get_model() };
	Gtk::TreeModel::iterator iter { ranks_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		Rank rnk { database->get_rank_with_full(row[rank_tmcr.full_str]) };
		database->update_rank_full(rnk, new_text);
		row[rank_tmcr.full_str] = new_text;
	} catch (Database::DBError& dbe) {
		std::cerr << "RanksDialog::on_rank_full_edited() " << dbe.get_message() << std::endl;
	} catch (const Database::DBEmptyResultException& dbe) {
		std::cerr << "RanksDialog::on_rank_full_edited() " << dbe.get_message() << std::endl;
	}
}

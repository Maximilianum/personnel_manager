/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * ranks_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _RANKS_DIALOG_HH_
#define _RANKS_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "rank_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class RanksDialog : public ControlDialog {
 public:
	inline RanksDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline RanksDialog(const RanksDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(RanksDialog* sd);
	void load_ranks_in_treeview(void);
	void on_ranks_treeselection_changed(void);
	void get_unique_rank(Rank& rnk);
	void on_add_rank_button_clicked(void);
	void on_move_up_rank_button_clicked(void);
	void on_move_down_rank_button_clicked(void);
	void on_remove_rank_button_clicked(void);
	void on_rank_short_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_rank_full_edited(const Glib::ustring& path, const Glib::ustring& new_text);

	Gtk::Box* ranks_box;
	Glib::RefPtr<Gtk::ListStore> ranks_liststore;
	Gtk::TreeView* ranks_treeview;
	Glib::RefPtr<Gtk::TreeSelection> ranks_treeselection;
	Gtk::CellRendererText* rank_short_cellrenderertext;
	Gtk::CellRendererText* rank_full_cellrenderertext;
	Gtk::Button* add_rank_button;
	Gtk::Button* remove_rank_button;
	Gtk::Button* move_up_rank_button;
	Gtk::Button* move_down_rank_button;
	RankTreeModelColumnRecord rank_tmcr;
	Database* database;
};

#endif // _RANKS_DIALOG_HH_

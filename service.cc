/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * service.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "service.hh"
#include <vector>

Glib::ustring Service::sqlite_table { Glib::ustring("TABLE SERVICE (" \
                                                 "MEMBER TEXT NOT NULL, " \
                                                 "START INTEGER NOT NULL, " \
												 "END INTEGER, " \
												 "PRIMARY KEY(MEMBER, START), " \
												 "FOREIGN KEY(MEMBER) REFERENCES MEMBER(ID));") };

int Service::retrieve_service(void *data, int argc, char **argv, char **azColName) {
	Service srv;
	std::vector<Service>* services { static_cast<std::vector<Service>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]).compare(Glib::ustring("MEMBER")) == 0 && argv[i]) {
			srv.set_member(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("START") && argv[i]) {
			srv.set_start(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("END") && argv[i]) {
			srv.set_end(argv[i]);
		}
	}
	services->push_back(srv);
	return 0;
}

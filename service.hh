/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * service.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SERVICE_HH_
#define _SERVICE_HH_

#include <glibmm/ustring.h>
#include "custom_date.hh"

class Service {
 public:
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_service(void *data, int argc, char **argv, char **azColName);
	inline Service() : start (CustomDate::start_of_day(CustomDate::now())), end_is_null(true) {}
	inline Service(const Glib::ustring& id, const CustomDate& st, const CustomDate& en) : member (id), start (st), end (en), end_is_null(false) {}
	inline Service(const Glib::ustring& id, const CustomDate& st) : member (id), start (st), end_is_null(true) {}
	inline bool operator<(const Service& srv) const { return start < srv.start; }
	inline bool operator==(const Service& srv) const { return member.compare(srv.member) == 0 && start == srv.start && end == srv.end; }
	inline void set_member(const Glib::ustring& str) { member.assign(str); }
	inline Glib::ustring get_member(void) const { return member; }
	inline void set_start(const CustomDate& cd) { start = cd; }
	inline void set_start(const char* cstr) { start = CustomDate { cstr }; }
	inline CustomDate get_start() const { return start; }
	inline void set_end(const CustomDate& cd) { end = cd; end_is_null = false; }
	inline void set_end(const char* cstr) { set_end(CustomDate { cstr }); }
	inline void set_end_null() { end = CustomDate(); end_is_null = true; }
	inline CustomDate get_end() const { return end; }
	inline bool is_end_null() const { return end_is_null; }
	inline bool is_active(const CustomDate& ref) const { return end_is_null || (!end_is_null && ref <= CustomDate::end_of_day(end)); }
 private:
	static Glib::ustring sqlite_table;
	Glib::ustring member;
	CustomDate start;
	CustomDate end;
	bool end_is_null;
};

#endif // _SERVICE_HH_

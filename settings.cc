/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * settings.cc
 * Copyright (C) 2017 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.hh"
#include "database.hh"

Glib::ustring Settings::sqlite_member_settings_table { Glib::ustring("TABLE MEMBER_SETTINGS (" \
																	 "ENTRY TEXT NOT NULL, " \
																	 "MEMBER TEXT NOT NULL, " \
																	 "VALUE TEXT, " \
																	 "PRIMARY KEY (ENTRY,MEMBER), " \
																	 "FOREIGN KEY(MEMBER) REFERENCES MEMBER(ID)); ") };

Glib::ustring Settings::sqlite_settings_table { Glib::ustring("TABLE SETTINGS (" \
															  "ENTRY TEXT PRIMARY KEY NOT NULL, " \
															  "VALUE INTEGER NOT NULL); ") };

Glib::ustring Settings::sqlite_str_settings_table { Glib::ustring("TABLE STR_SETTINGS (" \
																  "ENTRY TEXT PRIMARY KEY NOT NULL, " \
																  "VALUE TEXT NOT NULL); ") };

int Settings::retrieve_member_settings_complete(void *data, int argc, char **argv, char **azColName) {
	mbr_settings mbs;
	std::vector<mbr_settings>* vec { static_cast<std::vector<mbr_settings>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("ENTRY") && argv[i]) {
			mbs.entry.assign(argv[i]);
		}
		if (Glib::ustring(azColName[i]) == Glib::ustring("MEMBER") && argv[i]) {
			mbs.mbr_id.assign(argv[i]);
		}
		if (Glib::ustring(azColName[i]) == Glib::ustring("VALUE") && argv[i]) {
			mbs.value.assign(argv[i]);
		}
	}
	vec->push_back(mbs);
	return 0;
}

int Settings::retrieve_setting(void *data, int argc, char **argv, char **azColName) {
	int val;
	std::vector<int>* integers { static_cast<std::vector<int>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("VALUE") && argv[i]) {
			val = atoi(argv[i]);
		}
	}
	integers->push_back(val);
	return 0;
}

int Settings::retrieve_str_setting(void *data, int argc, char **argv, char **azColName) {
	Glib::ustring val;
	std::vector<Glib::ustring>* strings { static_cast<std::vector<Glib::ustring>*>(data) };
	for(int i=0; i<argc; i++) {
		if (Glib::ustring(azColName[i]) == Glib::ustring("VALUE") && argv[i]) {
			val = argv[i];
		}
	}
	strings->push_back(val);
	return 0;
}

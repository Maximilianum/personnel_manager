/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * settings.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SETTINGS_HH_
#define _SETTINGS_HH_

#include <glibmm/ustring.h>
#include <gdkmm/rgba.h>
#include "member.hh"
#include "time_preset.hh"

class Database;

class Settings {
 public:
	static inline Glib::ustring get_sqlite_member_settings_table(void) { return sqlite_member_settings_table; }
	static inline Glib::ustring get_sqlite_settings_table(void) { return sqlite_settings_table; }
	static inline Glib::ustring get_sqlite_str_settings_table(void) { return sqlite_str_settings_table; }
	static int retrieve_member_settings_complete(void *data, int argc, char **argv, char **azColName);
	static int retrieve_time_preset(void *data, int argc, char **argv, char **azColName);
	static int retrieve_setting(void *data, int argc, char **argv, char **azColName);
	static int retrieve_str_setting(void *data, int argc, char **argv, char **azColName);
	static int retrieve_label(void *data, int argc, char **argv, char **azColName);
	// constructors
	inline Settings() {}
	// operators
	inline Settings& operator=(const Settings &stgs);
 private:
	static Glib::ustring sqlite_member_settings_table;
	static Glib::ustring sqlite_settings_table;
	static Glib::ustring sqlite_line_text_table;
	static Glib::ustring sqlite_str_settings_table;
};

#endif // _SETTINGS_HH_


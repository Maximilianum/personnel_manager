/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * shift.cc
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "shift.hh"
#include "database.hh"

/**
 * 
 * static class functions
 * 
 */

std::ostream& operator<<(std::ostream& os, const Shift& sft) {
	return os << sft.get_member().raw() << "  \'" << sft.get_label().raw() << "\' " << sft.get_license_year() << " " << sft.get_start() << " " << sft.get_end() << " note: " << sft.get_note().raw() << " flags = " << int(sft.flags) << " other break = " << sft.get_other_break_min();
}

int Shift::get_worked_minutes(const int st, const int en, const std::uint8_t flgs, const int brk) {
	int val = en - st;
	// check if lunch break is included
	if (flgs & mask0 && st < lunch_start && en > lunch_end) {
		val -= meal_break;
	}
	// check if dinner break is included
	if (flgs & mask1 && st < dinner_start && en > dinner_end) {
		val -= meal_break;
	}
	// check other break
	if (flgs & mask2) {
		val -= brk;
	}
	return val;
}

unsigned short Shift::get_number_of_meals(int st, int en, const std::uint8_t flgs) {
	unsigned short meals = 0;
	// check if lunch break is included
	if (flgs & mask0 && st < lunch_start && en > lunch_end) {
		meals++;
	}
	// check if dinner break is included
	if (flgs & mask1 && st < dinner_start && en > dinner_end) {
		meals++;
	}
	return meals;
}

/**
 * 
 * operators
 * 
 */

Shift& Shift::operator=(const Shift &sft) {
	if (this != &sft) {
		member = sft.member;
		label = sft.label;
		lic_year = sft.lic_year;
		start = sft.start;
		end = sft.end;
		note = sft.note;
		flags = sft.flags;
		obreak = sft.obreak;
	}
	return *this;
}

/**
 * 
 * class member functions
 *
 */

	int Shift::get_finish_in_minute() const {
		int h = end.get_hour();
		if (h == 0) {
			h = 24;
		}
		return (h * 60) + end.get_minute();
	}

bool Shift::check_consistency(Database* database) const {
	int begin = get_start_in_minute();
	int end = get_finish_in_minute();
	if (label.compare(Glib::ustring()) == 0) {
		if (end - begin >= 60) {
			if (get_max_work_length() > 0) {
				if (get_worked_minutes() <= get_max_work_length()) {
					return true;
				} else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	} else {
		bool success = true;
		bool sunday = false;
		bool saturday = false;
		bool weekday = false;
		bool holiday = false;
		try {
			OffWork ow = database->get_offwork(label);
			sunday = ow.get_sunday();
			saturday = ow.get_saturday();
			weekday = ow.get_weekday();
			holiday = ow.get_holiday();
		} catch (Database::DBError& dbe) {
			std::cerr << "Shift::check_consistency() " << dbe.get_message() << std::endl;
		throw dbe;
		} catch (Database::DBEmptyResultException& dbe) {
			success = false;
		}
		if (!success) {
			try {
				LicenseType lt = database->get_license_type(label);
				sunday = lt.get_sunday();
				saturday = lt.get_saturday();
				weekday = lt.get_weekday();
				holiday = lt.get_holiday();
			} catch (Database::DBError& dbe) {
				std::cerr << "Shift::check_consistency() " << dbe.get_message() << std::endl;
				throw dbe;
			} catch (Database::DBEmptyResultException& dbe) {
				std::cerr << "Shift::check_consistency() " << dbe.get_message() << std::endl;
				throw dbe;
			}
		}
		short int wk = start.get_weekday();
		if (wk == 0 && sunday) {
			return true;
		} else if (wk == 6 && saturday) {
			return true;
		} else if (wk > 0 && (start.is_holiday() && holiday)) {
			return true;
		} else if (wk > 0 && wk < 6 && weekday && !start.is_holiday()) {
			return true;
		}
	}
	return false;
}

bool Shift::check_intersection(const Shift* shift) const {
	if (start >= shift->start && start < shift->end) {
		return true;
	} else if (end > shift->start && end < shift->end) {
		return true;
	}
	return false;
}

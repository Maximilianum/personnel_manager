/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * shift.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SHIFT_HH_
#define _SHIFT_HH_

#include <iostream>
#include <iomanip>
#include "duty.hh"
#include "member.hh"
#include "label.hh"

class CalendarData;
class Database;
class Day;
class Settings;

class Shift : public Duty {
	friend std::ostream& operator<<(std::ostream& os, const Shift& sft);
	friend class CalendarData;
	friend class Settings;
 public:
	// static class functions
	static int get_worked_minutes(const int st, const int en, const std::uint8_t flgs = 0b00000011, const int brk = 0);
	static unsigned short get_number_of_meals(int st, int en, const std::uint8_t flgs = 0b00000011);
	// constructors
	inline Shift() : Duty() {}
	inline Shift(const Duty& dt) : Duty(dt) {}
	inline Shift(const CustomDate& cds, const CustomDate& cde, const Glib::ustring &id) : Duty(cds, cde, id) {}
	inline Shift(const Glib::ustring &lbl, const Glib::ustring &id, const CustomDate& cd) : Duty(lbl, id, cd) {}
	// operators
	Shift& operator=(const Shift &sft);
	inline bool operator<(const Shift &sft) const { return start < sft.start || (start == sft.start && end < sft.end); }
	inline bool operator==(const Shift &sft) const { return start == sft.start && end == sft.end && member == sft.member && label == sft.label && lic_year == sft.lic_year && note == sft.note && flags == sft.flags && obreak == sft.obreak; }
	inline bool operator!=(const Shift &sft) const { return start != sft.start || end != sft.end || member != sft.member || label != sft.label || lic_year != sft.lic_year && note == sft.note && flags == sft.flags && obreak == sft.obreak; }
	// class member functions
	inline int get_start_in_minute() const { return (start.get_hour() * 60) + start.get_minute(); }
	int get_finish_in_minute() const;
	inline int get_worked_minutes() const { return Shift::get_worked_minutes(get_start_in_minute(), get_finish_in_minute(), flags, obreak); }
	inline unsigned short get_number_of_meals() const { return Shift::get_number_of_meals(get_start_in_minute(), get_finish_in_minute(), flags); }
	bool check_consistency(Database* database) const;
	bool check_intersection(const Shift* shift) const;
};

#endif // _SHIFT_HH_


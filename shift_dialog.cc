/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * shift_dialog.cc
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "shift_dialog.hh"
#include <gtkmm/messagedialog.h>

void ShiftDialog::build_gui(ShiftDialog* sd) {
	Gtk::Box* box = sd->get_content_area();
	sd->bldr->get_widget("shift_box", sd->shift_box);
	sd->bldr->get_widget("shift_note_entry", sd->shift_note_entry);
	sd->bldr->get_widget("shift_notebook", sd->shift_notebook);
	sd->shift_start_hour1_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_start_hour1_combobox", sd->shift_start_hour1_combobox);
	sd->shift_start_hour1_combobox->set_model(sd->shift_start_hour1_liststore);
	sd->shift_start_hour0_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_start_hour0_combobox", sd->shift_start_hour0_combobox);
	sd->shift_start_hour0_combobox->set_model(sd->shift_start_hour0_liststore);
	sd->shift_start_minute1_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_start_minute1_combobox", sd->shift_start_minute1_combobox);
	sd->shift_start_minute1_combobox->set_model(sd->shift_start_minute1_liststore);
	sd->shift_start_minute0_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_start_minute0_combobox", sd->shift_start_minute0_combobox);
	sd->shift_start_minute0_combobox->set_model(sd->shift_start_minute0_liststore);
	sd->shift_finish_hour1_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_finish_hour1_combobox", sd->shift_finish_hour1_combobox);
	sd->shift_finish_hour1_combobox->set_model(sd->shift_finish_hour1_liststore);
	sd->shift_finish_hour0_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_finish_hour0_combobox", sd->shift_finish_hour0_combobox);
	sd->shift_finish_hour0_combobox->set_model(sd->shift_finish_hour0_liststore);
	sd->shift_finish_minute1_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_finish_minute1_combobox", sd->shift_finish_minute1_combobox);
	sd->shift_finish_minute1_combobox->set_model(sd->shift_finish_minute1_liststore);
	sd->shift_finish_minute0_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_finish_minute0_combobox", sd->shift_finish_minute0_combobox);
	sd->shift_finish_minute0_combobox->set_model(sd->shift_finish_minute0_liststore);
	sd->bldr->get_widget("shift_time_treeview", sd->shift_time_treeview);
	sd->shift_time_liststore = Gtk::ListStore::create(sd->time_preset_tree_mod_col_record);
	sd->shift_time_treeview->set_model(sd->shift_time_liststore);
	sd->shift_time_treeselection = sd->shift_time_treeview->get_selection();
	sd->shift_time_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	sd->bldr->get_widget("lunch_break_checkbutton", sd->lunch_break_checkbutton);
	sd->bldr->get_widget("dinner_break_checkbutton", sd->dinner_break_checkbutton);
	sd->bldr->get_widget("other_break_checkbutton", sd->other_break_checkbutton);
	sd->bldr->get_widget("other_break_entry", sd->other_break_entry);
	sd->bldr->get_widget("shift_label_radiobutton", sd->shift_label_radiobutton);
	sd->shift_label_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("shift_label_combobox", sd->shift_label_combobox);
	sd->shift_label_combobox->set_model(sd->shift_label_liststore);
	sd->bldr->get_widget("shift_license_radiobutton", sd->shift_license_radiobutton);
	sd->shift_license_liststore = Gtk::ListStore::create(sd->license_combobox_tmcr);
	sd->bldr->get_widget("shift_license_combobox", sd->shift_license_combobox);
	sd->shift_license_combobox->set_model(sd->shift_license_liststore);
	sd->bldr->get_widget("confirm_shift_button", sd->confirm_shift_button);
	sd->bldr->get_widget("cancel_shift_button", sd->cancel_shift_button);
	box->pack_start(*(sd->shift_box));
	/*
	 * load values
	 */
	std::vector<Glib::ustring> digit {};
	for (short i = 0; i < 10; i++) {
		digit.push_back(Glib::ustring::format(i));
	}
	sd->load_data_combobox(digit.cbegin(), digit.cend() - 7, sd->shift_start_hour1_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend(), sd->shift_start_hour0_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend() - 4, sd->shift_start_minute1_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend(), sd->shift_start_minute0_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend() - 7, sd->shift_finish_hour1_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend(), sd->shift_finish_hour0_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend() - 4, sd->shift_finish_minute1_liststore);
	sd->load_data_combobox(digit.cbegin(), digit.cend(), sd->shift_finish_minute0_liststore);
	try {
		std::vector<Glib::ustring> labels { sd->database->get_offworks_label() };
		sd->load_data_combobox(labels.cbegin(), labels.cend(), sd->shift_label_liststore);
		std::vector<TimePreset> tps { sd->database->get_time_presets() };
		sd->load_time_presets_in_treeview(tps);
		
	} catch (const Database::DBError& dbe) {
		std::cerr << "ShiftDialog::build_gui() " << dbe.get_message() << std::endl;
	}
	/*
	 * signals connections
	 */
	sd->shift_time_treeselection->signal_changed().connect(sigc::mem_fun(sd, &ShiftDialog::on_shift_time_treeselection_changed));
	sd->shift_time_treeview->signal_button_press_event().connect_notify(sigc::mem_fun(sd, &ShiftDialog::on_shift_time_treeview_clicked));
	sd->shift_label_radiobutton->signal_toggled().connect(sigc::mem_fun(sd, &ShiftDialog::on_shift_label_changed));
	sd->confirm_shift_button->signal_clicked().connect(sigc::mem_fun(sd, &ShiftDialog::on_confirm_shift_button_clicked));
	sd->cancel_shift_button->signal_clicked().connect(sigc::mem_fun(sd, &ShiftDialog::on_cancel_shift_button_clicked));
}

void ShiftDialog::set_shift(const Shift& shift) {
	member_id = shift.get_member();
	load_licenses();
	if (shift.is_working()) {
		shift_notebook->set_current_page(0);
		set_shift_time_comboboxes(TimePreset(shift.get_start(),shift.get_end()));
		lunch_break_checkbutton->set_active(shift.is_lunch_break_enabled());
		dinner_break_checkbutton->set_active(shift.is_dinner_break_enabled());
		other_break_checkbutton->set_active(shift.is_other_break_enabled());
		int val = shift.get_other_break_min();
		if (val > 0) {
			other_break_entry->set_text(Glib::ustring::format(val));
		}
	} else {
		shift_notebook->set_current_page(1);
		if (shift.get_license_year() > 0) {
			shift_license_radiobutton->set_active(true);
			Gtk::TreeModel::iterator iter { shift_license_liststore->children().begin() };
			Gtk::TreeModel::iterator end { shift_license_liststore->children().end() };
			while (iter != end) {
				Gtk::TreeModel::Row row = *iter;
				if (row[license_combobox_tmcr.label] == shift.get_label() && row[license_combobox_tmcr.year] == shift.get_license_year()) {
					shift_license_combobox->set_active(iter);
					break;
				}
				iter++;
			}
		} else {
			shift_label_radiobutton->set_active(true);
			Gtk::TreeModel::iterator iter { shift_label_liststore->children().begin() };
			Gtk::TreeModel::iterator end { shift_label_liststore->children().end() };
			while (iter != end) {
				Gtk::TreeModel::Row row = *iter;
				if (shift.get_label().compare(row[combobox_tmcr.column]) == 0) {
					shift_label_combobox->set_active(iter);
					break;
				}
				iter++;
			}
		}
	}
	shift_note_entry->set_text(shift.get_note());
}

Shift ShiftDialog::get_shift() const {
	Shift shift;
	Selection sel { calendata->get_selection() };
	Gtk::TreeModel::iterator it;
	Gtk::TreeModel::Row row;
	CustomDate date { &sel.date };
	CustomDate start = CustomDate::start_of_day(date);
	CustomDate finish { start };
	switch (shift_notebook->get_current_page()) {
	case 0:
		{	
			// get start time
			struct tm* std_date = start.get_struct_time();
			int h1 = shift_start_hour1_combobox->get_active_row_number();
			int h0 = shift_start_hour0_combobox->get_active_row_number();
			int m1 = shift_start_minute1_combobox->get_active_row_number();
			int m0 = shift_start_minute0_combobox->get_active_row_number();
			start.set_date_time(std_date->tm_year, std_date->tm_mon, std_date->tm_mday, (h1 * 10) + h0, (m1 * 10) + m0, 0);
			// get finish time
			std_date = finish.get_struct_time();
			h1 = shift_finish_hour1_combobox->get_active_row_number();
			h0 = shift_finish_hour0_combobox->get_active_row_number();
			m1 = shift_finish_minute1_combobox->get_active_row_number();
			m0 = shift_finish_minute0_combobox->get_active_row_number();
			finish.set_date_time(std_date->tm_year, std_date->tm_mon, std_date->tm_mday, (h1 * 10) + h0, (m1 * 10) + m0, 0);
			shift = Shift(start, finish, sel.note);
			shift.set_lunch_break(lunch_break_checkbutton->get_active());
			shift.set_dinner_break(dinner_break_checkbutton->get_active());
			shift.set_other_break(other_break_checkbutton->get_active());
			shift.set_other_break_min(other_break_entry->get_text().c_str());
			break;
		}
	case 1:
		if (shift_label_radiobutton->get_active())	{
			it = shift_label_combobox->get_active();
			row = *it;
			Glib::ustring full_str = row[combobox_tmcr.column];
			shift = Shift(full_str, sel.note, start);
		} else if (shift_license_radiobutton->get_active())	{
			it = shift_license_combobox->get_active();
			row = *it;
			Glib::ustring full_str = row[license_combobox_tmcr.label];
			shift = Shift(full_str, sel.note, start);
			shift.set_license_year(row[license_combobox_tmcr.year]);
		}
		break;
	}
	shift.set_note(shift_note_entry->get_text());
	return shift;
}

void ShiftDialog::load_time_presets_in_treeview(const std::vector<TimePreset>& vec) {
	shift_time_liststore->clear();
	std::vector<TimePreset>::const_iterator iter = vec.cbegin();
	std::vector<TimePreset>::const_iterator end = vec.cend();
	while (iter != end)	{
		Gtk::TreeModel::iterator iter_list = shift_time_liststore->append();
		Gtk::TreeModel::Row row = *iter_list;
		std::pair<short,short> start { iter->get_start() };
		std::pair<short,short> finish { iter->get_finish() };
		row[time_preset_tree_mod_col_record.start_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), start.first);
		row[time_preset_tree_mod_col_record.start_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), start.second);
		row[time_preset_tree_mod_col_record.finish_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish.first);
		row[time_preset_tree_mod_col_record.finish_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish.second);
		row[time_preset_tree_mod_col_record.id] = iter->get_id();
		iter++;
	}
}

void ShiftDialog::load_licenses() {
	shift_license_liststore->clear();
	CustomDate first_day { CustomDate::get_first_day_month(calendata->get_reference_date()) };
	std::vector<License> licenses;
	try {
		licenses = database->get_valid_licenses_for_member(member_id, first_day);
		std::vector<License>::const_iterator iter = licenses.cbegin();
		std::vector<License>::const_iterator end = licenses.cend();
		Selection sel { calendata->get_selection() };
		CustomDate cd { &sel.date };
		CustomDate now = CustomDate::end_of_day(cd);
		Shift sft;
		if (calendata->has_shifts(database, sel)) {
			sft = calendata->get_shift(database, cd, member_id);
		}
		while (iter != end) {
			std::vector<Shift> shifts { database->get_license_shifts_for_member(member_id, iter->get_full_label(), iter->get_ref_year(), now) };
			if (iter->get_days() - shifts.size() > 0 || sft.get_license_year() == iter->get_ref_year()) {
				Gtk::TreeModel::iterator iter_list = shift_license_liststore->append();
				Gtk::TreeModel::Row row = *iter_list;
				row[license_combobox_tmcr.label] = iter->get_full_label();
				row[license_combobox_tmcr.year] = iter->get_ref_year();
			}
			iter++;
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "ShiftDialog::load_licenses() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

void ShiftDialog::set_shift_time_comboboxes(const TimePreset& tp) {
	std::pair<short,short> start { tp.get_start() };
	std::pair<short,short> finish { tp.get_finish() };
	if (finish.first == 0) {
		finish.first = 24;
	}
	shift_start_hour1_combobox->set_active(start.first / 10);
	shift_start_hour0_combobox->set_active(start.first % 10);
	shift_start_minute1_combobox->set_active(start.second / 10);
	shift_start_minute0_combobox->set_active(start.second % 10);
	shift_finish_hour1_combobox->set_active(finish.first / 10);
	shift_finish_hour0_combobox->set_active(finish.first % 10);
	shift_finish_minute1_combobox->set_active(finish.second / 10);
	shift_finish_minute0_combobox->set_active(finish.second % 10);
}

TimePreset ShiftDialog::get_time_preset_from_treeview() {
	if (shift_time_treeselection->count_selected_rows() > 0) {
		std::vector<Gtk::TreePath> selected = shift_time_treeselection->get_selected_rows();
		Glib::RefPtr<Gtk::TreeModel> shift_time_treemodel = shift_time_treeview->get_model();
		std::vector<Gtk::TreePath>::const_iterator iter = selected.begin();
		Gtk::TreeModel::iterator iter_view = shift_time_treemodel->get_iter(*iter);
		Gtk::TreeModel::Row row = *iter_view;
		Glib::ustring start_h_str = row[time_preset_tree_mod_col_record.start_h];
		Glib::ustring start_m_str = row[time_preset_tree_mod_col_record.start_m];
		Glib::ustring finish_h_str = row[time_preset_tree_mod_col_record.finish_h];
		Glib::ustring finish_m_str = row[time_preset_tree_mod_col_record.finish_m];
		return TimePreset(0, (short)atoi(start_h_str.c_str()), (short)atoi(start_m_str.c_str()), (short)atoi(finish_h_str.c_str()), (short)atoi(finish_m_str.c_str()));
	}
	return TimePreset();
}

void ShiftDialog::on_shift_time_treeselection_changed() {
	set_shift_time_comboboxes(get_time_preset_from_treeview());
}

void ShiftDialog::on_shift_time_treeview_clicked(GdkEventButton* event) {
	if (event->button == 1 && (event->type == GDK_BUTTON_PRESS || event->type == GDK_2BUTTON_PRESS)) {
		set_shift_time_comboboxes(get_time_preset_from_treeview());
	}
	if (event->button == 1 && event->type == GDK_2BUTTON_PRESS) {
		on_confirm_shift_button_clicked();
	}
}

void ShiftDialog::on_shift_label_changed() {
	shift_label_combobox->set_sensitive(shift_label_radiobutton->get_active());
	shift_license_combobox->set_sensitive(shift_license_radiobutton->get_active());
}

void ShiftDialog::on_confirm_shift_button_clicked(void) {
	Shift shift = get_shift();
	Selection sel { calendata->get_selection() };
	try {
		if (shift.check_consistency(database)) {
			if (!calendata->has_shifts(database, sel)) {
				calendata->add_shifts(database, sel, shift);
			} else {
				Shift old_shift = calendata->get_shift(database, CustomDate(&sel.date), shift.get_member());
				calendata->update_shift(database, old_shift, shift);
			}
		} else {
			Glib::ustring msg;
			if (shift.is_working()) {
				msg.assign(Glib::ustring("La durata di un turno di lavoro non può essere inferiore a 60 minuti o superiore alla durata massima."));
			} else {
				msg.assign(Glib::ustring("Il turno non è coerente con la data selezionata."));
			}
			Gtk::MessageDialog alert_dialog { *this, msg, true, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (Database::DBError& dbe) {
		std::cerr << "ShiftDialog::on_confirm_shift_button_clicked() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (Database::DBEmptyResultException& dbe) {
		std::cerr << "ShiftDialog::on_confirm_shift_button_clicked() " << dbe.get_message() << std::endl;
		throw dbe;
	}
	response(Gtk::RESPONSE_APPLY);
}

void ShiftDialog::on_cancel_shift_button_clicked(void) {
	response(Gtk::RESPONSE_CANCEL);
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * shift_dialog.hh
 * Copyright (C) 2022 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SHIFT_DIALOG_HH_
#define _SHIFT_DIALOG_HH_

#include <gtkmm/notebook.h>
#include <gtkmm/combobox.h>
#include <gtkmm/radiobutton.h>
#include <gtkmm/checkbutton.h>
#include "time_preset_tree_model_column_record.hh"
#include "license_combobox_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"
#include "calendar_data.hh"

class ShiftDialog : public ControlDialog {
 public:
	inline ShiftDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db, CalendarData* cd) : ControlDialog(path, parent), database (db), calendata (cd) { build_gui(this); }
	inline ShiftDialog(const ShiftDialog& sd) : ControlDialog(sd) { build_gui(this); }
	void set_shift(const Shift& sft);
	Shift get_shift() const;
	void load_time_presets_in_treeview(const std::vector<TimePreset>& vec);
 private:
	static void build_gui(ShiftDialog* sd);
	void load_licenses();
	void set_shift_time_comboboxes(const TimePreset& tp);
	TimePreset get_time_preset_from_treeview();
	void on_shift_time_treeselection_changed();
	void on_shift_time_treeview_clicked(GdkEventButton* event);
	void on_shift_label_changed();
	void on_confirm_shift_button_clicked(void);
	void on_cancel_shift_button_clicked(void);

	Gtk::Box* shift_box;
	Gtk::Entry* shift_note_entry;
	Gtk::Notebook* shift_notebook;
	Gtk::ComboBox* shift_start_hour1_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_start_hour1_liststore;
	Gtk::ComboBox* shift_start_hour0_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_start_hour0_liststore;
	Gtk::ComboBox* shift_start_minute1_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_start_minute1_liststore;
	Gtk::ComboBox* shift_start_minute0_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_start_minute0_liststore;
	Gtk::ComboBox* shift_finish_hour1_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_finish_hour1_liststore;
	Gtk::ComboBox* shift_finish_hour0_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_finish_hour0_liststore;
	Gtk::ComboBox* shift_finish_minute1_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_finish_minute1_liststore;
	Gtk::ComboBox* shift_finish_minute0_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_finish_minute0_liststore;
	Gtk::TreeView* shift_time_treeview;
	Glib::RefPtr<Gtk::ListStore> shift_time_liststore;
	Glib::RefPtr<Gtk::TreeSelection> shift_time_treeselection;
	Gtk::CheckButton* lunch_break_checkbutton;
	Gtk::CheckButton* dinner_break_checkbutton;
	Gtk::CheckButton* other_break_checkbutton;
	Gtk::Entry* other_break_entry;
	Gtk::RadioButton* shift_label_radiobutton;
	Gtk::ComboBox* shift_label_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_label_liststore;
	Gtk::RadioButton* shift_license_radiobutton;
	Gtk::ComboBox* shift_license_combobox;
	Glib::RefPtr<Gtk::ListStore> shift_license_liststore;
	Gtk::Button* cancel_shift_button;
	Gtk::Button* confirm_shift_button;
	TimePresetTreeModelColumnRecord time_preset_tree_mod_col_record;
	LicenseComboboxTreeModelColumnRecord license_combobox_tmcr;
	Database* database;
	CalendarData* calendata;
	Glib::ustring member_id;
};

#endif // _SHIFT_DIALOG_HH_

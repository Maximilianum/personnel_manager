/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * sign_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "sign_dialog.hh"
#include <gtkmm/messagedialog.h>

void SignDialog::build_gui(SignDialog* sd) {
	Gtk::Box* box { sd->get_content_area() };
	sd->bldr->get_widget("sign_box", sd->sign_box);
	sd->bldr->get_widget("sign_treeview", sd->sign_treeview);
	sd->sign_liststore = Gtk::ListStore::create(sd->line_text_tmcr);
	sd->sign_treeview->set_model(sd->sign_liststore);
	sd->sign_treeselection = sd->sign_treeview->get_selection();
	sd->sign_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	sd->sign_string_treeviewcolumn = sd->sign_treeview->get_column(0);
	sd->sign_string_cellrenderertext = (Gtk::CellRendererText*)sd->sign_string_treeviewcolumn->get_first_cell();
	sd->sign_size_treeviewcolumn = sd->sign_treeview->get_column(1);
	sd->sign_size_cellrenderertext = (Gtk::CellRendererText*)sd->sign_size_treeviewcolumn->get_first_cell();
	sd->load_sign_in_treeview();
	sd->bldr->get_widget("sign_add_button", sd->sign_add_button);
	sd->bldr->get_widget("sign_remove_button", sd->sign_remove_button);
	sd->sign_name_liststore = Gtk::ListStore::create(sd->combobox_tmcr);
	sd->bldr->get_widget("sign_name_combobox", sd->sign_name_combobox);
	sd->sign_name_combobox->set_model(sd->sign_name_liststore);
	sd->bldr->get_widget("sign_name_size_entry", sd->sign_name_size_entry);
	box->pack_start(*(sd->sign_box));
	/*
	 * load values
	 */
	std::vector<Member> members;
	try {
		members = sd->database->get_members();
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::build_gui() " << dbe.get_message() << std::endl;
	}
	std::vector<Glib::ustring> names;
	for (auto iter = members.begin(); iter != members.end(); iter++) {
		names.push_back(Glib::ustring::compose("%1 %2 %3",iter->get_rank().get_short_label(),iter->get_first_name(),iter->get_last_name()));
	}
	sd->load_data_combobox(names.cbegin(), names.cend(), sd->sign_name_liststore);
	TextLine sign_name { sd->get_sign_name() };
	sd->sign_name_combobox->set_active(sd->get_row_index_for_value_in_combobox(sd->sign_name_liststore, sign_name.get_text()));
	sd->sign_name_size_entry->set_text(Glib::ustring::format(std::fixed, std::setprecision(1), sign_name.get_font_size()));
	/*
	 * signals connections
	 */
	sd->sign_treeselection->signal_changed().connect(sigc::mem_fun(sd, &SignDialog::on_sign_treeselection_changed));
	sd->sign_add_button->signal_clicked().connect(sigc::mem_fun(sd, &SignDialog::on_add_sign_line_button_clicked));
	sd->sign_string_cellrenderertext->signal_edited().connect(sigc::mem_fun(sd, &SignDialog::on_sign_string_edited));
	sd->sign_size_cellrenderertext->signal_edited().connect(sigc::mem_fun(sd, &SignDialog::on_sign_size_edited));
	sd->sign_remove_button->signal_clicked().connect(sigc::mem_fun(sd, &SignDialog::on_remove_sign_line_button_clicked));
    sd->sign_name_combobox->signal_changed().connect(sigc::mem_fun(sd, &SignDialog::on_sign_name_selected));
    sd->sign_name_size_entry->signal_activate().connect(sigc::mem_fun(sd, &SignDialog::on_sign_name_size_edited));
}

void SignDialog::load_sign_in_treeview(void) {
	sign_liststore->clear();
	std::vector<TextLine> sign_lines { database->get_text_lines(SIGN_LINE) };
	for (auto iter = sign_lines.begin(); iter != sign_lines.end(); iter++) {
		Gtk::TreeModel::iterator iter_tm { sign_liststore->append() };
		Gtk::TreeModel::Row row { *iter_tm };
		row[line_text_tmcr.row] = iter->get_row();
		row[line_text_tmcr.string] = iter->get_text();
		row[line_text_tmcr.size] = Glib::ustring::format(std::fixed, std::setprecision(1), iter->get_font_size());
	}
}

void SignDialog::on_add_sign_line_button_clicked(void) {
	try {
		std::vector<TextLine> lines { database->get_text_lines(SIGN_LINE) };
		int row { lines.size() > 0 ? lines.back().get_row() + 1 : 0 };
		TextLine ntl { row, Glib::ustring(SIGN_LINE), Glib::ustring("sign line"), 12.0 };
		database->insert(ntl);
		load_sign_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::on_add_sign_line_button_clicked() " << dbe.get_message() << std::endl;
	}
}

void SignDialog::on_sign_string_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> sign_model { sign_treeview->get_model() };
	Gtk::TreeModel::iterator iter { sign_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		database->update_text_line_string(new_text, SIGN_LINE, row[line_text_tmcr.row]);
		row[line_text_tmcr.string] = new_text;
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::on_sign_string_edited() " << dbe.get_message() << std::endl;
	}
}

void SignDialog::on_sign_size_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> sign_model { sign_treeview->get_model() };
	Gtk::TreeModel::iterator iter { sign_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		database->update_text_line_fsize(atof(new_text.c_str()), SIGN_LINE, row[line_text_tmcr.row]);
		row[line_text_tmcr.size] = Glib::ustring::format(std::fixed, std::setprecision(1), atof(new_text.c_str()));
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::on_sign_size_edited() " << dbe.get_message() << std::endl;
	}
}

void SignDialog::on_remove_sign_line_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> sign_treemodel { sign_treeview->get_model() };
		std::vector<Gtk::TreeModel::Path> selected { sign_treeselection->get_selected_rows() };
		for (auto iter = selected.cbegin(); iter != selected.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { sign_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			try {
				database->delete_text_line(SIGN_LINE, row[line_text_tmcr.row]);
				sign_liststore->erase(iter_tm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "SignDialog::on_remove_sign_line_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

void SignDialog::on_sign_name_selected(void) {
	Gtk::TreeModel::iterator iter { sign_name_combobox->get_active() };
	Gtk::TreeModel::Row row { *iter };
	TextLine txtl { 0, SIGN_NAME, row[combobox_tmcr.column], atof(sign_name_size_entry->get_text().c_str()) };
	try {
		if (get_sign_name() == TextLine()) {
			database->insert(txtl);
		} else {
			database->update_text_line_string(txtl.get_text(),SIGN_NAME,0);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::on_sign_name_selected() " << dbe.get_message() << std::endl;
	}
}

void SignDialog::on_sign_name_size_edited(void) {
	double fsize { atof(sign_name_size_entry->get_text().c_str()) };
	try {
		database->update_text_line_fsize(fsize,SIGN_NAME,0);
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::on_sign_name_size_edited() " << dbe.get_message() << std::endl;
	}
}

TextLine SignDialog::get_sign_name(void) const {
	try {
		std::vector<TextLine> name { database->get_text_lines(SIGN_NAME) };
		return name.size() > 0 ? name.front() : TextLine();
	} catch (const Database::DBError& dbe) {
		std::cerr << "SignDialog::get_sign_name() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

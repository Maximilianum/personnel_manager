/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * sign_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SIGN_DIALOG_HH_
#define _SIGN_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/combobox.h>
#include <gtkmm/entry.h>
#include <gtkmm/button.h>
#include "line_text_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

#define SIGN_LINE				"SGN_LINE"
#define SIGN_NAME               "SGN_NAME"

class SignDialog : public ControlDialog {
 public:
	inline SignDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline SignDialog(const SignDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(SignDialog* sd);
	void load_sign_in_treeview(void);
	inline void on_sign_treeselection_changed(void) { sign_remove_button->set_sensitive(sign_treeselection->count_selected_rows() > 0); }
	void on_add_sign_line_button_clicked(void);
	void on_sign_string_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_sign_size_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_remove_sign_line_button_clicked(void);
	void on_sign_name_selected(void);
	void on_sign_name_size_edited(void);
	TextLine get_sign_name(void) const;

	Gtk::Box* sign_box;
	Gtk::TreeView* sign_treeview;
	Glib::RefPtr<Gtk::ListStore> sign_liststore;
	Glib::RefPtr<Gtk::TreeSelection> sign_treeselection;
	Gtk::TreeViewColumn* sign_string_treeviewcolumn;
	Gtk::CellRendererText* sign_string_cellrenderertext;
	Gtk::TreeViewColumn* sign_size_treeviewcolumn;
	Gtk::CellRendererText* sign_size_cellrenderertext;
	Gtk::Button* sign_add_button;
	Gtk::Button* sign_remove_button;
	Gtk::ComboBox* sign_name_combobox;
	Glib::RefPtr<Gtk::ListStore> sign_name_liststore;
	Gtk::CellRendererText* sign_name_cellrenderertext;
	Gtk::Entry* sign_name_size_entry;
	LineTextTreeModelColumnRecord line_text_tmcr;
	Database* database;
};

#endif // _SIGN_DIALOG_HH_

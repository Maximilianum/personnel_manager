/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * single_string_tmcr.hh
 * Copyright (C) 2015 - 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SINGLE_STRING_TREE_MODEL_COLUMN_RECORD_HH_
#define _SINGLE_STRING_TREE_MODEL_COLUMN_RECORD_HH_

#include <gtkmm/treemodelcolumn.h>

class SingleStringTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
 public:
	SingleStringTreeModelColumnRecord() { add(column); }
	Gtk::TreeModelColumn<Glib::ustring> column;
};

#endif // _SINGLE_STRING_TREE_MODEL_COLUMN_RECORD_HH_


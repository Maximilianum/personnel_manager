/*
 * siris_month.cc
 * Copyright (C) 2020 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "siris_month.hh"

SirisMonth::SirisMonth() : month (0), romc (0), romp (0), pb (0), rrs (0), rri (0) {	
	clear();
}

void SirisMonth::clear(void) {
	weeks.clear();
}

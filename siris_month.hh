/*
 * siris_month.hh
 * Copyright (C) 2020 - 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SIRIS_MONTH_HH_
#define _SIRIS_MONTH_HH_

#include <vector>
#include "siris_week.hh"

class SirisMonth {
 public:
	SirisMonth();
	inline void set_month(short int val) { month = val; }
	inline short int get_month(void) const { return month; }
	inline void set_romc(int val) { romc = val; }
	inline int get_romc(void) const { return romc; }
	inline void set_romp(int val) { romp = val; }
	inline int get_romp(void) const { return romp; }
	inline void set_pb(int val) { pb = val; }
	inline int get_pb(void) const { return pb; }
	inline void set_rrs(int val) { rrs = val; }
	inline int get_rrs(void) const { return rrs; }
	inline void set_rri(int val) { rri = val; }
	inline int get_rri(void) const { return rri; }
	inline void add_week(const SirisWeek& sw) { weeks.push_back(sw); }
	inline bool has_week(int index) const { return index < weeks.size(); }
	inline SirisWeek* get_week(int index) { return &(weeks.at(index)); }
	inline int size(void) const { return weeks.size(); } // used for debug only
	void clear(void);
 private:
	short int month;
	std::vector<SirisWeek> weeks;
	int romc;
	int romp;
	int pb;
	int rrs;
	int rri;
};

#endif // _SIRIS_MONTH_HH_


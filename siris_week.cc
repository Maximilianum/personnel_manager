/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * siris_week.cc
 * Copyright (C) 2019 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "siris_week.hh"
#include <iostream>
#include "shift.hh"

SirisWeek::SirisWeek() : month (0), romc (0), romp (0), pb (0), rrs (0), rri (0) {	
	clear();
}

void SirisWeek::set_duty(unsigned short index, int val) {
	if (index < 7) {
		duty[index] = val;
	}
}

void SirisWeek::set_start(unsigned short index, int val) {
	if (index < 7) {
		start[index] = val;
	}
}

void SirisWeek::set_end(unsigned short index, int val) {
	if (index < 7) {
		end[index] = val;
	}
}

bool SirisWeek::is_ready(void) {
	for (int i = 0; i < 7; i++)	{
		if (duty[i] < 0) {
			return false;
		}
	}
	return true;
}

int SirisWeek::get_total_duty(int w1, int w2) {
	int total = 0;
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++)	{
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		total += duty[n];
	}
	return total;
}

int SirisWeek::get_worked(unsigned short index) {
	if (index < 7) {
		if (start[index] >= 0 && end[index] >= 0) {
			return Shift::get_worked_minutes(start[index], end[index]);
		}
	}
	return 0;
}

int SirisWeek::get_programmed_work(unsigned short index) {
	if (index < 7) {
		if (p_start[index] >= 0 && p_end[index] >= 0) {
			int worked = p_end[index] - p_start[index];
			if (programmed[index].size() > 0 && programmed[index].substr(0, 2).compare(Glib::ustring("RC")) != 0) { // RC doesn't have meal break
				worked -= Shift::get_number_of_meals(p_start[index], p_end[index]) * (Duty::get_meal_break());
			}
			return worked;
		}
	}
	return 0;
}

int SirisWeek::get_total_worked(int w1, int w2) {
	int total = 0;
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++) {
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		total += get_worked(n);
	}
	return total;
}

int SirisWeek::get_total_programmed_work(int w1, int w2) {
	int total = 0;
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++) {
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		total += get_programmed_work(n);
	}
	return total;
}

void SirisWeek::clear(void) {
	for (int i = 0; i < 7; i++) {
		duty[i] = -1;
		start[i] = -1;
		end[i] = -1;
		p_start[i] = -1;
		p_end[i] = -1;
		programmed[i] = "";
		executed[i] = "";
	}
}

void SirisWeek::compute(void) {
	int ehm = 0; // extra hours current month
	int ehp = 0; // extra hours other month
	if (date[0].get_month() != date[1].get_month()) {
		int eh1 = 0;
		int eh2 = 0;
		int w1 = check_week_part(1, CustomDate::get_last_day_month(date[1].get_time_t()).get_weekday());
		int w2 = check_week_part(CustomDate::get_first_day_month(date[0].get_time_t()).get_weekday(), 0);
		if (w1 < 0) {
			if (w2 + w1 >= 0) {
				eh1 = compute_week(1, CustomDate::get_last_day_month(date[1].get_time_t()).get_weekday(), -w1);
				eh2 = compute_week(CustomDate::get_first_day_month(date[0].get_time_t()).get_weekday(), 0, w1);
			} else {
				std::cerr << date[1] << " w1 = " << w1 << ", w2 = " << w2 << ", w1 < 0; w2 + w1 < 0; I can't correct this problem." << std::endl;
			}
		} else if (w2 < 0) {
			if (w1 + w2 >= 0) {
				eh1 = compute_week(1, CustomDate::get_last_day_month(date[1].get_time_t()).get_weekday(), w2);
				eh2 = compute_week(CustomDate::get_first_day_month(date[0].get_time_t()).get_weekday(), 0, -w2);
			} else {
				std::cerr << date[1] << " w1 = " << w1 << ", w2 = " << w2 << ", w2 < 0; w1 + w2 < 0; I can't correct this problem." << std::endl;
			}
		} else {
			eh1 = compute_week(1, CustomDate::get_last_day_month(date[1].get_time_t()).get_weekday());
			eh2 = compute_week(CustomDate::get_first_day_month(date[0].get_time_t()).get_weekday(), 0);
		}
		ehm = date[1].get_month() == month ? eh1 : eh2;
		ehp = date[1].get_month() == month ? eh2 : eh1;
	} else {
		ehm = compute_week(1, 0);
	}
	if (ehp > 360) {
		ehp = 360;
	}
	romc += ehm + ehp;
	romp += ehp;
}

bool SirisWeek::check_computation(void) {
	if (date[0].get_month() != date[1].get_month()) {
		int w1 = check_week_part(1, CustomDate::get_last_day_month(date[1].get_time_t()).get_weekday());
		int w2 = check_week_part(CustomDate::get_first_day_month(date[0].get_time_t()).get_weekday(), 0);
		if (w1 < 0) {
			if (w2 + w1 >= 0) {
				return true;
			} else {
				std::cerr << date[1] << " w1 = " << w1 << ", w2 = " << w2 << ", w1 < 0; w2 + w1 < 0; I can't correct this problem." << std::endl;
				return false;
			}
		} else if (w2 < 0) {
			if (w1 + w2 >= 0) {
				return true;
			} else {
				std::cerr << date[1] << " w1 = " << w1 << ", w2 = " << w2 << ", w2 < 0; w1 + w2 < 0; I can't correct this problem." << std::endl;
				return false;
			}
		} else {
			return true;
		}
	}
	return true;
}

int SirisWeek::compute_week(int w1, int w2, int hw) {
	int total_duty = get_total_duty(w1, w2) + hw;
	int total_worked = get_total_worked(w1, w2);
	int diff = total_worked - total_duty;
	int wdiff = diff;
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	// compute programmed shifts
	if (diff > 0) {
		int work_days = count_work_days(w1, w2);
		int average = (total_duty / work_days) - ((total_duty / work_days) % 60);
		if ((total_duty / work_days) % 60 > 0) {
			average += 60;
		}
		for (int i = w1; i <= w2; i++)	{
			int n = i == 7 ? 0 : i; // to avoid the Sunday problem
			if (programmed[n].compare(Glib::ustring("")) == 0 && start[n] >= 0)	{
				compute_program(n, average);
				total_duty -= Shift::get_worked_minutes(p_start[n], p_end[n]);
				work_days--;
				if (work_days > 0) {
					average = (total_duty / work_days) - ((total_duty / work_days) % 60);
					if ((total_duty / work_days) % 60 > 0) {
						average += 60;
					}
				} else {
					average = total_duty;
				}
			}
		}
		if (total_duty > 0) {
			add_hours_on_programmed(w1, w2, hw);
		}
	} else if (diff < 0) {
		for (int i = w1; i <= w2; i++) {
			int n = i == 7 ? 0 : i; // to avoid the Sunday problem
			compute_program(n);
		}
		while (diff < 0) {
			if (diff <= -720) { // 720 minutes = 12 hours
				diff = add_rc(w1, w2, hw);   // add an RC in order to cover missing working hours
			} else {
				diff = add_extra_hours_to_programmed(w1, w2, hw); // add hours in the first available working day
			}
		}
	} else {
		// worked hours are equal to duty: simple compute
		for (int i = w1; i <= w2; i++) {
			int n = i == 7 ? 0 : i; // to avoid the Sunday problem
			compute_program(n);
		}
	}
	// compute executed shifts
	for (int i = w1; i <= w2; i++)	{
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		if (p_start[n] >= 0) {
			if (programmed[n].size() > 0) {
				if (programmed[n].substr(0, 2).compare(Glib::ustring("RC")) == 0) {
					unsigned int start_h, start_m, end_h, end_m;
					start_h = p_start[n] / 60;
					start_m = p_start[n] % 60;
					end_h = p_end[n] / 60;
					end_m = p_end[n] % 60;
					executed[n].assign(Glib::ustring::compose("ROMC %1:%2 - %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
				} else if (programmed[n].substr(0, 1).compare(Glib::ustring("P")) == 0) {
					compute_executed(w1, n);
				}
			}
		}
	}
	return wdiff;
}

void SirisWeek::set_programmed_start(unsigned short index, int val) {
	if (index < 7) {
		p_start[index] = val;
	}
}

void SirisWeek::set_programmed_end(unsigned short index, int val) {
	if (index < 7) {
		p_end[index] = val;
	}
}

/*
 * compute programmed day for a given amount of hours if given
 */
void SirisWeek::compute_program(unsigned short index, unsigned short amount) {
	if (programmed[index].compare(Glib::ustring("")) == 0 && start[index] >= 0) {
		unsigned int start_h, start_m, end_h, end_m;
		start_h = start[index] / 60;
		start_m = start[index] % 60;
		p_start[index] = start[index];
		if (amount == 0) {
			end_h = end[index] / 60;
			end_m = end[index] % 60;
			p_end[index] = end[index];
		} else {
			unsigned int val = start[index] == 0 ? 360 : start[index] + amount;
			// check if lunch break is included
			if (p_start[index] < Duty::get_lunch_start() && val > Duty::get_lunch_end()) {
				val += Duty::get_meal_break();
			}
			// check if dinner break is included
			if (p_start[index] < Duty::get_dinner_start() && val >= Duty::get_dinner_end()) {
				val += Duty::get_meal_break();
			}
			// check if going to program more than worked
			if (val > end[index]) {
				val = end[index];
			}
			end_h = val / 60;
			end_m = val % 60;
			p_end[index] = val;
		}
		programmed[index].assign(Glib::ustring::compose("P %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m)));
		programmed[index].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
		int mealbreak = (p_end[index] - p_start[index]) - Shift::get_worked_minutes(p_start[index], p_end[index]);
		programmed[index].append(Glib::ustring::compose("  (%1)", Glib::ustring::format(std::setfill(L'0'), std::setw(2), mealbreak)));
	}
}

/*
 * add an RC in the first empty day
 */
int SirisWeek::add_rc(int w1, int w2, int hw) {
	int hour_left = get_total_duty(w1, w2) + hw - get_total_programmed_work(w1, w2);
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++) {
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		if (!date[n].is_holiday()) {
			if (start[n] < 0) { // if start == -1 it means it's a not working day
				unsigned int val = hour_left + 420; // 420 = 07:00
				if (val > 1440) {
					val = 1440;
				}
				unsigned int end_h = val / 60;
				unsigned int end_m = val % 60;
				programmed[n].assign(Glib::ustring::compose("RC 07:00 - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
				// by setting p_start and p_end will allow to calculate the RC as worked hours
				p_start[n] = 420;
				p_end[n] = val;
				hour_left -= p_end[n] - p_start[n];
				return -hour_left;
			}
		}
	}
	std::cerr << "couldn't find a valid day to program an RC" << std::endl;
	return 0;
}

/*
 * add extra hours to the first available working day
 */
int SirisWeek::add_extra_hours_to_programmed(int w1, int w2, int hw) {
	int hour_left = get_total_duty(w1, w2) + hw - get_total_programmed_work(w1, w2);
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++)	{
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		if (end[n] >= 0 && end[n] < 1440) {
			unsigned int val = end[n] + hour_left;
			if (val > 1440) {
				val = 1440;
			}
			unsigned int start_h = start[n] / 60;
			unsigned int start_m = start[n] % 60;
			unsigned int end_h = val / 60;
			unsigned int end_m = val % 60;
			programmed[n].assign(Glib::ustring::compose("P %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m)));
			programmed[n].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
			int mealbreak = (val - start[n]) - Shift::get_worked_minutes(start[n], val);
			programmed[n].append(Glib::ustring::compose("  (%1)", Glib::ustring::format(std::setfill(L'0'), std::setw(2), mealbreak)));
			p_start[n] = start[n];
			p_end[n] = val;
			hour_left = val - end[n];
			return hour_left;
		}
		/*if (start[n] >= 480) {
			std::cout << "try to put extra hours at the start of the shift - " << date[n] << std::endl;
			int val = start[n] - hour_left;
			if (val > 360) {
				val = 360;
			}
			unsigned int start_h = val / 60;
			unsigned int start_m = val % 60;
			unsigned int end_h = end[n] / 60;
			unsigned int end_m = end[n] % 60;
			programmed[n].assign(Glib::ustring::compose("P %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m)));
			programmed[n].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
			int mealbreak = (val - start[n]) - Shift::get_worked_minutes(start[n], val);
			programmed[n].append(Glib::ustring::compose("  (%1)", Glib::ustring::format(std::setfill(L'0'), std::setw(2), mealbreak)));
			p_start[n] = val;
			p_end[n] = end[n];
			hour_left -= start[n] - val;
			return hour_left;
		}*/
	}
	std::cerr << "couldn't find a valid working day to add some extra hours - " << date[1] << std::endl;
	return 0;
}

/*
 * count days where he work
 */
int SirisWeek::count_work_days(int w1, int w2) {
	int wd = 0;
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++)	{
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		if (start[n] >= 0) {
			wd++;
		}
	}
	return wd;
}

/*
 * add some hours to the first available working day
 */
void SirisWeek::add_hours_on_programmed(int w1, int w2, int hw) {
	int hour_left = get_total_duty(w1, w2) + hw - get_total_programmed_work(w1, w2);
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++) {
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		if (p_end[n] >= 0 && p_end[n] < 1440 && p_end[n] < end[n]) {
			int prev_prog_hours = get_programmed_work(n);
			unsigned int val = start[n] + prev_prog_hours + hour_left;
			// check if lunch break is included
			if (p_start[n] < Duty::get_lunch_start() && val > Duty::get_lunch_end()) {
				val += Duty::get_meal_break();
			}
			// check if dinner break is included
			if (p_start[n] < Duty::get_dinner_start() && val >= Duty::get_dinner_end()) {
				val += Duty::get_meal_break();
			}
			if (val > end[n]) {
				val = end[n];
			}
			unsigned int start_h = start[n] / 60;
			unsigned int start_m = start[n] % 60;
			unsigned int end_h = val / 60;
			unsigned int end_m = val % 60;
			programmed[n].assign(Glib::ustring::compose("P %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m)));
			programmed[n].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
			int mealbreak = (val - start[n]) - Shift::get_worked_minutes(start[n], val);
			programmed[n].append(Glib::ustring::compose("  (%1)", Glib::ustring::format(std::setfill(L'0'), std::setw(2), mealbreak)));
			p_start[n] = start[n];
			p_end[n] = val;
			hour_left -= get_programmed_work(n) - prev_prog_hours;
		}
		if (hour_left == 0) {
			return;
		}
	}
	std::cerr << "couldn't find a valid working day to add some hours" << std::endl;
}

/*
 * compute executed shift
 */
void SirisWeek::compute_executed(int w1, unsigned short index) {
	// start shift
	unsigned int start_h, start_m, end_h, end_m;
	if (p_start[index] < start[index]) {
		std::cout << "I should put a romc/romp/pb at the start of the service here - " << date[index] << std::endl;
	}
	start_h = p_start[index] / 60;
	start_m = p_start[index] % 60;
	executed[index].assign(Glib::ustring::compose("SERV %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m)));
	// check if lunch break is included
	if (start[index] < Duty::get_lunch_start() && end[index] > Duty::get_lunch_end()) {
		if (p_end[index] <= 840) {
			executed[index].append(Glib::ustring::compose(" - %1:%2\nSERV %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] % 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), (p_end[index] + Duty::get_meal_break()) / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), (p_end[index] + Duty::get_meal_break()) % 60)));
		} else {
			executed[index].append(Glib::ustring(" - 12:00\nSERV 13:00"));
		}
		// check if dinner break is included
		if (start[index] < Duty::get_dinner_start() && end[index] > Duty::get_dinner_end()) {
			if (p_end[index] >= 1140 && p_end[index] <= 1260) {
				executed[index].append(Glib::ustring::compose(" - %1:%2\nSERV %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] % 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), (p_end[index] + Duty::get_meal_break()) / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), (p_end[index] + Duty::get_meal_break()) % 60)));
			} else {
				executed[index].append(Glib::ustring(" - 19:00\nSERV 20:00"));
			}
		}
	}
	end_h = end[index] / 60;
	end_m = end[index] % 60;
	// check if the programmed ends before 24 but effective work ends past 22
	if (p_end[index] < 1440 && end[index] > 1320) {
		int npb = p_end[index] >= 1320 ? end[index] - p_end[index] : end[index] - 1320; // needed PB
		if (pb < npb) {
			provide_pb(index);
			npb = p_end[index] >= 1320 ? end[index] - p_end[index] : end[index] - 1320; // updated needed PB
		}
		int diff = pb - npb;
		int startpb = diff >= 0 ? 1440 - npb : 1440 - pb;
		if (diff >= 0) {
			pb -= npb;
		} else if (pb > 0) {
			pb = 0;
		}
		if (startpb < 1440) {
			executed[index].append(Glib::ustring::compose(" - %1:%2\nREPB %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), startpb / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), startpb % 60)));
		}
	}
	executed[index].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
	if (end[index] < p_end[index]) {
		int wextra = 0;
		if (index != w1) {
			unsigned short int daybefore = index == 0 ? 6 : index - 1;
			wextra = get_total_worked(w1, daybefore) - get_total_programmed_work(w1, daybefore);
		}
		if (date[index].get_month() != month) {
			start_h = end_h;
			start_m = end_m;
			end_h = p_end[index] / 60;
			end_m = p_end[index] % 60;
			executed[index].append(Glib::ustring::compose("\nROMC %1:%2 - %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
		} else {
			int diff = p_end[index] - end[index];
			if (romp > 0) {		// try to use ROMP
				start_h = end_h;
				start_m = end_m;
				if (diff >= romp) {
					end_h = (end[index] + romp) / 60;
					end_m = (end[index] + romp) % 60;
					diff -= romp;
					romp = 0;
				} else {
					end_h = p_end[index] / 60;
					end_m = p_end[index] % 60;
					romp -= diff;
					diff = 0;
				}
				executed[index].append(Glib::ustring::compose("\nROMP %1:%2 - %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
			}
			if (diff > 0) {		// try to use ROMC
				if (romc + wextra > 0) {
					start_h = end_h;
					start_m = end_m;
					if (diff >= romc + wextra) {
						end_h = end_h + ((romc + wextra) / 60);
						end_m = end_m + ((romc + wextra) % 60);
						diff -= romc + wextra;
					} else {
						end_h = p_end[index] / 60;
						end_m = p_end[index] % 60;
						diff = 0;
					}
					executed[index].append(Glib::ustring::compose("\nROMC %1:%2 - %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
				}
			}
			if (diff > 0) {		// use PB
				start_h = end_h;
				start_m = end_m;
				end_h = p_end[index] / 60;
				end_m = p_end[index] % 60;
				executed[index].append(Glib::ustring::compose("\nPB %1:%2 - %3:%4", Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start_m), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_h), Glib::ustring::format(std::setfill(L'0'), std::setw(2), end_m)));
				pb += ((end_h - start_h) * 60) + (end_m - start_m);
			}
		}
	}
}

int SirisWeek::check_week_part(int w1, int w2) {
	short int p = 0; // presences
	short int sw = 0; // should work minutes
	if (w1 == 0) {
		w1 = 7; // Sunday index moved to 7
	}
	if (w2 == 0) {
		w2 = 7; // Sunday index moved to 7
	}
	for (int i = w1; i <= w2; i++) {
		int n = i == 7 ? 0 : i; // to avoid the Sunday problem
		sw += duty[n];
		if (start[n] >= 0) {
			p++;
		}
	}
	return sw - (p * 360);
}

void SirisWeek::provide_pb(unsigned short index) {
	if (p_start[index] > 360) {
		int needed = end[index] - 1320; // needed PB
		int x = p_start[index] - needed;
		if (x < 360) {
			x = 360;
		}
		int tw = get_programmed_work(index);
		int meal = 0;
		p_start[index] = x;
		p_end[index] = x + tw;
		// check if lunch break is included
		if (p_start[index] < Duty::get_lunch_start() && p_end[index] > Duty::get_lunch_end()) {
			p_end[index] += Duty::get_meal_break();
			meal++;
		}
		// check if dinner break is included
		if (p_start[index] < Duty::get_dinner_start() && p_end[index] >= Duty::get_dinner_end()) {
			p_end[index] += Duty::get_meal_break();
			meal++;
		}
		programmed[index].assign(Glib::ustring::compose("P %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_start[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_start[index] % 60)));
		programmed[index].append(Glib::ustring::compose(" - %1:%2", Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_end[index] % 60)));
		programmed[index].append(Glib::ustring::compose("  (%1)", Glib::ustring::format(std::setfill(L'0'), std::setw(2), Duty::get_meal_break() * meal)));
		executed[index].insert(0, Glib::ustring::compose("PB %1:%2 - %3:%4\n", Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_start[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), p_start[index] % 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start[index] / 60), Glib::ustring::format(std::setfill(L'0'), std::setw(2), start[index] % 60)));
		pb += start[index] - p_start[index];
	}
}

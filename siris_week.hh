/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * siris_week.hh
 * Copyright (C) 2019 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * personnel_manager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * personnel_manager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SIRIS_WEEK_H_
#define _SIRIS_WEEK_H_

#include <glibmm/ustring.h>
#include "custom_date.hh"

class SirisWeek {
	 public:
		SirisWeek();
		inline void set_month(short int val) { month = val; }
		void set_duty(unsigned short index, int val);
		inline int get_duty(unsigned short index) const { return duty[index]; }
		void set_start(unsigned short index, int val);
		inline int get_start(unsigned short index) const { return start[index]; }
		void set_end(unsigned short index, int val);
		inline int get_end(unsigned short index) const { return end[index]; }
		inline void set_date(unsigned short index, const CustomDate& cd) { date[index] = cd; }
		inline CustomDate get_date(short int index) const { return date[index]; }
		inline void set_label(unsigned short index, const Glib::ustring& str) { programmed[index].assign(str); }
		inline Glib::ustring get_programmed(unsigned short index) const { return programmed[index]; }
		inline Glib::ustring get_executed(unsigned short index) const { return executed[index]; }
		inline void set_romc(int val) { romc = val; }
		inline int get_romc(void) const { return romc; }
		inline void set_romp(int val) { romp = val; }
		inline int get_romp(void) const { return romp; }
		inline void set_pb(int val) { pb = val; }
		inline int get_pb(void) const { return pb; }
		inline void set_rrs(int val) { rrs = val; }
		inline int get_rrs(void) const { return rrs; }
		inline void set_rri(int val) { rri = val; }
		inline int get_rri(void) const { return rri; }
		bool is_ready(void);
		int get_total_duty(int w1, int w2);
		int get_worked(unsigned short index);
		int get_programmed_work(unsigned short index);
		int get_total_worked(int w1, int w2);
		int get_total_programmed_work(int w1, int w2);
		void clear(void);
		void compute(void);
		bool check_computation(void);
	 protected:
		int compute_week(int w1, int w2, int hw =0);
		void set_programmed_start(unsigned short index, int val);
		inline int get_programmed_start(unsigned short index) const { return p_start[index]; }
		void set_programmed_end(unsigned short index, int val);
		inline int get_programmed_end(unsigned short index) const { return p_end[index]; }
		void compute_program(unsigned short index, unsigned short amount = 0);
		int add_rc(int w1, int w2, int hw = 0);
		int add_extra_hours_to_programmed(int w1, int w2, int hw = 0);
		int count_work_days(int w1, int w2);
		void add_hours_on_programmed(int w1, int w2, int hw = 0);
		void compute_executed(int w1, unsigned short index);
		int check_week_part(int w1, int w2);
		void provide_pb(unsigned short index);
	private:
		short int month;
		int duty[7];	// should work minutes
		int start[7];   // real work start
		int end[7];		// real work end
		CustomDate date[7];
		int p_start[7];	 // programmed start
		int p_end[7];	 // programmed end
		Glib::ustring programmed[7];
		Glib::ustring executed[7];
		int romc;
		int romp;
		int pb;
		int rrs;
		int rri;
};

#endif // _SIRIS_WEEK_H_


/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * stats_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stats_dialog.hh"

CalendarData StatsDialog::calendata { CalendarData() };

void StatsDialog::build_gui(StatsDialog* sd) {
	Gtk::Box* box = sd->get_content_area();
	sd->member_liststore = Gtk::ListStore::create(sd->combobox2_tmcr);
	sd->bldr->get_widget("member_combobox", sd->member_combobox);
	sd->member_combobox->set_model(sd->member_liststore);
	sd->bldr->get_widget("stats_box", sd->stats_box);
	sd->bldr->get_widget("previous_toolbutton", sd->previous_toolbutton);
	sd->bldr->get_widget("next_toolbutton", sd->next_toolbutton);
	sd->bldr->get_widget("month_label", sd->month_label);
	sd->bldr->get_widget("stats_member_textview", sd->stats_member_textview);
	sd->stats_member_buffer = sd->stats_member_textview->get_buffer();
	box->pack_start(*(sd->stats_box));
	/*
	 * load values
	 */
	sd->load_members_in_combobox();
	sd->member_combobox->set_active(0);
	sd->update_statistic();
	/*
	 * signals connections
	 */
	sd->member_combobox->signal_changed().connect(sigc::mem_fun(sd, &StatsDialog::update_statistic));
	sd->next_toolbutton->signal_clicked().connect(sigc::mem_fun(sd, &StatsDialog::on_next_toolbutton_clicked));
	sd->previous_toolbutton->signal_clicked().connect(sigc::mem_fun(sd, &StatsDialog::on_previous_toolbutton_clicked));
}

void StatsDialog::load_members_in_combobox() const {
	member_liststore->clear();
	try {
		auto members { database->get_members() };
		for (auto iter = members.cbegin(); iter != members.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { member_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[combobox2_tmcr.column1] = Glib::ustring::compose("%1 %2 %3",iter->get_rank().get_short_label(),iter->get_first_name(),iter->get_last_name());
			row[combobox2_tmcr.column2] = iter->get_id();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "StatsDialog::load_members_in_combobox() " << dbe.get_message() << std::endl;
	}
}

Glib::ustring StatsDialog::get_selected_member() const {
	Gtk::TreeModel::iterator iter { member_combobox->get_active() };
	if (iter != member_liststore->children().end()) {
		Gtk::TreeModel::Row row { *iter };
		return row[combobox2_tmcr.column2];
	}
	return Glib::ustring();
}

void StatsDialog::on_next_toolbutton_clicked(void) {
	calendata.set_next_month();
	update_statistic();
}

void StatsDialog::on_previous_toolbutton_clicked(void) {
	calendata.set_previous_month();
	update_statistic();
}

void StatsDialog::update_statistic(void) const {
	Glib::ustring text;
	Glib::ustring id { get_selected_member() };
	if (id.compare(Glib::ustring()) != 0) {
		std::map<const Glib::ustring, std::pair<const Glib::ustring, int> > month_stats;
		std::map<const Glib::ustring, std::pair<const Glib::ustring, int> > year_stats;
		try {
			Member member = database->get_member(id);
			CustomDate cd_day = calendata.get_reference_cdate();
			CustomDate ldaym = CustomDate::get_last_day_month(calendata.get_reference_date());
			month_label->set_text(cd_day.get_formatted_string(DATE_FORMAT::monthyear));
			text.append(Glib::ustring("\tnel mese ") + Glib::ustring(" :\n\n"));
			text.append(Glib::ustring("- Presenze: ") + Glib::ustring::format(calendata.get_presence_count(database, member.get_id())));
			text.append(Glib::ustring("\t(Buoni pasto: ") + Glib::ustring::format(calendata.get_number_of_meals(database, member.get_id())) + Glib::ustring(")\n"));
			calendata.get_stats(database, CustomDate::get_first_day_month(calendata.get_reference_date()), ldaym, member.get_id(), &month_stats);
			for (auto iter = month_stats.cbegin(); iter != month_stats.cend(); iter++) {
				if (iter->second.second > 0) {
					text.append(Glib::ustring("- " + iter->second.first + Glib::ustring(": ") + Glib::ustring::format(iter->second.second) + Glib::ustring("\n")));
				}
			}
			text.append(Glib::ustring("\n\tnell'anno ") + Glib::ustring(" :\n\n"));
			calendata.get_stats(database, CustomDate::get_first_day_year(calendata.get_reference_date()), ldaym, member.get_id(), &year_stats);
			for (auto iter = year_stats.begin(); iter != year_stats.end(); iter++) {
				if (iter->second.second > 0) {
					text.append(Glib::ustring("- " + iter->second.first + Glib::ustring(": ") + Glib::ustring::format(iter->second.second) + Glib::ustring("\n")));
				}
			}
		} catch (const Database::DBError& dbe) {
			std::cerr << "StatsDialog::update_statistic() " << dbe.get_message() << std::endl;
			throw dbe;
		} catch (const Database::DBEmptyResultException& dbe) {
			std::cerr << "StatsDialog::update_statistic() " << dbe.get_message() << std::endl;
			throw dbe;
		}
	}
	stats_member_buffer->set_text(text);
}

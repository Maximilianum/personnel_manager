/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * stats_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _STATS_DIALOG_HH_
#define _STATS_DIALOG_HH_

#include <gtkmm/combobox.h>
#include <gtkmm/textview.h>
#include <gtkmm/toolbutton.h>
#include <gtkmm/label.h>
#include "dual_string_tmcr.hh"
#include "control_dialog.hh"
#include "database.hh"
#include "calendar_data.hh"

class StatsDialog : public ControlDialog {
 public:
	class IteratorNotFoundException
	{
	public:
    IteratorNotFoundException(const std::string& str) : message (str) {}
		std::string get_message(void) const { return message; }
	private:
		const std::string message;
	};
	inline StatsDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline StatsDialog(const StatsDialog& sd) : ControlDialog(sd) { build_gui(this); }
	inline static void set_calendar_data(const CalendarData* cd) { calendata = *cd; }
 private:
	static void build_gui(StatsDialog* sd);
	void load_members_in_combobox() const;
	Glib::ustring get_selected_member() const;
	void on_next_toolbutton_clicked(void);
	void on_previous_toolbutton_clicked(void);
	void update_statistic(void) const;

	Gtk::Box* stats_box;
	Gtk::ComboBox* member_combobox;
	Glib::RefPtr<Gtk::ListStore> member_liststore;
	Gtk::ToolButton* previous_toolbutton;
	Gtk::ToolButton* next_toolbutton;
	Gtk::Label* month_label;
	Gtk::TextView* stats_member_textview;
	Glib::RefPtr<Gtk::TextBuffer> stats_member_buffer;
	DualStringTreeModelColumnRecord combobox2_tmcr;
	static CalendarData calendata;
	Database* database;
};

#endif // _STATS_DIALOG_HH_

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * text_line.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "text_line.hh"

std::ostream& operator<<(std::ostream& os, const TextLine& txtl) {
	return os << "#" << txtl.get_row() << " \'" << txtl.get_type().raw() << "\', \'" << txtl.get_text().raw() << "\', font size:" << txtl.get_font_size();
}

Glib::ustring TextLine::sqlite_table { Glib::ustring("TABLE TEXTLINE (" \
													 "ROW INTEGER NOT NULL, " \
													 "TYPE TEXT NOT NULL, " \
													 "STRING TEXT, "	\
													 "FSIZE REAL, "		\
													 "PRIMARY KEY (ROW,TYPE)); ") };

int TextLine::retrieve_text_line(void *data, int argc, char **argv, char **azColName) {
	TextLine txtl;
	std::vector<TextLine>* lines { static_cast<std::vector<TextLine>*>(data) };
	for(int i=0; i<argc; i++) {
		if ((Glib::ustring(azColName[i]).compare(Glib::ustring("ROW")) == 0 || Glib::ustring(azColName[i]).compare(Glib::ustring("ID")) == 0) && argv[i]) {
			txtl.set_row(atoi(argv[i]));
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("TYPE") && argv[i]) {
			txtl.set_type(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("STRING") && argv[i]) {
			txtl.set_text(argv[i]);
		} else if (Glib::ustring(azColName[i]) == Glib::ustring("FSIZE") && argv[i]) {
			txtl.set_font_size(atof(argv[i]));
		}
	}
	lines->push_back(txtl);
	return 0;
}

TextLine& TextLine::operator=(const TextLine& txtl){
	if (this != &txtl) {
		row = txtl.row;
		type = txtl.type;
		text = txtl.text;
		font_size = txtl.font_size;
	}
	return *this;
}


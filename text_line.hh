/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * text_line.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TEXT_LINE_HH_
#define _TEXT_LINE_HH_

#include <vector>
#include <glibmm/ustring.h>

class TextLine {
	friend std::ostream& operator<<(std::ostream& os, const TextLine& txtl);
 public:
	static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
	static int retrieve_text_line(void *data, int argc, char **argv, char **azColName);
 TextLine() : row (-1), font_size (12.0) {}
 TextLine(const int r, const Glib::ustring& ty, const Glib::ustring& txt, const double val) : row (r), type (ty), text (txt), font_size (val) {}
	TextLine& operator=(const TextLine& txtl);
	inline bool operator<(const TextLine& txtl) const { return row < txtl.row; }
	inline bool operator==(const TextLine& txtl) const { return row == txtl.row && type.compare(txtl.type) == 0 && text.compare(txtl.text) == 0 && font_size == txtl.font_size; }
	inline bool operator!=(const TextLine& txtl) const { return row != txtl.row || type.compare(txtl.type) != 0 || text.compare(txtl.text) != 0 || font_size != txtl.font_size; }
	inline void set_row(const int val) { row = val; }
	inline int get_row(void) const { return row; }
	inline void set_type(const Glib::ustring& str) { type.assign(str); }
	inline Glib::ustring get_type(void) const { return type; }
	inline void set_text(const Glib::ustring& str) { text.assign(str); }
	inline Glib::ustring get_text(void) const { return text; }
	inline void set_font_size(const double val) { font_size = val; }
	inline double get_font_size(void) const { return font_size; }
 private:
	static Glib::ustring sqlite_table;
	int row;
	Glib::ustring type;
	Glib::ustring text;
	double font_size;
};

#endif // _TEXT_LINE_HH_


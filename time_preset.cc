/*
 * time_preset.cc
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "time_preset.hh"
#include <vector>

Glib::ustring TimePreset::sqlite_table { Glib::ustring("TABLE TIME_PRESET (" \
						       "ID INTEGER PRIMARY KEY AUTOINCREMENT, " \
						       "START_H INTEGER NOT NULL, " \
						       "START_M INTEGER NOT NULL, " \
						       "FINISH_H INTEGER NOT NULL, " \
						       "FINISH_M INTEGER NOT NULL); ") };

int TimePreset::retrieve_time_preset(void *data, int argc, char **argv, char **azColName) {
  TimePreset tp;
  std::vector<TimePreset>* time_presets { static_cast<std::vector<TimePreset>*>(data) };
  for(int i=0; i<argc; i++) {
    if (Glib::ustring(azColName[i]) == Glib::ustring("ID") && argv[i]) {
      tp.set_id(atoi(argv[i]));
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("START_H") && argv[i]) {
      tp.set_start_hour(atoi(argv[i]));
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("START_M") && argv[i]) {
      tp.set_start_minute(atoi(argv[i]));
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("FINISH_H") && argv[i]) {
      tp.set_finish_hour(atoi(argv[i]));
    } else if (Glib::ustring(azColName[i]) == Glib::ustring("FINISH_M") && argv[i]) {
      tp.set_finish_minute(atoi(argv[i]));
    }
  }
  time_presets->push_back(tp);
  return 0;
}

TimePreset& TimePreset::operator=(const TimePreset& tp) {
  if (this != &tp) {
    id = tp.id;
    start = tp.start;
    finish = tp.finish;
  }
  return *this;
}

bool TimePreset::operator<(const TimePreset& tp) const {
  if (start.first < tp.start.first) {
    return true;
  } else if (start.first == tp.start.first) {
    if (start.second < tp.start.second) {
      return true;
    } else if (start.second == tp.start.second) {
      if (finish.first < tp.finish.first) {
	return true;
      } else if (finish.first == tp.finish.first) {
	if (finish.second < tp.finish.second) {
	  return true;
	}
      }
    }
  }
  return false;
}

bool TimePreset::check_consistency() {
  int begin = (start.first * 60) + start.second;
  int end = (finish.first * 60) + finish.second;
  if (end - begin >= 60) {
    return true;
  }
  return false;
}

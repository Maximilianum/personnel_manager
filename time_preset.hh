/*
 * time_preset.hh
 * Copyright (C) 2022 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TIME_PRESET_HH_
#define _TIME_PRESET_HH_

#include <utility>
#include <glibmm/ustring.h>
#include "custom_date.hh"

class TimePreset {
public:
  // static class functions
  static inline Glib::ustring get_sqlite_table(void) { return sqlite_table; }
  static int retrieve_time_preset(void *data, int argc, char **argv, char **azColName);
  // constructors
  inline TimePreset() : id (0), start (std::pair<short,short>(0,0)), finish (std::pair<short,short>(0,0)) {}
  inline TimePreset(int n, short sh, short sm, short fh, short fm) : id (n), start (std::pair<short,short>(sh,sm)), finish (std::pair<short,short>(fh,fm)) {}
  inline TimePreset(const CustomDate& cds, const CustomDate& cde) : id(0), start (std::pair<short,short>(cds.get_hour(),cds.get_minute())), finish (std::pair<short,short>(cde.get_hour(),cde.get_minute())) {}
  // operators
  TimePreset& operator=(const TimePreset& tp);
  bool operator<(const TimePreset& tp) const;
  inline bool operator==(const TimePreset& tp) const { return start == tp.start && finish == tp.finish; }
  // class members functions
  inline void set_id(int val) { id = val; }
  inline int get_id() const { return id; }
  inline void set_start(std::pair<short,short> p) { start = p; }
  inline void set_start_hour(short h) { start.first = h; }
  inline void set_start_minute(short m) { start.second = m; }
  inline std::pair<short,short> get_start() const { return start; }
  inline void set_finish(std::pair<short,short> p) { finish = p; }
  inline void set_finish_hour(short h) { finish.first = h; }
  inline void set_finish_minute(short m) { finish.second = m; }
  inline std::pair<short,short> get_finish() const { return finish; }
  bool check_consistency();
private:
  static Glib::ustring sqlite_table;
  int id;
  std::pair<short,short> start;
  std::pair<short,short> finish;
};

#endif // _TIME_PRESET_

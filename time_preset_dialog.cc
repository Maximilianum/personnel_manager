/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * time_preset_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "time_preset_dialog.hh"
#include <gtkmm/messagedialog.h>

void TimePresetDialog::build_gui(TimePresetDialog* tp) {
	Gtk::Box* box { tp->get_content_area() };
	tp->bldr->get_widget("time_preset_box", tp->time_preset_box);
	tp->time_preset_liststore = Gtk::ListStore::create(tp->time_preset_tmcr);
	tp->bldr->get_widget("time_preset_treeview", tp->time_preset_treeview);
	tp->time_preset_treeview->set_model(tp->time_preset_liststore);
	tp->time_preset_treeselection = tp->time_preset_treeview->get_selection();
	tp->time_preset_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	tp->time_preset_start_hour_treeviewcolumn = tp->time_preset_treeview->get_column(0);
	tp->time_preset_start_hour_cellrenderertext = (Gtk::CellRendererText*)tp->time_preset_start_hour_treeviewcolumn->get_first_cell();
	tp->time_preset_start_minute_treeviewcolumn = tp->time_preset_treeview->get_column(1);
	tp->time_preset_start_minute_cellrenderertext = (Gtk::CellRendererText*)tp->time_preset_start_minute_treeviewcolumn->get_first_cell();
	tp->time_preset_end_hour_treeviewcolumn = tp->time_preset_treeview->get_column(2);
	tp->time_preset_end_hour_cellrenderertext = (Gtk::CellRendererText*)tp->time_preset_end_hour_treeviewcolumn->get_first_cell();
	tp->time_preset_end_minute_treeviewcolumn = tp->time_preset_treeview->get_column(3);
	tp->time_preset_end_minute_cellrenderertext = (Gtk::CellRendererText*)tp->time_preset_end_minute_treeviewcolumn->get_first_cell();
	tp->bldr->get_widget("add_time_preset_button", tp->add_time_preset_button);
	tp->bldr->get_widget("remove_time_preset_button", tp->remove_time_preset_button);
	box->pack_start(*(tp->time_preset_box));
	/*
	 * load values
	 */
	tp->load_time_preset_in_liststore(tp->time_preset_liststore);
	/*
	 * signals connections
	 */
	tp->time_preset_treeselection->signal_changed().connect(sigc::mem_fun(tp, &TimePresetDialog::on_time_presets_treeselection_changed));
	tp->add_time_preset_button->signal_clicked().connect(sigc::mem_fun(tp, &TimePresetDialog::on_add_time_preset_button_clicked));
	tp->remove_time_preset_button->signal_clicked().connect(sigc::mem_fun(tp, &TimePresetDialog::on_remove_time_preset_button_clicked));
	tp->time_preset_start_hour_cellrenderertext->signal_edited().connect(sigc::mem_fun(tp, &TimePresetDialog::on_time_preset_start_h_edited));
	tp->time_preset_start_minute_cellrenderertext->signal_edited().connect(sigc::mem_fun(tp, &TimePresetDialog::on_time_preset_start_m_edited));
	tp->time_preset_end_hour_cellrenderertext->signal_edited().connect(sigc::mem_fun(tp, &TimePresetDialog::on_time_preset_finish_h_edited));
	tp->time_preset_end_minute_cellrenderertext->signal_edited().connect(sigc::mem_fun(tp, &TimePresetDialog::on_time_preset_finish_m_edited));
}

void TimePresetDialog::load_time_preset_in_liststore(Glib::RefPtr<Gtk::ListStore> list_store) {
	list_store->clear();
	try {
		std::vector<TimePreset> tps { database->get_time_presets() };
		for (auto iter = tps.cbegin(); iter != tps.cend(); iter++) {
			Gtk::TreeModel::iterator iter_list { list_store->append() };
			Gtk::TreeModel::Row row { *iter_list };
			std::pair<short,short> start { iter->get_start() };
			std::pair<short,short> finish { iter->get_finish() };
			row[time_preset_tmcr.start_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), start.first);
			row[time_preset_tmcr.start_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), start.second);
			row[time_preset_tmcr.finish_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish.first);
			row[time_preset_tmcr.finish_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), finish.second);
			row[time_preset_tmcr.id] = iter->get_id();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "TimePresetDialog::load_time_presets_in_liststore() " << dbe.get_message() << std::endl;
	}
}

void TimePresetDialog::on_add_time_preset_button_clicked(void) {
	TimePreset new_time_preset { TimePreset(0, 8, 0, 14, 0) };
	try {
		database->insert(new_time_preset);
	} catch (const Database::DBError& dbe) {
		std::cerr << "TimePresetDialog::on_add_time_preset_button_clicked() " << dbe.get_message() << std::endl;
	}
	load_time_preset_in_liststore(time_preset_liststore);
}

void TimePresetDialog::on_remove_time_preset_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'orario selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> time_preset_treemodel { time_preset_treeview->get_model() };
		std::vector<Gtk::TreePath> selected { time_preset_treeselection->get_selected_rows() };
		for (auto iter = selected.cbegin(); iter != selected.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { time_preset_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			int id { row[time_preset_tmcr.id] };
			try {
				database->delete_time_preset(id);
				time_preset_liststore->erase(iter_tm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "TimePresetDialog::on_remove_time_preset_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

TimePreset TimePresetDialog::get_time_preset_from_row(Gtk::TreeModel::Row row) {
	int start_h { atoi(Glib::ustring(row[time_preset_tmcr.start_h]).c_str()) };
	int start_m { atoi(Glib::ustring(row[time_preset_tmcr.start_m]).c_str()) };
	int finish_h { atoi(Glib::ustring(row[time_preset_tmcr.finish_h]).c_str()) };
	int finish_m { atoi(Glib::ustring(row[time_preset_tmcr.finish_m]).c_str()) };
	int id { row[time_preset_tmcr.id] };
	TimePreset tp { TimePreset(id, start_h, start_m, finish_h, finish_m) };
	return tp;
}

short int TimePresetDialog::get_consistent_time_value(const Glib::ustring &str, short int min, short int max, bool flag = false) {
	int ctime { atoi(str.c_str()) };
	if (ctime < min) {
		ctime = min;
	}
	if (ctime > max) {
		ctime = max;
	}
	return flag ? ctime - (ctime % 10) : ctime;
}

void TimePresetDialog::on_time_preset_start_h_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	short int new_time { get_consistent_time_value(new_text, 0, 23) };
	Glib::RefPtr<Gtk::TreeModel> time_preset_model { time_preset_treeview->get_model() };
	Gtk::TreeModel::iterator iter { time_preset_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	TimePreset tp { get_time_preset_from_row(row) };
	tp.set_start_hour(new_time);
	if (tp.check_consistency()) {
		try {
			database->update_start_h(tp.get_id(), new_time);
			row[time_preset_tmcr.start_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), new_time);
		} catch (const Database::DBError& dbe) {
			std::cerr << "TimePresetDialog::on_time_preset_start_h_edited() " << dbe.get_message() << std::endl;
		}
	} else {
		Gtk::MessageDialog alert_dialog { *this, "La durata di un turno di lavoro non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		int res_id { alert_dialog.run() };
	}
}

void TimePresetDialog::on_time_preset_start_m_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	short int new_time { get_consistent_time_value(new_text, 0, 50, true) };
	Glib::RefPtr<Gtk::TreeModel> time_preset_model { time_preset_treeview->get_model() };
	Gtk::TreeModel::iterator iter { time_preset_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	TimePreset tp { get_time_preset_from_row(row) };
	tp.set_start_minute(new_time);
	if (tp.check_consistency()) {
		try {
			database->update_start_m(tp.get_id(), new_time);
			row[time_preset_tmcr.start_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), new_time);
		} catch (const Database::DBError& dbe) {
			std::cerr << "TimePresetDialog::on_time_preset_start_m_edited() " << dbe.get_message() << std::endl;
		}
	} else {
		Gtk::MessageDialog alert_dialog { *this, "La durata di un turno di lavoro non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		int res_id { alert_dialog.run() };
	}
}

void TimePresetDialog::on_time_preset_finish_h_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	short int new_time { get_consistent_time_value(new_text, 1, 24) };
	Glib::RefPtr<Gtk::TreeModel> time_preset_model { time_preset_treeview->get_model() };
	Gtk::TreeModel::iterator iter { time_preset_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	TimePreset tp { get_time_preset_from_row(row) };
	tp.set_finish_hour(new_time);
	if (tp.check_consistency()) {
		try {
			database->update_finish_h(tp.get_id(), new_time);
			row[time_preset_tmcr.finish_h] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), new_time);
		} catch (const Database::DBError& dbe) {
			std::cerr << "TimePresetDialog::on_time_preset_finish_h_edited() " << dbe.get_message() << std::endl;
		}
	} else {
		Gtk::MessageDialog alert_dialog { *this, "La durata di un turno di lavoro non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		int res_id { alert_dialog.run() };
	}
}

void TimePresetDialog::on_time_preset_finish_m_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	short int new_time { get_consistent_time_value(new_text, 0, 50, true) };
	Glib::RefPtr<Gtk::TreeModel> time_preset_model { time_preset_treeview->get_model() };
	Gtk::TreeModel::iterator iter { time_preset_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	TimePreset tp { get_time_preset_from_row(row) };
	tp.set_finish_minute(new_time);
	if (tp.check_consistency()) {
		try {
			database->update_finish_m(tp.get_id(), new_time);
			row[time_preset_tmcr.finish_m] = Glib::ustring::format(std::setfill(L'0'), std::setw(2), new_time);
		} catch (const Database::DBError& dbe) {
			std::cerr << "TimePresetDialog::on_time_preset_finish_m_edited() " << dbe.get_message() << std::endl;
		}
	} else {
		Gtk::MessageDialog alert_dialog { *this, "La durata di un turno di lavoro non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
		int res_id { alert_dialog.run() };
	}
}

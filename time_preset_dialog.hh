/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * time_preset_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TIME_PRESET_DIALOG_HH_
#define _TIME_PRESET_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "time_preset_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

class TimePresetDialog : public ControlDialog {
 public:
	inline TimePresetDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline TimePresetDialog(const TimePresetDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(TimePresetDialog* sd);
	void load_time_preset_in_liststore(Glib::RefPtr<Gtk::ListStore> list_store);
	inline void on_time_presets_treeselection_changed(void) { remove_time_preset_button->set_sensitive(time_preset_treeselection->count_selected_rows() > 0); }
	void on_add_time_preset_button_clicked(void);
	void on_remove_time_preset_button_clicked(void);
	TimePreset get_time_preset_from_row(Gtk::TreeModel::Row row);
	short int get_consistent_time_value(const Glib::ustring &str, short int min, short int max, bool flag);
	void on_time_preset_start_h_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_time_preset_start_m_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_time_preset_finish_h_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_time_preset_finish_m_edited(const Glib::ustring& path, const Glib::ustring& new_text);

	Gtk::Box* time_preset_box;
	Glib::RefPtr<Gtk::ListStore> time_preset_liststore;
	Gtk::TreeView* time_preset_treeview;
	Glib::RefPtr<Gtk::TreeSelection> time_preset_treeselection;
	Gtk::TreeViewColumn* time_preset_start_hour_treeviewcolumn;
	Gtk::CellRendererText* time_preset_start_hour_cellrenderertext;
	Gtk::TreeViewColumn* time_preset_start_minute_treeviewcolumn;
	Gtk::CellRendererText* time_preset_start_minute_cellrenderertext;
	Gtk::TreeViewColumn* time_preset_end_hour_treeviewcolumn;
	Gtk::CellRendererText* time_preset_end_hour_cellrenderertext;
	Gtk::TreeViewColumn* time_preset_end_minute_treeviewcolumn;
	Gtk::CellRendererText* time_preset_end_minute_cellrenderertext;
	Gtk::Button* add_time_preset_button;
	Gtk::Button* remove_time_preset_button;
	TimePresetTreeModelColumnRecord time_preset_tmcr;
	Database* database;
};

#endif // _TIME_PRESET_DIALOG_HH_

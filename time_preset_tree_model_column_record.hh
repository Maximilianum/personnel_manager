/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * time_preset_tree_model_column_record.hh
 * Copyright (C) 2015 - 2020 Massimiliano Maniscalco <maximilianum@protonmail.com>
 *
 * PersonnelManager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PersonnelManager is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TIME_PRESET_TREE_MODEL_COLUMN_RECORD_H_
#define _TIME_PRESET_TREE_MODEL_COLUMN_RECORD_H_

class TimePresetTreeModelColumnRecord: public Gtk::TreeModelColumnRecord {
	 public:
		TimePresetTreeModelColumnRecord() { add(start_h); add(start_m); add(finish_h); add(finish_m); add(id); }
		Gtk::TreeModelColumn<Glib::ustring> start_h;
		Gtk::TreeModelColumn<Glib::ustring> start_m;
		Gtk::TreeModelColumn<Glib::ustring> finish_h;
		Gtk::TreeModelColumn<Glib::ustring> finish_m;
		Gtk::TreeModelColumn<short> id;
};

#endif // _TIME_PRESET_TREE_MODEL_COLUMN_RECORD_H_


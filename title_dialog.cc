/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * title_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "title_dialog.hh"
#include <gtkmm/messagedialog.h>

void TitleDialog::build_gui(TitleDialog* td) {
	Gtk::Box* box = td->get_content_area();
	td->bldr->get_widget("title_box", td->title_box);
	td->bldr->get_widget("title_treeview", td->title_treeview);
	td->title_liststore = Gtk::ListStore::create(td->line_text_tmcr);
	td->title_treeview->set_model(td->title_liststore);
	td->title_treeselection = td->title_treeview->get_selection();
	td->title_treeselection->set_mode(Gtk::SELECTION_SINGLE);
	td->title_string_treeviewcolumn = td->title_treeview->get_column(0);
	td->title_string_cellrenderertext = (Gtk::CellRendererText*)td->title_string_treeviewcolumn->get_first_cell();
	td->title_size_treeviewcolumn = td->title_treeview->get_column(1);
	td->title_size_cellrenderertext = (Gtk::CellRendererText*)td->title_size_treeviewcolumn->get_first_cell();
	td->bldr->get_widget("title_add_button", td->title_add_button);
	td->bldr->get_widget("title_remove_button", td->title_remove_button);
	box->pack_start(*(td->title_box));
	/*
	 * load values
	 */
	td->load_title_in_treeview();
	/*
	 * signals connections
	 */
	td->title_treeselection->signal_changed().connect(sigc::mem_fun(td, &TitleDialog::on_title_treeselection_changed));
	td->title_add_button->signal_clicked().connect(sigc::mem_fun(td, &TitleDialog::on_add_title_line_button_clicked));
	td->title_string_cellrenderertext->signal_edited().connect(sigc::mem_fun(td, &TitleDialog::on_title_string_edited));
	td->title_size_cellrenderertext->signal_edited().connect(sigc::mem_fun(td, &TitleDialog::on_title_size_edited));
	td->title_remove_button->signal_clicked().connect(sigc::mem_fun(td, &TitleDialog::on_remove_title_line_button_clicked));
}

void TitleDialog::load_title_in_treeview(void) {
	title_liststore->clear();
	try {
		std::vector<TextLine> title_lines { database->get_text_lines(TITLE_LINE) };
		for (auto iter = title_lines.begin(); iter != title_lines.end(); iter++) {
			Gtk::TreeModel::iterator iter_tm { title_liststore->append() };
			Gtk::TreeModel::Row row { *iter_tm };
			row[line_text_tmcr.row] = iter->get_row();
			row[line_text_tmcr.string] = iter->get_text();
			row[line_text_tmcr.size] = Glib::ustring::format(std::fixed, std::setprecision(1), iter->get_font_size());
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "TitleDialog::load_title_in_treeview() " << dbe.get_message() << std::endl;
	}
}

void TitleDialog::on_add_title_line_button_clicked(void) {
	try {
		std::vector<TextLine> lines { database->get_text_lines(TITLE_LINE) };
		int row { lines.size() > 0 ? lines.back().get_row() + 1 : 0 };
		TextLine txtl { row, Glib::ustring(TITLE_LINE), Glib::ustring("title line"), 12.0};
		database->insert(txtl);
		load_title_in_treeview();
	} catch (const Database::DBError& dbe) {
		std::cerr << "TitleDialog::on_add_title_line_button_clicked() " << dbe.get_message() << std::endl;
	}
}

void TitleDialog::on_title_string_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	try {
		Glib::RefPtr<Gtk::TreeModel> title_model { title_treeview->get_model() };
		Gtk::TreeModel::iterator iter { title_model->get_iter(path) };
		Gtk::TreeModel::Row row { *iter };
		database->update_text_line_string(new_text, TITLE_LINE, row[line_text_tmcr.row]);
		row[line_text_tmcr.string] = new_text;
	} catch (const Database::DBError& dbe) {
		std::cerr << "TitleDialog::on_title_string_edited() " << dbe.get_message() << std::endl;
	}
}

void TitleDialog::on_title_size_edited(const Glib::ustring& path, const Glib::ustring& new_text) {
	Glib::RefPtr<Gtk::TreeModel> title_model { title_treeview->get_model() };
	Gtk::TreeModel::iterator iter { title_model->get_iter(path) };
	Gtk::TreeModel::Row row { *iter };
	try {
		database->update_text_line_fsize(atof(new_text.c_str()), TITLE_LINE, row[line_text_tmcr.row]);
		row[line_text_tmcr.size] = Glib::ustring::format(std::fixed, std::setprecision(1), atof(new_text.c_str()));
	} catch (const Database::DBError& dbe) {
		std::cerr << "TitleDialog::on_title_size_edited() " << dbe.get_message() << std::endl;
	}
}

void TitleDialog::on_remove_title_line_button_clicked(void) {
	Gtk::MessageDialog alert_dialog { *this, "Vuoi eliminare definitivamente l'elemento selezionato?", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_YES_NO, true };
	int res_id { alert_dialog.run() };
	if (res_id == Gtk::RESPONSE_YES) {
		Glib::RefPtr<Gtk::TreeModel> title_treemodel { title_treeview->get_model() };
		std::vector<Gtk::TreeModel::Path> selected { title_treeselection->get_selected_rows() };
		for (auto iter = selected.cbegin(); iter != selected.cend(); iter++) {
			Gtk::TreeModel::iterator iter_tm { title_treemodel->get_iter(*iter) };
			Gtk::TreeModel::Row row { *iter_tm };
			try {
				database->delete_text_line(TITLE_LINE, row[line_text_tmcr.row]);
				title_liststore->erase(iter_tm);
			} catch (const Database::DBError& dbe) {
				std::cerr << "TitleDialog::on_remove_title_line_button_clicked() " << dbe.get_message() << std::endl;
			}
		}
	}
}

/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * title_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _TITLE_DIALOG_HH_
#define _TITLE_DIALOG_HH_

#include <gtkmm/treeview.h>
#include <gtkmm/button.h>
#include "line_text_tree_model_column_record.hh"
#include "control_dialog.hh"
#include "database.hh"

#define TITLE_LINE				"TTL_LINE"

class TitleDialog : public ControlDialog {
 public:
	inline TitleDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline TitleDialog(const TitleDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(TitleDialog* td);
	void load_title_in_treeview(void);
	inline void on_title_treeselection_changed(void) { title_remove_button->set_sensitive(title_treeselection->count_selected_rows() > 0); }
	void on_add_title_line_button_clicked(void);
	void on_title_string_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_title_size_edited(const Glib::ustring& path, const Glib::ustring& new_text);
	void on_remove_title_line_button_clicked(void);

	Gtk::Box* title_box;
	Gtk::TreeView* title_treeview;
	Glib::RefPtr<Gtk::ListStore> title_liststore;
	Glib::RefPtr<Gtk::TreeSelection> title_treeselection;
	Gtk::TreeViewColumn* title_string_treeviewcolumn;
	Gtk::CellRendererText* title_string_cellrenderertext;
	Gtk::TreeViewColumn* title_size_treeviewcolumn;
	Gtk::CellRendererText* title_size_cellrenderertext;
	Gtk::Button* title_add_button;
	Gtk::Button* title_remove_button;
	LineTextTreeModelColumnRecord line_text_tmcr;
	Database* database;
};

#endif // _TITLE_DIALOG_HH_

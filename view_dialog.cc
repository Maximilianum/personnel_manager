/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * view_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "view_dialog.hh"

void ViewDialog::build_gui(ViewDialog* vd) {
	Gtk::Box* box = vd->get_content_area();
	vd->bldr->get_widget("color_frame", vd->color_frame);
	vd->bldr->get_widget("work_color_colorbutton", vd->work_color_colorbutton);
	vd->bldr->get_widget("absent_color_colorbutton", vd->absent_color_colorbutton);
	vd->bldr->get_widget("license_color_colorbutton", vd->license_color_colorbutton);
	box->pack_start(*(vd->color_frame));
	/*
	 * load values
	 */
	vd->work_color_colorbutton->set_rgba(vd->database->get_work_color());
	vd->absent_color_colorbutton->set_rgba(vd->database->get_absent_color());
	vd->license_color_colorbutton->set_rgba(vd->database->get_license_color());
	/*
	 * signals connections
	 */
	vd->work_color_colorbutton->signal_color_set().connect(sigc::mem_fun(vd, &ViewDialog::on_work_color_set));
	vd->absent_color_colorbutton->signal_color_set().connect(sigc::mem_fun(vd, &ViewDialog::on_absent_color_set));
	vd->license_color_colorbutton->signal_color_set().connect(sigc::mem_fun(vd, &ViewDialog::on_license_color_set));
}

void ViewDialog::on_work_color_set(void) {
	try {
		database->set_work_color(work_color_colorbutton->get_rgba());
	} catch (const Database::DBError& dbe) {
		std::cerr << "ViewDialog::on_work_color_set() " << dbe.get_message() << std::endl;
	}
}

void ViewDialog::on_absent_color_set(void) {
	try {
		database->set_absent_color(absent_color_colorbutton->get_rgba());
	} catch (const Database::DBError& dbe) {
		std::cerr << "ViewDialog::on_absent_color_set() " << dbe.get_message() << std::endl;
	}
}

void ViewDialog::on_license_color_set(void) {
	try {
		database->set_license_color(license_color_colorbutton->get_rgba());
	} catch (const Database::DBError& dbe) {
		std::cerr << "ViewDialog::on_license_color_set() " << dbe.get_message() << std::endl;
	}
}

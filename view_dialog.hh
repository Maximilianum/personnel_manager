/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * view_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _VIEW_DIALOG_HH_
#define _VIEW_DIALOG_HH_

#include <gtkmm/frame.h>
#include <gtkmm/colorbutton.h>
#include "control_dialog.hh"
#include "database.hh"

class ViewDialog : public ControlDialog {
 public:
	inline ViewDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline ViewDialog(const ViewDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(ViewDialog* vd);
	void on_work_color_set(void);
	void on_absent_color_set(void);
	void on_license_color_set(void);

	Gtk::Frame* color_frame;
	Gtk::ColorButton* work_color_colorbutton;
	Gtk::ColorButton* absent_color_colorbutton;
	Gtk::ColorButton* license_color_colorbutton;
	Database* database;
};

#endif // _VIEW_DIALOG_HH_

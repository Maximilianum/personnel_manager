/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * work_time_dialog.cc
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "work_time_dialog.hh"
#include <gtkmm/messagedialog.h>

void WorkTimeDialog::build_gui(WorkTimeDialog* wt) {
	Gtk::Box* box { wt->get_content_area() };
	wt->bldr->get_widget("work_time_box", wt->work_time_box);
	wt->bldr->get_widget("max_work_length_scale", wt->max_work_length_scale);
	wt->bldr->get_widget("work_time_from_hour_combobox", wt->work_time_from_hour_combobox);
	wt->work_time_from_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_from_hour_combobox->set_model(wt->work_time_from_hour_liststore);
	wt->bldr->get_widget("work_time_from_minutes_combobox", wt->work_time_from_minutes_combobox);
	wt->work_time_from_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_from_minutes_combobox->set_model(wt->work_time_from_minutes_liststore);
	wt->bldr->get_widget("work_time_to_hour_combobox", wt->work_time_to_hour_combobox);
	wt->work_time_to_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_to_hour_combobox->set_model(wt->work_time_to_hour_liststore);
	wt->bldr->get_widget("work_time_to_minutes_combobox", wt->work_time_to_minutes_combobox);
	wt->work_time_to_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_to_minutes_combobox->set_model(wt->work_time_to_minutes_liststore);
	wt->bldr->get_widget("work_time_close_saturday_checkbutton", wt->work_time_close_saturday_checkbutton);
	wt->bldr->get_widget("work_time_disable_saturday_checkbutton", wt->work_time_disable_saturday_checkbutton);
	wt->bldr->get_widget("work_time_saturday_down_box", wt->work_time_saturday_down_box);
	wt->bldr->get_widget("work_time_saturday_from_hour_combobox", wt->work_time_saturday_from_hour_combobox);
	wt->work_time_saturday_from_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_saturday_from_hour_combobox->set_model(wt->work_time_saturday_from_hour_liststore);
	wt->bldr->get_widget("work_time_saturday_from_minutes_combobox", wt->work_time_saturday_from_minutes_combobox);
	wt->work_time_saturday_from_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_saturday_from_minutes_combobox->set_model(wt->work_time_saturday_from_minutes_liststore);
	wt->bldr->get_widget("work_time_saturday_to_hour_combobox", wt->work_time_saturday_to_hour_combobox);
	wt->work_time_saturday_to_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_saturday_to_hour_combobox->set_model(wt->work_time_saturday_to_hour_liststore);
	wt->bldr->get_widget("work_time_saturday_to_minutes_combobox", wt->work_time_saturday_to_minutes_combobox);
	wt->work_time_saturday_to_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_saturday_to_minutes_combobox->set_model(wt->work_time_saturday_to_minutes_liststore);
	wt->bldr->get_widget("work_time_close_holiday_checkbutton", wt->work_time_close_holiday_checkbutton);
	wt->bldr->get_widget("work_time_disable_holiday_checkbutton", wt->work_time_disable_holiday_checkbutton);
	wt->bldr->get_widget("work_time_holiday_down_box", wt->work_time_holiday_down_box);
	wt->bldr->get_widget("work_time_holiday_from_hour_combobox", wt->work_time_holiday_from_hour_combobox);
	wt->work_time_holiday_from_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_holiday_from_hour_combobox->set_model(wt->work_time_holiday_from_hour_liststore);
	wt->bldr->get_widget("work_time_holiday_from_minutes_combobox", wt->work_time_holiday_from_minutes_combobox);
	wt->work_time_holiday_from_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_holiday_from_minutes_combobox->set_model(wt->work_time_holiday_from_minutes_liststore);
	wt->bldr->get_widget("work_time_holiday_to_hour_combobox", wt->work_time_holiday_to_hour_combobox);
	wt->work_time_holiday_to_hour_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_holiday_to_hour_combobox->set_model(wt->work_time_holiday_to_hour_liststore);
	wt->bldr->get_widget("work_time_holiday_to_minutes_combobox", wt->work_time_holiday_to_minutes_combobox);
	wt->work_time_holiday_to_minutes_liststore = Gtk::ListStore::create(wt->combobox_tmcr);
	wt->work_time_holiday_to_minutes_combobox->set_model(wt->work_time_holiday_to_minutes_liststore);
	box->pack_start(*(wt->work_time_box));
	/*
	 * load values
	 */
	std::vector<Glib::ustring> hour {};
	for (short i = 0; i < 25; i++) {
		hour.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
	}
	std::vector<Glib::ustring> minute {};
	for (short i = 0; i < 60; i += 10) {
		minute.push_back(Glib::ustring::format(std::setfill(L'0'), std::setw(2), i));
	}
	wt->load_data_combobox(hour.cbegin(), hour.cend() - 1, wt->work_time_from_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_from_minutes_liststore);
	wt->load_data_combobox(hour.cbegin() + 1, hour.cend(), wt->work_time_to_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_to_minutes_liststore);
	wt->load_data_combobox(hour.cbegin(), hour.cend() - 1, wt->work_time_saturday_from_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_saturday_from_minutes_liststore);
	wt->load_data_combobox(hour.cbegin() + 1, hour.cend(), wt->work_time_saturday_to_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_saturday_to_minutes_liststore);
	wt->load_data_combobox(hour.cbegin(), hour.cend() - 1, wt->work_time_holiday_from_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_holiday_from_minutes_liststore);
	wt->load_data_combobox(hour.cbegin() + 1, hour.cend(), wt->work_time_holiday_to_hour_liststore);
	wt->load_data_combobox(minute.cbegin(), minute.cend(), wt->work_time_holiday_to_minutes_liststore);
	wt->max_work_length_scale->set_value(wt->get_max_work_length() / 60);
	Glib::ustring string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_start() / 60);
	int index = wt->get_row_index_for_value_in_combobox(wt->work_time_from_hour_liststore, string_value);
	wt->work_time_from_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_start() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_from_minutes_liststore, string_value);
	wt->work_time_from_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_end() / 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_to_hour_liststore, string_value);
	wt->work_time_to_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_end() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_to_minutes_liststore, string_value);
	wt->work_time_to_minutes_combobox->set_active(index);
	bool state_close { wt->get_work_time_saturday_close() };
	wt->work_time_close_saturday_checkbutton->set_active(state_close);
	wt->work_time_saturday_down_box->set_visible(!state_close);
	wt->work_time_disable_saturday_checkbutton->set_visible(!state_close);
	bool saturday_state_enabled { wt->is_work_time_saturday_enabled() };
	wt->work_time_disable_saturday_checkbutton->set_active(!saturday_state_enabled);
	wt->work_time_saturday_down_box->set_sensitive(saturday_state_enabled);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_saturday_start() / 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_saturday_from_hour_liststore, string_value);
	wt->work_time_saturday_from_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_saturday_start() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_saturday_from_minutes_liststore, string_value);
	wt->work_time_saturday_from_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_saturday_end() / 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_saturday_to_hour_liststore, string_value);
	wt->work_time_saturday_to_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_saturday_end() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_saturday_to_minutes_liststore, string_value);
	wt->work_time_saturday_to_minutes_combobox->set_active(index);
	state_close = wt->get_work_time_holiday_close();
	wt->work_time_close_holiday_checkbutton->set_active(state_close);
	wt->work_time_holiday_down_box->set_visible(!state_close);
	wt->work_time_disable_holiday_checkbutton->set_visible(!state_close);
	bool holiday_state_enabled { wt->is_work_time_holiday_enabled() };
	wt->work_time_disable_holiday_checkbutton->set_active(!holiday_state_enabled);
	wt->work_time_holiday_down_box->set_sensitive(holiday_state_enabled);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_holiday_start() / 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_holiday_from_hour_liststore, string_value);
	wt->work_time_holiday_from_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_holiday_start() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_holiday_from_minutes_liststore, string_value);
	wt->work_time_holiday_from_minutes_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_holiday_end() / 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_holiday_to_hour_liststore, string_value);
	wt->work_time_holiday_to_hour_combobox->set_active(index);
	string_value = Glib::ustring::format(std::setfill(L'0'), std::setw(2), wt->get_work_time_holiday_end() % 60);
	index = wt->get_row_index_for_value_in_combobox(wt->work_time_holiday_to_minutes_liststore, string_value);
	wt->work_time_holiday_to_minutes_combobox->set_active(index);
	/*
	 * signals connections
	 */
	wt->max_work_length_scale->signal_value_changed().connect(sigc::mem_fun(wt, &WorkTimeDialog::on_max_work_length_changed));
	wt->work_time_from_hour_combobox->signal_changed().connect(sigc::bind<Gtk::ComboBox*>(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_from_value_changed), wt->work_time_from_hour_combobox));
	wt->work_time_from_minutes_combobox->signal_changed().connect(sigc::bind<Gtk::ComboBox*>(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_from_value_changed), wt->work_time_from_minutes_combobox));
	wt->work_time_to_hour_combobox->signal_changed().connect(sigc::bind<Gtk::ComboBox*>(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_to_value_changed), wt->work_time_to_hour_combobox));
	wt->work_time_to_minutes_combobox->signal_changed().connect(sigc::bind<Gtk::ComboBox*>(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_to_value_changed), wt->work_time_to_minutes_combobox));
	wt->work_time_close_saturday_checkbutton->signal_toggled().connect(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_close_saturday_toggled));
	wt->work_time_disable_saturday_checkbutton->signal_toggled().connect(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_disable_saturday_toggled));
	if (wt->is_work_time_saturday_enabled()) {
		wt->connect_work_time_saturday();
	}
	wt->work_time_close_holiday_checkbutton->signal_toggled().connect(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_close_holiday_toggled));
	wt->work_time_disable_holiday_checkbutton->signal_toggled().connect(sigc::mem_fun(wt, &WorkTimeDialog::on_work_time_disable_holiday_toggled));
	if (wt->is_work_time_holiday_enabled()) {
		wt->connect_work_time_holiday();
	}
}

void WorkTimeDialog::on_work_time_from_value_changed(Gtk::ComboBox* combobox) {
	Gtk::TreeModel::iterator iter_h = work_time_from_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_from_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (get_work_time_end() - value >= 60) {
			set_work_time_start(value);
			if (!is_work_time_saturday_enabled()) {
				if (combobox == work_time_from_hour_combobox) {
					work_time_saturday_from_hour_combobox->set_active(combobox->get_active_row_number());
				} else if (combobox == work_time_from_minutes_combobox) {
					work_time_saturday_from_minutes_combobox->set_active(combobox->get_active_row_number());
				}
			}
			if (!is_work_time_holiday_enabled()) {
				if (combobox == work_time_from_hour_combobox) {
					work_time_holiday_from_hour_combobox->set_active(combobox->get_active_row_number());
				} else if (combobox == work_time_from_minutes_combobox) {
					work_time_holiday_from_minutes_combobox->set_active(combobox->get_active_row_number());
				}
			}
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_from_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_to_value_changed(Gtk::ComboBox* combobox) {
	Gtk::TreeModel::iterator iter_h = work_time_to_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_to_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (value - get_work_time_start() >= 60) {
			set_work_time_end(value);
			if (!is_work_time_saturday_enabled()) {
				if (combobox == work_time_to_hour_combobox) {
					work_time_saturday_to_hour_combobox->set_active(combobox->get_active_row_number());
				} else if (combobox == work_time_to_minutes_combobox) {
					work_time_saturday_to_minutes_combobox->set_active(combobox->get_active_row_number());
				}
			}
			if (!is_work_time_holiday_enabled()) {
				if (combobox == work_time_to_hour_combobox) {
					work_time_holiday_to_hour_combobox->set_active(combobox->get_active_row_number());
				} else if (combobox == work_time_to_minutes_combobox) {
					work_time_holiday_to_minutes_combobox->set_active(combobox->get_active_row_number());
				}
			}
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_to_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_close_saturday_toggled(void) {
	bool state = work_time_close_saturday_checkbutton->get_active();
	work_time_disable_saturday_checkbutton->set_visible(!state);
	work_time_saturday_down_box->set_visible(!state);
	try {
		set_work_time_saturday_close(state);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_close_saturday_toggled() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_disable_saturday_toggled(void) {
	bool state = !work_time_disable_saturday_checkbutton->get_active();
	if (state) {
		// make work time saturday comboboxes sensitive
		work_time_saturday_down_box->set_sensitive(true);
		// enable signal connections
		connect_work_time_saturday();
	} else {
		// make work time saturday comboboxes unsensitive
		work_time_saturday_down_box->set_sensitive(false);
		// disable signal connections
		work_time_saturday_from_hour_combobox_connection.disconnect();
		work_time_saturday_from_minutes_combobox_connection.disconnect();
		work_time_saturday_to_hour_combobox_connection.disconnect();
		work_time_saturday_to_minutes_combobox_connection.disconnect();
		// set work time saturday equal to work time
		work_time_saturday_from_hour_combobox->set_active(work_time_from_hour_combobox->get_active_row_number());
		work_time_saturday_from_minutes_combobox->set_active(work_time_from_minutes_combobox->get_active_row_number());
		work_time_saturday_to_hour_combobox->set_active(work_time_to_hour_combobox->get_active_row_number());
		work_time_saturday_to_minutes_combobox->set_active(work_time_to_minutes_combobox->get_active_row_number());
	}
	try {
		set_work_time_saturday_enabled(state);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_disable_saturday_toggled() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_saturday_from_value_changed(void) {
	Gtk::TreeModel::iterator iter_h = work_time_saturday_from_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_saturday_from_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (get_work_time_saturday_end() - value >= 60) {
			set_work_time_saturday_start(value);
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_saturday_from_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_saturday_to_value_changed(void) {
	Gtk::TreeModel::iterator iter_h = work_time_saturday_to_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_saturday_to_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (value - get_work_time_saturday_start() >= 60) {
			set_work_time_saturday_end(value);
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_saturday_to_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_close_holiday_toggled(void) {
	bool state = work_time_close_holiday_checkbutton->get_active();
	work_time_disable_holiday_checkbutton->set_visible(!state);
	work_time_holiday_down_box->set_visible(!state);
	try {
		set_work_time_holiday_close(state);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_close_holiday_toggled() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_disable_holiday_toggled(void) {
	bool state = !work_time_disable_holiday_checkbutton->get_active();
	if (state) {
		// make work time holiday comboboxes sensitive
		work_time_holiday_down_box->set_sensitive(true);
		// enable signal connections
		connect_work_time_holiday();
	} else {
		// make work time holiday comboboxes unsensitive
		work_time_holiday_down_box->set_sensitive(false);
		// disable signal connections
		work_time_holiday_from_hour_combobox_connection.disconnect();
		work_time_holiday_from_minutes_combobox_connection.disconnect();
		work_time_holiday_to_hour_combobox_connection.disconnect();
		work_time_holiday_to_minutes_combobox_connection.disconnect();
		// set work time saturday equal to work time
		work_time_holiday_from_hour_combobox->set_active(work_time_from_hour_combobox->get_active_row_number());
		work_time_holiday_from_minutes_combobox->set_active(work_time_from_minutes_combobox->get_active_row_number());
		work_time_holiday_to_hour_combobox->set_active(work_time_to_hour_combobox->get_active_row_number());
		work_time_holiday_to_minutes_combobox->set_active(work_time_to_minutes_combobox->get_active_row_number());
	}
	try {
		set_work_time_holiday_enabled(state);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_disable_holiday_toggled() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_holiday_from_value_changed(void) {
	Gtk::TreeModel::iterator iter_h = work_time_holiday_from_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_holiday_from_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (get_work_time_holiday_end() - value >= 60) {
			set_work_time_holiday_start(value);
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_holiday_from_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_work_time_holiday_to_value_changed(void) {
	Gtk::TreeModel::iterator iter_h = work_time_holiday_to_hour_combobox->get_active();
	Gtk::TreeModel::Row row_h = *iter_h;
	Glib::ustring value_str_h = row_h[combobox_tmcr.column];
	Gtk::TreeModel::iterator iter_m = work_time_holiday_to_minutes_combobox->get_active();
	Gtk::TreeModel::Row row_m = *iter_m;
	Glib::ustring value_str_m = row_m[combobox_tmcr.column];
	int value = (atoi(value_str_h.c_str()) * 60) + atoi(value_str_m.c_str());
	try {
		if (value - get_work_time_holiday_start() >= 60) {
			set_work_time_holiday_end(value);
		} else {
			Gtk::MessageDialog alert_dialog { *this, "Il periodo di attività non può essere negativa o inferiore a 60 minuti.", false, Gtk::MESSAGE_WARNING, Gtk::BUTTONS_OK, true };
			int res_id = alert_dialog.run();
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_work_time_holiday_to_value_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::on_max_work_length_changed(void) {
	int number = max_work_length_scale->get_value() * 60;
	try {
		set_max_work_length(number);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::on_settings_max_work_length_changed() " << dbe.get_message() << std::endl;
	}
}

void WorkTimeDialog::set_max_work_length(int val) {
	Duty::set_max_work_length(val);
	try {
		if (database->check_setting(MAX_WRK_LENGTH)) {
			database->update(MAX_WRK_LENGTH, val);
		} else {
			database->insert(MAX_WRK_LENGTH, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_max_work_length() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_max_work_length(void) const {
	try {
		return database->get_setting(MAX_WRK_LENGTH);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_max_work_length() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_MAX_WORK;
}

// work time

void WorkTimeDialog::set_work_time_start(int val) {
	Duty::set_work_time_start(val);
	try {
		if (database->check_setting(WORK_TIME_START)) {
			database->update(WORK_TIME_START, val);
		} else {
			database->insert(WORK_TIME_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_start() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_start(void) const {
	try {
		return database->get_setting(WORK_TIME_START);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_start() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_START;
}

void WorkTimeDialog::set_work_time_end(int val) {
	Duty::set_work_time_end(val);
	try {
		if (database->check_setting(WORK_TIME_END)) {
			database->update(WORK_TIME_END, val);
		} else {
			database->insert(WORK_TIME_END, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_end() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_end(void) const {
	try {
		return database->get_setting(WORK_TIME_END);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_end() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_END;
}

// work time saturday

void WorkTimeDialog::set_work_time_saturday_close(bool flag) {
	Duty::set_work_time_saturday_close(flag);
	try {
		if (database->check_setting(WRKTM_SATURDAY_CLOSE)) {
			database->update(WRKTM_SATURDAY_CLOSE, flag);
		} else {
			database->insert(WRKTM_SATURDAY_CLOSE, flag);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_saturday_close() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}
bool WorkTimeDialog::get_work_time_saturday_close(void) const {
	try {
		return database->get_setting(WRKTM_SATURDAY_CLOSE);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_saturday_close() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return false;
}

void WorkTimeDialog::set_work_time_saturday_enabled(bool flag) {
	Duty::set_work_time_saturday_enabled(flag);
	try {
		if (database->check_setting(WRKTM_SATURDAY_ENABLE)) {
			database->update(WRKTM_SATURDAY_ENABLE, flag);
		} else {
			database->insert(WRKTM_SATURDAY_ENABLE, flag);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_saturday_enabled() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

bool WorkTimeDialog::is_work_time_saturday_enabled(void) const {
	try {
		return database->get_setting(WRKTM_SATURDAY_ENABLE);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::is_work_time_saturday_enabled() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return true;
}

void WorkTimeDialog::set_work_time_saturday_start(int val) {
	Duty::set_work_time_saturday_start(val);
	try {
		if (database->check_setting(WRKTM_SATURDAY_START)) {
			database->update(WRKTM_SATURDAY_START, val);
		} else {
			database->insert(WRKTM_SATURDAY_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_saturday_start() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_saturday_start(void) const {
	try {
		return database->get_setting(WRKTM_SATURDAY_START);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_saturday_start() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_SATURDAY_START;
}

void WorkTimeDialog::set_work_time_saturday_end(int val) {
	Duty::set_work_time_saturday_end(val);
	try {
		if (database->check_setting(WRKTM_SATURDAY_END)) {
			database->update(WRKTM_SATURDAY_END, val);
		} else {
			database->insert(WRKTM_SATURDAY_END, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_saturday_end() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_saturday_end(void) const {
	try {
		return database->get_setting(WRKTM_SATURDAY_END);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_saturday_end() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_SATURDAY_END;
}

void WorkTimeDialog::connect_work_time_saturday(void) {
	work_time_saturday_from_hour_combobox_connection = work_time_saturday_from_hour_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_saturday_from_value_changed));
	work_time_saturday_from_minutes_combobox_connection = work_time_saturday_from_minutes_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_saturday_from_value_changed));
	work_time_saturday_to_hour_combobox_connection = work_time_saturday_to_hour_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_saturday_to_value_changed));
	work_time_saturday_to_minutes_combobox_connection = work_time_saturday_to_minutes_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_saturday_to_value_changed));
}

// work time holiday

void WorkTimeDialog::set_work_time_holiday_close(bool flag) {
	Duty::set_work_time_holiday_close(flag);
	try {
		if (database->check_setting(WRKTM_HOLIDAY_CLOSE)) {
			database->update(WRKTM_HOLIDAY_CLOSE, flag);
		} else {
			database->insert(WRKTM_HOLIDAY_CLOSE, flag);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_holiday_close() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}
bool WorkTimeDialog::get_work_time_holiday_close(void) const {
	try {
		return database->get_setting(WRKTM_HOLIDAY_CLOSE);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_holiday_close() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return false;
}

void WorkTimeDialog::set_work_time_holiday_enabled(bool flag) {
	Duty::set_work_time_holiday_enabled(flag);
	try {
		if (database->check_setting(WRKTM_HOLIDAY_ENABLE)) {
			database->update(WRKTM_HOLIDAY_ENABLE, flag);
		} else {
			database->insert(WRKTM_HOLIDAY_ENABLE, flag);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_holiday_enabled() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

bool WorkTimeDialog::is_work_time_holiday_enabled(void) const {
	try {
		return database->get_setting(WRKTM_HOLIDAY_ENABLE);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::is_work_time_holiday_enabled() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return true;
}

void WorkTimeDialog::set_work_time_holiday_start(int val) {
	Duty::set_work_time_holiday_start(val);
	try {
		if (database->check_setting(WRKTM_HOLIDAY_START)) {
			database->update(WRKTM_HOLIDAY_START, val);
		} else {
			database->insert(WRKTM_HOLIDAY_START, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_holiday_start() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_holiday_start(void) const {
	try {
		return database->get_setting(WRKTM_HOLIDAY_START);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_holiday_start() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_HOLIDAY_START;
}

void WorkTimeDialog::set_work_time_holiday_end(int val) {
	Duty::set_work_time_holiday_end(val);
	try {
		if (database->check_setting(WRKTM_HOLIDAY_END)) {
			database->update(WRKTM_HOLIDAY_END, val);
		} else {
			database->insert(WRKTM_HOLIDAY_END, val);
		}
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::set_work_time_holiday_end() " << dbe.get_message() << std::endl;
		throw dbe;
	}
}

int WorkTimeDialog::get_work_time_holiday_end(void) const {
	try {
		return database->get_setting(WRKTM_HOLIDAY_END);
	} catch (const Database::DBError& dbe) {
		std::cerr << "WorkTimeDialog::get_work_time_holiday_end() " << dbe.get_message() << std::endl;
		throw dbe;
	} catch (const Database::DBEmptyResultException& dbe) {}
	return DEFAULT_WORK_TIME_HOLIDAY_END;
}

void WorkTimeDialog::connect_work_time_holiday(void) {
	work_time_holiday_from_hour_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_holiday_from_value_changed));
	work_time_holiday_from_minutes_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_holiday_from_value_changed));
	work_time_holiday_to_hour_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_holiday_to_value_changed));
	work_time_holiday_to_minutes_combobox->signal_changed().connect(sigc::mem_fun(this, &WorkTimeDialog::on_work_time_holiday_to_value_changed));
}

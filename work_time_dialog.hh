/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * work_time_dialog.hh
 * Copyright (C) 2023 Massimiliano Maniscalco <maximilianum@protonmail.com>
 * 
 * This is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _WORK_TIME_DIALOG_HH_
#define _WORK_TIME_DIALOG_HH_

#include <gtkmm/combobox.h>
#include <gtkmm/scale.h>
#include <gtkmm/checkbutton.h>
#include "control_dialog.hh"
#include "database.hh"

#define MAX_WRK_LENGTH			"MAX_WRK_LEN"
#define WORK_TIME_START			"WRK_TM_STRT"
#define WORK_TIME_END			"WRK_TM_END"
#define WRKTM_SATURDAY_CLOSE	"WRKTM_SAT_CLS"
#define WRKTM_SATURDAY_ENABLE	"WRKTM_SAT_ENA"
#define WRKTM_SATURDAY_START	"WRKTM_SAT_STRT"
#define WRKTM_SATURDAY_END		"WRKTM_SAT_END"
#define WRKTM_HOLIDAY_CLOSE		"WRKTM_HOL_CLS"
#define WRKTM_HOLIDAY_ENABLE	"WRKTM_HOL_ENA"
#define WRKTM_HOLIDAY_START		"WRKTM_HOL_STRT"
#define WRKTM_HOLIDAY_END		"WRKTM_HOL_END"

class WorkTimeDialog : public ControlDialog {
 public:
	inline WorkTimeDialog(const Glib::ustring& path, Gtk::Window& parent, Database* db) : ControlDialog(path, parent), database (db) { build_gui(this); }
	inline WorkTimeDialog(const WorkTimeDialog& sd) : ControlDialog(sd) { build_gui(this); }
 private:
	static void build_gui(WorkTimeDialog* wt);
	void on_max_work_length_changed(void);
	void on_work_time_from_value_changed(Gtk::ComboBox* combobox);
	void on_work_time_to_value_changed(Gtk::ComboBox* combobox);
	void on_work_time_close_saturday_toggled(void);
	void on_work_time_disable_saturday_toggled(void);
	void on_work_time_saturday_from_value_changed(void);
	void on_work_time_saturday_to_value_changed(void);
	void on_work_time_close_holiday_toggled(void);
	void on_work_time_disable_holiday_toggled(void);
	void on_work_time_holiday_from_value_changed(void);
	void on_work_time_holiday_to_value_changed(void);
	// max work length
	void set_max_work_length(int val);
	int get_max_work_length(void) const;
	// work time weekday
	void set_work_time_start(int val);
	int get_work_time_start(void) const;
	void set_work_time_end(int val);
	int get_work_time_end(void) const;
	// work time saturday
	void set_work_time_saturday_close(bool flag);
	bool get_work_time_saturday_close(void) const;
	void set_work_time_saturday_enabled(bool flag);
	bool is_work_time_saturday_enabled(void) const;
	void set_work_time_saturday_start(int val);
	int get_work_time_saturday_start(void) const;
	void set_work_time_saturday_end(int val);
	int get_work_time_saturday_end(void) const;
	void connect_work_time_saturday(void);
	// work time holiday
	void set_work_time_holiday_close(bool flag);
	bool get_work_time_holiday_close(void) const;
	void set_work_time_holiday_enabled(bool flag);
	bool is_work_time_holiday_enabled(void) const;
	void set_work_time_holiday_start(int val);
	int get_work_time_holiday_start(void) const;
	void set_work_time_holiday_end(int val);
	int get_work_time_holiday_end(void) const;
	void connect_work_time_holiday(void);

	Gtk::Box* work_time_box;
	Gtk::Scale* max_work_length_scale;
	Gtk::ComboBox* work_time_from_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_from_hour_liststore;
	Gtk::ComboBox* work_time_from_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_from_minutes_liststore;
	Gtk::ComboBox* work_time_to_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_to_hour_liststore;
	Gtk::ComboBox* work_time_to_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_to_minutes_liststore;
	Gtk::CheckButton* work_time_close_saturday_checkbutton;
	Gtk::CheckButton* work_time_disable_saturday_checkbutton;
	Gtk::Box* work_time_saturday_down_box;
	Gtk::ComboBox* work_time_saturday_from_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_saturday_from_hour_liststore;
	Gtk::ComboBox* work_time_saturday_from_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_saturday_from_minutes_liststore;
	Gtk::ComboBox* work_time_saturday_to_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_saturday_to_hour_liststore;
	Gtk::ComboBox* work_time_saturday_to_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_saturday_to_minutes_liststore;
	Gtk::CheckButton* work_time_close_holiday_checkbutton;
	Gtk::CheckButton* work_time_disable_holiday_checkbutton;
	Gtk::Box* work_time_holiday_down_box;
	Gtk::ComboBox* work_time_holiday_from_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_holiday_from_hour_liststore;
	Gtk::ComboBox* work_time_holiday_from_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_holiday_from_minutes_liststore;
	Gtk::ComboBox* work_time_holiday_to_hour_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_holiday_to_hour_liststore;
	Gtk::ComboBox* work_time_holiday_to_minutes_combobox;
	Glib::RefPtr<Gtk::ListStore> work_time_holiday_to_minutes_liststore;

	sigc::connection work_time_saturday_from_hour_combobox_connection;
	sigc::connection work_time_saturday_from_minutes_combobox_connection;
	sigc::connection work_time_saturday_to_hour_combobox_connection;
	sigc::connection work_time_saturday_to_minutes_combobox_connection;
	sigc::connection work_time_holiday_from_hour_combobox_connection;
	sigc::connection work_time_holiday_from_minutes_combobox_connection;
	sigc::connection work_time_holiday_to_hour_combobox_connection;
	sigc::connection work_time_holiday_to_minutes_combobox_connection;
	
	Database* database;
};

#endif // _WORK_TIME_DIALOG_HH_
